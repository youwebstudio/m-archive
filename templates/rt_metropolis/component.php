<?php
/**
* @version   $Id: component.php 4521 2012-10-25 23:04:32Z josh $
 * @author RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 * Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
 *
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted index access' );

// load and inititialize gantry class
require_once(dirname(__FILE__).'/lib/gantry/gantry.php');
$gantry->init();

?>
<?php if (JRequest::getString('type')=='raw'):?>
	<jdoc:include type="component" />
<?php else: ?>
	<!doctype html>
	<html xml:lang="<?php echo $gantry->language; ?>" lang="<?php echo $gantry->language;?>" >
		<head>
			<meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
			<?php
				$gantry->displayHead();
				$gantry->addLess('global.less', 'master.css', 8, array('main-accent'=>$gantry->get('main-accent', '#FFEB54'), 'blocks-default'=>$gantry->get('blocks-default', '#1685bb'), 'blocks-box1'=>$gantry->get('blocks-box1', '#e1563f'), 'blocks-box2'=>$gantry->get('blocks-box2', '#23b1bf'), 'blocks-box3'=>$gantry->get('blocks-box3', '#ad4455'), 'blocks-box4'=>$gantry->get('blocks-box4', '#336598'), 'main-overlay'=>$gantry->get('main-overlay','light'), 'defaultOverlay'=>$gantry->get('blocks-default-overlay','dark')));
                //
                $gantry->addScript('jquery-3.2.0.min.js');
                $gantry->addScript('jquery.inputmask.bundle.min.js');
                $gantry->addScript('component.js');
			?>
		</head>
		<body class="component-body">
			<div class="component-content">
		    	<jdoc:include type="message" />
				<jdoc:include type="component" />
			</div>
		</body>
	</html>
<?php endif; ?>
<?php
$gantry->finalize();
?>
