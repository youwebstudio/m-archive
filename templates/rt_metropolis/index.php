<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted index access' );
// load and inititialize gantry class
require_once(dirname(__FILE__).'/lib/gantry/gantry.php');
$gantry->init();
?>

<!doctype html>
<html xml:lang="<?php echo $gantry->language; ?>" lang="<?php echo $gantry->language;?>" >
<head>
  <!--<meta name="viewport" content="width=device-width, initial-scale=1.0" /> !-->
    <?php

    $gantry->displayHead();

    $gantry->addStyle('grid-responsive.css', 5);
    $gantry->addLess('bootstrap.less', 'bootstrap.css', 6);
    $gantry->addLess('global.less', 'master.css', 8, array(
        'main-accent'=>$gantry->get('main-accent', '#FFEB54'),
        'blocks-default'=>$gantry->get('blocks-default', '#1685bb'),
        'blocks-box1'=>$gantry->get('blocks-box1', '#e1563f'),
        'blocks-box2'=>$gantry->get('blocks-box2', '#23b1bf'),
        'blocks-box3'=>$gantry->get('blocks-box3', '#ad4455'),
        'blocks-box4'=>$gantry->get('blocks-box4', '#336598'),
        'main-overlay'=>$gantry->get('main-overlay','light'),
        'defaultOverlay'=>$gantry->get('blocks-default-overlay','dark')));

    if ($gantry->browser->name == 'ie'){
      if ($gantry->browser->shortversion == 8){
        $gantry->addScript('html5shim.js');
      }
    }
    if ($gantry->get('layout-mode', 'responsive') == 'responsive') $gantry->addScript('rokmediaqueries.js');
    if ($gantry->get('loadtransition')) {  
        $gantry->addScript('load-transition.js');
        $hidden = ' class="rt-hidden"';
    }
    $gantry->addScript('jquery-3.2.0.min.js');
    $gantry->addScript('main.js');

    ?>
  <link href="<?php echo $this->baseurl ;?>/templates/rt_metropolis/fancybox/jquery.fancybox-1.3.4.css" type="text/css" rel="stylesheet">
  <link href="<?php echo $this->baseurl ;?>/templates/rt_metropolis/css/structure.css" type="text/css" rel="stylesheet">
  <link href="<?php echo $this->baseurl ;?>/templates/rt_metropolis/css/ieMain.css" type="text/css" rel="stylesheet">
  <link href="<?php echo $this->baseurl ;?>/templates/rt_metropolis/css/media-queries.css" type="text/css" rel="stylesheet">
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
  <!--[if IE 11]><link rel="stylesheet" type="text/css" href="http://m-arhiv.dev-z.ru/templates/rt_metropolis/css/ieMain.css" /> <![endif]-->
  <script type="text/javascript" src="<?php echo $this->baseurl ;?>/templates/rt_metropolis/fancybox/jquery.fancybox-1.3.4.min.js" ></script>
  <script type="text/javascript" src="<?php echo $this->baseurl ;?>/templates/rt_metropolis/js/jquery.carouFredSel-6.2.1-packed.js" ></script>
  <script type="text/javascript" src="<?php echo $this->baseurl ;?>/templates/rt_metropolis/js/base64.js" ></script>
  <script type="text/javascript" src="<?php echo $this->baseurl ;?>/templates/rt_metropolis/js/tabs.js" ></script>
  <link type="text/css" href="jcart/css/jcart.css" rel="stylesheet" media="screen, projection" />
  <script type="text/javascript" src="jcart/js/jcart2.js"></script>
<script>
$(document).ready(function(){
 
    var url_l = '/index.php?tmpl=ajax_mod&mod_pos=mod_l';
 
    $.ajax({
        url: url_l,
        dataType : "html",
        success: function(html){
            $("#ajax_l").append(html);
            var url=document.location.href;
            $.each($("div.moduletablesideBarMenu a"),function(){
            if(this.href==url){$(this).addClass('act');};
            });
        }
    });
 
});

</script>
   <meta name='yandex-verification' content='5f2dd61bef376cd0' />
     <script type="text/javascript" src="/GA.js" ></script> 
	 <script type="text/javascript">
var __cs = __cs || [];
__cs.push(["setCsAccount", "TqDOlpTg7FCLcWgdm78QT6uXqtTTz9Br"]);
__cs.push(["setCsHost", "//server.comagic.ru/comagic"]);
</script>
<script type="text/javascript" async src="//app.comagic.ru/static/cs.min.js"></script>

</head>
<body <?php echo $gantry->displayBodyTag(); ?>>
  <div class="rt-bg"><div class="rt-bg2">
  <div class="rt-container">
      <?php /** Begin Drawer **/ if ($gantry->countModules('drawer')) : ?>
      <div id="rt-drawer">
          <?php echo $gantry->displayModules('drawer','standard','standard'); ?>
          <div class="clear"></div>
      </div>
      <?php /** End Drawer **/ endif; ?>
      <?php /** Begin Top Surround **/ if ($gantry->countModules('top') or $gantry->countModules('header')) : ?>
      <header id="rt-top-surround" class="<?php echo ($gantry->get('headerpanel-overlay') == 'light' ? 'rt-light' : 'rt-dark'); ?>">
      <?php /** Begin Top **/ if ($gantry->countModules('top')) : ?>
      <div id="rt-top">
        <?php echo $gantry->displayModules('top','standard','standard'); ?>
        <div class="clear"></div>
      </div>
      <?php /** End Top **/ endif; ?>
      <?php /** Begin Header **/ if ($gantry->countModules('header')) : ?>
      <div id="rt-header">
        <?php echo $gantry->displayModules('header','standard','standard'); ?>
        <div class="clear"></div>
      </div>
      <?php /** End Header **/ endif; ?>
    </header>
    <?php /** End Top Surround **/ endif; ?>
    <?php /** Begin Showcase **/ if ($gantry->countModules('showcase')) : ?>
    <div id="rt-showcase" class="<?php echo ($gantry->get('showcasepanel-overlay') == 'light' ? 'rt-light' : 'rt-dark'); ?>">
      <?php echo $gantry->displayModules('showcase','standard','standard'); ?>
      <div class="clear"></div>
    </div>
    <?php /** End Showcase **/ endif; ?>
    <?php /** Begin Social **/ if ($gantry->countModules('social')) : ?>
    <?php echo $gantry->displayModules('social','basic','basic'); ?>
    <?php /** End Social **/ endif; ?>
    <div id="rt-transition"<?php if ($gantry->get('loadtransition')) echo $hidden; ?>>
      <div id="rt-mainbody-surround" class="<?php echo ($gantry->get('body-overlay') == 'light' ? 'rt-light' : 'rt-dark'); ?>">
     
        <?php /** Begin Feature **/ if ($gantry->countModules('feature')) : ?>
        <div id="rt-feature">
          <?php echo $gantry->displayModules('feature','standard','standard'); ?>
          <div class="clear"></div>
        </div>
        <?php /** End Feature **/ endif; ?>
		
		
        <?php /** Begin Utility **/ if ($gantry->countModules('utility')) : ?>
        <div id="rt-utility">
          <?php echo $gantry->displayModules('utility','standard','standard'); ?>
          <div class="clear"></div>
        </div>
        <?php /** End Utility **/ endif; ?>
        <?php /** Begin Breadcrumbs **/ if ($gantry->countModules('breadcrumb')) : ?>
        <div id="rt-breadcrumbs">
          <?php echo $gantry->displayModules('breadcrumb','standard','standard'); ?>
          <div class="clear"></div>
        </div>
        <?php /** End Breadcrumbs **/ endif; ?>
        <?php /** Begin Main Top **/ if ($gantry->countModules('maintop')) : ?>
        <div id="rt-maintop">
          <?php echo $gantry->displayModules('maintop','standard','standard'); ?>
          <div class="clear"></div>
        </div>
        <?php /** End Main Top **/ endif; ?>
        <?php /** Begin Main Body **/ ?>
            <?php echo $gantry->displayMainbody('mainbody','sidebar','standard','standard','standard','standard','standard'); ?>
        <?php /** End Main Body **/ ?>
        <?php /** Begin Main Bottom **/ if ($gantry->countModules('mainbottom')) : ?>
        <div id="rt-mainbottom">
          <?php echo $gantry->displayModules('mainbottom','standard','standard'); ?>
          <div class="clear"></div>
        </div>
        <?php /** End Main Bottom **/ endif; ?>
        <?php /** Begin Extension **/ if ($gantry->countModules('extension')) : ?>
        <div id="rt-extension">
          <?php echo $gantry->displayModules('extension','standard','standard'); ?>
          <div class="clear"></div>
        </div>
        <?php /** End Extension **/ endif; ?>
      </div>
    </div>
    <?php /** Begin Bottom **/ if ($gantry->countModules('bottom')) : ?>
    <div id="rt-bottom" class="<?php echo ($gantry->get('bottompanel-overlay') == 'light' ? 'rt-light' : 'rt-dark'); ?>">
      <?php echo $gantry->displayModules('bottom','standard','standard'); ?>
      <div class="clear"></div>
    </div>
    <?php /** End Bottom **/ endif; ?>
    <?php /** Begin Footer Section **/ if ($gantry->countModules('footer') or $gantry->countModules('copyright')) : ?>
    <footer id="rt-footer-surround" class="<?php echo ($gantry->get('footerpanel-overlay') == 'light' ? 'rt-light' : 'rt-dark'); ?>">
      <?php /** Begin Footer **/ if ($gantry->countModules('footer')) : ?>
      <div id="rt-footer">
      <?php echo $gantry->displayModules('footer','standard','standard'); ?>
        <div class="clear"></div>
        
      </div>
      <?php /** End Footer **/ endif; ?>
      <?php /** Begin Copyright **/ if ($gantry->countModules('copyright')) : ?>
      <div id="rt-copyright">

        <?php echo $gantry->displayModules('copyright','standard','standard'); ?>
        <div class="clear"></div>
      </div>
      <?php /** End Copyright **/ endif; ?>
    </footer>
    <?php /** End Footer Surround **/ endif; ?>
  </div>
</div></div>
  <?php /** Begin Debug **/ if ($gantry->countModules('debug')) : ?>
  <div id="rt-debug">
    <div class="rt-container">
      <?php echo $gantry->displayModules('debug','standard','standard'); ?>
      <div class="clear"></div>
    </div>
  </div>
  <?php /** End Debug **/ endif; ?>
  <?php /** Begin Auxiliary **/ if ($gantry->countModules('auxiliary') and ($gantry->countModules('sidepanel'))) : ?>
  <?php echo $gantry->displayModules('auxiliary','basic','basic'); ?>
  <?php /** End Auxiliary **/ endif; ?>
  <?php /** Begin Popups **/
      echo $gantry->displayModules('popup','popup','popup');
      echo $gantry->displayModules('login','login','popup');
      /** End Popup s**/ ?>
  <?php /** Begin Analytics **/ if ($gantry->countModules('analytics')) : ?>
  <?php echo $gantry->displayModules('analytics','basic','basic'); ?>
  <?php /** End Analytics **/ endif; ?>
  <!-- Yandex.Metrika counter --><script type="text/javascript">var yaParams = {/*Здесь параметры визита*/};</script><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter7553026 = new Ya.Metrika({id:7553026, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true,params:window.yaParams||{ }}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/7553026" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
   <!-- 9d881599d3e00381 -->

<script type="text/javascript" src="<?php echo $this->baseurl ;?>/templates/rt_metropolis/js/muve.js"></script>
<script type="text/javascript">shineLinks('ajax_l');</script>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'fFLVxrYib6';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
    <style>.module-content .search {margin-top: -145px;}</style>
  </body>
</html>
<?php
$gantry->finalize();
?> 