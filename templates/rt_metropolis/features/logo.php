<?php
/**
* @version   $Id: logo.php 4168 2012-10-05 19:31:55Z josh $
* @author    RocketTheme http://www.rockettheme.com
* @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*
* Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
*
*/

defined('JPATH_BASE') or die();

gantry_import('core.gantryfeature');

/**
 * @package     gantry
 * @subpackage  features
 */
class GantryFeaturelogo extends GantryFeature {
    var $_feature_name = 'logo';

    function isEnabled() {
        /** @var $gantry Gantry */
		global $gantry;
        if (!isset($gantry->browser)) return $this->get('enabled');
        if ($gantry->browser->platform != 'iphone' && $gantry->browser->platform != 'android') return $this->get('enabled');

        $prefix = $gantry->get('template_prefix');
        $cookiename = $prefix.$gantry->browser->platform.'-switcher';
        $cookie = $gantry->retrieveTemp('platform', $cookiename);

        if ($cookie == 1 && $gantry->get($gantry->browser->platform.'-enabled')) return true;
        return $this->get('enabled');
    }

    function isInPosition($position){
        /** @var $gantry Gantry */
		global $gantry;
        if (!isset($gantry->browser)) return $this->get('enabled');
        if ($gantry->browser->platform != 'iphone' && $gantry->browser->platform != 'android') return ($this->getPosition() == $position);

        $prefix = $gantry->get('template_prefix');
        $cookiename = $prefix.$gantry->browser->platform.'-switcher';
        $cookie = $gantry->retrieveTemp('platform', $cookiename);

        if ($cookie == 1 && $gantry->get($gantry->browser->platform.'-enabled') && ($position == 'mobile-top')) return true;

        return ($this->getPosition() == $position);
    }

    function render($position) {
        /** @var $gantry Gantry */
		global $gantry;

        ob_start();
        ?>
        <div class="rt-block logo-block">
                <a href="<?php echo $gantry->baseUrl; ?>" id="rt-logo">
                    <img src="/images/pages/logo.png" alt="Производство стеллажей и металлоконструкций - компания Архив-Проект">
                </a>
                <div>Производство и установка стеллажных систем</div>
        </div>

        <div class="buttons">
            <a class="jcepopup noicon" href="/zapros-na-raschet?view=form" target="_blank" data-mediabox="width[600];height[813]">Заявка на расчет</a>
            <a class="jcepopup noicon" href="/zamer?view=form" target="_blank" data-mediabox="width[550];height[500]">Бесплатный замер</a>
        </div>

        <div class="hederFone">
            <p class="pfone"><span class="comagic_phone">(495) 212-17-56</span></p>
            <p class="recall"><a class="jcepopup noicon" href="index.php?option=com_chronoforms&amp;view=form&amp;Itemid=325" target="_blank" data-mediabox="width[400];height[250]">Перезвоните мне</a></p>
<!--            <div id="basket">-->
<!--                <div class="basket basket_big"><a href="/checkout"><img src="/templates/rt_metropolis/images/icons/picto_cart.png"> 0 товаров на 0,00 р.</a></div>-->
<!--                <div class="basket basket_small"><a href="/checkout"><img src="/templates/rt_metropolis/images/icons/picto_cart.png"> (0)</a></div>-->
<!--            </div>-->
        </div>
        <?php
        return ob_get_clean();
    }
}