
<!--1488_idd-->
<!doctype html>
<html xml:lang="ru-ru" lang="ru-ru" >
<head>
  <!--<meta name="viewport" content="width=device-width, initial-scale=1.0" /> !-->
      <base href="http://m-arhiv.dev-z.ru/site-map/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="title" content="Карта сайта" />
  <title>Карта сайта</title>
  <link href="/templates/rt_metropolis/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <link rel="stylesheet" href="/plugins/system/jcemediabox/css/jcemediabox.css?v=1017" type="text/css" />
  <link rel="stylesheet" href="/plugins/system/jcemediabox/themes/light/css/style.css?version=1017" type="text/css" />
  <link rel="stylesheet" href="/libraries/gantry/css/grid-responsive.css" type="text/css" />
  <link rel="stylesheet" href="/templates/rt_metropolis/css-compiled/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="/templates/rt_metropolis/css-compiled/master-0595777452debf2e24d7ca013aedc332.css" type="text/css" />
  <link rel="stylesheet" href="/templates/rt_metropolis/css-compiled/1200fixed.css" type="text/css" />
  <style type="text/css">
#rt-logo {background: url(/images/pages/logo.png) 50% 0 no-repeat !important;}
#rt-logo {width: 170px;height: 80px;}

  </style>
  <script src="/plugins/system/jcemediabox/js/jcemediabox.js?v=1017" type="text/javascript"></script>
  <script src="/plugins/system/jcemediabox/js/mediaobject.js?v=1017" type="text/javascript"></script>
  <script src="/plugins/system/jcemediabox/addons/default.js?v=1017" type="text/javascript"></script>
  <script src="/media/system/js/mootools-core.js" type="text/javascript"></script>
  <script src="/media/system/js/core.js" type="text/javascript"></script>
  <script src="/media/system/js/mootools-more.js" type="text/javascript"></script>
  <script src="/libraries/gantry/js/gantry-totop.js" type="text/javascript"></script>
  <script src="/libraries/gantry/js/browser-engines.js" type="text/javascript"></script>
  <script type="text/javascript">
	JCEMediaObject.init('/', {flash:"10,0,22,87",windowmedia:"5,1,52,701",quicktime:"6,0,2,0",realmedia:"7,0,0,0",shockwave:"8,5,1,0"});JCEMediaBox.init({popup:{width:"",height:"",legacy:0,lightbox:0,shadowbox:0,resize:1,icons:1,overlay:1,overlayopacity:0.8,overlaycolor:"#000000",fadespeed:500,scalespeed:500,hideobjects:0,scrolling:"fixed",close:2,labels:{'close':'Close','next':'Next','previous':'Previous','cancel':'Cancel','numbers':'{$current} of {$total}'}},tooltip:{className:"tooltip",opacity:0.8,speed:150,position:"br",offsets:{x: 16, y: 16}},base:"/",imgpath:"plugins/system/jcemediabox/img",theme:"light",themecustom:"",themepath:"plugins/system/jcemediabox/themes"});
  </script>
  <link href="/templates/rt_metropolis/fancybox/jquery.fancybox-1.3.4.css" type="text/css" rel="stylesheet">
  <link href="/templates/rt_metropolis/css/structure.css" type="text/css" rel="stylesheet">
  <link href="/templates/rt_metropolis/css/ieMain.css" type="text/css" rel="stylesheet">
  <link href="/templates/rt_metropolis/css/media-queries.css" type="text/css" rel="stylesheet">
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
  <!--[if IE 11]><link rel="stylesheet" type="text/css" href="http://m-arhiv.dev-z.ru/templates/rt_metropolis/css/ieMain.css" /> <![endif]-->
  <script type="text/javascript" src="/templates/rt_metropolis/fancybox/jquery.fancybox-1.3.4.min.js" ></script>
  <script type="text/javascript" src="/templates/rt_metropolis/js/jquery.carouFredSel-6.2.1-packed.js" ></script>
  <script type="text/javascript" src="/templates/rt_metropolis/js/base64.js" ></script>
  <script type="text/javascript" src="/templates/rt_metropolis/js/tabs.js" ></script>
  <link type="text/css" href="/jcart/css/jcart.css" rel="stylesheet" media="screen, projection" />
  <script type="text/javascript" src="/jcart/js/jcart.js"></script>
<script>
$(document).ready(function(){
 
    var url_l = '/index.php?tmpl=ajax_mod&mod_pos=mod_l';
 
    $.ajax({
        url: url_l,
        dataType : "html",
        success: function(html){
            $("#ajax_l").append(html);
            var url=document.location.href;
            $.each($("div.moduletablesideBarMenu a"),function(){
            if(this.href==url){$(this).addClass('act');};
            });
        }
    });
 
});

</script>
   <meta name='yandex-verification' content='5f2dd61bef376cd0' />
        <script type="text/javascript" src="/GA.js" ></script> 
	 <script type="text/javascript">
var __cs = __cs || [];
__cs.push(["setCsAccount", "TqDOlpTg7FCLcWgdm78QT6uXqtTTz9Br"]);
__cs.push(["setCsHost", "//server.comagic.ru/comagic"]);
</script>
<script type="text/javascript" async src="//app.comagic.ru/static/cs.min.js"></script>
</head>
<body  class="logo-type-custom main-bg-wood blocks-default-overlay-dark font-family-lucida font-size-is-default menu-type-dropdownmenu layout-mode-1200fixed col12 option-com-xmap menu-site-map ">
        
  <div class="rt-bg"><div class="rt-bg2">
  <div class="rt-container">
            <div id="rt-drawer">
                    <div class="clear"></div>
      </div>
                  <header id="rt-top-surround" class="rt-dark">
            <div id="rt-top">
        <div class="rt-grid-12 rt-alpha rt-omega">
               <div class="rt-block rt-dark-block top-fone">
           	<div class="module-surround">
	           		                	<div class="module-content">
	                		

<div class="customtop-fone"  >
	<div class="hederFone">
<p style="font-size: 18px; text-shadow: 1px 1px 1px #333; text-decoration: underline; text-align: center;"><a class="jcepopup noicon" href="/obratniy-zvonok?view=form" target="_blank" data-mediabox="width[400];height[450]">Заказать звонок</a></p>
<p class="pfone"><span class="comagic_phone">+7(495) 212-17-56</span></p>
</div></div>
	                	</div>
                	</div>
           </div>
	
</div>
        <div class="clear"></div>
      </div>
                  <div id="rt-header">
        <div class="rt-grid-3 rt-alpha">
<div class="rt-block logo-block">
                <a href="/" id="rt-logo"><img src="/images/pages/logo.png" alt="Производство стеллажей и металлоконструкций - компания Архив-Проект"></a>
        </div>
        
</div>
<div class="rt-grid-9 rt-omega">
               <div class="rt-block rt-dark-block">
           	<div class="module-surround">
	           		                	<div class="module-content">
	                		

<div class="custom"  >
	<ul class="myMenu">
<li><a href="#" data-key="hureirhfcnxjd2645se5a3" data-type="href">О компании</a></li>
<li><a href="#" data-key="ncmkdxfjeir216df5g6rea" data-type="href">Услуги</a></li>
<li><a href="#" data-key="nbnush54tw663336sdf55s" data-type="href">Отзывы</a></li>
<li><a href="#" data-key="eoiwjfd321grt54w6aar5a" data-type="href">Контакты</a></li>
</ul></div>
	                	</div>
                	</div>
           </div>
	           <div class="rt-block rt-dark-block slogan">
           	<div class="module-surround">
	           		                	<div class="module-content">
	                		

<div class="customslogan"  >
	<p style="font-size: 25px;">на рынке с 2003 года</p>
<p>производство и установка стеллажных систем</p></div>
	                	</div>
                	</div>
           </div>
	
</div>
        <div class="clear"></div>
      </div>
          </header>
                <div id="rt-transition">
      <div id="rt-mainbody-surround" class="rt-dark">
     
                                                              <div id="rt-main" class="sa3-mb9">
                    <div class="rt-grid-9 rt-push-3">
                                                						<div class="rt-block component-block main-overlay-light">
	                        <div id="rt-mainbody">
								<div class="component-content">
	                            	<div id="xmap">


    
    <h2 class="menutitle"></h2>
<ul class="level_0">
<li><a href="/nestandartnie-konstrykcii" title="Нестандартные конструкции">Нестандартные конструкции</a></li><li><a href="/stellazhi-dlya-sklada" title="150-300 кг. на полку">150-300 кг. на полку</a></li><li><a href="/peredvizhnie-stellazhi-dlya-sklada" title="Передвижные стеллажи для склада">Передвижные стеллажи для склада</a></li><li><a href="/stellazhi-setchatye-stil" title="Стеллажи сетчатые «Стиль»">Стеллажи сетчатые «Стиль»</a></li><li><a href="/skladskie-stellazhi-mkf" title="Стеллажи МКФ">Стеллажи МКФ</a></li><li><a href="/rekviziti" title="Реквизиты Архив-Проект">Реквизиты Архив-Проект</a></li><li><a href="/vakansii" title="Вакансии">Вакансии</a></li><li><a href="/forma-s-kalkulyatorom" title="Форма с калькулятором">Форма с калькулятором</a></li><li><a href="/yglovie-bilblioteki" title="Угловые библиотеки">Угловые библиотеки</a></li><li><a href="/biblioteka-razdvizhnaya-2-ryadnaya" title="Библиотека раздвижная 2-х рядная">Библиотека раздвижная 2-х рядная</a></li><li><a href="/biblioteka-razdvizhnaya-3-ryadnaya" title="Библиотека раздвижная 3-х рядная">Библиотека раздвижная 3-х рядная</a></li><li><a href="/razdvizhnaya-biblioteka-i-shkaf" title="Раздвижная библиотека + шкаф для одежды">Раздвижная библиотека + шкаф для одежды</a></li><li><a href="/biblioteka-razdvizhnaya-s-tv-i-garderob" title="Библиотека раздвижная с ТВ и гардеробом">Библиотека раздвижная с ТВ и гардеробом</a></li><li><a href="/ofisnoe-ispolnenie" title="Офисное исполнение">Офисное исполнение</a></li><li><a href="/dostavka-i-oplata" title="Доставка и оплата">Доставка и оплата</a></li><li><a href="/garantiya" title="Гарантия">Гарантия</a></li><li><a href="/proektirovanie-nestandartnix-konstrykci" title="Проектирование нестандартных конструкций">Проектирование нестандартных конструкций</a></li><li><a href="/skamejka-dlya-razdevalki" title="Скамейка для раздевалки">Скамейка для раздевалки</a></li><li><a href="/peredvizhnie-stellazhi" title="ЛП Передвижные стеллажи">ЛП Передвижные стеллажи</a></li><li><a href="/zamer" title="Вызвать замерщика">Вызвать замерщика</a></li><li><a href="/zapros-na-raschet" title="Запрос на расчет стоимости">Запрос на расчет стоимости</a></li><li><a href="/razdvizhnye-biblioteki" title="Раздвижные библиотеки">Раздвижные библиотеки</a></li><li><a href="/malie-arhivnie-formi" title="Малые архивные формы">Малые архивные формы</a></li><li><a href="/universalnye-stellazhy" title="Универсальные стеллажи">Универсальные стеллажи</a></li></ul>
<h2 class="menutitle"></h2>
<ul class="level_0">
<li><span>Передвижные стеллажи</span></li><li><a href="/arxivnye-peredvizhnye-stellazhi-st" title="Передвижные стеллажи СТ">Передвижные стеллажи СТ</a>
<ul class="level_1">
<li><a href="/arxivnye-peredvizhnye-stellazhi-st/mobilniy-stellazh-2080x1000x600" title="Мобильный стеллаж 2080х1000x600">Мобильный стеллаж 2080х1000x600</a></li><li><a href="/arxivnye-peredvizhnye-stellazhi-st/mobilniy-stellazh-2080x2000x600" title="Мобильный стеллаж 2080х2000x600">Мобильный стеллаж 2080х2000x600</a></li><li><a href="/arxivnye-peredvizhnye-stellazhi-st/mobilniy-stellazh-2080x3000x600" title="Мобильный стеллаж 2080х3000x600">Мобильный стеллаж 2080х3000x600</a></li><li><a href="/arxivnye-peredvizhnye-stellazhi-st/mobilniy-stellazh-2080x4000x600" title="Мобильный стеллаж 2080х4000x600">Мобильный стеллаж 2080х4000x600</a></li><li><a href="/arxivnye-peredvizhnye-stellazhi-st/mobilniy-stellazh-2080x5000x600" title="Мобильный стеллаж 2080x5000x600">Мобильный стеллаж 2080x5000x600</a></li><li><a href="/arxivnye-peredvizhnye-stellazhi-st/mobilniy-stellazh-2080x6000x600" title="Мобильный стеллаж 2080x6000x600">Мобильный стеллаж 2080x6000x600</a></li><li><a href="/arxivnye-peredvizhnye-stellazhi-st/mobilniy-stellazh-2080x7000x600" title="Мобильный стеллаж 2080x7000x600">Мобильный стеллаж 2080x7000x600</a></li><li><a href="/arxivnye-peredvizhnye-stellazhi-st/mobilniy-stellazh-2460x1000x600" title="Мобильный стеллаж 2460x1000x600">Мобильный стеллаж 2460x1000x600</a></li><li><a href="/arxivnye-peredvizhnye-stellazhi-st/mobilniy-stellazh-2460x2000x600" title="Мобильный стеллаж 2460x2000x600">Мобильный стеллаж 2460x2000x600</a></li><li><a href="/arxivnye-peredvizhnye-stellazhi-st/mobilniy-stellazh-2460x3000x600" title="Мобильный стеллаж 2460x3000x600">Мобильный стеллаж 2460x3000x600</a></li><li><a href="/arxivnye-peredvizhnye-stellazhi-st/mobilniy-stellazh-2460x4000x600" title="Мобильный стеллаж 2460x4000x600">Мобильный стеллаж 2460x4000x600</a></li><li><a href="/arxivnye-peredvizhnye-stellazhi-st/mobilniy-stellazh-2460x5000x600" title="Мобильный стеллаж 2460x5000x600">Мобильный стеллаж 2460x5000x600</a></li><li><a href="/arxivnye-peredvizhnye-stellazhi-st/mobilniy-stellazh-2460x6000x600" title="Мобильный стеллаж 2460x6000x600">Мобильный стеллаж 2460x6000x600</a></li><li><a href="/arxivnye-peredvizhnye-stellazhi-st/mobilniy-stellazh-2460x7000x600" title="Мобильный стеллаж 2460x7000x600">Мобильный стеллаж 2460x7000x600</a></li></ul>
</li><li><a href="/arxivnye-stellazhi-standart" title="Передвижные стеллажи Стандарт">Передвижные стеллажи Стандарт</a>
<ul class="level_1">
<li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2065x6000x600" title="Передвижной стеллаж 2065х6000x600">Передвижной стеллаж 2065х6000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2065x7000x600" title="Передвижной стеллаж 2065х7000x600">Передвижной стеллаж 2065х7000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2415x2000x600" title="Передвижной стеллаж 2415х2000x600">Передвижной стеллаж 2415х2000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2415x3000x600" title="Передвижной стеллаж 2415х3000x600">Передвижной стеллаж 2415х3000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2415x4000x600" title="Передвижной стеллаж 2415х4000x600">Передвижной стеллаж 2415х4000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2415x5000x600" title="Передвижной стеллаж 2415х5000x600">Передвижной стеллаж 2415х5000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2415x6000x600" title="Передвижной стеллаж 2415х6000x600">Передвижной стеллаж 2415х6000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2415x7000x600" title="Передвижной стеллаж 2415х7000x600">Передвижной стеллаж 2415х7000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2765x1000x600" title="Передвижной стеллаж 2765х1000x600">Передвижной стеллаж 2765х1000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2765x2000x600" title="Передвижной стеллаж 2765х2000x600">Передвижной стеллаж 2765х2000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2765x3000x600" title="Передвижной стеллаж 2765х3000x600">Передвижной стеллаж 2765х3000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2765x4000x600" title="Передвижной стеллаж 2765х4000x600">Передвижной стеллаж 2765х4000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2765x5000x600" title="Передвижной стеллаж 2765х5000x600">Передвижной стеллаж 2765х5000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2765x6000x600" title="Передвижной стеллаж 2765х6000x600">Передвижной стеллаж 2765х6000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2765x7000x600" title="Передвижной стеллаж 2765х7000x600">Передвижной стеллаж 2765х7000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2065x2000x600" title="Передвижной стеллаж 2065х2000x600">Передвижной стеллаж 2065х2000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2065x1000x600" title="Передвижной стеллаж 2065х1000x600">Передвижной стеллаж 2065х1000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2415x1000x600" title="Передвижной стеллаж 2415х1000x600">Передвижной стеллаж 2415х1000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2065x3000x600" title="Передвижной стеллаж 2065х3000x600">Передвижной стеллаж 2065х3000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2065x4000x600" title="Передвижной стеллаж 2065х4000x600">Передвижной стеллаж 2065х4000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2065x5000x600" title="Передвижной стеллаж 2065х5000x600">Передвижной стеллаж 2065х5000x600</a></li><li><a href="/arxivnye-stellazhi-standart/peredvizhnoy-stellazh-2065x1000x6001" title="Архивный передвижной стеллаж 2065x100x600">Архивный передвижной стеллаж 2065x100x600</a></li></ul>
</li><li><a href="/peredvizhnie-stellazhi-s-elektroprivodom" title="Передвижные стеллажи с электроприводом">Передвижные стеллажи с электроприводом</a></li><li><a href="/polochnie-stllazhi" title="Полочные стеллажи">Полочные стеллажи</a></li><li><a href="/polochnye-stellazhi-serii-st" title="Полочные стеллажи серии СТ">Полочные стеллажи серии СТ</a></li><li><a href="/polochnye-stellazhi-serii-su" title="Полочные стеллажи серии СУ">Полочные стеллажи серии СУ</a></li><li><a href="/stellazhi-sy-s-perforirovannimi-polkami" title="С перфорированными полками">С перфорированными полками</a></li><li><a href="/stellazhi-sy-so-shtangoy-dlya-odezhdi" title="Со штангой для одежды">Со штангой для одежды</a></li><li><a href="/metallicheskie-stellazhi-dlya-arxiva-i-ofisa" title="Архивные стеллажи">Архивные стеллажи</a></li><li><a href="/arhivnie-stellazhi-strandart" title="Стеллажи Стандарт">Стеллажи Стандарт</a></li><li><a href="/metallicheskie-stellazhi-serii-st-do-120-kg-na-polku" title="Стеллажи СТ">Стеллажи СТ</a></li><li><a href="/arhivnye-stellazhy-serii-su" title="Стеллажи СУ">Стеллажи СУ</a></li><li><a href="/skladskie-stellazhi" title="Складские стеллажи">Складские стеллажи</a></li><li><a href="/skladskie-stellazhi-solid" title="Стеллажи Солид">Стеллажи Солид</a>
<ul class="level_1">
<li><a href="/skladskie-stellazhi-solid/solid-254" title="Стеллаж складской Солид 2000x1540x455">Стеллаж складской Солид 2000x1540x455</a></li><li><a href="/skladskie-stellazhi-solid/solid-255" title="Стеллаж складской Солид 2000x1540x500">Стеллаж складской Солид 2000x1540x500</a></li><li><a href="/skladskie-stellazhi-solid/solid-256" title="Стеллаж складской Солид 2000x1540x655">Стеллаж складской Солид 2000x1540x655</a></li><li><a href="/skladskie-stellazhi-solid/solid-257" title="Стеллаж складской Солид 2000x1540x770">Стеллаж складской Солид 2000x1540x770</a></li><li><a href="/skladskie-stellazhi-solid/solid-2510" title="Стеллаж складской Солид 2000x1540x1000">Стеллаж складской Солид 2000x1540x1000</a></li><li><a href="/skladskie-stellazhi-solid/solid-2554" title="Стеллаж складской Солид 2500x1540x455">Стеллаж складской Солид 2500x1540x455</a></li><li><a href="/skladskie-stellazhi-solid/solid-2555" title="Стеллаж складской Солид 2500x1540x500">Стеллаж складской Солид 2500x1540x500</a></li><li><a href="/skladskie-stellazhi-solid/solid-2556" title="Стеллаж складской Солид 2500x1540x655">Стеллаж складской Солид 2500x1540x655</a></li><li><a href="/skladskie-stellazhi-solid/solid-2557" title="Стеллаж складской Солид 2500x1540x770">Стеллаж складской Солид 2500x1540x770</a></li><li><a href="/skladskie-stellazhi-solid/solid-25510" title="Стеллаж складской Солид 2500x1540x1000">Стеллаж складской Солид 2500x1540x1000</a></li><li><a href="/skladskie-stellazhi-solid/solid-354" title="Стеллаж складской Солид 3000x1540x455">Стеллаж складской Солид 3000x1540x455</a></li><li><a href="/skladskie-stellazhi-solid/solid-355" title="Стеллаж складской Солид 3000x1540x500">Стеллаж складской Солид 3000x1540x500</a></li><li><a href="/skladskie-stellazhi-solid/solid-356" title="Стеллаж складской Солид 3000x1540x655">Стеллаж складской Солид 3000x1540x655</a></li><li><a href="/skladskie-stellazhi-solid/solid-357" title="Стеллаж складской Солид 3000x1540x770">Стеллаж складской Солид 3000x1540x770</a></li><li><a href="/skladskie-stellazhi-solid/solid-3510" title="Стеллаж складской Солид 3000x1540x1000">Стеллаж складской Солид 3000x1540x1000</a></li></ul>
</li><li><a href="/skladskie-stellazhi-sgr" title="Стеллажи СГР">Стеллажи СГР</a>
<ul class="level_1">
<li><a href="/skladskie-stellazhi-sgr/rama-sgr-26" title="Рама СГР 2000x600">Рама СГР 2000x600</a></li><li><a href="/skladskie-stellazhi-sgr/rama-sgr-36" title="Рама СГР 3000x600">Рама СГР 3000x600</a></li><li><a href="/skladskie-stellazhi-sgr/rama-sgr-31" title="Рама СГР 3000x1000">Рама СГР 3000x1000</a></li><li><a href="/skladskie-stellazhi-sgr/rama-sgr-38" title="Рама СГР 3000x800">Рама СГР 3000x800</a></li><li><a href="/skladskie-stellazhi-sgr/rama-sgr-28" title="Рама СГР 2000x800">Рама СГР 2000x800</a></li><li><a href="/skladskie-stellazhi-sgr/rama-sgr-21" title="Рама СГР 2000x1000">Рама СГР 2000x1000</a></li><li><a href="/skladskie-stellazhi-sgr/rama-sgr-256" title="Рама СГР 2500x600">Рама СГР 2500x600</a></li><li><a href="/skladskie-stellazhi-sgr/rama-sgr-258" title="Рама СГР 2500x800">Рама СГР 2500x800</a></li><li><a href="/skladskie-stellazhi-sgr/polka-metallicheskaya-sgr-126" title="Полка металлическая СГР 1200x600">Полка металлическая СГР 1200x600</a></li><li><a href="/skladskie-stellazhi-sgr/polka-metallicheskaya-sgr-156" title="Полка металлическая СГР 1500x600">Полка металлическая СГР 1500x600</a></li><li><a href="/skladskie-stellazhi-sgr/polka-metallicheskaya-sgr-186" title="Полка металлическая СГР 1800x600">Полка металлическая СГР 1800x600</a></li><li><a href="/skladskie-stellazhi-sgr/polka-metallicheskaya-sgr-216" title="Полка металлическая СГР 2100x600">Полка металлическая СГР 2100x600</a></li><li><a href="/skladskie-stellazhi-sgr/polka-metallicheskaya-sgr-127" title="Полка металлическая СГР 1200x700">Полка металлическая СГР 1200x700</a></li><li><a href="/skladskie-stellazhi-sgr/polka-metallicheskaya-sgr-157" title="Полка металлическая СГР 1500x700">Полка металлическая СГР 1500x700</a></li><li><a href="/skladskie-stellazhi-sgr/polka-metallicheskaya-sgr-187" title="Полка металлическая СГР 1800x700">Полка металлическая СГР 1800x700</a></li><li><a href="/skladskie-stellazhi-sgr/polka-metallicheskaya-sgr-217" title="Полка металлическая СГР 2100x700">Полка металлическая СГР 2100x700</a></li><li><a href="/skladskie-stellazhi-sgr/polka-metallicheskaya-sgr-121" title="Полка металлическая СГР 1200x1000">Полка металлическая СГР 1200x1000</a></li><li><a href="/skladskie-stellazhi-sgr/polka-metallicheskaya-sgr-151" title="Полка металлическая СГР 1500x1000">Полка металлическая СГР 1500x1000</a></li><li><a href="/skladskie-stellazhi-sgr/polka-metallicheskaya-sgr-181" title="Полка металлическая СГР 1800x1000">Полка металлическая СГР 1800x1000</a></li><li><a href="/skladskie-stellazhi-sgr/polka-metallicheskaya-sgr-211" title="Полка металлическая СГР 2100x1000">Полка металлическая СГР 2100x1000</a></li></ul>
</li><li><a href="/stellazhi-grozd" title="Стеллажи Гроздь">Стеллажи Гроздь</a></li><li><a href="/palletnye-stellazhi" title="Паллетные стеллажи">Паллетные стеллажи</a></li><li><a href="/nabivnie-stellazhi" title="Набивные стеллажи">Набивные стеллажи</a></li><li><span>Специализированные стеллажи</span></li><li><a href="/konsolnie-stellazhi" title="Консольные стеллажи">Консольные стеллажи</a></li><li><a href="/mezonin" title="Мезонины">Мезонины</a></li><li><a href="/stellazhi-dlya-garazha" title="Стеллажи для гаража">Стеллажи для гаража</a></li><li><a href="/stellazhi-dlya-shin" title="Стеллажи для шин">Стеллажи для шин</a></li><li><a href="/stellazhi-dlya-doma" title="Стеллажи для дома">Стеллажи для дома</a></li><li><a href="/stellazhi-dlya-balkona" title="Стеллажи для балкона">Стеллажи для балкона</a></li><li><span>Металлические шкафы</span></li><li><a href="/metallicheskie-shkafy-dlya-odezhdy" title="Для спецодежды">Для спецодежды</a></li><li><a href="/plastikovye-kontejnery-sistema-7000" title="Пластиковые контейнеры «Система 7000»">Пластиковые контейнеры «Система 7000»</a></li><li><a href="/plastikovye-kontejnery-sistema-9000" title="Пластиковые контейнеры «Система 9000»">Пластиковые контейнеры «Система 9000»</a></li></ul>


    <span class="article_separator">&nbsp;</span>
</div>
								</div>
	                        </div>
                            <div class="clear"></div>
						</div>
                                                                    </div>
					 
                                <div class="rt-grid-3 rt-pull-9">
                <div id="rt-sidebar-a">
                               <div class="rt-block rt-dark-block korzina_wrapper">
           	<div class="module-surround">
	           		                	<div class="module-content">
	                		<div id="korzina">
	<div id="jcart"><div id="cart_display" class="cart_hide"></div>
<form method="post" action="http://m-arhiv.dev-z.ru/checkout" >

	<input type="hidden" name="jcartToken" value="">
	<table>
		<thead>
			<tr>
				
								<th colspan="3">
										Ваша корзина
				</th>
							</tr>
		</thead>
		<tbody>
					<tr>
				<td id="jcart-empty" colspan="2">Ваша корзина пуста!</td>
			</tr>
				</tbody>
		<tfoot>
			<tr>
				<th colspan='2'>
										<input type="submit"  id="jcart-checkout" name="jcartCheckout" class="jcart-button" value="Оформить заказ" />
										<!--subtotal-->
					<span id="jcart-subtotal">Всего: <strong>0.00 руб.</strong></span>
					<script type="text/javascript">
						var cart_subtotal = 0;
					</script>
					<!--subtotal end-->
				</th>
			</tr>
		</tfoot>
	</table>
	<div id="jcart-buttons">
		<input type="submit"  name="jcartUpdateCart" value="Обновить" class="jcart-button" />
		<input type="submit"  name="jcartEmpty" value="Очистить" class="jcart-button" />
	</div>
</form>


<div id="jcart-tooltip"></div></div>
<div class="showHide">
<span id="cart_show" style="display:none;" href="/">Развернуть корзину</span>
<span id="cart_hide" style="display:none;" href="/">Свернуть корзину</span>
</div>
</div>	                	</div>
                	</div>
           </div>
	           <div class="rt-block rt-dark-block sideBarMenu">
           	<div class="module-surround">
	           		                	<div class="module-content">
	                		

<div class="customsideBarMenu"  >
	<div id="ajax_l"></div></div>
	                	</div>
                	</div>
           </div>
	
                </div>
            </div>

					
                    <div class="clear"></div>
            </div>
                                         </div>
    </div>
            <footer id="rt-footer-surround" class="rt-dark">
            <div id="rt-footer"> 
      <div class="rt-grid-3 rt-alpha">
               <div class="rt-block rt-dark-block">
           	<div class="module-surround">
	           				<div class="module-title">
	                		<p class="title" style="font-size: 1.9em;">Разделы</p>
			</div>
	                		                	<div class="module-content">
	                		

<div class="custom"  >
	<ul class="rt-demo-footer-menu">
<li><a href="http://m-arhiv.dev-z.ru/arxivnye-stellazhi-standart">Передвижные стеллажи</a></li>
<li><a href="http://m-arhiv.dev-z.ru/metallicheskie-stellazhi-dlya-arxiva-i-ofisa">Архивные стеллажи</a></li>
<li><a href="http://m-arhiv.dev-z.ru/skladskie-stellazhi">Стеллажи складские</a></li>
<li><a href="http://m-arhiv.dev-z.ru/palletnye-stellazhi">Паллетные стеллажи</a></li>
<li><a href="http://m-arhiv.dev-z.ru/stellazhi-dlya-shin">Стеллажи для шин</a></li>
</ul></div>
	                	</div>
                	</div>
           </div>
	
</div>
<div class="rt-grid-3">
               <div class="rt-block rt-dark-block">
           	<div class="module-surround">
	           				<div class="module-title">
	                		<p class="title" style="font-size: 1.9em;">О компании</p>
			</div>
	                		                	<div class="module-content">
	                		

<div class="custom"  >
	<ul class="rt-demo-footer-menu">
<li><a href="#" data-key="gdfg2df1g6d54rf4d2fx2d" data-type="href">Реквизиты</a></li>
<li><a href="#" data-key="kfljieorjszldeeiwehfhg" data-type="href">Вакансии</a></li>
<li><a href="#" data-key="hhgtrt564dre66wae54sgf" data-type="href">Статьи</a></li>
<li><a class="jcepopup noicon" href="/forma-obratnoy-svyazi?view=form" target="_blank" data-mediabox="width[550];height[500]">Обратная связь</a></li>
</ul></div>
	                	</div>
                	</div>
           </div>
	
</div>
<div class="rt-grid-3">
               <div class="rt-block rt-dark-block hidden-phone">
           	<div class="module-surround">
	           				<div class="module-title">
	                		<p class="title" style="font-size: 1.9em;">Помощь</p>
			</div>
	                		                	<div class="module-content">
	                		

<div class="customhidden-phone"  >
	<ul class="rt-demo-footer-menu">
<li><a href="#" data-key="nvrueiaj21h654tr6s5fd5" data-type="href">Доставка и оплата</a></li>
<li><a href="#" data-key="mncvksdjfhgiur21656xmf" data-type="href">Гарантия</a></li>
<li><a href="#" data-key="ryertiuroiy2165adf4gfs" data-type="href">Проектирование нестандартных конструкций</a></li>
<li><a href="http://m-arhiv.dev-z.ru/site-map/" data-type="href">Карта сайта</a></li>
</ul></div>
	                	</div>
                	</div>
           </div>
	
</div>
<div class="rt-grid-3 rt-omega">
               <div class="rt-block rt-dark-block hidden-phone">
           	<div class="module-surround">
	           				<div class="module-title">
	                		<p class="title" style="font-size: 1.9em;">Контакты</p>
			</div>
	                		                	<div class="module-content">
	                		

<div class="customhidden-phone"  >
	<div itemscope itemtype="http://schema.org/Corporation">
<div itemprop="name"><strong>Архив-Проект</strong></div>
<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
<span itemprop="postalCode">117105</span>, 
<span itemprop="addressCountry">Россия</span>, 
<span itemprop="addressLocality">Москва</span>, 
<span itemprop="streetAddress">Нагорный проезд, д.12, к.1</span><br>
</div>
  Телефон:<span itemprop="telephone"><span class="comagic_phone">+7(495) 212-17-56</span></span><br>
  Email: <span itemprop="email">
 <script type='text/javascript'>
 <!--
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy12949 = 'z&#97;k&#97;z' + '&#64;';
 addy12949 = addy12949 + 'm-&#97;rh&#105;v' + '&#46;' + 'r&#117;';
 document.write('<a ' + path + '\'' + prefix + ':' + addy12949 + '\'>');
 document.write(addy12949);
 document.write('<\/a>');
 //-->\n </script><script type='text/javascript'> 
 <!--
 document.write('<span style=\'display: none;\'>');
 //-->
 </script>Этот адрес электронной почты защищен от спам-ботов. У вас должен быть включен JavaScript для просмотра.
 <script type='text/javascript'>
 <!--
 document.write('</');
 document.write('span>');
 //-->
 </script></span><br>
  Часы работы: пн-вс с 9:00 до 19:00
</div></div>
	                	</div>
                	</div>
           </div>
	
</div>
        <div class="clear"></div>
        
      </div>
                  <div id="rt-copyright">

        <div class="rt-grid-4 rt-alpha">
    	<div class="clear"></div>
	<div class="rt-block">
		<p style="color: #d6d0cc;">Copyright © 2006 - 2017 Ltd. "Archive-Project"</p>
	</div>
	
</div>
<div class="rt-grid-4">
               <div class="rt-block rt-dark-block">
           	<div class="module-surround">
	           		                	<div class="module-content">
	                		

<div class="custom"  >
	<img src="/images/melochi/PortalUser.png" alt="" class="portalUslug"/></div>
	                	</div>
                	</div>
           </div>
	
</div>
<div class="rt-grid-4 rt-omega">
    	<div class="clear"></div>
	<div class="rt-block">
		<a href="#" id="gantry-totop" rel="nofollow">Top</a>
	</div>
	
</div>
        <div class="clear"></div>
      </div>
          </footer>
      </div>
</div></div>
          <!-- Yandex.Metrika counter --><script type="text/javascript">var yaParams = {/*Здесь параметры визита*/};</script><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter7553026 = new Ya.Metrika({id:7553026, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true,params:window.yaParams||{ }}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/7553026" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
  <!-- Google -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-35092378-1', 'auto');
  ga('send', 'pageview');

</script>

  <!-- /Google --> <!-- 9d881599d3e00381 -->

<script type="text/javascript" src="/templates/rt_metropolis/js/muve.js"></script>
<script type="text/javascript">shineLinks('ajax_l');</script>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'fFLVxrYib6';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
  </body>
</html>
 