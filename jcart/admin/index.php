<?php

# jCart v1.3.5
# http://conceptlogic.com/jcart/
# http://jcart.info

# Подключение настроек
include_once dirname(__FILE__) . '/../jcart.inc.php';
include_once (dirname(__FILE__) . '/jcart_admin_init.inc.php');
$config = $jcart->config;

# Простейшая авторизация
include_once dirname(__FILE__) . '/../modules/M_Admin.inc.php';
$mAdmin = M_Admin::Instance();

if (!$mAdmin->CheckLogin())
	die;

# Кодировка.
header('Content-type: text/html; charset=' . $config['encoding']);

# Обработка имени файла
$file = str_replace(dirname(__FILE__) . '/', '', (__FILE__));
$file = str_replace('.php', '', $file);

# Вывод дизайна
include_once dirname(__FILE__) . '/_menu.php';

if ($config['database']['enabled'] == false)
{
	include_once dirname(__FILE__) . '/../design/AdminHeader.tpl.php';
	die('<div class="container-fluid"><div class="well well-inverse message-block"><div class="alert alert-error">Использование базы данных отключено в настройках. Админка в данном случае бесполезна и будет выдавать ошибки.</div></div></div>');
}

# Подключение модуля работы с базой данных.
include_once dirname(__FILE__) . '/../modules/M_DB.inc.php';
$mDB = M_DB::Instance();
# Подсчёт оплаченных заказов
$PaidOrders = $mDB->GetItemsByParam('custom', 'status', '1');
$PaidOrdersCount = count($PaidOrders);

$NewOrdersCount .= $mDB->Plural($NewOrdersCount, ' новый заказ', ' новых заказа', ' новых заказов');
$PaidOrdersCount .= $mDB->Plural($PaidOrdersCount, ' оплачен и ожидает отправки', ' оплачены и ожидают отправки', ' оплачены и ожидают отправки');

include_once dirname(__FILE__) . '/../design/AdminIndex.tpl.php';