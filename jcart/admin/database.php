<?php

# jCart v1.3
# http://conceptlogic.com/jcart/
# http://jcart.info

# Подключение настроек
include_once dirname(__FILE__) . '/../jcart.inc.php';
include_once (dirname(__FILE__) . '/jcart_admin_init.inc.php');
$config = $jcart->config;

# Простейшая авторизация
include_once dirname(__FILE__) . '/../modules/M_Admin.inc.php';
$mAdmin = M_Admin::Instance();

if (!$mAdmin->CheckLogin())
	die;

# Кодировка.
header('Content-type: text/html; charset=' . $config['encoding']);

# Обработка имени файла
$file = str_replace(dirname(__FILE__) . '/', '', (__FILE__));
$file = str_replace('.php', '', $file);

# Подключение меню
include_once dirname(__FILE__) . '/_menu.php';

if ($config['database']['enabled'] == false)
{
	include_once dirname(__FILE__) . '/../design/AdminHeader.tpl.php';
	die('<div class="container-fluid"><div class="well well-inverse message-block"><div class="alert alert-error">Использование базы данных отключено в настройках. Админка в данном случае бесполезна и будет выдавать ошибки.</div></div></div>');
}

# Подключение модуля работы с базой данных.
include_once dirname(__FILE__) . '/../modules/M_Products.inc.php';
include_once dirname(__FILE__) . '/../modules/M_Files.inc.php';
$mProducts = M_Products::Instance();
$mFiles = M_Files::Instance();

# Очищение временной папки
$mFiles->CleanTemp();

# Обработка POST запроса
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    // Обработка экспорта
    if (isset($_POST['export']))
    {
		if (isset($_POST['export_table']))
		{
			$first_line = (isset($_POST['csv_col_output']))? true : false ;
			if($mFiles->MySQL2CSV($_POST['export_table'], $first_line))
				$_SESSION['success_message'] = 'Выгрузка произведена успешно';
			else
				$_SESSION['error_message'] = 'Что-то пошло не так!';
		}
	    else
			$_SESSION['error_message'] = 'База данных не выбрана!';
    }

    // Обработка импорта
    if (isset($_POST['import']))
    {
        $path = dirname(__FILE__) . '/../tmp/';


        if ($file = $mFiles->UploadFile($_FILES['database'], $path))
        {
            if(isset($_POST['csv_col_input']) && $_POST['csv_col_input'] == true)
			{
				if ($rows = $mFiles->CSV2MySQLv2($file, $_POST['import_table']))
					$_SESSION['success_message'] = 'Обновлено товаров: ' . $rows;
				else
					$_SESSION['error_message'] = 'Что-то пошло не так!';
			}
			else
			{
				if ($rows = $mFiles->CSV2MySQL($file, $_POST['import_table']))
					$_SESSION['success_message'] = 'Обновлено товаров: ' . $rows;
				else
					$_SESSION['error_message'] = 'Что-то пошло не так!';
        	}
		}
	}
	header('Location: ' . $_SERVER['HTTP_REFERER']);
	die;
}

include_once dirname(__FILE__) . '/../design/AdminDatabase.tpl.php';