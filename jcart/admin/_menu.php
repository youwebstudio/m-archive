<?php

# Генерация меню
function GetMenuItems($page, $menu_items)
{
	global $config;

	$menu = '';

	foreach ($menu_items as $i => $item)
	{
		if ($page == $item[0])
			$menu .= '<li class="active"><a href="' . $config['sitelink'] . $config['jcartPath'] . 'admin/' . $item[0] . '.php" title="' . $item[2] . '">' . $item[1] . '</a></li>';
		else
			$menu .= '<li><a href="' . $config['sitelink'] . $config['jcartPath'] . 'admin/' . $item[0] . '.php" title="' . $item[2] . '">' . $item[1] . '</a></li>';
	}

	return $menu;
}

if($config['database']['enabled'] != false)
{
	# Подключение модуля работы с базой данных.
	include_once dirname(__FILE__) . '/../modules/M_DB.inc.php';
	$mDB = M_DB::Instance();
	$NewOrdersCount = $mDB->GetItemsByParam('custom', 'status', '0');
	$NewOrdersCount = count($NewOrdersCount);
}
else
	$NewOrdersCount = 0;

$menu_items = array (
	array ('index', 'Главная', 'Главная страница админки'),
	array ('orders', 'Заказы <span class="badge badge-success">' . $NewOrdersCount . ' </span>', 'Управление заказами'),
	array ('products', 'Товары', 'Управление товарами'),
	array ('database', 'База данных', 'Загрузка/выгрузка данных'),
	array ('users', 'Клиенты', 'Управление клиентами'),
	array ('settings', 'Настройки', 'Управление настройками'),
	array ('help', 'Справка', 'Справка')
);
