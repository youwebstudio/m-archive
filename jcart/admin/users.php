<?php

# jCart v1.3.5
# http://conceptlogic.com/jcart/
# http://jcart.info

# Подключение настроек
include_once dirname(__FILE__) . '/../jcart.inc.php';
include_once (dirname(__FILE__) . '/jcart_admin_init.inc.php');
$config = $jcart->config;

# Простейшая авторизация
include_once dirname(__FILE__) . '/../modules/M_Admin.inc.php';
$mAdmin = M_Admin::Instance();

if (!$mAdmin->CheckLogin())
	die;

# Кодировка.
header('Content-type: text/html; charset=' . $config['encoding']);

# Обработка имени файла
$file = str_replace(dirname(__FILE__) . '/', '', (__FILE__));
$file = str_replace('.php', '', $file);

# Подключение меню
include_once dirname(__FILE__) . '/_menu.php';

if ($config['database']['enabled'] == false)
{
	include_once dirname(__FILE__) . '/../design/AdminHeader.tpl.php';
	die('<div class="container-fluid"><div class="well well-inverse message-block"><div class="alert alert-error">Использование базы данных отключено в настройках. Админка в данном случае бесполезна и будет выдавать ошибки.</div></div></div>');
}

# Подключение необходимых модулей
include_once dirname(__FILE__) . '/../modules/M_Users.inc.php';
$mUsers = M_Users::Instance();

# Устанавливаем текущую страницу.
if (isset($_GET['page']))
    $navi['page'] = $_GET['page'];
else
    $navi['page'] = 1;

# Устанавливаем текущего пользователя.
if (isset($_GET['user']))
    $id_user = $_GET['user'];

# Обработка POST запроса
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	# Удаление пользователя
	if (isset($_POST['user_delete']))
	{
		$mDB->DeleteItemById('user', $_POST['user_id']);
		/* удаляем все заказы и элементы?
		$mDB->DeleteItemById('custom', $_POST['order_id']);
        $mDB->DeleteItemsByParam('custom_item', 'id_custom',  $_POST['order_id']);*/
		header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/users.php');
		die;
	}

	# Отмена редактирования пользователя
	if (isset($_POST['user_cancel']))
	{
		header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/users.php');
		die;
	}

	# Редактирование пользователя
	if (isset($_POST['user_edit']))
    {
		# Редактирование данных пользователя
		$params = array (	'email' => $_POST['user_email'],
							'date' => $_POST['user_date'],
							'status' => $_POST['user_status']
						);

		$mDB->EditItemById('user', $params, $_POST['user_id'], true);

		header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/users.php');
		die;
	}
}
else
{
	# Вывод текущего пользователя
	if (isset($id_user))
	{
		# Выбор пользователя и его заказов
		$user = $mDB->GetItemById('user', $id_user);
		$orders = $mDB->GetItemsByParam('custom', 'id_user', $id_user);

		# Определение общей суммы заказов
		$user['sum'] = $user['whole_sum'] = 0;
		foreach ($orders as $order)
		{
			$user['whole_sum'] += $order['sum'];
			
			if ($order['status'] == 1)
				$user['sum'] += $order['sum'];
		}

		# Определение статуса
		$statuses = $config['user_statuses'];

		# Определение валюты
		$currencySymbol = $jcart->currencySymbol($config['currencyCode']);

		include_once dirname(__FILE__) . '/../design/AdminUserEdit.tpl.php';
	}
	# Вывод списка пользователей
	else
	{
		# Выбор пользователей с учётом пагинации
		$navi = $mDB->Paginate('user', $navi['page'], $config['admin']['usersList']);
		$users = $mDB->GetPaginatedList('user', $navi['start'], $config['admin']['usersList']);

		# Определение статусов
		foreach ($users as $i => $user)
			$users[$i]['status'] = $config['user_statuses'][$user['status']];

		ob_start();
		include_once dirname(__FILE__) . '/../design/AdminPagination.tpl.php';
		$pagination = ob_get_clean();

		include_once dirname(__FILE__) . '/../design/AdminUserList.tpl.php';
	}
}