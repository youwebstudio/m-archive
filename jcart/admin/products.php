<?php

# jCart v1.3.5
# http://conceptlogic.com/jcart/
# http://jcart.info

# Подключение настроек
include_once dirname(__FILE__) . '/../jcart.inc.php';
include_once (dirname(__FILE__) . '/jcart_admin_init.inc.php');
$config = $jcart->config;

# Простейшая авторизация
include_once dirname(__FILE__) . '/../modules/M_Admin.inc.php';
$mAdmin = M_Admin::Instance();

if (!$mAdmin->CheckLogin())
	die;

# Кодировка.
header('Content-type: text/html; charset=' . $config['encoding']);

# Обработка имени файла
$file = str_replace(dirname(__FILE__) . '/', '', (__FILE__));
$file = str_replace('.php', '', $file);

# Подключение меню
include_once dirname(__FILE__) . '/_menu.php';

if ($config['database']['enabled'] == false)
{
	include_once dirname(__FILE__) . '/../design/AdminHeader.tpl.php';
	die('<div class="container-fluid"><div class="well well-inverse message-block"><div class="alert alert-error">Использование базы данных отключено в настройках. Админка в данном случае бесполезна и будет выдавать ошибки.</div></div></div>');
}

# Подключение необходимых модулей для работы
include_once dirname(__FILE__) . '/../modules/M_Categories.inc.php';
$mCategories = M_Categories::Instance();
include_once dirname(__FILE__) . '/../modules/M_Products.inc.php';
$mProducts = M_Products::Instance();
include_once dirname(__FILE__) . '/../modules/M_Pictures.inc.php';
$mPictures = M_Pictures::Instance();
include_once dirname(__FILE__) . '/../modules/M_Files.inc.php';
$mFiles = M_Files::Instance();

# Очищение временной папки
$mFiles->CleanTemp();

# Определение текущей страницы
if (isset($_GET['page']))
    $navi['page'] = $_GET['page'];
else
    $navi['page'] = 1;

# Определение текущей категории
if (!empty($_GET['category']))
	$current_category = $mDB->GetItemByCode('category', $_GET['category']);
else
	$current_category['code'] = '';

# Обработка POST запроса
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	# Редактирование подкатегорий и товаров
	if (isset($_POST['products_edit']))
	{
		# Редактирование подкатегорий
		$category_id = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
		$category_code = (!empty($_POST['category_code'])) ? $_POST['category_code'] : '';
		$category_parent = (!empty($_POST['category_parent'])) ? $_POST['category_parent'] : '';
		$category_title = (!empty($_POST['category_title'])) ? $_POST['category_title'] : '';

		if (!empty($category_id))
		{
			foreach ($category_id as $key => $category)
			{
				$cat = $mDB->GetItemById('category', $category_id[$key]);

				$mCategories->EditCategory($category_id[$key], $category_code[$key], $category_parent[$key], $category_title[$key]);

				if($category_code[$key] != $cat['code'])
				{
					
					$sub_cats = $mCategories->GetSubcategories($cat['code']);
					foreach($sub_cats as $i=>$sub_cat)
					{
						$params = array('parent'=>$category_code[$key]);
						$mDB->EditItemById('category', $params, $sub_cat['id_category'], true);
						unset($params);
					}
					$prods = $mDB->GetItemsByParam('product', 'category', $cat['code']);
					foreach($prods as $j=>$prod)
					{
						$params = array('category'=>$category_code[$key]);
						$mDB->EditItemById('product', $params, $prod['id_product'], true);
						unset($params);
					}
					$mPictures->ChengeCategory($cat['code'], $category_code[$key]);
				}
			}
        }

		# Добавление новых подкатегорий
		if (!empty($_POST['new_category_code']))
		{
			$new_category_code = $_POST['new_category_code'];
			$new_category_parent = $_POST['new_category_parent'];
			$new_category_title = $_POST['new_category_title'];

			foreach ($new_category_code as $key => $category)
			{
				$mCategories->CreateCategory($new_category_code[$key], $new_category_parent[$key], $new_category_title[$key]);
			}
		}

		# Редактирование товаров
		$product_id = (!empty($_POST['product_id'])) ? $_POST['product_id'] : '';
		$product_code = (!empty($_POST['product_code'])) ? $_POST['product_code'] : '';
		$product_category = (!empty($_POST['product_category'])) ? $_POST['product_category'] : '';
		$product_title = (!empty($_POST['product_title'])) ? $_POST['product_title'] : '';
		$product_quantity = (!empty($_POST['product_quantity'])) ? $_POST['product_quantity'] : '';
		$product_price = (!empty($_POST['product_price'])) ? $_POST['product_price'] : '';
		$product_discount = (!empty($_POST['product_discount'])) ? $_POST['product_discount'] : '';
		$product_discount_date = (!empty($_POST['product_discount_date'])) ? $_POST['product_discount_date'] : '';
		$product_description = (!empty($_POST['product_description'])) ? $_POST['product_description'] : '';
		$product_unit = (!empty($_POST['product_unit'])) ? $_POST['product_unit'] : '';
		$product_color = (!empty($_POST['product_color'])) ? $_POST['product_color'] : '';
		$product_size = (!empty($_POST['product_size'])) ? $_POST['product_size'] : '';
		$product_param = (!empty($_POST['product_param'])) ? $_POST['product_param'] : '';
		$product_pic_name = (!empty($_POST['pic_name'])) ? $_POST['pic_name'] : '';
		$product_pic_rename = (!empty($_POST['pic_rename'])) ? $_POST['pic_rename'] : '';
		$product_file = (!empty($_FILES['product_file'])) ? $_FILES['product_file'] : '';
		$product_picture = (!empty($_FILES['product_picture'])) ? $_FILES['product_picture'] : '';
		$path = dirname(__FILE__) . '/../' . $config['download']['dir'] . '/';
		$picture_path = dirname(__FILE__) . '/../' . $config['images']['dir'] . '/';

		//echo var_dump($_POST);
		//die;

		if (!empty($product_id))
		{
			foreach ($product_id as $key => $product)
			{
                $new_product_type = $product_picture_type = '';

				# Если есть загруженный файл
				if (is_array($product_file) && isset($product_file['tmp_name'][$key]) && $product_file['tmp_name'][$key] != '' && $config['download']['enabled'] == true)
                {
					# Получение типа файла
					$temp = explode('.', $product_file['name'][$key]);
					$product_type = $temp[count($temp) - 1];

					# Подготовка загрузки
					$upload_file = array();
					$upload_file['name'] = $product_code[$key] . '.' . $product_type;
					$upload_file['tmp_name'] = $product_file['tmp_name'][$key];
					$upload_file['size'] = $product_file['size'][$key];
					$upload_file['type'] = $product_file['type'][$key];
					$upload_file['error'] = $product_file['error'][$key];

					if ($product_type == $config['download']['type'])
					{
						# Удаление старого файла
						if (is_file($path . $product_code[$key] . '.' . $product_type))
							unlink($path . $product_code[$key] . '.' . $product_type);

						# Загрузка нового файла
						$mFiles->UploadFile($upload_file, $path);
						unset($upload_file);
					}
					else
						$_SESSION['error_message'] = 'Неверное разрешение загружаемого файла';
				}

				# Если есть загруженное изображение
				if (is_array($product_picture) && is_array($product_picture['tmp_name'][$key]) && $config['images']['enabled'] == true)
                {	
					
					foreach ($product_picture['name'][0] as $i=> $name)
					{
						# Получение типа файла
						$temp = explode('.', $product_picture['name'][0][$i]);
						$product_picture_type = $temp[count($temp) - 1];

						# Подготовка загрузки
						$upload_file = array();
						$upload_file['name'] = $product_code[$key] . '_'.$i.'.' . $product_picture_type;
						$upload_file['tmp_name'] = $product_picture['tmp_name'][0][$i];
						$upload_file['size'] = $product_picture['size'][$key][$i];
						$upload_file['type'] = $product_picture['type'][$key][$i];
						$upload_file['error'] = $product_picture['error'][$key][$i];

						# Загрузка нового изображения
						if ($config['images']['resize']['enabled'])
							$mPictures->UploadPicture($upload_file, $picture_path, $product_category[$key]);
						else
							$mFiles->UploadFile($upload_file, $picture_path, $product_category[$key]);
						unset($upload_file);
					}
				}
				#
				$prod = $mDB->GetItemById('product', $product_id[$key]);
				# Заглушка
				$product_discount[$key] = (isset($product_discount[$key])) ? $product_discount[$key] : '';
				$product_discount_date[$key] = (isset($product_discount_date[$key])) ? $product_discount_date[$key] : '';

				$product_color[$key] = (isset($product_color[$key])) ? $product_color[$key] : '';
				$product_size[$key] = (isset($product_size[$key])) ? $product_size[$key] : '';
				$product_param[$key] = (isset($product_param[$key])) ? $product_param[$key] : '';
				
				$product_description[$key] = stripslashes($product_description[$key]);
				
				# Редактирование товара
				$res = $mProducts->EditProduct($product_id[$key], $product_code[$key], $product_category[$key], $product_title[$key], $product_quantity[$key], $product_price[$key], $product_discount[$key], $product_discount_date[$key], $product_description[$key], $product_unit[$key], $product_color[$key], $product_size[$key], $product_param[$key]);

				if($res)
				{
					# Если изменилась категория товара, переносим его изображения в новую категорию
					if ($product_category[$key] != $prod['category'])
						$mPictures->ChengeCategory($prod['category'], $product_category[$key], $prod['code']);
					# Проверяем, происходит ли изменение кода товара, если да то переименовываеи изображения
					if ($product_code[$key] != $prod['code'])
						$mPictures->RenamePictures($prod['code'], $product_code[$key], $product_category[$key]);
				}
				# Изменение id изображения
				if (isset($_POST['pic_rename']))
				{
					foreach ($product_pic_rename[0] as $i => $new_pic_id)
					{
						foreach ($product_pic_name[0] as $j => $old_pic_id)
						{
							if ($i == $j && $new_pic_id != $old_pic_id)
							{
								if(preg_match('|([0-9]+$)|', $new_pic_id))
								{
									$res = $mPictures->RenamePicture($product_code[$key], $new_pic_id, $old_pic_id, $product_category[$key]);
									if (!$res)
										$_SESSION['error_message'] = 'Файл с таким именем существует.';
								}
								else
									$_SESSION['error_message'] = 'Неверный формат идентификатора изображения';
							}
						}
					}
				}
			}
		}

		# Добавление новых товаров
		if (!empty($_POST['new_product_code']))
		{
			$new_product_code = $_POST['new_product_code'];
			$new_product_category = $_POST['new_product_category'];
			$new_product_title = $_POST['new_product_title'];
			$new_product_quantity = $_POST['new_product_quantity'];
			$new_product_price = $_POST['new_product_price'];
			$new_product_description = $_POST['new_product_description'];
			$new_product_unit = $_POST['new_product_unit'];
			$new_product_color = $_POST['new_product_color'];
			$new_product_size = $_POST['new_product_size'];
			$new_product_param = $_POST['new_product_param'];

			foreach ($new_product_code as $key => $product)
			{
				$new_product_description[$key] = stripslashes($new_product_description[$key]);
				$mProducts->CreateProduct($new_product_code[$key], $new_product_category[$key], $new_product_title[$key], $new_product_quantity[$key], $new_product_price[$key], $new_product_description[$key], $new_product_unit[$key], $new_product_color[$key], $new_product_size[$key], $new_product_param[$key]);
			}
		}
	}

	# Отмена редактирования заказа
	if (isset($_POST['product_cancel']))
	{
		header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/products.php?category='.$_POST['product_category'][0]);
		die;
	}
	
	# Удаление товара со странички редактирования товара
	if (isset($_POST['product_delete']))
	{
		$product = $mDB->GetItemById('product', $_POST['product_id'][0]);
		$mDB->DeleteItemById('product', $_POST['product_id'][0]);
		if ($config['download']['enabled'] == true)
		{
			$file = dirname(__FILE__) . '/../' . $config['download']['dir'] . '/' . $product['code'] . '.' . $config['download']['type'];
			if (is_file($file))
			unlink($file);
		}
		
		$res = $mPictures->DelProdPics($product['code'], $product['category']);		
		if(!$res)
				$_SESSION['error_message'] = 'Проверьте папку с изображениями, возможно не все файлы были удалены.';
		header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/products.php?category='. $_POST['product_category'][0]);
		die;
	}
	
	if (isset($_GET['product']))
	{
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		die;
	}

	# Редирект на список
	header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/products.php?page=' . $navi['page'] . '&category=' . $current_category['code']);
	die;
}

else
{
	# Обработка GET запроса
	if ($_SERVER['REQUEST_METHOD'] == 'GET')
	{
		# Удаление товара
		if (isset($_GET['delete_product_id']))
		{
			$product = $mDB->GetItemById('product', $_GET['delete_product_id']);
			$mDB->DeleteItemById('product', $_GET['delete_product_id']);
			if ($config['download']['enabled'] == true)
			{
				$file = dirname(__FILE__) . '/../' . $config['download']['dir'] . '/' . $product['code'] . '.' . $config['download']['type'];
				if (is_file($file))
					unlink($file);
			}
			$res = $mPictures->DelProdPics($product['code'], $product['category']);
			if(!$res)
				$_SESSION['error_message'] = 'Проверьте папку с изображениями, возможно не все файлы были удалены.';
			header('Location: ' . $_SERVER['HTTP_REFERER']);
			die;
		}

		# Удаление файла товара
		if (isset($_GET['delete_file']))
		{
			$file = dirname(__FILE__) . '/../' . $config['download']['dir'] . '/' . $_GET['delete_file'];
			if (is_file($file))
				unlink($file);
			header('Location: ' . $_SERVER['HTTP_REFERER']);
			die;
		}

		# Удаление изображения товара
		if (isset($_GET['delete_picture']))
		{
			$file = dirname(__FILE__) . '/../' . $config['images']['dir'] . '/' . $_GET['del_cat'] . '/' . $_GET['delete_picture'];
			$thumb = dirname(__FILE__) . '/../' . $config['images']['dir'] . '/' . $_GET['del_cat'] . '/thumb/' . $_GET['delete_picture'];
			if (is_file($file))
				unlink($file);
			if (is_file($thumb))
				unlink($thumb);
			header('Location: ' . $_SERVER['HTTP_REFERER']);
			die;
		}

		# Удаление категории и всех связанных элементов
		if (isset($_GET['delete_category_id']))
		{
			//new_vesion
			$del[0] = $mDB->GetItemById('category', $_GET['delete_category_id']);
			$res[0] = $mDB->GetItemsByParam('category', 'parent', $del[0]['code']);
			$del = (!empty($res[0]))? array_merge($del, $res[0]): $del;
			$count[0] = count($res[0]);
			$i=0;
			while ($count[$i] > 0)
			{
				foreach ($res[$i] as $j => $sub)
				{
					$res_tmp[$j] = $mDB->GetItemsByParam('category', 'parent', $sub['code']);
					if (!empty($res_tmp[$j]))
					{
						$res[$i + 1] = array();
						$res[$i + 1] = array_merge($res[$i + 1], $res_tmp[$j]);
						$del = array_merge($del, $res_tmp[$j]);
					}
				}
				$i++;
				if (!empty($res[$i]))
					$count[$i] = count($res[$i]);
				else
					$count[$i] = 0;
			}

			for($i = count($del)-1; $i>=0; $i--)
			{
				$cat_fold = dirname(__FILE__) . '/../' . $config['images']['dir'] . '/' . $del[$i]['code'];
				$cat_thumb = dirname(__FILE__) . '/../' . $config['images']['dir'] .'/' . $del[$i]['code'] . '/thumb';
				if(is_dir($cat_fold))
				{
					if(is_dir($cat_thumb))
					{
						$mFiles->CleanDir($cat_thumb);
						rmdir($cat_thumb);
					}
				$mFiles->CleanDir($cat_fold);
				rmdir($cat_fold);
				}
				if($config['download']['enabled'] == true)
				{
					$prods = $mDB->GetItemsByParam('product', 'category', $del[$i]['code']);
					foreach($prods as $prod)
					{
						$file = dirname(__FILE__) . '/../' . $config['download']['dir'] . '/' . $prod['code'] . '.' . $config['download']['type'];
							if (is_file($file))
								unlink($file);
					}
				}
				$mDB->DeleteItemsByParam('product', 'category', $del[$i]['code']);
				$mDB->DeleteItemById('category', $del[$i]['id_category']);
			}
			
			header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/products.php?page=' . $navi['page'] . '&category=' . $current_category['code']);
			die;
		}
	}

	# Вывод всех категорий
	$all_categories = $mDB->GetAllItems('category');

	# Вывод продукта
	if (isset($_GET['product']))
	{
		# Выбор продукта
		$product = $mDB->GetItemById('product', $_GET['product']);

		# Массив картинок товара
		$prod_pics = $mPictures->GetProdPics($product['code'], $product['category']);

		# Подключение редактора
		include_once dirname(__FILE__) . '/bueditor/bueditor.php';

		include_once dirname(__FILE__) . '/../design/AdminProductsEdit.tpl.php';
	}
	else
	{
		if (isset($_GET['category'])) # Вывод категории или списка категорий
		{
			$categories = $mCategories->GetSubcategories($_GET['category']);
			$navi = $mProducts->Paginate($_GET['category'], $navi['page'], $config['admin']['productsList']);
			$products = $mProducts->GetPaginatedList($_GET['category'], $navi['start'], $config['admin']['productsList']);
		}
		else
		{
			$categories = $mCategories->GetMainCategories();
			$navi = $mProducts->Paginate('', $navi['page'], $config['admin']['productsList']);
			$products = $mProducts->GetPaginatedList('', $navi['start'], $config['admin']['productsList']);
		}

		ob_start();
		include_once dirname(__FILE__) . '/../design/AdminPagination.tpl.php';
		$pagination = ob_get_clean();

		include_once dirname(__FILE__) . '/../design/AdminProductsList.tpl.php';
	}
}