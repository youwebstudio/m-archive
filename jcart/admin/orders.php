<?php

# jCart v1.3.5
# http://conceptlogic.com/jcart/
# http://jcart.info

# Подключение настроек
include_once dirname(__FILE__) . '/../jcart.inc.php';
include_once (dirname(__FILE__) . '/jcart_admin_init.inc.php');
$config = $jcart->config;

# Простейшая авторизация
include_once dirname(__FILE__) . '/../modules/M_Admin.inc.php';
$mAdmin = M_Admin::Instance();

if (!$mAdmin->CheckLogin())
	die;

# Кодировка.
header('Content-type: text/html; charset=' . $config['encoding']);

# Обработка имени файла
$file = str_replace(dirname(__FILE__) . '/', '', (__FILE__));
$file = str_replace('.php', '', $file);

# Подключение меню
include_once dirname(__FILE__) . '/_menu.php';

if ($config['database']['enabled'] == false)
{
	include_once dirname(__FILE__) . '/../design/AdminHeader.tpl.php';
	die('<div class="container-fluid"><div class="well well-inverse message-block"><div class="alert alert-error">Использование базы данных отключено в настройках. Админка в данном случае бесполезна и будет выдавать ошибки.</div></div></div>');
}

# Подключение необходимых модулей
include_once dirname(__FILE__) . '/../modules/M_Orders.inc.php';
$mOrders = M_Orders::Instance();

# Настройка пагинации списка заказов
if(isset($_POST['ordersList']))
{
	$_SESSION['ordersList'] = $_POST['ordersList'];
	header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/orders.php');
	die;
}
if(isset($_SESSION['ordersList']))
	$ordersList = $_SESSION['ordersList'];
else
	$ordersList = $config['admin']['ordersList'];

# Определение текущей страницы
if (isset($_GET['page']))
    $navi['page'] = $_GET['page'];
else
    $navi['page'] = 1;

# Определение текущего заказа
if (isset($_GET['order']))
    $id_order = $_GET['order'];

# Определение текущего пользователя
if (isset($_GET['user']))
    $id_user = $_GET['user'];

# Определение статуса заказов для фильтра
$status = (isset($_GET['status'])) ? intval($_GET['status']) : 'all';

# Обработка POST запроса
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	# Поиск
	if(isset($_POST['search_submit']))
	{
		$pre_search = (isset($_POST['search']))?$_POST['search']:array();
		$enabled = array_unique(array_keys($pre_search));
		$search_type = array();
		foreach($enabled as $field)
		{
			if(isset($pre_search[$field]) && (is_array($pre_search[$field]) || !preg_match('|^(\s*$)|', $pre_search[$field])))
			{
				if($field == 'product' )
				{
					foreach($pre_search['product'] as $key => $val)
					{
						if (!empty($val))
						{
							$prod_params[] = '`' . $key . '` =';
							$prod_values[] = '\'' . $val . '\'';
							$prod_search_type[] = 'AND';
						}
					}
					if(isset($prod_params) && !empty($prod_params))
					{
						$tmp_params = $mDB->SearchItemsByParamArray('custom_item', $prod_params, $prod_values, $prod_search_type);

						$i = 0;
						foreach($tmp_params as $key => $val)
						{
							$pre = ($i == 0)? ' ( ': '';
							if($i < count($tmp_params)-1)
							{
								$search_type[] = 'OR';
								$app = '';
							}
							else
							{
								$search_type[] = 'AND';
								$app = ' ) ';
							}
							$params[] = $pre . '`id_custom` =';
							$values[] = '\'' . $val['id_custom'] . '\'' . $app;

							$i++;
						}

						if(count($tmp_params) ==0)
						{
							$params[] = '`id_custom` =';
							$values[] = '\'false_product_dummy_string_for_search_result_prevention\'';
							$search_type[] = 'AND';
						}
					}
					else
					{
						unset($pre_search['product']);
					}
				}
				elseif($field == 'date_from')
				{
					$params[] = '`date` >= ';
					$date_from = date("Y-m-d H:i:s", strtotime($pre_search['date_from']));
					$values[] = '\'' . $date_from . '\'';
					$search_type[] = 'AND';
				}
				elseif($field == 'date_to')
				{
					$params[] = '`date` <=';
					$date_to =  date("Y-m-d H:i:s", strtotime($pre_search['date_to']) + 24*3600);
					$values[] = '\'' . $date_to . '\'';
					$search_type[] = 'AND';
				}
				elseif(preg_match('|(.*)_from|', $field, $tmp))
				{
					$tmp = $tmp[1];
					if(isset($pre_search[$tmp . '_from']) && preg_match('|[0-9]+|', $pre_search[$tmp . '_from']))
					{
						$params[] = '`' . $tmp . '` >= ';
						$values[] = number_format($pre_search[$tmp . '_from'], $config['priceFormat']['decimals'], $config['priceFormat']['dec_point'], $config['priceFormat']['thousands_sep']);
					}
					$search_type[] = 'AND';
					unset($tmp);
				}
				elseif(preg_match('|(.*)_to|', $field, $tmp))
				{
					$tmp = $tmp[1];
					if(isset($pre_search[$tmp . '_to']) && preg_match('|[0-9]+|', $pre_search[$tmp . '_to']))
					{
						$params[] = '`' . $tmp . '` <= ';
						$values[] = number_format($pre_search[$tmp . '_to'] ,$config['priceFormat']['decimals'], $config['priceFormat']['dec_point'], $config['priceFormat']['thousands_sep']);
					}
					$search_type[] = 'AND';
					unset($tmp);
				}
				elseif($field == 'status' || $field == 'payment' || $field == 'delivery')
				{
					$i = 0;
					foreach($pre_search[$field] as $num => $stat)
					{
						$pre = ($i == 0)? ' ( ': '';
						if(count($pre_search[$field])>1 && $i < count($pre_search[$field])-1)
						{
							$search_type[] = 'OR';
							$app = '';
						}
						else
						{
							$search_type[] = 'AND';
							$app = ' ) ';
						}
						$params[] = $pre . '`' . $field . '` =';
						$values[] = '\'' . $stat . '\'' . $app;

						$i++;
					}
				}
				elseif($field != 'phone' && $field != 'zip' && $field != 'id_custom')
				{
					$params[] = '`' . $field . '` LIKE';
					$values[] = '\'%%' . $pre_search[$field] . '%%\'';
					$search_type[] = 'AND';
				}
				else
				{
					$params[] = '`' . $field . '` =';
					$values[] = '\'' . $pre_search[$field] . '\'';
					$search_type[] = 'AND';
				}
			}
		}
		//array_walk_recursive($search, create_function('$val, $key, $obj', 'array_push($obj, $val);'), &$output);
		if(isset($values) && count($values)>0)
		{
			$orders = $mDB->SearchItemsByParamArray('custom', $params, $values, $search_type);//die;
			if(isset($orders) && count($orders) > 0)
			{
				$pre_search['total_sum'] = 0;
				foreach ($orders as $i => $order)
				{
					$pre_search['total_sum'] += $order['sum'];

					foreach($config['statuses'] as $j => $status)
					{
						if(!isset($pre_search['status_sum'][$j]))
							$pre_search['status_sum'][$j] = 0;
						if(!isset($pre_search['status_num'][$j]))
							$pre_search['status_num'][$j] = 0;
						if ($order['status'] == $j)
						{
							$pre_search['status_sum'][$j] += $order['sum'];
							$pre_search['status_sum'][$j] = number_format($pre_search['status_sum'][$j],$config['priceFormat']['decimals'], $config['priceFormat']['dec_point'], $config['priceFormat']['thousands_sep']);

							$pre_search['status_num'][$j]++;
						}
					}
				}
				$pre_search['total_sum'] = number_format($pre_search['total_sum'],$config['priceFormat']['decimals'], $config['priceFormat']['dec_point'], $config['priceFormat']['thousands_sep']);
				$_SESSION['success_message'] = 'Найдено результатов: ' . count($orders) . ' на сумму ' . $pre_search['total_sum'] . ' ' . $jcart->currencySymbol($config['currencyCode']);
			}
		elseif(!isset($orders) || count($orders) == 0)
				$_SESSION['search_massage'] = 'Ничего не найдено';
		}
		else
			$_SESSION['search_massage'] = 'Введите данные для поиска';

		$_SESSION['search']['res'] = (isset($orders))? $orders : null;
		$_SESSION['search']['form'] = $pre_search;

		header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/orders.php');
		die;
	}

	# Удаление заказа
	if (isset($_POST['order_delete']))
	{
		$mDB->DeleteItemById('custom', $_POST['order_id']);
        $mDB->DeleteItemsByParam('custom_item', 'id_custom',  $_POST['order_id']);
		header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/orders.php');
		die;
	}

	# Смена статуса заказов
	if (isset($_POST['change_status']))
	{
		foreach ($_POST['id'] as $id_custom)
			$mDB->ChangeStatusById('custom', $id_custom, $_POST['status']);

		header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/orders.php');
		die;
	}

	# Отмена редактирования заказа
	if (isset($_POST['order_cancel']))
	{
		header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/orders.php');
		die;
	}

	# Редактирование заказа
	if (isset($_POST['order_edit']))
    {
        # Редактирование позиций заказа
		$orderItemID = (isset($_POST['orderItemID'])) ? $_POST['orderItemID'] : '';
		$orderItemProduct = (isset($_POST['orderItemProduct'])) ? $_POST['orderItemProduct'] : '';
		$orderItemName = (isset($_POST['orderItemName'])) ? $_POST['orderItemName'] : '';
		$orderItemQty = (isset($_POST['orderItemQty'])) ? $_POST['orderItemQty'] : '';
		$orderItemPrice = (isset($_POST['orderItemPrice'])) ? $_POST['orderItemPrice'] : '';
		$orderItemDiscount = (isset($_POST['orderItemDiscount'])) ? $_POST['orderItemDiscount'] : '';
		$orderItemUnit = (isset($_POST['orderItemUnit'])) ? $_POST['orderItemUnit'] : '';
		$orderItemColor = (isset($_POST['orderItemColor'])) ? $_POST['orderItemColor'] : '';
		$orderItemSize = (isset($_POST['orderItemSize'])) ? $_POST['orderItemSize'] : '';
		$orderItemParam = (isset($_POST['orderItemParam'])) ? $_POST['orderItemParam'] : '';

		$order_sum = 0;
		if (!empty($orderItemID))
		{
			foreach ($orderItemID as $key => $order)
			{
				$mOrders->EditOrderItem($orderItemID[$key], $id_order, $orderItemProduct[$key], $orderItemName[$key], $orderItemQty[$key], $orderItemPrice[$key], $orderItemDiscount[$key], $orderItemUnit[$key], $orderItemColor[$key], $orderItemSize[$key], $orderItemParam[$key]);

				$order_sum += $orderItemQty[$key] * $orderItemPrice[$key] * (1 - $orderItemDiscount[$key] / 100);
			}
		}

		if (!empty($_POST['new_orderItemProduct']))
		{
			# Добавление позиций заказа
			$new_orderItemProduct = $_POST['new_orderItemProduct'];
			$new_orderItemName = $_POST['new_orderItemName'];
			$new_orderItemQty = $_POST['new_orderItemQty'];
			$new_orderItemPrice = $_POST['new_orderItemPrice'];
			$new_orderItemDiscount = $_POST['new_orderItemDiscount'];
			$new_orderItemUnit = $_POST['new_orderItemUnit'];
			$new_orderItemColor = $_POST['new_orderItemColor'];
			$new_orderItemSize = $_POST['new_orderItemSize'];
			$new_orderItemParam = $_POST['new_orderItemParam'];

			if (!empty($new_orderItemProduct))
			{
				foreach ($new_orderItemProduct as $key => $orderItem)
				{
					$mOrders->CreateOrderItem($id_order, $new_orderItemProduct[$key], $new_orderItemName[$key], $new_orderItemQty[$key], $new_orderItemPrice[$key], $new_orderItemDiscount[$key], $new_orderItemUnit[$key], $new_orderItemColor[$key], $new_orderItemSize[$key], $new_orderItemParam[$key]);

					$order_sum += $new_orderItemQty[$key] * $new_orderItemPrice[$key] * (1 - $new_orderItemDiscount[$key] / 100);
				}
			}
		}

		# Добавление стоимости доставки
		$order_sum += $_POST['order_delivery_cost'];

		# Приведение суммы к нормальному виду
		$order_sum = number_format($order_sum, 2, '.', '');

		# Разбор данных юридического лица
		$juridical = $_POST['juridical'];

		if ($juridical[0] == '' && $juridical[1] == '' && $juridical[2] == '')
			$juridical = '';
		else
			$juridical[2] = stripslashes($juridical[2]);

		if (preg_match('|^(\s*$)|', $_POST['order_name']) && $config['form']['order']['name']['enabled'] == true && $config['form']['order']['name']['required'] == true)
		{
			die('<p>Данные не были сохранены. Проверьте корректность заполнения полей.</p><p><strong style="color: red;">Поле имя не должно быть пустым.</strong></p>');
		}
		if (preg_match('|^(\s*$)|', $_POST['order_lastname']) && $config['form']['order']['lastname']['enabled'] == true && $config['form']['order']['lastname']['required'] == true)
		{
			die('<p>Данные не были сохранены. Проверьте корректность заполнения полей.</p><p><strong style="color: red;">Поле фамилия не должно быть пустым.</strong></p>');
		}
		if (preg_match('|^(\s*$)|', $_POST['order_fathername']) && $config['form']['order']['fathername']['enabled'] == true && $config['form']['order']['fathername']['required'] == true)
		{
			die('<p>Данные не были сохранены. Проверьте корректность заполнения полей.</p><p><strong style="color: red;">Поле отчество не должно быть пустым.</strong></p>');
		}
		if (preg_match('|^(\s*$)|', $_POST['order_email']) && $config['form']['order']['email']['enabled'] == true && $config['form']['order']['email']['required'] == true)
		{
			die('<p>Данные не были сохранены. Проверьте корректность заполнения полей.</p><p><strong style="color: red;">Поле email не должно быть пустым.</strong></p>');
		}

		if(preg_match('|^(\s*$)|', $_POST['order_date']))
		{
			die('<p>Данные не были сохранены. Проверьте корректность заполнения полей.</p><p><strong style="color: red;">Поле дата не должно быть пустым.</strong></p>');
		}

		if(preg_match('|^(\s*$)|', $_POST['order_payment']))
		{
			die('<p>Данные не были сохранены. Проверьте корректность заполнения полей.</p><p><strong style="color: red;">Поле Форма оплаты не должно быть пустым.</strong></p>');
		}

		# Редактирование данных заказа
		$_POST['order_comment'] = stripslashes($_POST['order_comment']);
		if (!$mOrders->EditOrder($_POST['order_id'], $_POST['order_lastname'], $_POST['order_name'], $_POST['order_fathername'], $_POST['order_email'], $_POST['order_phone'], $_POST['order_zip'], $_POST['order_region'], $_POST['order_city'], $_POST['order_address'], $_POST['order_comment'], $_POST['order_payment'], $juridical, $_POST['order_delivery'], $_POST['order_delivery_cost'], $_POST['order_date'], $order_sum, $_POST['order_status']))
			die('<p>Данные не были сохранены. Проверьте корректность заполнения полей.</p>');

		# Отправка уведомления об изменении статуса покупателю
		if (isset($_POST['send_notice']))
		{
			# Выбор заказа и его элементов
			$order = $mDB->GetItemById('custom', $id_order);
			$order_items = $mDB->GetItemsByParam('custom_item', 'id_custom', $id_order);

			# Подключение модуля работы с письмами
			include_once dirname(__FILE__) . '/../modules/M_Email.inc.php';
			$mEmail = M_Email::Instance();

			# Определение данных формы оплаты
			$payment = $config['payments'][$order['payment']];
			$payment['type'] = $order['payment'];

			# Определение данных способа доставки
			$delivery = $config['deliveries'][$order['delivery']];
			$delivery['cost'] = $order['delivery_cost'];

			# Определение статуса заказа
			$status = $config['statuses'][$order['status']];

			# Опредение символов валюты заказа
			$currencySymbol = $jcart->currencySymbol($config['currencyCode']);

			# Маркер, сигнализирующий создание нового заказа
			$new_order = true;

			# Мелочи
			$address = $order['address'];
			$name = $order['name'];
			$lastname = $order['lastname'];
			$fathername = $order['fathername'];

			# Подключение модуля оплаты (создание ссылок для оплаты и скачивания)
			if (is_file(dirname(__FILE__) . '/../modules/C_Payment.php'))
				include_once dirname(__FILE__) . '/../modules/C_Payment.php';

			# Подключение модуля работы с файлами (создание ссылки на скачивание)
			if ($config['download']['enabled'] == true && $order['status'] == 1 && is_file(dirname(__FILE__) . '/../modules/C_Files.inc.php'))
				include_once dirname(__FILE__) . '/../modules/C_Files.inc.php';

			# Подготовка текста письма
			$content = $mEmail->PrepareChangeStatus($order, $payment, $delivery, $status, $order_items, $currencySymbol, $config['priceFormat']);

			$invoices = (isset($invoices) && is_array($invoices) && $order['status'] != 1) ? $invoices : '';

			# Отправка письма владельцу и покупателю
			$mEmail->SendEmail($order['email'], $config['email']['answer'], $config['email']['subjectStatus'], $content, $config['email']['answerName'], $invoices, $config['encoding']);
			$mEmail->SendEmail($config['email']['receiver'], $config['email']['answer'], $config['email']['subjectStatus'], $content, $config['email']['answerName'], $invoices, $config['encoding']);
		}

		header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/orders.php');
		die;
	}
}
else
{
	# Обработка GET запроса
	if ($_SERVER['REQUEST_METHOD'] == 'GET')
	{
		# Удаление товара
		if (isset($_GET['delete_order_item']))
		{
			$mDB->DeleteItemById('custom_item', $_GET['delete_order_item']);
			$items = $mDB->GetItemsByParam('custom_item', 'id_custom', $_GET['from_order']);
			$sum = 0;
			foreach($items as $item)
			{
				$sum += $item['price']*$item['quantity'] * (1 - $item['discount'] / 100);
			}
			$params = array('sum'=>$sum);
			$mDB->EditItemById('custom', $params, $_GET['from_order']);

			header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/orders.php?&order=' . $_GET['from_order']);
			die;
		}

		# Удаление заказа
		if (isset($_GET['delete_order']))
		{
			$mDB->DeleteItemById('custom', $_GET['delete_order']);
			$mDB->DeleteItemsByParam('custom_item', 'id_custom',  $_GET['delete_order']);
			header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/orders.php');
			die;
		}
    }

    # Вывод текущего заказа
	if (isset($id_order))
	{
		# Выбор заказа и его элементов
		$order = $mDB->GetItemById('custom', $id_order);
		$order_items = $mDB->GetItemsByParam('custom_item', 'id_custom', $id_order);

		# Определение статуса заказа
		$statuses = $config['statuses'];
		$orders['status'] = $statuses[$order['status']];

		# Опредение валюты заказа
		$currencySymbol = $jcart->currencySymbol($config['currencyCode']);

		# Разбор данных юридического лица.
		$juridical = explode('|', $order['juridical']);

		# Подключение редактора
		include_once dirname(__FILE__) . '/bueditor/bueditor.php';

		include_once dirname(__FILE__) . '/../design/AdminOrderEdit.tpl.php';
	}
	# Вывод списка заказов текущего пользователя
	elseif (isset($id_user))
	{
		# Выбор пользователя и его заказов из БД
		$user = $mDB->GetItemById('user', $id_user);
		$orders = $mDB->GetItemsByParam('custom', 'id_user', $id_user);

		# Подсчёт полной суммы
		$user['sum'] = $user['whole_sum'] = 0;
		$statuses = $config['statuses'];

		# Подсчёт суммы заказов
		foreach ($orders as $key => $order)
		{
			$user['whole_sum'] += $order['sum'];

			if ($order['status'] == 1)
				$user['sum'] += $order['sum'];

			$orders[$key]['status'] = $statuses[$order['status']];
		}

		# Определение валюты заказа
		$currencySymbol = $jcart->currencySymbol($config['currencyCode']);

		include_once dirname(__FILE__) . '/../design/AdminOrdersList.tpl.php';
	}
	elseif(isset($_SESSION['search']['res']))
	{
		$orders = $_SESSION['search']['res'];
		$search = $_SESSION['search']['form'];
		$search['payment'] = (isset($search['payment']))?array_flip($search['payment']):null;
		$search['delivery'] = (isset($search['delivery']))?array_flip($search['delivery']):null;
		$search['status'] = (isset($search['status']))?array_flip($search['status']):null;
		unset($_SESSION['search']);
		# Определение статуса заказов

		foreach ($orders as $i => $order)
			$orders[$i]['status'] = $config['statuses'][$order['status']];

		# Опредение валюты заказа
		$currencySymbol = $jcart->currencySymbol($config['currencyCode']);

		$current_category['code'] = '';

		$statuses = $config['statuses'];

		include_once dirname(__FILE__) . '/../design/AdminOrdersList.tpl.php';
	}
    # Вывод списка заказов
	else
	{
		$search = (isset($_SESSION['search']['form']))?$_SESSION['search']['form']:null;
        # Выбор всех заказов с учётом пагинации
		if($ordersList == false)
		{
			$orders = $mDB->GetAllItems('custom');
		}
		elseif ($status === 'all')
		{
			$navi = $mDB->Paginate('custom', $navi['page'], $ordersList);
			$orders = $mDB->GetPaginatedList('custom', $navi['start'], $ordersList);
		}
		# Или заказов определённого статуса
		else
		{
			$navi = $mDB->PaginateWithParam('custom', $navi['page'], $ordersList, 'status', $status);
			$orders = $mDB->GetPaginatedListWithParam('custom', $navi['start'], $ordersList, 'status', $status);
		}

		# Определение статуса заказов
		foreach ($orders as $i => $order)
			$orders[$i]['status'] = $config['statuses'][$order['status']];

		# Опредение валюты заказа
		$currencySymbol = $jcart->currencySymbol($config['currencyCode']);
		
		$current_category['code'] = '';

		if($ordersList != false)
		{
			ob_start();
			include_once dirname(__FILE__) . '/../design/AdminPagination.tpl.php';
			$pagination = ob_get_clean();
		}

		$statuses = $config['statuses'];

		include_once dirname(__FILE__) . '/../design/AdminOrdersList.tpl.php';
	}
}