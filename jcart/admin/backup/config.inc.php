<?php 

# jCart v1.3.5
# http://conceptlogic.com/jcart/
# http://jcart.info

////////////////////////////////////////////////////////////////////////////////
#ВАЖНЫЕ НАСТРОЙКИ

# Вывод и логирование ошибок
$config['display_errors']							= false; #
$config['log_errors']								= true; #

# Секретное слово для генерации антиспама
$config['secretWord']								= '1386955333'; #

$config['sitename']									= 'Архив Проект'; # Название магазина
$config['sitelink']									= 'http://m-arhiv.ru/'; # URL магазина
$config['smallCart']								= false; # Краткая версия корзины на страницах кроме оформления заказа?

$config['email']['receiver']						= 'zakaz@m-arhiv.ru'; # E-mail адрес, на который отправляется заказ
$config['email']['answer']							= 'zakaz@m-arhiv.ru'; # E-mail адрес, от имени которого отправляется ответное письмо покупателю
$config['email']['answerName']						= 'Архив Проект';	# Имя робота
$config['email']['answerMessage']					= 'Спасибо за покупку на сайте <!--suppress HtmlUnknownTarget -->
<a href="' . $config['sitelink'] . '">' . $config['sitename'] . '</a><br><br>'; # Ответное письмо покупателю
$config['email']['subjectOrder']					= 'Получен заказ на сайте «' . $config['sitename'] . '»'; # Тема письма о заказе
$config['email']['subjectFeedback']					= 'Письмо с сайта «' . $config['sitename'] . '»'; # Тема письма обратной связи
$config['email']['subjectStatus']					= 'Изменение статуса заказа на сайте «' . $config['sitename'] . '»'; # Тема письма об изменении статуса
$config['email']['subjectDownload']					= 'Получите ссылку для скачивания товара'; # Тема письма о ссылке на скачивание
$config['email']['subjectPassword']					= 'Ваши данные доступа'; # Тема письма о пароле
$config['email']['subjectQuestion']					= 'Задан вопрос на сайте ' . $config['sitename']; # Тема письма о вопросе
$config['email']['subjectAnswer']					= 'Получен ответ на сайте ' . $config['sitename']; # Тема письма об ответе
$config['email']['signature']						= '<p>--------------<br>Ещё раз спасибо за покупку. Наш телефон: (495) 212-17-56</p>'; # Подпись в письме

# Страница, уведомляющая об успешном оформлении заказ. Указывать от корня сайта.
# Если параметр оставить пустым, то после оплаты покупатель будет просто попадать на главную страницу магазина.
$config['returnUrl']								= ''; #
$config['successUrl']								= 'success.php'; #
$config['failUrl']									= 'fail.php'; #

# Настройки БД
$config['database']['enabled']						= true; # Использовать БД?
$config['database']['host']							= 'p29940.mysql.ihc.ru'; # Хост
$config['database']['user']							= 'p29940_korzina'; # Пользователь
$config['database']['pass']							= '04128413'; # Пароль
$config['database']['name']							= 'p29940_korzina'; # Название базы
$config['database']['quantityEnabled']				= false; # Изменять количество при заказе?

////////////////////////////////////////////////////////////////////////////////
// НАСТРОЙКА МЕТОДОВ ОПЛАТЫ

# Настройка отправки на почту
$config['payments']['email']['enabled']				= false; # Использовать отправку на почту?
$config['payments']['email']['title']				= 'Наложенный платеж'; # Название оплаты курьеру в выборе формы оплаты
$config['payments']['email']['info']				= 'Наложенный платеж - оплата при получении.'; # Описание
$config['payments']['email']['details']				= ''; # Детали (будут высланы на email)

# Отправка курьеру (та же отправка на почту, только по другому называется)
$config['payments']['courier']['enabled']			= true; # Использовать отправку на почту?
$config['payments']['courier']['title']				= 'Наличные курьеру'; # Название оплаты курьеру в выборе формы оплаты
$config['payments']['courier']['info']				= 'Передать наличные курьеру после получения товара.'; # Описание
$config['payments']['courier']['details']			= ''; # Детали (будут высланы на email)

# Настройки RoboKassa
$config['payments']['robokassa']['enabled']			= false; # Использовать RoboKassa?
$config['payments']['robokassa']['title']			= 'Оплата на сайте с помощью RoboKassa'; # Название онлайн платежей в выборе формы оплаты
$config['payments']['robokassa']['info']			= 'Ссылку для оплаты Вы получите в письме.'; # Описание
$config['payments']['robokassa']['details']			= ''; # Детали (будут высланы на email)

$config['payments']['robokassa']['login']			= 'login'; # Логин в Робокассе
$config['payments']['robokassa']['pass1']			= 'pass1'; # Пароль 1 в Робокассе
$config['payments']['robokassa']['pass2']			= 'pass2'; # Пароль 2 в Робокассе
$config['payments']['robokassa']['description']		= 'Оплата покупки в магазине «Архив Проект»';   # Описание оплаты
$config['payments']['robokassa']['test']			= false; # Тестовый режим?

# Настройка печати счёта
$config['payments']['invoice']['enabled']			= true; # Использовать платежи по безналу?
$config['payments']['invoice']['title']				= 'Оплата по безналу'; # Название онлайн платежей в выборе формы оплаты
$config['payments']['invoice']['info']				= ''; #Безналичный расчёт. Счёт-фактура прилагается к Вашему заказу.  Описание
$config['payments']['invoice']['details']			= ''; # Детали (будут высланы на email)

# Настройка LiqPay
$config['payments']['liqpay']['enabled']			= false;  # Использовать платежи по безналу?
$config['payments']['liqpay']['title']				= 'Оплата картой с помощью LiqPay'; # Название платежей банковской картой в выборе формы оплаты
$config['payments']['liqpay']['info']				= 'Автоматическая оплата банковской картой.'; # Описание
$config['payments']['liqpay']['details']			= ''; # Детали (будут высланы на email)

$config['payments']['liqpay']['description']		= 'Оплата покупки в магазине «Архив Проект»'; # Описание оплаты
$config['payments']['liqpay']['id']					= 'i00000000000'; # ID мерчанта в LiqPay
$config['payments']['liqpay']['sign']				= '2899d7f83e79b1eb38a9caa36e917be368ff1905'; # Подпись

# Настройки InterKassa
$config['payments']['interkassa']['enabled']		= false; # Использовать InterKassa?
$config['payments']['interkassa']['title']			= 'Оплата на сайте с помощью InterKassa'; # Название онлайн платежей в выборе формы оплаты
$config['payments']['interkassa']['info']			= 'Кнопку для оплаты Вы получите в письме.'; # Описание
$config['payments']['interkassa']['details']		= ''; # Детали (будут высланы на email)

$config['payments']['interkassa']['shop_id']		= '172C3C3D-0DBE-F781-F11E-9BCC4584C2E8';   # Идентификатор магазина
$config['payments']['interkassa']['secret_key']		= 'rt8g6MjSRBUFEBn7';   # Cекретный ключ
$config['payments']['interkassa']['description']	= 'Оплата покупки в магазине «Архив Проект»';   # Описание оплаты

# Настройка A1Pay
$config['payments']['a1pay']['enabled']				= false; # Использовать эту форму оплаты?
$config['payments']['a1pay']['title']				= 'Оплата на сайте с помощью A1Pay'; # Название в выборе формы оплаты
$config['payments']['a1pay']['info']				= 'Кнопку для оплаты Вы получите в письме.'; # Описание
$config['payments']['a1pay']['details']				= ''; # Детали (будут высланы на email)

$config['payments']['a1pay']['secret_word']			= 'asdfasgash'; # Cекретное слово (устанавливается вами)
$config['payments']['a1pay']['secret_key']			= 'rsDU8RH9jxc/Yu+/Gkv/KdzSkMdMKB6CoZWpXCndxso='; # Cекретный ключ (взять при создании кнопки)
$config['payments']['a1pay']['description']			= 'Оплата покупки в магазине «Архив Проект»'; # Описание оплаты


////////////////////////////////////////////////////////////////////////////////
// НАСТРОЙКА МЕТОДОВ ДОСТАВКИ

# Доставка №1
$config['deliveries']['1']['enabled']				= true; #
$config['deliveries']['1']['title']					= 'Срочная доставка (2 дня)'; #
$config['deliveries']['1']['info']					= ''; #
$config['deliveries']['1']['details'] 				= ''; #
$config['deliveries']['1']['cost']					= '1000'; #

# Доставка №2
$config['deliveries']['2']['enabled']				= true; #
$config['deliveries']['2']['title']					= 'Доставка в течении 4 дней'; #
$config['deliveries']['2']['info']					= ''; #
$config['deliveries']['2']['details'] 				= ''; #
$config['deliveries']['2']['cost']					= '700'; #

# Доставка №3
$config['deliveries']['3']['enabled']				= true; #
$config['deliveries']['3']['title']					= 'Самовывоз'; #
$config['deliveries']['3']['info']					= 'Товар Вы можете забрать по адресу: Москва, Нагорный проезд, д.12, к.1'; #
$config['deliveries']['3']['details']				= ''; #
$config['deliveries']['3']['cost']					= ''; #

////////////////////////////////////////////////////////////////////////////////
# НАСТРОЙКА УВЕДОМЛЕНИЙ

# Уведомления
$config['form']['sent']								= '<h3>Заказ успешно отправлен.</h3> <h4>Спасибо.Сейчас вы будете перенаправлены.</h4>'; # Успешная отправка
$config['form']['notSent']							= 'Извините, письмо не было отправлено. Пожалуйста, повторите отправку.'; # Неудачная отправка
$config['form']['isSpam']							= 'Если вы видите данное сообщение, то скорее всего у вас отключен JavaScript. Для успешного оформления покупки просто заново отправьте заказ.';	# СПАМ!
$config['form']['emptyEmail']						= 'Извините, e-mail не введён либо его формат неверен.'; # Нет мыла!
$config['form']['emptyName']						= 'Извините, имя не введено либо его формат неверен.'; # Нет имени!
$config['form']['emptyPhone']						= 'Извините, телефон не введён либо его формат неверен.'; # Нет телефона!
$config['form']['emptyAddress']						= 'Извините, адрес не введён либо его формат неверен.'; # Нет адреса!
$config['form']['emptyTopic']						= 'Извините, вы забыли указать тему письма.'; # Нет темы!
$config['form']['emptyMessage']						= 'Перед отправкой напишите сообшение.'; # Нет сообщения!
$config['form']['feedbackSent']						= 'Сообщение отправлено администратору. Спасибо.Сейчас вы будете перенаправлены на главную страницу.'; # Сообщение отправлено
$config['form']['notLogined']						= 'Связка логин-пароль не найдена. Попробуйте восстановить пароль.'; # Не получилось войти
$config['form']['downloadSent']						= '<h3>Просто проверьте почту!</h3><h4>Письмо со ссылкой должно прийти в течении нескольких минут!</h4>'; # Ссылка отправлена
$config['form']['questionSent']						= '<h3>Ваш вопрос отправлен специалисту.</h3> <h4>Вы получите уведомление на email, когда он ответит на него.</h4>'; # Вопрос отправлен
$config['form']['delayed']							= '<h3>Заказ отправлен администратору.</h3> <h4>Как только он его просмотрит, вы получите счёт для оплаты на почту.</h4>'; # Уведомление об отложенном заказе
$config['form']['noUpdate']							= 'Видимо вы ещё не оплатили ваш заказ. Либо не заказывали данный товар вообще.<br>Если уверены, что это ошибка, обратитесь к администратору <a href="' . $config['sitelink'] . 'contact.html">по email</a>.'; # Обновление не удалось

////////////////////////////////////////////////////////////////////////////////
# НАСТРОЙКА ПОЛЕЙ ФОРМ

# Поля формы заказа

$config['form']['order']['name']['label']			= 'Имя:'; # Название поля
$config['form']['order']['name']['enabled']			= true; # Используется
$config['form']['order']['name']['required']		= true; # Обязательное поле

$config['form']['order']['email']['label']			= 'Email:'; # Название поля
$config['form']['order']['email']['enabled']		= true; # Используется
$config['form']['order']['email']['required']		= true; # Обязательное поле

$config['form']['order']['lastname']['label']		= 'Фамилия:'; # Название поля
$config['form']['order']['lastname']['enabled']		= true; # Используется
$config['form']['order']['lastname']['required']	= true; # Обязательное поле

$config['form']['order']['fathername']['label']		= 'Отчество:'; # Название поля
$config['form']['order']['fathername']['enabled']	= true; # Используется
$config['form']['order']['fathername']['required']	= false; # Обязательное поле

$config['form']['order']['phone']['label']			= 'Телефон:'; # Название поля
$config['form']['order']['phone']['enabled']		= true; # Используется
$config['form']['order']['phone']['required']		= true; # Обязательное поле

$config['form']['order']['zip']['label']			= 'Индекс:'; # Название поля
$config['form']['order']['zip']['enabled']			= true; # Используется
$config['form']['order']['zip']['required']			= false; # Обязательное поле

$config['form']['order']['region']['label']			= 'Регион:'; # Название поля
$config['form']['order']['region']['enabled']		= true; # Используется
$config['form']['order']['region']['required']		= false; # Обязательное поле

$config['form']['order']['city']['label']			= 'Город:'; # Название поля
$config['form']['order']['city']['enabled']			= true; # Используется
$config['form']['order']['city']['required']		= false; # Обязательное поле

$config['form']['order']['address']['label']		= 'Адрес:'; # Название поля
$config['form']['order']['address']['enabled']		= true; # Используется
$config['form']['order']['address']['required']		= true; # Обязательное поле

$config['form']['order']['comment']['label']		= 'Комментарий:'; # Название поля
$config['form']['order']['comment']['enabled']		= true; # Используется
$config['form']['order']['comment']['required']		= false; # Обязательное поле

# Поля формы отправки сообщения

$config['form']['feedback']['name']['label']		= 'Имя:'; # Название поля
$config['form']['feedback']['name']['enabled']		= true; # Используется
$config['form']['feedback']['name']['required']		= true; # Обязательное поле

$config['form']['feedback']['email']['label']		= 'Email:'; # Название поля
$config['form']['feedback']['email']['enabled']		= true; # Используется
$config['form']['feedback']['email']['required']	= true; # Обязательное поле

$config['form']['feedback']['phone']['label']		= 'Телефон:'; # Название поля
$config['form']['feedback']['phone']['enabled']		= true; # Используется
$config['form']['feedback']['phone']['required']	= true; # Обязательное поле

$config['form']['feedback']['subject']['label']		= 'Тема письма:'; # Название поля
$config['form']['feedback']['subject']['enabled']	= true; # Используется
$config['form']['feedback']['subject']['required']	= true; # Обязательное поле

$config['form']['feedback']['comment']['label']		= 'Сообщение:'; # Название поля
$config['form']['feedback']['comment']['enabled']	= true; # Используется
$config['form']['feedback']['comment']['required']	= false; # Обязательное поле

////////////////////////////////////////////////////////////////////////////////
# ВСЕ ОСТАЛЬНЫЕ НАСТРОЙКИ

# Статусы заказа
$config['statuses'] = array ('Получен', 'Оплачен', 'Отправлен', 'Доставлен', 'Возврат', 'Отменён', 'Отложен'); #
$config['user_statuses'] = array ('Нормальный', 'Проблемный'); #

# Настройки админки
$config['admin']['login']							= 'arhiv'; # Логин админки
$config['admin']['password']						= 'be37226c4949ebaf84dc0fa73e58dd20'; # Пароль админки. Хранится в зашифрованом виде. Получить шифр пароля можно здесь: http://pr-cy.ru/md5
$config['admin']['ordersList']						= '20'; # Количество заказов в списке админки
$config['admin']['productsList']					= '20'; # Количество товаров в списке админки
$config['admin']['usersList']						= '20'; # Количество клиентов в списке админки
$config['admin']['newsList']						= '5'; # Количество новостей в списке админки
$config['admin']['consultList']						= '5'; # Количество консультаций в списке админки
$config['admin']['mailList']						= '5'; # Количество рассылок в списке админки

# Линейка накопительных скидок. Сумма => % скидки. Если не используете, просто закомментируйте.
/*$config['discounts']								= array (100 => 1,500 => 2,1000 => 3);*/#

# Настройки доставки цифровых товаров
$config['download']['enabled']						= false; # Загрузка файлов включена?
$config['download']['uses']							= 3; # Количество скачиваний по ссылке
$config['download']['hours']						= 24; # Количество часов действия ссылки
$config['download']['dir']							= 'files'; # Папка с файлами товаров
$config['download']['type']							= 'zip'; # Разрешение файлов товаров

$config['upload']['dir']							= 'images/other'; #

# Настройка загрузки изображений
$config['images']['enabled']						= false; # Загрузка изображений включена?
$config['images']['dir']							= 'images'; # Папка с файлами изображений товаров (внутри папки jcart)

# Настройки ресайза изображений
$config['images']['resize']['enabled']				= false; # Изменение размеров изображения включено (все изменённые файлы загружаются на сервер в формате jpg)
$config['images']['thumb']['width']					= 150; # Максимальная ширина эскиза товара в пикселях
$config['images']['height']							= 600; # Максимальная высота изображения товара в пикселях
$config['images']['width']							= 800; # Максимальная ширина изображения товара в пикселях

$config['images']['cart']['enabled']				= false; # Изображение товара в корзине включено
$config['images']['cart']['width']					= 40; # Максимальная ширина изображения товара в корзине в пикселях

# Модальные (всплывающие) окошки
$config['fancybox']['enabled']						= false; # Разрешение увеличения изображений
$config['fancybox']['gallery']						= false; # Вывод изображений как галлереи
$config['fancybox']['type']							= 'product'; # Тип галлереи
$config['fancybox']['cyclic']						= false; # Цикличность галлереи


# Настройки регистрации пользователей
$config['auth']['enabled']							= false; # Быстрая регистрация
$config['auth']['authURL']							= 'auth.php'; # Страница входа пользователя и напоминания пароля
$config['auth']['cabinetURL']						= 'cabinet.php'; # Страница кабинета пользователя

# Настройки вывода просмотренных ранее продуктов
$config['seen_products'] 							= 3; # Количество просмотренных ранее товаров

# Настройки новостей
$config['news']['pageURL']							= 'news.php'; # Страница новостей
$config['news']['list']								= 5; # Количество новостей в списке

# Настройки консультаций
$config['consult']['pageURL']						= 'consult.php'; # Страница консультаций
$config['consult']['list']							= 5; # Количество консультаций в списке

# Пагинация списка товаров
$config['products']['pagination_enabled']			= false; # Пагинация (разбивка на страници)
$config['products']['pagination_items_enabled']		= false; # Отображение выбора количества товаров на странице
$config['products']['pagination']					= 10; # Количество товаров на странице

# Сортировка товаров
$config['products']['sort_enabled']					= false; # Сортировка товаров
$config['products']['sort_items_enabled']			= false; # Отображение выбора сортировки товаров на странице
$config['products']['sort']							= 'price'; # Допустимые значения:id_product(порядковый номер), code(код), title(название), price(цена)
$config['products']['sort_type']					= 'up'; # Тип сортировки товыра. допустимые значения: up(возрастание), down(убывание)

$config['products']['title_link']					= false; # Отображать заголовок товара как ссылку на товар?
$config['products']['sub_cat_link']					= false; # Отображать заголовок подкатегорий как ссылку на подкатегорию?
$config['products']['sub_cat']						= 'none'; # Отображать список подкатегорий или выводить товары

# Товар по умолчанию в корзине. Добавляется 1 раз при открытии магазина в браузере
/*$config['default_product']						= array (
														'id' => 'consult',
														'name' => 'Бесплатные консультации',
														'price' => '15.00',
														'discount' => '0',
														'qty' => 1,
														'url' => '',
														'size' => '',
														'color' => '',
														'param' => ''
														);*/