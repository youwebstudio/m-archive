<?php 

# jCart v1.3.5
# http://conceptlogic.com/jcart/
# http://jcart.info

////////////////////////////////////////////////////////////////////////////////
# ОБЯЗАТЕЛЬНЫЕ НАСТРОЙКИ

# Путь к файлам jcart
$config['jcartPath']							= 'jcart/'; #

# Путь к странице оформления заказа
$config['checkoutPath']							= 'checkout'; #

# Кодировка. Может быть windows-1251 или utf-8. Лучше utf-8.
$config['encoding']								= 'utf-8'; #

# HTML атрибуты name, используемые в форме добавления товара
# Обязательные атрибуты
$config['item']['add']							= 'my_add_button';    # Add to cart button
$config['item']['id']							= 'my_item_id';       # Item id
$config['item']['name']							= 'my_item_name';     # Item name
$config['item']['price']						= 'my_item_price';    # Item price
$config['item']['discount']						= 'my_item_discount'; # Item discount
$config['item']['qty']							= 'my_item_qty';      # Item quantity
$config['item']['unit']							= 'my_item_unit';     # Единици измерения
$config['item']['cat']							= 'my_item_cat';      # Категория
# Дополнительные атрибуты
$config['item']['url']							= 'my_item_url';      # Item URL (optional)
$config['item']['color']						= 'my_item_color';    # Item color (optional)
$config['item']['size']							= 'my_item_size';     # Item size (optional)
$config['item']['param']						= 'my_item_param';    # Item param (optional)

# Уникализация товара в корзине ( за исключением 'id') Пример: $config['unique'][] = 'color';

////////////////////////////////////////////////////////////////////////////////
# ОПЦИОНАЛЬНЫЕ НАСТРОЙКИ

# Код валюты
$config['currencyCode']							= 'RUR'; #

# Добавить уникальный маркер в форму для предотвращения CSRF эксплоитов
# Подробней: http://conceptlogic.com/jcart/security.php
$config['csrfToken']							= false; #

# Переопределите текст кнопок корзины, заданный по умолчанию
$config['text']['cartTitle']					= '';     # Корзина
$config['text']['singleItem']					= '';     # товар (в единственном числе) Пример: 101 штука
$config['text']['multipleItems1']				= '';     # товара (в множественном числе) Пример: 32 штуки
$config['text']['multipleItems2']				= '';     # товаров (в множественном числе) Пример: 305 штук
$config['text']['subtotal']						= '';     # Всего
$config['text']['update']						= '';     # Обновить
$config['text']['checkout']						= '';     # Оформить заказ
$config['text']['checkoutPaypal']				= '';     # Отправить заказ
$config['text']['removeLink']					= '';     # Удалить
$config['text']['emptyButton']					= '';     # Очистить
$config['text']['emptyMessage']					= '';     # Ваша корзина пуста!
$config['text']['itemAdded']					= '';     # Товар добавлен
$config['text']['priceError']					= '';     # Неверный формат цены
$config['text']['quantityError']				= '';     # Количество товаров должно быть целым числом!
$config['text']['checkoutError']				= '';     # Ваш заказ не может быть обработан!

# Переопределите кнопки, заданные по умолчанию, путем ввода пути к изображениям кнопки
$config['button']['checkout']					= '';     # Оформить заказ
$config['button']['paypal']						= '';     # Отправить заказ
$config['button']['update']						= '';     # Обновить
$config['button']['empty']						= '';     # Очистить


////////////////////////////////////////////////////////////////////////////////
# ПРОДВИНУТЫЕ НАСТРОЙКИ

# Отображать всплывающую подсказку после добавления товара в корзину?
$config['tooltip']								= true; #
$config['modal']								= false; #

# Разрешить десятичные дроби в поле количества?
$config['decimalQtys']							= false; #

# Сколько знаков после запятой разрешено в значении количества?
$config['decimalPlaces']						= 1; #

# Формат числа для цены, см: http://php.net/manual/ru/function.number-format.php
# Количество знаков после запятой, Разделитель целого и дроби, Разделитель тысяч
$config['priceFormat']							= array('decimals' => 2, 'dec_point' => '.', 'thousands_sep' => ''); #