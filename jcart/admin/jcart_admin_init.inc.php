<?php

session_set_cookie_params(0, '/jcart/admin/');
@session_start();

# Initialize jcart after session start
$jcart = (isset($_SESSION['jcart'])) ? $_SESSION['jcart'] : '';
if (!is_object($jcart))
{
	$jcart = $_SESSION['jcart'] = new Jcart();
}

# Enable request_uri for non-Apache environments
# See: http://api.drupal.org/api/function/request_uri/7
if (!function_exists('request_uri'))
{
	function request_uri()
	{
		if (isset($_SERVER['REQUEST_URI']))
		{
			$uri = $_SERVER['REQUEST_URI'];
		} else
		{
			if (isset($_SERVER['argv']))
			{
				$uri = $_SERVER['SCRIPT_NAME'] . '?' . $_SERVER['argv'][0];
			} elseif (isset($_SERVER['QUERY_STRING']))
			{
				$uri = $_SERVER['SCRIPT_NAME'] . '?' . $_SERVER['QUERY_STRING'];
			} else
			{
				$uri = $_SERVER['SCRIPT_NAME'];
			}
		}
		$uri = '/' . ltrim($uri, '/');
		return $uri;
	}
}

# Функция вывода ошибки
if (!function_exists('error404'))
{
	function error404($pageout = false, $encoding = 'utf-8')
	{
		header('Cache-Control: no-cache, no-store');
		header('Content-Type: text/html; charset=' . $encoding);
		header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
		if ($pageout)
			readfile('404.shtml');
		die;
	}
}