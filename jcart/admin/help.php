<?php

# jCart v1.3.5
# http://conceptlogic.com/jcart/
# http://jcart.info

@session_start();

# Подключение настроек
include_once dirname(__FILE__) . '/../config-loader.php';

# Простейшая авторизация
include_once dirname(__FILE__) . '/../modules/M_Admin.inc.php';
$mAdmin = M_Admin::Instance();

if(isset($_SESSION['adminLogin']) && isset($_SESSION['adminPassword']) && $_SESSION['adminLogin'] == $config['admin']['login'] && $_SESSION['adminPassword'] == $config['admin']['password'])
{
	$install_path = dirname(__FILE__) . '/../install/';
	if(isset($_SESSION['kill_install']) && $_SESSION['kill_install'] === true && is_dir($install_path))
	{
		include_once dirname(__FILE__) . '/../modules/M_Files.inc.php';
		$mFiles = M_Files::Instance();
		$mFiles->CleanDir($install_path);
		rmdir($install_path);
	}
	$_SESSION = array();
	@session_destroy();
}
elseif (!$mAdmin->CheckLogin()) {
	die;
}

# Кодировка.
header('Content-type: text/html; charset=' . $config['encoding']);

# Обработка имени файла
$file = str_replace(dirname(__FILE__) . '/', '', (__FILE__));
$file = str_replace('.php', '', $file);

# Подключение меню
include_once dirname(__FILE__) . '/_menu.php';

# Шаблон страницы
include_once dirname(__FILE__) . '/../design/AdminHelp.tpl.php';