<?php

# jCart v1.3.5
# http://conceptlogic.com/jcart/
# http://jcart.info

# pro

error_reporting(E_ALL); # Уровень вывода ошибок
ini_set('display_errors', 'off'); # Вывод ошибок выключен

ini_set("log_errors", 'on'); # Логирование включено
ini_set("error_log", dirname(__FILE__) . '/error_log.txt'); # Путь файла с логами

# Абсолютный путь
$config_path = dirname(__FILE__) . '/../config.php';
$config_inc_path = dirname(__FILE__) . '/../config.inc.php';
$bup_path = dirname(__FILE__) . '/backup/';
$c_path = dirname(__FILE__) . '/../';

# Подключение модулей
include_once dirname(__FILE__) . '/../modules/M_Admin.inc.php';
$mAdmin = M_Admin::Instance();

$c_file = file_get_contents(dirname(__FILE__) . '/../config.php');
$c_inc_file = file_get_contents(dirname(__FILE__) . '/../config.inc.php');
$c_load_file = file_get_contents(dirname(__FILE__) . '/../config-loader.php');

$res = $res1 = false;

# Проверки ошибок в файлах конфигов
ob_start();
$res1 = eval('if (0) {?>'.$c_file. '
}; return true;');
$error_text1 = ob_get_clean();

ob_start();
$res = eval('if (0) {?>'.$c_inc_file . '
}; return true;');
$error_text = ob_get_clean();

# устранение ошибки 500 в функции eval(), при директиве display_errors = off;
header('HTTP/1.0 200 OK');

# Подключение настроек
if (!$res && !empty($error_text)) {
	$c_file = file_get_contents($bup_path . 'config.php');
	$c_inc_file = file_get_contents($bup_path . 'config.inc.php');
	$mAdmin->SaveConfig($c_path . 'config.php', $c_file);
	$mAdmin->SaveConfig($c_path . 'config.inc.php', $c_inc_file);
	include_once $config_path;
	include_once $config_inc_path;
	$_SESSION['tmp_message'] = 'Произошел сбой. Файлы настроек восстановлены из бэкапа.';
}
elseif(!$res1 && !empty($error_text1))
{
	$c_file = file_get_contents($bup_path . 'config.php');
	$c_inc_file = file_get_contents($bup_path . 'config.inc.php');
	$mAdmin->SaveConfig($c_path . 'config.php', $c_file);
	$mAdmin->SaveConfig($c_path . 'config.inc.php', $c_inc_file);
	include_once $config_path;
	include_once $config_inc_path;
	$_SESSION['tmp_message'] = 'Произошел сбой. Файлы настроек восстановлены из бэкапа.';
}
else
{
	include_once $config_path;
	include_once $config_inc_path;
	$c_file = file_get_contents(dirname(__FILE__) . '/../config.php');
	$c_inc_file = file_get_contents(dirname(__FILE__) . '/../config.inc.php');
}

# Простейшая авторизация
include_once dirname(__FILE__) . '/../modules/M_Admin.inc.php';
$mAdmin = M_Admin::Instance();

if (!$mAdmin->CheckLogin())
	die;

# Кодировка.
header('Content-type: text/html; charset=' . $config['encoding']);

# Обработка имени файла
$file = str_replace(dirname(__FILE__) . '/', '', (__FILE__));
$file = str_replace('.php', '', $file);

# Подключение меню
include_once dirname(__FILE__) . '/_menu.php';

# Обработка отправки формы
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if (isset($_POST['backup']))
	{
		$c_file = file_get_contents($bup_path . 'config.php');
		$c_inc_file = file_get_contents($bup_path . 'config.inc.php');
		$mAdmin->SaveConfig($c_path . 'config.php', $c_file);
		$mAdmin->SaveConfig($c_path . 'config.inc.php', $c_inc_file);
	} # Сохранение
	elseif (isset($_POST['save']))
	{
		$settings = $_POST['settings'];

		# Сохраняем бэкап конфигов

		$mAdmin->SaveConfig($bup_path . 'config.php', $c_file);
		$mAdmin->SaveConfig($bup_path . 'config.inc.php', $c_inc_file);

		# config.php
		$c_file = $mAdmin->SetConfig($c_file, 'config\[\'jcartPath\'\]', trim(strip_tags($settings['jcartPath'])));
		$c_file = $mAdmin->SetConfig($c_file, 'config\[\'checkoutPath\'\]', trim(strip_tags($settings['checkoutPath'])));
		$c_file = $mAdmin->SetConfig($c_file, 'config\[\'currencyCode\'\]', trim(strip_tags($settings['currencyCode'])));
		$c_file = $mAdmin->SetConfig($c_file, 'config\[\'tooltip\'\]', (isset($settings['tooltip']) ? 'true' : 'false'));
		$c_file = $mAdmin->SetConfig($c_file, 'config\[\'modal\'\]', (isset($settings['modal']) ? 'true' : 'false'));
		$c_file = $mAdmin->SetConfig($c_file, 'config\[\'decimalQtys\'\]', (isset($settings['decimalQtys']) ? 'true' : 'false'));
		$c_file = $mAdmin->SetConfig($c_file, 'config\[\'decimalPlaces\'\]', $settings['decimalPlaces']);
		$c_file = $mAdmin->SetConfig($c_file, 'config\[\'encoding\']', trim(strip_tags($settings['encoding'])));


		if($mAdmin->ValidateConfig($settings['priceFormat_decimals'])==true && $mAdmin->ValidateConfig($settings['priceFormat_dec_point'])==true && $mAdmin->ValidateConfig($settings['priceFormat_thousands_sep'])==true)
		{
			$settings['priceFormat_new'] =
				"array('decimals' => $settings[priceFormat_decimals], 'dec_point' => '$settings[priceFormat_dec_point]', 'thousands_sep' => '$settings[priceFormat_thousands_sep]')";
			$c_file = $mAdmin->SetConfig($c_file, 'config\[\'priceFormat\'\]', $settings['priceFormat_new']);
		}

		$mAdmin->SaveConfig($c_path . 'config.php', $c_file);

		# config.ini.php
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'sitename\'\]', trim(strip_tags($settings['sitename'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'sitelink\'\]', trim(strip_tags($settings['sitelink'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'secretWord\']', trim(strip_tags($settings['secretWord'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'email\'\]\[\'receiver\']', trim(strip_tags($settings['email']['receiver'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'email\'\]\[\'answer\']', trim(strip_tags($settings['email']['answer'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'email\'\]\[\'answerName\']', trim(strip_tags($settings['email']['answerName'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'smallCart\'\]', (isset($settings['smallCart']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'display_errors\'\]', (isset($settings['display_errors']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'log_errors\'\]', (isset($settings['log_errors']) ? 'true' : 'false'));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'database\'\]\[\'enabled\'\]', (isset($settings['database']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'database\'\]\[\'host\'\]', trim(strip_tags($settings['database']['host'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'database\'\]\[\'user\'\]', trim(strip_tags($settings['database']['user'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'database\'\]\[\'pass\'\]', trim(strip_tags($settings['database']['pass'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'database\'\]\[\'name\'\]', trim(strip_tags($settings['database']['name'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'database\'\]\[\'quantityEnabled\'\]', (isset($settings['database']['quantityEnabled']) ? 'true' : 'false'));

		# Настройка полей формы заказа
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'name\'\]\[\'label\'\]', trim(strip_tags($settings['form']['order']['name']['label'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'name\'\]\[\'enabled\'\]', (isset($settings['form']['order']['name']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'name\'\]\[\'required\'\]', (isset($settings['form']['order']['name']['required']) ? 'true' : 'false'));
		
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'email\'\]\[\'label\'\]', trim(strip_tags($settings['form']['order']['email']['label'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'email\'\]\[\'enabled\'\]', (isset($settings['form']['order']['email']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'email\'\]\[\'required\'\]', (isset($settings['form']['order']['email']['required']) ? 'true' : 'false'));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'lastname\'\]\[\'label\'\]', trim(strip_tags($settings['form']['order']['lastname']['label'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'lastname\'\]\[\'enabled\'\]', (isset($settings['form']['order']['lastname']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'lastname\'\]\[\'required\'\]', (isset($settings['form']['order']['lastname']['required']) ? 'true' : 'false'));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'fathername\'\]\[\'label\'\]', trim(strip_tags($settings['form']['order']['fathername']['label'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'fathername\'\]\[\'enabled\'\]', (isset($settings['form']['order']['fathername']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'fathername\'\]\[\'required\'\]', (isset($settings['form']['order']['fathername']['required']) ? 'true' : 'false'));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'phone\'\]\[\'label\'\]', trim(strip_tags($settings['form']['order']['phone']['label'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'phone\'\]\[\'enabled\'\]', (isset($settings['form']['order']['phone']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'phone\'\]\[\'required\'\]', (isset($settings['form']['order']['phone']['required']) ? 'true' : 'false'));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'zip\'\]\[\'label\'\]', trim(strip_tags($settings['form']['order']['zip']['label'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'zip\'\]\[\'enabled\'\]', (isset($settings['form']['order']['zip']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'zip\'\]\[\'required\'\]', (isset($settings['form']['order']['zip']['required']) ? 'true' : 'false'));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'region\'\]\[\'label\'\]', trim(strip_tags($settings['form']['order']['region']['label'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'region\'\]\[\'enabled\'\]', (isset($settings['form']['order']['region']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'region\'\]\[\'required\'\]', (isset($settings['form']['order']['region']['required']) ? 'true' : 'false'));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'city\'\]\[\'label\'\]', trim(strip_tags($settings['form']['order']['city']['label'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'city\'\]\[\'enabled\'\]', (isset($settings['form']['order']['city']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'city\'\]\[\'required\'\]', (isset($settings['form']['order']['city']['required']) ? 'true' : 'false'));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'address\'\]\[\'label\'\]', trim(strip_tags($settings['form']['order']['address']['label'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'address\'\]\[\'enabled\'\]', (isset($settings['form']['order']['address']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'address\'\]\[\'required\'\]', (isset($settings['form']['order']['address']['required']) ? 'true' : 'false'));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'comment\'\]\[\'label\'\]', trim(strip_tags($settings['form']['order']['comment']['label'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'comment\'\]\[\'enabled\'\]', (isset($settings['form']['order']['comment']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'comment\'\]\[\'required\'\]', (isset($settings['form']['order']['comment']['required']) ? 'true' : 'false'));

		# Настройка полей формы сообщения
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'name\'\]\[\'label\'\]', trim(strip_tags($settings['form']['feedback']['name']['label'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'name\'\]\[\'enabled\'\]', (isset($settings['form']['feedback']['name']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'name\'\]\[\'required\'\]', (isset($settings['form']['feedback']['name']['required']) ? 'true' : 'false'));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'email\'\]\[\'label\'\]', trim(strip_tags($settings['form']['feedback']['email']['label'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'email\'\]\[\'enabled\'\]', (isset($settings['form']['feedback']['email']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'email\'\]\[\'required\'\]', (isset($settings['form']['feedback']['email']['required']) ? 'true' : 'false'));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'phone\'\]\[\'label\'\]', trim(strip_tags($settings['form']['feedback']['phone']['label'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'phone\'\]\[\'enabled\'\]', (isset($settings['form']['feedback']['phone']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'phone\'\]\[\'required\'\]', (isset($settings['form']['feedback']['phone']['required']) ? 'true' : 'false'));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'subject\'\]\[\'label\'\]', trim(strip_tags($settings['form']['feedback']['subject']['label'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'subject\'\]\[\'enabled\'\]', (isset($settings['form']['feedback']['subject']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'subject\'\]\[\'required\'\]', (isset($settings['form']['feedback']['subject']['required']) ? 'true' : 'false'));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'comment\'\]\[\'label\'\]', trim(strip_tags($settings['form']['feedback']['comment']['label'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'comment\'\]\[\'enabled\'\]', (isset($settings['form']['feedback']['comment']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'comment\'\]\[\'required\'\]', (isset($settings['form']['feedback']['comment']['required']) ? 'true' : 'false'));

		# Настройка отправки на почту
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'email\'\]\[\'enabled\'\]', (isset($settings['payments']['email']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'email\'\]\[\'title\'\]', trim(strip_tags($settings['payments']['email']['title'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'email\'\]\[\'info\'\]', trim(strip_tags($settings['payments']['email']['info'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'email\'\]\[\'details\'\]', trim(strip_tags($settings['payments']['email']['details'])));

		# Отправка курьеру (та же отправка на почту, только по другому называется)
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'courier\'\]\[\'enabled\'\]', (isset($settings['payments']['courier']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'courier\'\]\[\'title\'\]', trim(strip_tags($settings['payments']['courier']['title'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'courier\'\]\[\'info\'\]', trim(strip_tags($settings['payments']['courier']['info'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'courier\'\]\[\'details\'\]', trim(strip_tags($settings['payments']['courier']['details'])));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'enabled\'\]', (isset($settings['payments']['robokassa']['enabled'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'title\'\]', trim(strip_tags($settings['payments']['robokassa']['title'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'info\'\]', trim(strip_tags($settings['payments']['robokassa']['info'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'details\'\]', trim(strip_tags($settings['payments']['robokassa']['details'])));

		# Настройки RoboKassa
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'login\'\]', trim(strip_tags($settings['payments']['robokassa']['login'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'pass1\'\]', trim(strip_tags($settings['payments']['robokassa']['pass1'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'pass2\'\]', trim(strip_tags($settings['payments']['robokassa']['pass2'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'description\'\]', trim(strip_tags($settings['payments']['robokassa']['description'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'test\'\]', (isset($settings['payments']['robokassa']['test'])? 'true' : 'false'));

		# Настройка печати счёта
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'invoice\'\]\[\'enabled\'\]', (isset($settings['payments']['invoice']['enabled'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'invoice\'\]\[\'title\'\]', trim(strip_tags($settings['payments']['invoice']['title'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'invoice\'\]\[\'info\'\]', trim(strip_tags($settings['payments']['invoice']['info'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'invoice\'\]\[\'details\'\]', trim(strip_tags($settings['payments']['invoice']['details'])));

		# Настройка LiqPay
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'liqpay\'\]\[\'enabled\'\]', (isset($settings['payments']['liqpay']['enabled'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'liqpay\'\]\[\'title\'\]', trim(strip_tags($settings['payments']['liqpay']['title'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'liqpay\'\]\[\'info\'\]', trim(strip_tags($settings['payments']['liqpay']['info'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'liqpay\'\]\[\'details\'\]', trim(strip_tags($settings['payments']['liqpay']['details'])));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'liqpay\'\]\[\'description\'\]', trim(strip_tags($settings['payments']['liqpay']['description'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'liqpay\'\]\[\'id\'\]', trim(strip_tags($settings['payments']['liqpay']['id'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'liqpay\'\]\[\'sign\'\]', trim(strip_tags($settings['payments']['liqpay']['sign'])));

		# Настройки InterKassa
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'interkassa\'\]\[\'enabled\'\]', (isset($settings['payments']['interkassa']['enabled'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'interkassa\'\]\[\'title\'\]', trim(strip_tags($settings['payments']['interkassa']['title'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'interkassa\'\]\[\'info\'\]', trim(strip_tags($settings['payments']['interkassa']['info'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'interkassa\'\]\[\'details\'\]', trim(strip_tags($settings['payments']['interkassa']['details'])));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'interkassa\'\]\[\'shop_id\'\]', trim(strip_tags($settings['payments']['interkassa']['shop_id'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'interkassa\'\]\[\'secret_key\'\]', trim(strip_tags($settings['payments']['interkassa']['secret_key'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'interkassa\'\]\[\'description\'\]', trim(strip_tags($settings['payments']['interkassa']['description'])));

		# Настройка A1Pay
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'a1pay\'\]\[\'enabled\'\]', (isset($settings['payments']['a1pay']['enabled'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'a1pay\'\]\[\'title\'\]', trim(strip_tags($settings['payments']['a1pay']['title'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'a1pay\'\]\[\'info\'\]', trim(strip_tags($settings['payments']['a1pay']['info'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'a1pay\'\]\[\'details\'\]', trim(strip_tags($settings['payments']['a1pay']['details'])));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'a1pay\'\]\[\'secret_word\'\]', trim(strip_tags($settings['payments']['a1pay']['secret_word'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'a1pay\'\]\[\'secret_key\'\]', trim(strip_tags($settings['payments']['a1pay']['secret_key'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'payments\'\]\[\'a1pay\'\]\[\'description\'\]', trim(strip_tags($settings['payments']['a1pay']['description'])));

		# Доставка №1
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'1\'\]\[\'enabled\'\]', (isset($settings['deliveries']['1']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'1\'\]\[\'title\'\]', trim(strip_tags($settings['deliveries']['1']['title'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'1\'\]\[\'info\'\]', trim(strip_tags($settings['deliveries']['1']['info'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'1\'\]\[\'details\'\]', trim(strip_tags($settings['deliveries']['1']['details'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'1\'\]\[\'cost\'\]', trim(strip_tags($settings['deliveries']['1']['cost'])));

		# Доставка №2
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'2\'\]\[\'enabled\'\]', (isset($settings['deliveries']['2']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'2\'\]\[\'title\'\]', trim(strip_tags($settings['deliveries']['2']['title'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'2\'\]\[\'info\'\]', trim(strip_tags($settings['deliveries']['2']['info'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'2\'\]\[\'details\'\]', trim(strip_tags($settings['deliveries']['2']['details'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'2\'\]\[\'cost\'\]', trim(strip_tags($settings['deliveries']['2']['cost'])));
		# Доставка №3
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'3\'\]\[\'enabled\'\]', (isset($settings['deliveries']['3']['enabled']) ? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'3\'\]\[\'title\'\]', trim(strip_tags($settings['deliveries']['3']['title'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'3\'\]\[\'info\'\]', trim(strip_tags($settings['deliveries']['3']['info'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'3\'\]\[\'details\'\]', trim(strip_tags($settings['deliveries']['3']['details'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'3\'\]\[\'cost\'\]', trim(strip_tags($settings['deliveries']['3']['cost'])));

		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'secretWord\'\]', trim(strip_tags($settings['secretWord'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'returnUrl\'\]', trim(strip_tags($settings['returnUrl'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'successUrl\'\]', trim(strip_tags($settings['successUrl'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'failUrl\'\]', trim(strip_tags($settings['failUrl'])));

		# admin config
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'admin\'\]\[\'login\'\]', trim(strip_tags($settings['admin']['login'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'admin\'\]\[\'password\'\]', trim(strip_tags($settings['admin']['password'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'admin\'\]\[\'ordersList\'\]', trim(strip_tags($settings['admin']['ordersList'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'admin\'\]\[\'productsList\'\]', trim(strip_tags($settings['admin']['productsList'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'admin\'\]\[\'usersList\'\]', trim(strip_tags($settings['admin']['usersList'])));

		# Настройки накопитеьных ссылок
		$valid_disc = true;
		$settings['discounts_new'] = 'array(';
		foreach($settings['discounts']['sum'] as $key => $sum)
		{
			$name = $settings['discounts']['name'][$key];
			if($mAdmin->ValidateConfig($sum)==true && $mAdmin->ValidateConfig($name)==true)
			{
				$sep = ($key != count($settings['discounts']['sum']) - 1)? ',':'';
				$settings['discounts_new'] .= " $sum => $name$sep";
			}
			else
				$valid_disc = false;
		}
		$settings['discounts_new'] .= ')';
		if($valid_disc)
		{
			if(isset($settings['discounts_enabled']) && $settings['discounts_prev'] == true)
				$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'discounts\'\]', $settings['discounts_new']);
			elseif(isset($settings['discounts_enabled']) && $settings['discounts_prev'] == false)
			{
				preg_match('|/\*(\$config\[\'discounts\'\]\s*=\s+).*;\*/|Uisu', $c_inc_file, $matches);
				if(isset($matches[0]))
				{
					$repl = $matches[1] . $settings['discounts_new'].';';
					$patt = '|(/\*\$config\[\'discounts\'\]\s*=\s*.*;\*)/|Uisu';
					$c_inc_file = preg_replace($patt, $repl, $c_inc_file);
				}
			}
			elseif(!isset($settings['discounts_enabled']) && $settings['discounts_prev'] == true)
			{
				preg_match('|\$config\[\'discounts\'\]\s*=\s*.*;|Uisu', $c_inc_file, $matches);
				if(isset($matches[0]))
				{
					$repl = '/\*'.$matches[0].'\*/';
					$patt = '|(\$config\[\'discounts\'\]\s*=\s*.*;)|Uisu';
					$c_inc_file = preg_replace($patt, $repl, $c_inc_file);
				}
			}
		}

		# auth
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'auth\'\]\[\'enabled\'\]', (isset($settings['authEnabled'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'auth\'\]\[\'authURL\'\]', trim(strip_tags($settings['authURL'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'auth\'\]\[\'cabinetURL\'\]', trim(strip_tags($settings['cabinetURL'])));

		# image config
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'images\'\]\[\'enabled\'\]', (isset($settings['images']['enabled'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'images\'\]\[\'dir\'\]', trim(strip_tags($settings['images']['dir'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'images\'\]\[\'resize\'\]\[\'enabled\'\]', (isset($settings['images']['resize']['enabled'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'images\'\]\[\'thumb\'\]\[\'width\'\]', trim(strip_tags($settings['images']['thumb']['width'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'images\'\]\[\'height\'\]', trim(strip_tags($settings['images']['height'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'images\'\]\[\'width\'\]', trim(strip_tags($settings['images']['width'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'images\'\]\[\'cart\'\]\[\'enabled\'\]', (isset($settings['images']['cart']['enabled'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'images\'\]\[\'cart\'\]\[\'width\'\]', trim(strip_tags($settings['images']['cart']['width'])));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'fancybox\'\]\[\'enabled\'\]', (isset($settings['fancybox']['enabled'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'fancybox\'\]\[\'gallery\'\]', (isset($settings['fancybox']['gallery'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'fancybox\'\]\[\'type\'\]', $settings['fancybox']['type']);
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'fancybox\'\]\[\'cyclic\'\]', (isset($settings['fancybox']['cyclic'])? 'true' : 'false'));

		# Пагинация списка товаров
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'products\'\]\[\'pagination_enabled\'\]', (isset($settings['products']['pagination_enabled'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'products\'\]\[\'pagination_items_enabled\'\]', (isset($settings['products']['pagination_items_enabled'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'products\'\]\[\'pagination\'\]', trim(strip_tags($settings['products']['pagination'])));

		# Сортировка товаров
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'products\'\]\[\'sort_enabled\'\]', (isset($settings['products']['sort_enabled'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'products\'\]\[\'sort_items_enabled\'\]', (isset($settings['products']['sort_items_enabled'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'products\'\]\[\'sort\'\]', $settings['products']['sort']);
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'products\'\]\[\'sort_type\'\]', $settings['products']['sort_type']);

		# Настройки отображения товаров
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'products\'\]\[\'title_link\'\]', (isset($settings['products']['title_link'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'products\'\]\[\'sub_cat_link\'\]', (isset($settings['products']['sub_cat_link'])? 'true' : 'false'));
		$c_inc_file = $mAdmin->SetConfig($c_inc_file, 'config\[\'products\'\]\[\'sub_cat\'\]', $settings['products']['sub_cat']);

		$mAdmin->SaveConfig($c_path . 'config.inc.php', $c_inc_file);

	}

	@session_start();
	$_SESSION = array();
	unset($_SESSION);
	session_destroy();

	# Перенаправление на главную админки
	header('Location: ' . $config['sitelink'] . $config['jcartPath'] . 'admin/settings.php');
	die;
} else
{
	# Выбор конфига

	# config.php
	$settings['jcartPath'] = $mAdmin->GetConfig($c_file, 'config\[\'jcartPath\'\]');
	$settings['checkoutPath'] = $mAdmin->GetConfig($c_file, 'config\[\'checkoutPath\'\]');
	$settings['currencyCode'] = $mAdmin->GetConfig($c_file, 'config\[\'currencyCode\'\]');
	$settings['tooltip'] = $mAdmin->GetConfig($c_file, 'config\[\'tooltip\'\]');
	$settings['modal'] = $mAdmin->GetConfig($c_file, 'config\[\'modal\'\]');
	$settings['decimalQtys'] = $mAdmin->GetConfig($c_file, 'config\[\'decimalQtys\'\]');
	$settings['decimalPlaces'] = $mAdmin->GetConfig($c_file, 'config\[\'decimalPlaces\'\]');
	$settings['encoding'] = $mAdmin->GetConfig($c_file, 'config\[\'encoding\']');


	$settings['priceFormat'] = $config['priceFormat'];

	# config.ini.php
	$settings['sitename'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'sitename\'\]');
	$settings['sitelink'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'sitelink\'\]');
	$settings['secretWord'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'secretWord\']');
	$settings['email']['receiver'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'email\'\]\[\'receiver\'\]');
	$settings['email']['answer'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'email\'\]\[\'answer\'\]');
	$settings['email']['answerName'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'email\'\]\[\'answerName\'\]');
	$settings['smallCart'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'smallCart\'\]');
	$settings['display_errors'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'display_errors\'\]');
	$settings['log_errors'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'log_errors\'\]');

	# database
	$settings['database']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'database\'\]\[\'enabled\'\]');
	$settings['database']['host'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'database\'\]\[\'host\'\]');
	$settings['database']['user'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'database\'\]\[\'user\'\]');
	$settings['database']['pass'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'database\'\]\[\'pass\'\]');
	$settings['database']['name'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'database\'\]\[\'name\'\]');
	$settings['database']['quantityEnabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'database\'\]\[\'quantityEnabled\'\]');

	# Настройка полей формы заказа
	$settings['form']['order']['name']['label'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'name\'\]\[\'label\'\]');
	$settings['form']['order']['name']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'name\'\]\[\'enabled\'\]');
	$settings['form']['order']['name']['required'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'name\'\]\[\'required\'\]');

	$settings['form']['order']['email']['label'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'email\'\]\[\'label\'\]');
	$settings['form']['order']['email']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'email\'\]\[\'enabled\'\]');
	$settings['form']['order']['email']['required'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'email\'\]\[\'required\'\]');

	$settings['form']['order']['lastname']['label'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'lastname\'\]\[\'label\'\]');
	$settings['form']['order']['lastname']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'lastname\'\]\[\'enabled\'\]');
	$settings['form']['order']['lastname']['required'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'lastname\'\]\[\'required\'\]');

	$settings['form']['order']['fathername']['label'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'fathername\'\]\[\'label\'\]');
	$settings['form']['order']['fathername']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'fathername\'\]\[\'enabled\'\]');
	$settings['form']['order']['fathername']['required'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'fathername\'\]\[\'required\'\]');

	$settings['form']['order']['phone']['label'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'phone\'\]\[\'label\'\]');
	$settings['form']['order']['phone']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'phone\'\]\[\'enabled\'\]');
	$settings['form']['order']['phone']['required'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'phone\'\]\[\'required\'\]');

	$settings['form']['order']['zip']['label'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'zip\'\]\[\'label\'\]');
	$settings['form']['order']['zip']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'zip\'\]\[\'enabled\'\]');
	$settings['form']['order']['zip']['required'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'zip\'\]\[\'required\'\]');

	$settings['form']['order']['region']['label'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'region\'\]\[\'label\'\]');
	$settings['form']['order']['region']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'region\'\]\[\'enabled\'\]');
	$settings['form']['order']['region']['required'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'region\'\]\[\'required\'\]');

	$settings['form']['order']['city']['label'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'city\'\]\[\'label\'\]');
	$settings['form']['order']['city']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'city\'\]\[\'enabled\'\]');
	$settings['form']['order']['city']['required'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'city\'\]\[\'required\'\]');

	$settings['form']['order']['address']['label'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'address\'\]\[\'label\'\]');
	$settings['form']['order']['address']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'address\'\]\[\'enabled\'\]');
	$settings['form']['order']['address']['required'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'address\'\]\[\'required\'\]');

	$settings['form']['order']['comment']['label'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'comment\'\]\[\'label\'\]');
	$settings['form']['order']['comment']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'comment\'\]\[\'enabled\'\]');
	$settings['form']['order']['comment']['required'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'order\'\]\[\'comment\'\]\[\'required\'\]');

	# Настройка полей формы сообщения
	$settings['form']['feedback']['name']['label'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'name\'\]\[\'label\'\]');
	$settings['form']['feedback']['name']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'name\'\]\[\'enabled\'\]');
	$settings['form']['feedback']['name']['required'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'name\'\]\[\'required\'\]');

	$settings['form']['feedback']['email']['label'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'email\'\]\[\'label\'\]');
	$settings['form']['feedback']['email']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'email\'\]\[\'enabled\'\]');
	$settings['form']['feedback']['email']['required'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'email\'\]\[\'required\'\]');

	$settings['form']['feedback']['phone']['label'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'phone\'\]\[\'label\'\]');
	$settings['form']['feedback']['phone']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'phone\'\]\[\'enabled\'\]');
	$settings['form']['feedback']['phone']['required'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'phone\'\]\[\'required\'\]');

	$settings['form']['feedback']['subject']['label'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'subject\'\]\[\'label\'\]');
	$settings['form']['feedback']['subject']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'subject\'\]\[\'enabled\'\]');
	$settings['form']['feedback']['subject']['required'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'subject\'\]\[\'required\'\]');

	$settings['form']['feedback']['comment']['label'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'comment\'\]\[\'label\'\]');
	$settings['form']['feedback']['comment']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'comment\'\]\[\'enabled\'\]');
	$settings['form']['feedback']['comment']['required'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'form\'\]\[\'feedback\'\]\[\'comment\'\]\[\'required\'\]');


	# Настройка отправки на почту
	$settings['payments']['email']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'email\'\]\[\'enabled\'\]');
	$settings['payments']['email']['title'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'email\'\]\[\'title\'\]');
	$settings['payments']['email']['info'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'email\'\]\[\'info\'\]');
	$settings['payments']['email']['details'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'email\'\]\[\'details\'\]');

	# Отправка курьеру (та же отправка на почту, только по другому называется)
	$settings['payments']['courier']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'courier\'\]\[\'enabled\'\]');
	$settings['payments']['courier']['title'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'courier\'\]\[\'title\'\]');
	$settings['payments']['courier']['info'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'courier\'\]\[\'info\'\]');
	$settings['payments']['courier']['details'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'courier\'\]\[\'details\'\]');

	$settings['payments']['robokassa']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'enabled\'\]');
	$settings['payments']['robokassa']['title'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'title\'\]');
	$settings['payments']['robokassa']['info'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'info\'\]');
	$settings['payments']['robokassa']['details'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'details\'\]');

	# Настройки RoboKassa
	$settings['payments']['robokassa']['login'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'login\'\]');
	$settings['payments']['robokassa']['pass1'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'pass1\'\]');
	$settings['payments']['robokassa']['pass2'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'pass2\'\]');
	$settings['payments']['robokassa']['description'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'description\'\]');
	$settings['payments']['robokassa']['test'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'robokassa\'\]\[\'test\'\]');

	# Настройка печати счёта
	$settings['payments']['invoice']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'invoice\'\]\[\'enabled\'\]');
	$settings['payments']['invoice']['title'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'invoice\'\]\[\'title\'\]');
	$settings['payments']['invoice']['info'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'invoice\'\]\[\'info\'\]');
	$settings['payments']['invoice']['details'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'invoice\'\]\[\'details\'\]');

	# Настройка LiqPay
	$settings['payments']['liqpay']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'liqpay\'\]\[\'enabled\'\]');
	$settings['payments']['liqpay']['title'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'liqpay\'\]\[\'title\'\]');
	$settings['payments']['liqpay']['info'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'liqpay\'\]\[\'info\'\]');
	$settings['payments']['liqpay']['details'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'liqpay\'\]\[\'details\'\]');

	$settings['payments']['liqpay']['description'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'liqpay\'\]\[\'description\'\]');
	$settings['payments']['liqpay']['id'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'liqpay\'\]\[\'id\'\]');
	$settings['payments']['liqpay']['sign'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'liqpay\'\]\[\'sign\'\]');

	# Настройки InterKassa
	$settings['payments']['interkassa']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'interkassa\'\]\[\'enabled\'\]');
	$settings['payments']['interkassa']['title'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'interkassa\'\]\[\'title\'\]');
	$settings['payments']['interkassa']['info'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'interkassa\'\]\[\'info\'\]');
	$settings['payments']['interkassa']['details'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'interkassa\'\]\[\'details\'\]');

	$settings['payments']['interkassa']['shop_id'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'interkassa\'\]\[\'shop_id\'\]');
	$settings['payments']['interkassa']['secret_key'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'interkassa\'\]\[\'secret_key\'\]');
	$settings['payments']['interkassa']['description'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'interkassa\'\]\[\'description\'\]');

	# Настройка A1Pay
	$settings['payments']['a1pay']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'a1pay\'\]\[\'enabled\'\]');
	$settings['payments']['a1pay']['title'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'a1pay\'\]\[\'title\'\]');
	$settings['payments']['a1pay']['info'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'a1pay\'\]\[\'info\'\]');
	$settings['payments']['a1pay']['details'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'a1pay\'\]\[\'details\'\]');

	$settings['payments']['a1pay']['secret_word'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'a1pay\'\]\[\'secret_word\'\]');
	$settings['payments']['a1pay']['secret_key'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'a1pay\'\]\[\'secret_key\'\]');
	$settings['payments']['a1pay']['description'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'payments\'\]\[\'a1pay\'\]\[\'description\'\]');

	# Доставка №1
	$settings['deliveries']['1']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'1\'\]\[\'enabled\'\]');
	$settings['deliveries']['1']['title'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'1\'\]\[\'title\'\]');
	$settings['deliveries']['1']['info'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'1\'\]\[\'info\'\]');
	$settings['deliveries']['1']['details'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'1\'\]\[\'details\'\]');
	$settings['deliveries']['1']['cost'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'1\'\]\[\'cost\'\]');

	# Доставка №2
	$settings['deliveries']['2']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'2\'\]\[\'enabled\'\]');
	$settings['deliveries']['2']['title'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'2\'\]\[\'title\'\]');
	$settings['deliveries']['2']['info'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'2\'\]\[\'info\'\]');
	$settings['deliveries']['2']['details'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'2\'\]\[\'details\'\]');
	$settings['deliveries']['2']['cost'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'2\'\]\[\'cost\'\]');
	# Доставка №3
	$settings['deliveries']['3']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'3\'\]\[\'enabled\'\]');
	$settings['deliveries']['3']['title'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'3\'\]\[\'title\'\]');
	$settings['deliveries']['3']['info'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'3\'\]\[\'info\'\]');
	$settings['deliveries']['3']['details'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'3\'\]\[\'details\'\]');
	$settings['deliveries']['3']['cost'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'deliveries\'\]\[\'3\'\]\[\'cost\'\]');

	$settings['secretWord'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'secretWord\'\]');
	$settings['returnUrl'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'returnUrl\'\]');
	$settings['successUrl'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'successUrl\'\]');
	$settings['failUrl'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'failUrl\'\]');

	# admin config
	$settings['admin']['login'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'admin\'\]\[\'login\'\]');
	$settings['admin']['password'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'admin\'\]\[\'password\'\]');
	$settings['admin']['ordersList'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'admin\'\]\[\'ordersList\'\]');
	$settings['admin']['productsList'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'admin\'\]\[\'productsList\'\]');
	$settings['admin']['usersList'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'admin\'\]\[\'usersList\'\]');

	# Настройки накопитеьных ссылок
	if(isset($config['discounts']))
	{
		$settings['discounts_enabled'] = true;
		$settings['discounts_prev'] = true;
		$settings['discounts'] = $config['discounts'];
	}
	else
	{
		$settings['discounts_enabled'] = false;
		$settings['discounts_prev'] = false;
		preg_match('|/\*\$config\[\'discounts\'\]\s*=\s*(.*);\*/|Uisu', $c_inc_file, $matches);
		if(isset($matches[1]))
		{
			eval("\$settings['discounts'] = $matches[1];");
		}
		else
		$settings['discounts'] = array (
			100 => 1,
			500 => 2,
			1000 => 3
		);
	}

	# auth
	$settings['authEnabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'auth\'\]\[\'enabled\'\]');
	$settings['authURL'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'auth\'\]\[\'authURL\'\]');
	$settings['cabinetURL'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'auth\'\]\[\'cabinetURL\'\]');

	# image config
	$settings['images']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'images\'\]\[\'enabled\'\]');
	$settings['images']['dir'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'images\'\]\[\'dir\'\]');
	$settings['images']['resize']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'images\'\]\[\'resize\'\]\[\'enabled\'\]');
	$settings['images']['thumb']['width'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'images\'\]\[\'thumb\'\]\[\'width\'\]');
	$settings['images']['height'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'images\'\]\[\'height\'\]');
	$settings['images']['width'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'images\'\]\[\'width\'\]');
	$settings['images']['cart']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'images\'\]\[\'cart\'\]\[\'enabled\'\]');
	$settings['images']['cart']['width'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'images\'\]\[\'cart\'\]\[\'width\'\]');
	$settings['fancybox']['enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'fancybox\'\]\[\'enabled\'\]');
	$settings['fancybox']['gallery'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'fancybox\'\]\[\'gallery\'\]');
	$settings['fancybox']['type'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'fancybox\'\]\[\'type\'\]');
	$settings['fancybox']['cyclic'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'fancybox\'\]\[\'cyclic\'\]');

	# Пагинация списка товаров
	$settings['products']['pagination_enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'products\'\]\[\'pagination_enabled\'\]');
	$settings['products']['pagination_items_enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'products\'\]\[\'pagination_items_enabled\'\]');
	$settings['products']['pagination'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'products\'\]\[\'pagination\'\]');

	# Сортировка товаров
	$settings['products']['sort_enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'products\'\]\[\'sort_enabled\'\]');
	$settings['products']['sort_items_enabled'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'products\'\]\[\'sort_items_enabled\'\]');
	$settings['products']['sort'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'products\'\]\[\'sort\'\]');
	$settings['products']['sort_type'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'products\'\]\[\'sort_type\'\]');

	# Настройки отображения товаров
	$settings['products']['title_link'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'products\'\]\[\'title_link\'\]');
	$settings['products']['sub_cat_link'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'products\'\]\[\'sub_cat_link\'\]');
	$settings['products']['sub_cat'] = $mAdmin->GetConfig($c_inc_file, 'config\[\'products\'\]\[\'sub_cat\'\]');

	include_once dirname(__FILE__) . '/../design/AdminSettings.tpl.php';
}