<?php

// f - имя файла 
// type - способ масштабирования 
// q - качество сжатия 
// src - исходное изображение 
// dest - результирующее изображение 
// w - ширина изображения 
// ratio - коэффициент пропорциональности 
// str - текстовая строка

//получаем параметры из GET-запроса
$f=$_GET["f"]; //имя файла
$r=$_GET["r"]; //поворот на 270 градусов (если r=1), 90 градусов (если r=2) или 180 градусов (если r=3)
$type=$_GET["type"]; //тип (0, 1, 2, 3 или 4)

// тип преобразования, если не указаны размеры 
if ($type == 0) $w = 70;  // квадратная 70x70 
if ($type == 1) $w = 90;  // квадратная 90x90 - категория?
if ($type == 2) $w = 150; // пропорциональная шириной 150 - списки
if ($type == 3) $w = 275; // пропорциональная шириной 275 - товар
if ($type == 4) $w = 630; // пропорциональная шириной 630 - товар увелич.
if ($type == 5) $w = 65; // пропорциональная шириной 65 - доп. товар
if ($type == 6) $w = 258; // пропорциональная шириной 260 - баннер

// качество jpeg по умолчанию 
if (!isset($q)) $q = 75;

// создаём исходное изображение на основе исходного файла и определяем его размеры

$source = imagecreatefromjpeg($f);

if ($r==270) $src = imagerotate($source,270,0); //поворот на 270 градусов
elseif ($r==90) $src = imagerotate($source,90,0); //поворот на 90 градусов
elseif ($r==180) $src = imagerotate($source,180,0); //поворот на 180 градусов
else $src = $source;

$w_src = imagesx($src); 
$h_src = imagesy($src);

header("Content-type: image/jpeg");

// если размер исходного изображения отличается от требуемого размера 
if ($w_src != $w) 
{
// операции для получения прямоугольного файла 
   if ($type==2 || $type==3 || $type==4 || $type==5 || $type==6)
   { 
       // вычисление пропорций 
       $ratio = $w_src/$w; 
       $w_dest = round($w_src/$ratio); 
       $h_dest = round($h_src/$ratio); 

       // создаём пустую картинку 
       // важно именно truecolor!, иначе будем иметь 8-битный результат 
       $dest = imagecreatetruecolor($w_dest,$h_dest); 
       imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);

//		create_watermark($image_old,$text_watermark,"comic.ttf",$red,$green,$blue,$alpha_level);
    }
// операции для получения квадратного файла 
    if (($type==0)||($type==1)) 
    { 
         // создаём пустую квадратную картинку 
         // важно именно truecolor!, иначе будем иметь 8-битный результат 
         $dest = imagecreatetruecolor($w,$w); 

         // вырезаем квадратную серединку по x, если фото горизонтальное 
         if ($w_src>$h_src) 
         imagecopyresampled($dest, $src, 0, 0,
                          round((max($w_src,$h_src)-min($w_src,$h_src))/2),
                          0, $w, $w, min($w_src,$h_src), min($w_src,$h_src)); 

         // вырезаем квадратную верхушку по y, 
         // если фото вертикальное (хотя можно тоже серединку) 
         if ($w_src<$h_src) 
         imagecopyresampled($dest, $src, 0, 0, 0, 0, $w, $w,
                          min($w_src,$h_src), min($w_src,$h_src)); 

         // квадратная картинка масштабируется без вырезок 
         if ($w_src==$h_src) 
         imagecopyresampled($dest, $src, 0, 0, 0, 0, $w, $w, $w_src, $w_src); 
     }

	// вывод картинки и очистка памяти 
	imagejpeg($dest,'',$q);
	imagedestroy($dest); 
	imagedestroy($src); 
}

else {
	imagejpeg($src,'',$q); 
	imagedestroy($dest); 
	imagedestroy($src); 
}
?>