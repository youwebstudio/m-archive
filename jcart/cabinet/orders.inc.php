<?php

# jCart v1.3.5
# http://conceptlogic.com/jcart/
# http://jcart.info

# Если пользователь не вошёл, скрипт умирает
if (!isset($_SESSION['id_user']))
    die;

# Подключение настроек
include_once dirname(__FILE__) . '/../jcart.inc.php';
$config = $jcart->config;

# Кодировка
// header('Content-type: text/html; charset=' . $config['encoding']);

# Подключение необходимых модулей
include_once dirname(__FILE__) . '/../modules/M_Orders.inc.php';
$mOrders = M_Orders::Instance();

# Установка текущей страницы
if (isset($_GET['page']))
	$navi['page'] = $_GET['page'];
else
	$navi['page'] = 1;

# Установка текущего заказа
if (isset($_GET['order']))
	$id_order = $_GET['order'];

# Вывод текущего заказа
if (isset($id_order))
{
	# Выбор заказа и его элементов
	$order = $mDB->GetItemById('custom', $id_order);
	$order_items = $mDB->GetItemsByParam('custom_item', 'id_custom', $id_order);

	# Определение статуса заказа
	$statuses = $config['statuses'];
	$orders['status'] = $statuses[$order['status']];

	# Разбор данных юридического лица
	$juridical = explode('|', $order['juridical']);

	# Определение валюты заказа
	$currencySymbol = $jcart->currencySymbol($config['currencyCode']);

	include_once dirname(__FILE__) . '/../design/UserOrderView.tpl.php';
}
# Вывод списка заказов
else
{
	# Выбор списка заказов с учётом пагинации
	$navi = $mDB->PaginateWithParam('custom', $navi['page'], $config['admin']['ordersList'], 'id_user', $_SESSION['id_user']);
	$orders = $mDB->GetPaginatedListWithParam('custom', $navi['start'], $config['admin']['ordersList'], 'id_user', $_SESSION['id_user']);

	# Определение общей суммы заказа
	$user['sum'] = $user['whole_sum'] = 0;
	foreach ($orders as $order)
	{
		$user['whole_sum'] += $order['sum'];

		if ($order['status'] == 1)
			$user['sum'] += $order['sum'];
	}

	# Определение валюты заказа
	$currencySymbol = $jcart->currencySymbol($config['currencyCode']);

	# Определение статуса заказов
	foreach ($orders as $i => $order)
		$orders[$i]['status'] = $config['statuses'][$order['status']];

	ob_start();
	include_once dirname(__FILE__) . '/../design/UserPagination.tpl.php';
	$pagination = ob_get_clean();

	include_once dirname(__FILE__) . '/../design/UserOrdersList.tpl.php';
}