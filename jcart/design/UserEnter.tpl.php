<script type="text/javascript">
function form_validator(form)
{
	if (form.user_email.value == '')
	{
		alert('Пожалуйста, укажите Ваш email адрес.');
		form.user_email.focus();
		return false;
	}
	if (form.user_password.value == '')
	{
		alert('Пожалуйста, укажите Ваш пароль доступа.');
		form.user_password.focus();
		return false;
	}
	if (confirm('Всё верно?'))
		form.user_nospam.value='<?= (isset($antispam)) ? $antispam : '' ?>';
	else
		return false;

	return true;
}
</script>

<? if (isset($form['message'])): ?>
<p style="color:red;"><?= $form['message'] ?></p>
<? endif ?>

<p>
	Введите ваш email и пароль, высланный вам на почту сразу после оформления первой покупки.
</p>

<form action="" method="post" onsubmit="return form_validator(this)">
	<p>
		<label>E-mail:</label><br>
		<input type="text" name="user_email" size="30" value="<?= (isset($email)) ? $email : '' ?>">
	</p>
	<p>
		<label>Пароль:</label><br>
		<input type="password" name="user_password" size="30" value="<?= (isset($pass)) ? $pass : '' ?>">
	</p>
	<p>
		<label>Запомнить меня:</label>
		<input type="checkbox" name="user_remember">
	</p>
	<p>
		<input type="hidden" name="user_nospam" value="">
		<input type="submit" name="user_enter" class="submit" value="Войти" title="Войти"> или <a href="<?= $config['sitelink'] . $config['auth']['authURL'] ?>?remind">Напомнить пароль</a>
	</p>
</form>

<p>
	<a href="<?= $config['sitelink'] ?>" title="Перейти на главную">Вернуться на главную страницу магазина</a>
</p>