<?/*
Шаблон корзины модального окна
==============
*/?>

<div style="display: none; padding: 20px;" id="modal_cart">
<!--modal-->
	<table>
		<thead>
			<tr>
				<th colspan="2">
					Ваша корзина
				</th>
			</tr>
		</thead>
		<tbody id="j-mod">
		
		<?if ($this->itemCount > 0): ?>
			<?foreach ($this->get_contents() as $item): ?>
			<tr>
				<td class="jcart-item-name">
					<? if ($config['images']['cart']['enabled'] == true && isset($item['cat']) && is_file(dirname(__FILE__) . '/../images/' . $item['cat'] . '/' . $item['id'] . '_0.jpg')): ?>
					<img width="<?=$config['images']['cart']['width']?>" src="<?= $config['sitelink'] . $config['jcartPath'] . 'images/' . $item['cat'] . '/' . $item['id'] ?>_0.jpg" alt="<?= $item['name'] ?>" style="float: right;">
					<? endif; ?>
					<?= $item['name'] ?>
					<br/>
					<? if ($item['color']): ?>Параметр1: <?= $item['color'] ?><br><? endif; ?>
					<? if ($item['size']): ?>Параметр2: <?= $item['size'] ?><br><? endif; ?>
					<? if ($item['param']): ?>Параметр3: <?= $item['param'] ?><? endif; ?>
				</td>

				<td class="jcart-item-price">
				 
					<input name="jcartItemId[]" type="hidden" value="<?= $item['id'] ?>" />
					<input name="jcartPath" type="hidden" value="<?= $config['jcartPath'] ?>" />
					<input id="jcartItemQty-<?= $item['id'] ?>" name="jcartItemQty[]" size="2" type="text" value="<?= $item['qty']?>" />
					<span><?= $item['unit'] ?></span>
					
					<div><!--price-->
					<? if ($item['subtotal'] != $item['price'] * $item['qty']): ?>
					<span class="striked"><?= $item['price'] * $item['qty'] ?></span> (-<?=max($item['discount'],$this->discount)?>%) <span class="attention">
					<? else: ?>
					<div><span class="attention">
					<? endif; ?>
					<?= number_format($item['subtotal'], $priceFormat['decimals'], $priceFormat['dec_point'], $priceFormat['thousands_sep']) ?> <?= $currencySymbol ?></span></div><!--/price-->
					</div>
					<a class="jcart-remove1" href="?jcartRemove=<?= $item['id'] ?>"><?= $config['text']['removeLink'] ?></a>
				</td>
			</tr>
			<?endforeach; ?>
		<? else: ?>
			<tr>
				<td id="jcart-empty" colspan="2"><?= $config['text']['emptyMessage'] ?></td>
			</tr>
		<? endif; ?>
		
		</tbody>
		<tfoot>
			<tr>
				<th colspan='2'>
					<a href="<?= $checkout ?>" class="f-right button">Оформить заказ</a>
					<!--subtotal-->
					<span id="jcart-subtotal"><?= $config['text']['subtotal'] ?>: <strong><?= number_format($this->subtotal, $priceFormat['decimals'], $priceFormat['dec_point'], $priceFormat['thousands_sep']) ?> <?= $currencySymbol ?></strong></span><!--/subtotal-->
				</th>
			</tr>
		</tfoot>
	</table>
<!--modal end-->
</div>
