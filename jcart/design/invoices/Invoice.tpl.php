<!DOCTYPE HTML PUBLIC  "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>Счет № <?= $id_order ?> от <?= date("d.m.Y") ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style type="text/css">
        body {
            width: 1000px;
            margin: 0 auto;
        }
        table {
            border-collapse: collapse; width: 100%;
            border: 1px solid #999;
        }
        td {
            border: 1px solid #999;
        }
        table.products td {
            text-align: center;
        }
        table.products td.left {
            text-align: left;
        }
    </style>
</head>
<body>
	<p>Внимание! Оплата данного счёта означает согласие с условиями поставки товара. Уведомление об оплате обязательно, в противном случае не гарантируется наличие товара на складе. Товар отпускается по факту прихода денег на р/с Поставщика, самовывозом, при наличии доверенности и паспорта.</p>

	<table>
	<tr>
		<td style="width: 60%">
			Получатель<br>ООО "Производственная компания "ОПТЭКС"
		</td>
		<td>
			Сч. № 40702810538000096156
		</td>
		<td>
			ИНН 7726367996<br>
			КПП 772601001
		</td>
	</tr>
	<tr>
		<td>
			Банк получателя<br>ПАО СБЕРБАНК Г. МОСКВА Г.МОСКВА
		</td>
		<td>
			БИК 044525225<br>
			Сч. № 30101810400000000225
		</td>
		<td>
			<!--...<br>
			...!-->
		</td>
	</tr>
	</table>

	<h3>Счет № <?= $id_order ?> от <?= date("d.m.Y") ?></h3>

	<hr><br>

	<table>
	<tr>
		<td style="width: 20%">
			Поставщик:
		</td>
		<td style="width: 70%">
			<strong>ООО "Производственная компания "ОПТЭКС", ИНН 7726367996, КПП 772601001, 117105, г. Москва, Нагорный проезд, дом 12, корпус 1</strong>
		</td>
	</tr>
	<!--<tr>
		<td>
			Факт. адрес:
		</td>
		<td>
			<strong>ООО «...», г. ..., ул. ..., д. ...  тел: ...</strong>
		</td>
	</tr>!-->
	<tr>
		<td>
			Покупатель:
		</td>
		<td>
		<? if (!isset($juridical) || !is_array($juridical)): ?>
			<strong>Физическое лицо, <?= $name['lastname'] ?> <?= $name['name'] ?> <?= $name['fathername'] ?></strong>
		<? else: ?>
			<strong>ИНН/КПП <?= (isset($juridical[0])) ? $juridical[0] : '' ?>/ <?= (isset($juridical[1])) ? $juridical[1] : '' ?>, <?= (isset($juridical[2])) ? $juridical[2] : '' ?></strong>
		<? endif ?>
		</td>
	</tr>
	</table>

	<br><br>

	<table class="products">
		<tr>
			<th>
				№
			</th>
			<th>
				Наименование товара
			</th>
			<th>
				Кол-во
			</th>
			<th>
				Ед.
			</th>
			<th>
				Цена
			</th>
			<th>
				Сумма
			</th>
		</tr>
		<? $total_sum = $count = 0; foreach($order_items as $order_item): ?>
		<tr>
			<td>
				<?= ++$count ?>
			</td>
			<td class="left">
				<?= $order_item['product'] ?>
			</td>
			<td>
				<?= $order_item['quantity'] ?>
			</td>
			<td>
				шт.
			</td>
			<td>
				<?= number_format($order_item['price'] * (1 - $order_item['discount']/100), 2, '.', '') ?>
			</td>
			<td>
				<?= $total = number_format($order_item['price'] * (1 - $order_item['discount']/100) * $order_item['quantity'], 2, '.', '') ?>
			</td>
		</tr>
		<?
		$total_sum += $total;
		?>
		<? endforeach ?>
	</table>

	<p><strong>Всего наименований <?= $count ?>, на сумму <?= number_format($total_sum, 2, '.', '') ?> <?= $currency ?></strong></p>
	<hr>

	<p>Условия оплаты: предоплата 100%</p>
	<p>Условия поставки: </p>
	<p>Руководитель _____________ (Чепурных А.В.)&nbsp;&nbsp;&nbsp;&nbsp;Бухгалтер _____________ (Чепурных А.В.)</p>
	<p><strong>Просим уведомить Продавца факсом (копия платежного поручения) об оплате.</strong></p>
	<p><strong><em>Счет действителен в течении 3-х банковских дней.</em></strong></p>
	<!-- <p>Автор документа: интернет-сайт (раздел продаж) ...</p> -->

    <script type="text/javascript">
           window.onload = print;
    </script>
</body>

</html>
