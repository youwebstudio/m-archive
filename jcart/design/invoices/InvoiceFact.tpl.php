<!DOCTYPE HTML PUBLIC  "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>Счет-фактура № <?= $id_order ?> от <?= date("d.m.Y") ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style type="text/css">
        body {
            width: 1000px;
            margin: 0 auto;
        }
        table {
            border-collapse: collapse; width: 100%;
            border: 1px solid #999;
        }
        td {
            border: 1px solid #999;
        }
        table.products td {
            text-align: center;
        }
        table.products td.left {
            text-align: left;
        }
    </style>
</head>
<body>
	<p>Приложение №1 к Правилам ведения журналов учета полученных и выставленных счетов-фактур, книг покупок и книг продаж при расчетах по налогу на добавленную стоимость, утвержденным постановлением Правительства Российской Федерации от 2 декабря 2000 г. N 91</p>

    <h3>Счет № <?= $id_order ?>-01 от <?= date("d.m.Y") ?></h3>

    <p>Продавец: ООО "Производственная компания "ОПТЭКС"<br>
    Адрес: 117105, г. Москва, Нагорный проезд, дом 12, корпус 1<br>
    ИНН/КПП продавца: 7726367996/772601001<br>
    Грузоотправитель и его адрес: Он же<br>
    Грузополучатель и его адрес: <?= $juridical[2]?>, <?= $address ?><br>
    К платежно-расчетному документу № <?= $id_order ?> от <?= date("d.m.Y") ?><br>
    Покупатель: <?= $juridical[2]?><br>
    Адрес: <?= $address ?><br>
    ИНН/КПП покупателя: <?= $juridical[0]?> / <?= $juridical[1]?></p>

	<p style="text-align: right;"><strong>Валюта: <?= $currency ?></strong></p>

	<table class="products">
		<tr>
			<th>
				Наименование товара (описание выполненных работ, оказанных услуг), имущественного права
			</th>
			<th>
                Единица измерения
			</th>
			<th>
                Количество
			</th>
			<th>
				Цена (тариф) за единицу измерения
			</th>
			<th>
                Стоимость товаров (работ, услуг), имущественных прав, всего без налога
			</th>
			<th>
                В том числе акциз
			</th>
			<th>
                Налоговая ставка
			</th>
			<th>
                Сумма налога
			</th>
			<th>
                Стоимость товаров (работ, услуг), имущественных прав, всего с учетом налога
			</th>
			<th>
                Страна происхождения
			</th>
			<th>
                Номер таможенной декларации
			</th>
		</tr>
		<tr>
			<td>
				1
			</td>
			<td>
                2
			</td>
			<td>
                3
			</td>
			<td>
				4
			</td>
			<td>
                5
			</td>
			<td>
                6
			</td>
			<td>
                7
			</td>
			<td>
                8
			</td>
			<td>
                9
			</td>
			<td>
                10
			</td>
			<td>
                11
			</td>
		</tr>
		<? 		$total_sum = $tax_sum = 0; foreach($order_items as $order_item): ?>
		<tr>
			<td class="left">
				<?= $order_item['product'] ?>
			</td>
			<td>
				шт.
			</td>
			<td>
				<?= number_format($order_item['quantity'], 2, '.', '') ?>
			</td>
			<td>
		        <?= number_format($order_item['price'] * (1 - $order_item['discount']/100), 2, '.', '') ?>
			</td>
			<td>
				<?= $total = number_format($order_item['price'] * $order_item['quantity'] * (1 - $order_item['discount']/100), 2, '.', '') ?>
			</td>
			<td>
                -
			</td>
			<td>
                <?= $tax = 18; ?>%
			</td>
			<td>
                <?= $tax_cost = number_format($total * $tax / 100, 2, '.', '') ?>
			</td>
			<td>
                <?= $total_cost = number_format($total + $tax_cost, 2, '.', '') ?>
			</td>
            <td>
                -
            </td>
            <td>
                -
            </td>
		</tr>
		<?
		$total_sum += $total_cost;
        $tax_sum += $tax_cost;
		?>
		<? endforeach ?>
        <tr>
            <td colspan="7" class="left">
                <strong>Всего к оплате</strong>
            </td>
            <td>
                <?= number_format($tax_sum, 2, '.', '') ?>
            </td>
            <td>
                <?= number_format($total_sum, 2, '.', '') ?>
            </td>
        </tr>
	</table>

	<p>Руководитель организации _____________ (Чепурных А.В.)&nbsp;&nbsp;&nbsp;&nbsp;Главный бухгалтер _____________ (Чепурных А.В.)</p>
    <p>Индивидуальный предприниматель _____________ ______________________</p>

    <script type="text/javascript">
           window.onload = print;
    </script>
</body>

</html>