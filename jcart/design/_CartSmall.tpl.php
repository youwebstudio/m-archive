<?/*
Шаблон краткой версии корзины
=============================
*/?>

<div class="smallCart">
<? if ($config['auth']['enabled'] == true): ?>
    <? if (isset($_SESSION['id_user'])): ?>
    <a href="<?= $config['sitelink'] . $config['cabinetURL'] ?>">Ваш кабинет</a> / <a href="<?= $config['sitelink'] . $config['auth']['authURL'] ?>?logout">Выйти</a><br><br>
    <? else: ?>
    <a href="<?= $config['sitelink'] . $config['auth']['authURL'] ?>?login">Войти</a> / <a href="<?= $config['sitelink'] . $config['auth']['authURL'] ?>?remind">Напомнить пароль</a><br><br>
    <? endif ?>
<? else: ?>
	<h3>Ваша корзина</h3>
<? endif ?>

<p><?= $this->itemCount ?> <?= $itemsText ?> на сумму <strong><?= number_format($this->subtotal, $priceFormat['decimals'], $priceFormat['dec_point'], $priceFormat['thousands_sep']) ?> <?= $currencySymbol ?></strong></p>

<br>
<p class="t-center">
    <a href="<?= $config['sitelink'] . $config['checkoutPath'] ?>" class="checkout_button" title="Перейти к оформлению заказа">Оформить покупку</a>
</p>
</div>

<?=(isset($modal_cart))?$modal_cart:''?>

<div id="jcart-tooltip"></div>