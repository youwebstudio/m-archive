<?/* Шаблон вывода товара. */?>
<div class="products clearfix">
	<?if(isset($_GET['sub_cat']) && $category_parent == $entry):?>
	<a href="<?= 'http://'. $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] ?>" class="clearfix" title="Назад"><h4>Назад</h4></a>
	<?elseif(isset($_GET['sub_cat'])):?>
	<a href="<?= 'http://'. $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] . '?sub_cat=' . $category_parent ?>" class="clearfix" title="Назад"><h4>Назад</h4></a>
	<?endif;?>
	<?if (isset($code_subcategories) && !empty($code_subcategories) && $jcart->config['products']['sub_cat'] == 'list'):?>
	<h3>Подкатегории</h3>
	<? foreach ($code_subcategories as $key => $sub_cat): ?>
		<?if($jcart->config['products']['sub_cat_link'] === true):?>
		<h4><a href="<?= 'http://'. $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] ?>?sub_cat=<?= $sub_cat ?>" title="<?= $title_subcategories[$key] ?>"><?= $title_subcategories[$key] ?></a></h4>
		<? else: ?>
		<h4><?= $title_subcategories[$key] ?></h4>
		<?endif;?>
	<?endforeach;?>

	<?=(isset($product_codes) && !empty($product_codes))?'<h3>Товары</h3>':''?>
	<?endif;?>
	<? foreach ($product_codes as $key => $id): ?>
	<div>
		<form method="post" action="" class="jcart">
			<input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken'];?>" />
			<input type="hidden" name="my_item_id" value="<?= $product_codes[$key] ?>" />
			<input type="hidden" name="my_item_name" value="<?= $product_titles[$key] ?>" />
			<input type="hidden" name="my_item_price" value="<?= $product_prices[$key] ?>" />
			<input type="hidden" name="my_item_discount" value="<?= (isset($product_discounts[$key]))? $product_discounts[$key] : '' ?>" />
			<input type="hidden" name="my_item_unit" value="<?= (isset($product_units[$key]))? $product_units[$key] : '' ?>" />
			<input type="hidden" name="my_item_cat" value="<?= (isset($product_cats[$key]))? $product_cats[$key] : ''?>" />
			
			<input type="hidden" name="my_item_color" value="<?= (isset($product_colors[$key]))? $product_colors[$key] : ''?>" />
			<input type="hidden" name="my_item_size" value="<?= (isset($product_sizes[$key]))? $product_sizes[$key] : ''?>" />
			<input type="hidden" name="my_item_param" value="<?= (isset($product_params[$key]))? $product_params[$key] : ''?>" />
			<input type="hidden" name="my_item_url" value="http://<?= $_SERVER['SERVER_NAME']  . $_SERVER['REQUEST_URI'] ?>" />


			<?if($jcart->config['products']['title_link'] === true):?>
			<h4><a href="<?= 'http://'. $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] ?>?product=<?= $product_codes[$key] ?>" title="<?= $product_titles[$key] ?>"><?= $product_titles[$key] ?></a></h4>
			<?else:?>
			<h4><?= $product_titles[$key] ?></h4>
			<?endif;?>

			<? if ($jcart->config['images']['enabled'] == true): ?><!--images-->

				<? if ($jcart->config['images']['resize']['enabled'] == true): ?><!--resize-->
				
					<? if ($jcart->config['fancybox']['enabled'] == true): ?><!--fancybox-->
						<?if ($prod_pics[$key] != false && isset($product_cats[$key])): ?>
						<? foreach ($prod_pics[$key] as $i => $pic):?>
							<a href="<?= $jcart->config['sitelink'] . $jcart->config['jcartPath'] . $jcart->config['images']['dir'] . '/' . $product_cats[$key]  . '/' . $pic?>" id="<?= $pic ?>" title="<?= $product_titles[$key] ?>" class="fancybox nolink" <? if ($jcart->config['fancybox']['gallery'] == true): ?>rel="<?= ($jcart->config['fancybox']['type'] == 'product')? $product_codes[$key]: (($jcart->config['fancybox']['type'] == 'all')?'gallery':$product_cats[$key]) ?>"<? endif ?>>
								<?if($i==0):?>
									<img src="<?= $jcart->config['sitelink'] . $jcart->config['jcartPath'] . $jcart->config['images']['dir'] . '/' . $product_cats[$key] . '/thumb/'. $product_codes[$key] ?>_0.jpg" alt="<?= $product_titles[$key] ?>">
								<?endif?>
							</a>
						<?endforeach?>
						<? endif ?>
					<? else: ?><!--/fancybox-->
						<? if (is_file(dirname(__FILE__) . '/../' . $jcart->config['images']['dir'] . '/' . $product_cats[$key] . '/thumb/' . $product_codes[$key] . '_0.jpg')): ?>
							<img src="<?= $jcart->config['sitelink'] . $jcart->config['jcartPath'] . $jcart->config['images']['dir'] . '/' . $product_cats[$key] . '/thumb/'. $product_codes[$key] ?>_0.jpg" alt="<?= $product_titles[$key] ?>" class="left">
						<? endif ?>
					<? endif ?>

				<? else: ?><!--/resize-->
					<? if (is_file(dirname(__FILE__) . '/../' . $jcart->config['images']['dir'] . '/' . $product_cats[$key] . '/' . $product_codes[$key] . '_0.jpg')): ?>
					<img src="<?= $jcart->config['sitelink'] . $jcart->config['jcartPath'] . $jcart->config['images']['dir'] . '/' . $product_cats[$key] . '/' . $product_codes[$key] ?>_0.jpg" alt="<?= $product_titles[$key] ?>">
					<br>
					<? endif ?>
				<? endif ?>

			<? endif ?><!--/images-->

			<div class="about clearfix">
				<?= $product_descriptions[$key] ?>
			</div>

			<? if ($product_discounts[$key] != 0): ?><!--discounts-->
			<span class="price">Цена: <span class="striked"><?= $product_prices[$key] ?></span> <span class="attention"><?= $product_prices[$key] * (1 - $product_discounts[$key]/100) ?> <?= $currencySymbol ?></span></span>
			<br>
			<div id="countdown-<?=$product_codes[$key]?>" class="attention small"></div>
			<script type="text/javascript">
				$(function() {
					$("#countdown-<?=$product_codes[$key]?>").countdown(new Date(<?=$product_discount_dates['Y'][$key]?>, <?=$product_discount_dates['m'][$key] - 1?>, <?=$product_discount_dates['d'][$key]?>, <?=$product_discount_dates['H'][$key]?>, <?=$product_discount_dates['i'][$key]?>, <?=$product_discount_dates['s'][$key]?>), {prefix:'Скидка: ', finish: 'Акция закончилась!',reload: true});
				});
			</script>
			<? else: ?><!--/discounts-->

			<span class="price">Цена: <?= $product_prices[$key] ?> <?= $currencySymbol ?></span><br>
			<? endif; ?>
			<label><input type="text" name="my_item_qty" value="1" size="2" /> <?= $product_units[$key] ?></label>
			<input type="submit" name="my_add_button" value="В корзину" class="button" />

			<? if ($jcart->config['download']['enabled'] == true && !empty($product_types[$key])): ?>
			<br>
			<?#todo update?>
			<a href="<?= $jcart->config['sitelink'] . $jcart->config['jcartPath'] ?>update/<?= $product_codes[$key] ?>/" title="Обновить <?= $product_titles[$key] ?>">Обновление</a>
			<? endif ?>
		</form>
	</div>
	<? endforeach; ?>
</div>