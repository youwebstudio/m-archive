<?
#pro
include_once dirname(__FILE__) . '/../design/AdminHeader.tpl.php';
?>

<div class = "container-fluid">

	<div id="top"></div>
	<ul class="breadcrumb">
		<li>
			<a title="Вернуться к началу" href="index.php"><i class="icon-home"></i>&nbsp;Админ.интерфейс</a>
			<span class="divider">/</span>
		</li>
		<li>
			<a href="settings.php">Настройки</a>
			<span class="divider">/</span>
		</li>
	</ul>

	<form action = "" method = "post" class = "form-horizontal">
		<div class = "well well-inverse message-block">
			<div class="alert alert-info">
				<h4>ВНИМАНИЕ!</h4>
				В полях не должно быть одинарных кавычек! Записи с кавычками будут проигнорированы при записи.
			</div>
			<? if (isset($_SESSION['tmp_message'])): ?>
			<div class="alert alert-error">
				<h4>ВНИМАНИЕ!</h4>
				<?= $_SESSION['tmp_message'] ?>
				<?unset($_SESSION['tmp_message']);?>
			</div>
			<? endif; ?>

			<noscript>
				<div class="alert">
					<h4>ВНИМАНИЕ!</h4>
					Для полноценного отображения этой страници необходимо активировать джава скрипт.
				</div>
			</noscript>
		</div>

		<div class = "well well-inverse">
			<h2 class="ruler" title="Кликните, чтобы развернуть">Основная информация о вашем сайте</h2>

			<div class = "row-fluid">
				<div class = "span6">
					<div class = "control-group">
						<label>Название:</label>
						<input class = "span8" type = "text" name = "settings[sitename]" value = "<?= (isset($settings['sitename'])) ? $settings['sitename'] : '' ?>">
					</div>
					<div class = "control-group">
						<label>Полный адрес магазина (со слешем на конце):</label>
						<input class = "span8" type = "text" name = "settings[sitelink]" value = "<?= (isset($settings['sitelink'])) ? $settings['sitelink'] : '' ?>">
					</div>
					<div class = "control-group">
						<label>Путь к файлам jcart:</label>
						<input class = "span8" type = "text" name = "settings[jcartPath]" value = "<?= (isset($settings['jcartPath'])) ? $settings['jcartPath'] : '' ?>">
					</div>
					<div class = "control-group">
						<label>Путь к странице оформления заказа:</label>
						<input class = "span8" type = "text" name = "settings[checkoutPath]" value = "<?= (isset($settings['checkoutPath'])) ? $settings['checkoutPath'] : ''?>">
					</div>
					<div class="control-group">
						<label>Кодировка. Может быть windows-1251 или utf-8. Лучше utf-8.</label>
						<input class="span8" type="text" name="settings[encoding]" value="<?= (isset($settings['encoding'])) ? $settings['encoding'] : ''?>">
					</div>
					<div class = "control-group checkbox">
						<input type = "checkbox" value = "true" id = "smallСart" name = "settings[smallCart]" <?= (isset($settings['smallCart']) && $settings['smallCart'] == 'true') ? 'checked="checked"' : '' ?>>
						<label for = "smallСart">Использовать краткую версию корзины?:</label>
					</div>
				</div>

				<div class = "span6">
					<div class = "control-group">
						<label>E-mail адрес продавца (на который отправляется заказ)</label>
						<input class = "span8" type = "text" name = "settings[email][receiver]" value = "<?= (isset($settings['email']['receiver'])) ? $settings['email']['receiver'] : '' ?>">
					</div>
					<div class = "control-group">
						<label>E-mail адрес робота (от имени которого отправляется ответное письмо покупателю)</label>
						<input class = "span8" type = "text" name = "settings[email][answer]" value = "<?= (isset($settings['email']['answer'])) ? $settings['email']['answer'] : '' ?>">
					</div>
					<div class = "control-group">
						<label>Имя робота (от имени которого отправляется ответное письмо покупателю)</label>
						<input class = "span8" type = "text" name = "settings[email][answerName]" value = "<?= (isset($settings['email']['answerName'])) ? $settings['email']['answerName'] : '' ?>">
					</div>
					<div class = "control-group">
						<label>Валюта магазина: (RUR, UAH, USD, EUR)</label>
						<input class = "span8" type = "text" name = "settings[currencyCode]" value = "<?= (isset($settings['currencyCode'])) ? $settings['currencyCode'] : '' ?>">
					</div>
					<div class="control-group checkbox">
						<input type="checkbox" name="settings[display_errors]" id="display_errors" <?= (isset($settings['display_errors']) && $settings['display_errors'] == 'true') ? 'checked="checked"' : '' ?>>
						<label for="display_errors">Показывать ошибки?:</label>
					</div>
					<div class="control-group checkbox">
						<input type="checkbox" name="settings[log_errors]" id="log_errors" <?= (isset($settings['log_errors']) && $settings['log_errors'] == 'true') ? 'checked="checked"' : '' ?>>
						<label for="log_errors">Вести лог ошибок?:</label>
					</div>
				</div>
			</div>
			<h2 class="ruler" title="Кликните, чтобы развернуть">Настройки подключения к БД</h2>

			<div>
				<div class = "row-fluid">
					<div class = "span6">
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "database_enabled" name = "settings[database][enabled]" <?= (isset($settings['database']['enabled']) && $settings['database']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "database_enabled">Использовать БД?</label>
						</div>
					</div>
					<div class = "span6">
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "database_quantityEnabled" name = "settings[database][quantityEnabled]" <?= (isset($settings['database']['quantityEnabled']) && $settings['database']['quantityEnabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "database_quantityEnabled">Изменять количество при заказе?</label>
						</div>
					</div>
				</div>
				<div class = "row-fluid">
					<div class = "span6">
						<div class = "control-group">
							<label>Хост:</label>
							<input class = "span9" type = "text" name = "settings[database][host]" value = "<?= (isset($settings['database']['host'])) ? $settings['database']['host'] : '' ?>">
						</div>
						<div class = "control-group">
							<label>База данных:</label>
							<input class = "span9" type = "text" name = "settings[database][name]" value = "<?= (isset($settings['database']['name'])) ? $settings['database']['name'] : '' ?>">
						</div>
					</div>
					<div class = "span6">
						<div class = "control-group">
							<label>Пользователь:</label>
							<input class = "span9" type = "text" name = "settings[database][user]" value = "<?= (isset($settings['database']['user'])) ? $settings['database']['user'] : '' ?>">
						</div>
						<div class = "control-group">
							<label>Пароль:</label>
							<input class = "span9" type = "password" name = "settings[database][pass]" value = "<?= (isset($settings['database']['pass'])) ? $settings['database']['pass'] : '' ?>">
						</div>
					</div>
				</div>
			</div>
			<h2 class="ruler" title="Кликните, чтобы развернуть">Формы оплаты</h2>

			<div>
				<div class = "row-fluid">
					<div class = "span6">
						<fieldset title = "Наложенный платеж">
							<legend>Наложенный платеж</legend>
							<div class = "control-group checkbox">
								<input type = "checkbox" value = "true" id = "email_enabled" name = "settings[payments][email][enabled]" <?= (isset($settings['payments']['email']['enabled']) && $settings['payments']['email']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
								<label for = "email_enabled">Использовать?</label>
							</div>
							<div class = "control-group">
								<label>Название (в выпадающем списке):</label>
								<input class = "span11" type = "text" name = "settings[payments][email][title]" value = "<?= (isset($settings['payments']['email']['title'])) ? $settings['payments']['email']['title'] : '' ?>">
							</div>
							<div class = "control-group">
								<label>Информация (выводится при выборе этой формы оплаты):</label>
								<input class = "span11" type = "text" name = "settings[payments][email][info]" value = "<?= (isset($settings['payments']['email']['info'])) ? $settings['payments']['email']['info'] : '' ?>">
							</div>
							<div class = "control-group">
								<label>Дополнительная информация (приходит в письме заказа):</label>
								<input class = "span11" type = "text" name = "settings[payments][email][details]" value = "<?= (isset($settings['payments']['email']['details'])) ? $settings['payments']['email']['details'] : '' ?>">
							</div>
						</fieldset>
						<br>
					</div>
					<div class = "span6">
						<fieldset title = "Отправка курьеру">
							<legend>Отправка курьеру</legend>
							<div class = "control-group checkbox">
								<input type = "checkbox" value = "true" id = "courier_enabled" name = "settings[payments][courier][enabled]" <?= (isset($settings['payments']['courier']['enabled']) && $settings['payments']['courier']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
								<label for = "courier_enabled">Использовать?</label>
							</div>
							<div class = "control-group">
								<label>Название (в выпадающем списке):</label>
								<input class = "span11" type = "text" name = "settings[payments][courier][title]" value = "<?= (isset($settings['payments']['courier']['title'])) ? $settings['payments']['courier']['title'] : '' ?>">
							</div>
							<div class = "control-group">
								<label>Информация (выводится при выборе этой формы оплаты):</label>
								<input class = "span11" type = "text" name = "settings[payments][courier][info]" value = "<?= (isset($settings['payments']['courier']['info'])) ? $settings['payments']['courier']['info'] : '' ?>">
							</div>
							<div class = "control-group">
								<label>Дополнительная информация (приходит в письме заказа):</label>
								<input class = "span11" type = "text" name = "settings[payments][courier][details]" value = "<?= (isset($settings['payments']['courier']['details'])) ? $settings['payments']['courier']['details'] : '' ?>">
							</div>
						</fieldset>
						<br>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						<fieldset title="Оплата по безналу">
							<legend>Оплата по безналу</legend>
							<div class="control-group checkbox">
								<input type="checkbox" value="true" id="invoice_enabled" name="settings[payments][invoice][enabled]" <?= (isset($settings['payments']['invoice']['enabled']) && $settings['payments']['invoice']['enabled']=='true') ? 'checked="checked"' : '' ?>>
								<label for="invoice_enabled">Использовать?</label>
							</div>
							<div class="control-group">
								<label>Название (в выпадающем списке):</label>
								<input class="span11" type="text" name="settings[payments][invoice][title]" value="<?= (isset($settings['payments']['invoice']['title'])) ? $settings['payments']['invoice']['title'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Информация (выводится при выборе этой формы оплаты):</label>
								<input class="span11" type="text" name="settings[payments][invoice][info]" value="<?= (isset($settings['payments']['invoice']['info'])) ? $settings['payments']['invoice']['info'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Дополнительная информация (приходит в письме заказа):</label>
								<input class="span11" type="text" name="settings[payments][invoice][details]" value="<?= (isset($settings['payments']['invoice']['details'])) ? $settings['payments']['invoice']['details'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Счёт будет приходить покупателю в письме о получении заказа. Предусмотрены счета для физических и юридических лиц. Юридические лица также получат счёт-фактуру.</label>
								<br>
								<label>Также потребуется изменить данные в шаблонах счетов. Файлы шаблонов находятся впапке <strong>jcart/design/invoices/</strong>. Вам понадобятся файлы <strong>Invoice.tpl.php</strong> и <strong>InvoiceFact.tpl.php</strong></label>
							</div>
						</fieldset>
						<br>
					</div>
					<div class="span6">
						<fieldset title="Оплата по LiqPay">
							<legend>Оплата по LiqPay</legend>
							<div class="control-group checkbox">
								<input type="checkbox" value="true" id="liqpay_enabled" name="settings[payments][liqpay][enabled]" <?= (isset($settings['payments']['liqpay']['enabled']) && $settings['payments']['liqpay']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
								<label for="liqpay_enabled">Использовать?</label>
							</div>
							<div class="control-group">
								<label>Название (в выпадающем списке):</label>
								<input class="span11" type="text" name="settings[payments][liqpay][title]" value="<?= (isset($settings['payments']['liqpay']['title'])) ? $settings['payments']['liqpay']['title'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Информация (выводится при выборе этой формы оплаты):</label>
								<input class="span11" type="text" name="settings[payments][liqpay][info]" value="<?= (isset($settings['payments']['liqpay']['info'])) ? $settings['payments']['liqpay']['info'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Дополнительная информация (приходит в письме заказа):</label>
								<input class="span11" type="text" name="settings[payments][liqpay][details]" value="<?= (isset($settings['payments']['liqpay']['details'])) ? $settings['payments']['liqpay']['details'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Описание оплаты для мерчанта:</label>
								<input class="span11" type="text" name="settings[payments][liqpay][description]" value="<?= (isset($settings['payments']['liqpay']['description'])) ? $settings['payments']['liqpay']['description'] : '' ?>">
							</div>
							<div class="control-group">
								<label>ID мерчанта в LiqPay (взять после регистрации магазина на сайте <a href="http://liqpay.com">LiqPay.com</a>):</label>
								<input class="span11" type="text" name="settings[payments][liqpay][id]" value="<?= (isset($settings['payments']['liqpay']['id'])) ? $settings['payments']['liqpay']['id'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Подпись (взять после регистрации магазина на сайте <a href="http://liqpay.com">LiqPay.com</a>):</label>
								<input class="span11" type="text" name="settings[payments][liqpay][sign]" value="<?= (isset($settings['payments']['liqpay']['sign'])) ? $settings['payments']['liqpay']['sign'] : '' ?>">
							</div>
						</fieldset>
						<br>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						<fieldset title="Оплата с помощью RoboKassa">
							<legend>Оплата с помощью RoboKassa</legend>
							<div class="control-group checkbox">
								<input type="checkbox" value="true" id="robokassa_enabled" name="settings[payments][robokassa][enabled]" <?= (isset($settings['payments']['robokassa']['enabled']) && $settings['payments']['robokassa']['enabled']=='true') ? 'checked="checked"' : '' ?>>
								<label for="robokassa_enabled">Использовать?</label>
							</div>
							<div class="control-group">
								<label>Название (в выпадающем списке):</label>
								<input class="span11" type="text" name="settings[payments][robokassa][title]" value="<?= (isset($settings['payments']['robokassa']['title'])) ? $settings['payments']['robokassa']['title'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Информация (выводится при выборе этой формы оплаты):</label>
								<input class="span11" type="text" name="settings[payments][robokassa][info]" value="<?= (isset($settings['payments']['robokassa']['info'])) ? $settings['payments']['robokassa']['info'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Дополнительная информация (приходит в письме заказа):</label>
								<input class="span11" type="text" name="settings[payments][robokassa][details]" value="<?= (isset($settings['payments']['robokassa']['details'])) ? $settings['payments']['robokassa']['details'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Описание оплаты для мерчанта:</label>
								<input class="span11" type="text" name="settings[payments][robokassa][description]" value="<?= (isset($settings['payments']['robokassa']['description'])) ? $settings['payments']['robokassa']['description'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Логин в RoboKassa (взять после регистрации магазина на сайте <a href="http://robokassa.ru">RoboKassa.ru</a>):</label>
								<input class="span11" type="text" name="settings[payments][robokassa][login]" value="<?= (isset($settings['payments']['robokassa']['login'])) ? $settings['payments']['robokassa']['login'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Пароль №1 в RoboKassa (взять после регистрации магазина на сайте <a href="http://robokassa.ru">RoboKassa.ru</a>):</label>
								<input class="span11" type="text" name="settings[payments][robokassa][pass1]" value="<?= (isset($settings['payments']['robokassa']['pass1'])) ? $settings['payments']['robokassa']['pass1'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Пароль №2 RoboKassa (взять после регистрации магазина на сайте <a href="http://robokassa.ru">RoboKassa.ru</a>):</label>
								<input class="span11" type="text" name="settings[payments][robokassa][pass2]" value="<?= (isset($settings['payments']['robokassa']['pass2'])) ? $settings['payments']['robokassa']['pass2'] : '' ?>">
							</div>
							<div class="control-group checkbox">
								<input type="checkbox" value="true" id="robokassa_test" name="settings[payments][robokassa][test]" <?= (isset($settings['payments']['robokassa']['test']) && $settings['payments']['robokassa']['test']=='true') ? 'checked="checked"' : '' ?>>
								<label for="robokassa_test">Использовать тестовый режим?:</label>
							</div>
						</fieldset>
						<br>
					</div>
					<div class="span6">
						<fieldset title="Оплата с помощью InterKassa">
							<legend>Оплата с помощью InterKassa</legend>
							<div class="control-group checkbox">
								<input type="checkbox" value="true" id="interkassa_enabled" name="settings[payments][interkassa][enabled]" <?= (isset($settings['payments']['interkassa']['enabled']) && $settings['payments']['interkassa']['enabled']=='true') ? 'checked="checked"' : '' ?>>
								<label for="interkassa_enabled">Использовать?</label>
							</div>
							<div class="control-group">
								<label>Название (в выпадающем списке):</label>
								<input class="span11" type="text" name="settings[payments][interkassa][title]" value="<?= (isset($settings['payments']['interkassa']['title'])) ? $settings['payments']['interkassa']['title'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Информация (выводится при выборе этой формы оплаты):</label>
								<input class="span11" type="text" name="settings[payments][interkassa][info]" value="<?= (isset($settings['payments']['interkassa']['info'])) ? $settings['payments']['interkassa']['info'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Дополнительная информация (приходит в письме заказа):</label>
								<input class="span11" type="text" name="settings[payments][interkassa][details]" value="<?= (isset($settings['payments']['interkassa']['details'])) ? $settings['payments']['interkassa']['details'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Описание оплаты для мерчанта:</label>
								<input class="span11" type="text" name="settings[payments][interkassa][description]" value="<?= (isset($settings['payments']['interkassa']['description'])) ? $settings['payments']['interkassa']['description'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Идентификатор магазина (взять после регистрации магазина на сайте <a href="http://interkassa.com">InterKassa.com</a>):</label>
								<input class="span11" type="text" name="settings[payments][interkassa][shop_id]" value="<?= (isset($settings['payments']['interkassa']['shop_id'])) ? $settings['payments']['interkassa']['shop_id'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Cекретный ключ (взять после регистрации магазина на сайте <a href="http://interkassa.com">InterKassa.com</a>):</label>
								<input class="span11" type="text" name="settings[payments][interkassa][secret_key]" value="<?= (isset($settings['payments']['interkassa']['secret_key'])) ? $settings['payments']['interkassa']['secret_key'] : '' ?>">
							</div>
						</fieldset>
						<br>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						<fieldset title="Оплата с помощью A1Pay">
							<legend>Оплата с помощью A1Pay</legend>
							<div class="control-group checkbox">
								<input type="checkbox" value="true" id="a1pay_enabled" name="settings[payments][a1pay][enabled]" <?= (isset($settings['payments']['a1pay']['enabled']) && $settings['payments']['a1pay']['enabled']=='true') ? 'checked="checked"' : '' ?>>
								<label for="a1pay_enabled">Использовать?</label>
							</div>
							<div class="control-group">
								<label>Название (в выпадающем списке):</label>
								<input class="span11" type="text" name="settings[payments][a1pay][title]" value="<?= (isset($settings['payments']['a1pay']['title'])) ? $settings['payments']['a1pay']['title'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Информация (выводится при выборе этой формы оплаты):</label>
								<input class="span11" type="text" name="settings[payments][a1pay][info]" value="<?= (isset($settings['payments']['a1pay']['info'])) ? $settings['payments']['a1pay']['info'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Дополнительная информация (приходит в письме заказа):</label>
								<input class="span11" type="text" name="settings[payments][a1pay][details]" value="<?= (isset($settings['payments']['a1pay']['details'])) ? $settings['payments']['a1pay']['details'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Описание оплаты для мерчанта:</label>
								<input class="span11" type="text" name="settings[payments][a1pay][description]" value="<?= (isset($settings['payments']['a1pay']['description'])) ? $settings['payments']['a1pay']['description'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Секретное слово (установить также на сайте <a href="http://a1pay.ru">A1Pay.ru</a>):</label>
								<input class="span11" type="text" name="settings[payments][a1pay][secret_word]" value="<?= (isset($settings['payments']['a1pay']['secret_word'])) ? $settings['payments']['a1pay']['secret_word'] : '' ?>">
							</div>
							<div class="control-group">
								<label>Секретный ключ (взять при создании кнопки оплаты на сайте <a href="http://a1pay.ru">A1Pay.ru</a>):</label>
								<input class="span11" type="text" name="settings[payments][a1pay][secret_key]" value="<?= (isset($settings['payments']['a1pay']['secret_key'])) ? $settings['payments']['a1pay']['secret_key'] : '' ?>">
							</div>
						</fieldset>
						<br>
					</div>
				</div>
			</div>
			<h2 class="ruler" title="Кликните, чтобы развернуть">Способы доставки</h2>

			<div>
				<div class = "row-fluid">
					<div class = "span6">
						<fieldset title = "Способ доставки №1">
							<legend>Способ доставки №1</legend>
							<div class = "control-group checkbox">
								<input type = "checkbox" value = "true" id = "deliveries_1_enabled" name = "settings[deliveries][1][enabled]" <?= (isset($settings['deliveries']['1']['enabled']) && $settings['deliveries']['1']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
								<label for = "deliveries_1_enabled">Использовать?</label>
							</div>
							<div class = "control-group">
								<label>Название (в выпадающем списке):</label>
								<input class = "span11" type = "text" name = "settings[deliveries][1][title]" value = "<?= (isset($settings['deliveries']['1']['title'])) ? $settings['deliveries']['1']['title'] : '' ?>">
							</div>
							<div class = "control-group">
								<label>Информация (выводится при выборе этого способа доставки):</label>
								<input class = "span11" type = "text" name = "settings[deliveries][1][info]" value = "<?= (isset($settings['deliveries']['1']['info'])) ? $settings['deliveries']['1']['info'] : '' ?>">
							</div>
							<div class = "control-group">
								<label>Стоимость доставки (добавляется к сумме заказа):</label>
								<input class = "span11" type = "text" name = "settings[deliveries][1][cost]" value = "<?= (isset($settings['deliveries']['1']['cost'])) ? $settings['deliveries']['1']['cost'] : '' ?>">
							</div>
							<div class = "control-group">
								<label>Детали (будут высланы на email):</label>
								<input class = "span11" type = "text" name = "settings[deliveries][1][details]" value = "<?= (isset($settings['deliveries']['1']['details'])) ? $settings['deliveries']['1']['details'] : '' ?>">
							</div>
						</fieldset>
					</div>
					<div class = "span6">
						<fieldset title = "Способ доставки №2">
							<legend>Способ доставки №2</legend>
							<div class = "control-group checkbox">
								<input type = "checkbox" value = "true" id = "deliveries_2_enabled" name = "settings[deliveries][2][enabled]" <?= (isset($settings['deliveries']['2']['enabled']) && $settings['deliveries']['2']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
								<label for = "deliveries_2_enabled">Использовать?</label>
							</div>
							<div class = "control-group">
								<label>Название (в выпадающем списке):</label>
								<input class = "span11" type = "text" name = "settings[deliveries][2][title]" value = "<?= (isset($settings['deliveries']['2']['title'])) ? $settings['deliveries']['2']['title'] : '' ?>">
							</div>
							<div class = "control-group">
								<label>Информация (выводится при выборе этого способа доставки):</label>
								<input class = "span11" type = "text" name = "settings[deliveries][2][info]" value = "<?= (isset($settings['deliveries']['2']['info'])) ? $settings['deliveries']['2']['info'] : '' ?>">
							</div>
							<div class = "control-group">
								<label>Стоимость доставки (добавляется к сумме заказа):</label>
								<input class = "span11" type = "text" name = "settings[deliveries][2][cost]" value = "<?= (isset($settings['deliveries']['2']['cost'])) ? $settings['deliveries']['2']['cost'] : '' ?>">
							</div>
							<div class = "control-group">
								<label>Детали (будут высланы на email):</label>
								<input class = "span11" type = "text" name = "settings[deliveries][2][details]" value = "<?= (isset($settings['deliveries']['2']['details'])) ? $settings['deliveries']['2']['details'] : '' ?>">
							</div>
						</fieldset>
					</div>
				</div>

				<div class = "row-fluid">
					<div class = "span6">
						<fieldset title = "Способ доставки №3">
							<legend>Способ доставки №3</legend>
							<div class = "control-group checkbox">
								<input type = "checkbox" value = "true" id = "deliveries_3_enabled" name = "settings[deliveries][3][enabled]" <?= (isset($settings['deliveries']['3']['enabled']) && $settings['deliveries']['3']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
								<label for = "deliveries_3_enabled">Использовать?</label>
							</div>
							<div class = "control-group">
								<label>Название (в выпадающем списке):</label>
								<input class = "span11" type = "text" name = "settings[deliveries][3][title]" value = "<?= (isset($settings['deliveries']['3']['title'])) ? $settings['deliveries']['3']['title'] : '' ?>">
							</div>
							<div class = "control-group">
								<label>Информация (выводится при выборе этого способа доставки):</label>
								<input class = "span11" type = "text" name = "settings[deliveries][3][info]" value = "<?= (isset($settings['deliveries']['3']['info'])) ? $settings['deliveries']['3']['info'] : '' ?>">
							</div>
							<div class = "control-group">
								<label>Стоимость доставки (добавляется к сумме заказа):</label>
								<input class = "span11" type = "text" name = "settings[deliveries][3][cost]" value = "<?= (isset($settings['deliveries']['3']['cost'])) ? $settings['deliveries']['3']['cost'] : '' ?>">
							</div>
							<div class = "control-group">
								<label>Детали (будут высланы на email):</label>
								<input class = "span11" type = "text" name = "settings[deliveries][3][details]" value = "<?= (isset($settings['deliveries']['3']['details'])) ? $settings['deliveries']['3']['details'] : '' ?>">
							</div>
						</fieldset>
						<br>
					</div>
				</div>
			</div>
			<h2 class="ruler" title="Кликните, чтобы развернуть">Дополнительные параметры</h2>

			<div class = "row-fluid">
				<div class = "span6">
					<fieldset title = "Мелочи">
						<legend>Мелочи</legend>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "tooltipEnabled" name = "settings[tooltip]" <?= (isset($settings['tooltip']) && $settings['tooltip'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "tooltipEnabled">Отображать всплывающую подсказку после добавления товара в
								корзину?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "modalEnabled" name = "settings[modal]" <?= (isset($settings['modal']) && $settings['modal'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "modalEnabled">Отображать модальную корзину при добавлении товара в корзину?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "decimalQtys" name = "settings[decimalQtys]" <?= (isset($settings['decimalQtys']) && $settings['decimalQtys'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "decimalQtys">Разрешить десятичные дроби в поле количества?</label>
						</div>
						<div class = "control-group">
							<label>Если да, то сколько знаков после запятой разрешено?</label>
							<input class = "input-mini validate1" type = "text" name = "settings[decimalPlaces]" value = "<?= (isset($settings['decimalPlaces'])) ? $settings['decimalPlaces'] : '' ?>">
						</div>

						<div class = "control-group">
							<label>Формат числа для цены (см: <a href = "http://php.net/manual/ru/function.number-format.php">документацию</a>):</label>
							<label>Количество знаков после запятой</label>
							<input class = "input-mini validate1" type = "text" name = "settings[priceFormat_decimals]" value = "<?= (isset($settings['priceFormat']['decimals'])) ? $settings['priceFormat']['decimals'] : '' ?>">
						</div>
						<div class = "control-group">
							<label>Разделитель целого и дроби</label>
							<input class = "input-mini" type = "text" name = "settings[priceFormat_dec_point]" value = "<?= (isset($settings['priceFormat']['dec_point'])) ? $settings['priceFormat']['dec_point'] : '' ?>">
						</div>
						<div class = "control-group">
							<label>Разделитель тысяч</label>
							<input class = "input-mini" type = "text" name = "settings[priceFormat_thousands_sep]"<?= (isset($settings['priceFormat']['thousands_sep'])) ? $settings['priceFormat']['thousands_sep'] : '' ?>">
						</div>

						<div class = "control-group">
							<label>Секретное слово для генерации антиспама:</label>
							<input class = "span4" type = "text" name = "settings[secretWord]" value = "<?= (isset($settings['secretWord'])) ? $settings['secretWord'] : '' ?>">
						</div>

						<div class="control-group checkbox">
							<input type="checkbox" value="true" id="products_title_link" name="settings[products][title_link]" <?= (isset($settings['products']['title_link']) && $settings['products']['title_link']=='true') ? 'checked="checked"' : '' ?>>
							<label for="products_title_link">Отображать заголовок товара как ссылку на товар?:</label>
						</div>
						<div class="control-group checkbox">
							<input type="checkbox" value="true" id="products_sub_cat_link" name="settings[products][sub_cat_link]" <?= (isset($settings['products']['sub_cat_link']) && $settings['products']['sub_cat_link']=='true') ? 'checked="checked"' : '' ?>>
							<label for="products_sub_cat_link">Отображать заголовок подкатегории как ссылку на подкатегорию?:</label>
						</div>

						<div class="control-group">
							<label>Отображение подкатегорий:</label>
							<div class="input-prepend">
								<span class="add-on"><i class="icon-check"></i></span>
								<select class="input-large" name="settings[products][sub_cat]">
									<option value="list" <?=(isset($settings['products']['sub_cat']) && $settings['products']['sub_cat'] == 'list')?'selected="selected"':''?>>выводить список названий</option>
									<option value="products" <?=(isset($settings['products']['sub_cat']) && $settings['products']['sub_cat'] == 'products')?'selected="selected"':''?>>выводить товары</option>
									<option value="none" <?=(isset($settings['products']['sub_cat']) && $settings['products']['sub_cat'] == 'none')?'selected="selected"':''?>>не отображать</option>
								</select>
							</div>
						</div>

					</fieldset>

					<fieldset title="Настройки пагинации и сортировки">
						<legend>Настройки пагинации и сортировки</legend>
						<div class="control-group checkbox">
							<input type="checkbox" value="true" id="products_pagination_enabled" name="settings[products][pagination_enabled]" <?= (isset($settings['products']['pagination_enabled']) && $settings['products']['pagination_enabled']=='true') ? 'checked="checked"' : '' ?>>
							<label for="products_pagination_enabled">Пагинация (разбивка на страници) включена?:</label>
						</div>

						<div class="control-group checkbox">
							<input type="checkbox" value="true" id="products_pagination_items_enabled" name="settings[products][pagination_items_enabled]" <?= (isset($settings['products']['pagination_items_enabled']) && $settings['products']['pagination_items_enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for="products_pagination_items_enabled">Отображать выбор количества товаров на странице?:</label>
						</div>

						<div class="control-group">
							<label>Количество товаров на странице по умолчанию:</label>
							<input class="input-mini validate1" type="text" name="settings[products][pagination]" value="<?= (isset($settings['products']['pagination'])) ? $settings['products']['pagination'] : '' ?>">
						</div>

						<div class="control-group checkbox">
							<input type="checkbox" value="true" id="products_sort_enabled" name="settings[products][sort_enabled]" <?= (isset($settings['products']['sort_enabled']) && $settings['products']['sort_enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for="products_sort_enabled">Сортировка товаров включена?:</label>
						</div>

						<div class="control-group checkbox">
							<input type="checkbox" value="true" id="products_sort_items_enabled" name="settings[products][sort_items_enabled]" <?= (isset($settings['products']['sort_items_enabled']) && $settings['products']['sort_items_enabled']=='true') ? 'checked="checked"' : '' ?>>
							<label for="products_sort_items_enabled">Отображать выбор сортировки товаров ?:</label>
						</div>

						<div class="control-group">
							<label>Сортировка по умолчанию:</label>
							<div class="input-prepend">
								<span class="add-on"><i class="icon-check"></i></span>
								<select class="input-large" name="settings[products][sort]">
									<option value="id_product" <?=(isset($settings['products']['sort']) && $settings['products']['sort'] == 'id_product')?'selected="selected"':''?>>по порядковому номеру</option>
									<option value="code" <?=(isset($settings['products']['sort']) && $settings['products']['sort'] == 'code')?'selected="selected"':''?>>по коду</option>
									<option value="title" <?=(isset($settings['products']['sort']) && $settings['products']['sort'] == 'title')?'selected="selected"':''?>>по названию</option>
									<option value="price" <?=(isset($settings['products']['sort']) && $settings['products']['sort'] == 'price')?'selected="selected"':''?>>по цене</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label>Тип сортировки по умолчанию:</label>
							<div class="input-prepend">
								<span class="add-on"><i class="icon-check"></i></span>
								<select class="input-large" name="settings[products][sort_type]"">
								<option value="up" <?=(isset($settings['products']['sort_type']) && $settings['products']['sort_type'] == 'up')?'selected="selected"':''?>>возрастание</option>
								<option value="down" <?=(isset($settings['products']['sort_type']) && $settings['products']['sort_type'] == 'down')?'selected="selected"':''?>>убывание</option>
								</select>
							</div>
						</div>

					</fieldset>

					<fieldset title="Настройки быстрой регистрации пользователей">
						<legend>Настройки быстрой регистрации пользователей</legend>
						<div class="control-group checkbox">
							<input type="checkbox" name="settings[authEnabled]" id="authEnabled" <?= (isset($settings['authEnabled']) && $settings['authEnabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for="authEnabled">Быстрая регистрация включена?</label>
						</div>
						<div class="control-group">
							<label>Страница входа пользователя и напоминания пароля:</label><br>
							<input class="span6" type="text" name="settings[authURL]" value="<?= (isset($settings['authURL'])) ? $settings['authURL'] : '' ?>">
						</div>
						<div class="control-group">
							<label>Страница кабинета пользователя:</label><br>
							<input class="span6" type="text" name="settings[cabinetURL]" value="<?= (isset($settings['cabinetURL'])) ? $settings['cabinetURL'] : '' ?>">
						</div>
					</fieldset>

					<fieldset title="Настройка накопительных скидок">
						<legend>Настройка накопительных скидок</legend>
						<div class="control-group checkbox">
							<input type="checkbox" value="true" id="discounts_enabled" name="settings[discounts_enabled]" <?= (isset($settings['discounts_enabled']) && $settings['discounts_enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for="discounts_enabled">Использовать накопительные скидки?</label>
							<input type="hidden" name="settings[discounts_prev]" value="<?=$settings['discounts_prev']?>"
						</div>
						<div class="control-group">
							<label>Линейка скидок (сумма покупок => процент скидки):</label>

							<div id="discount">
								<?foreach($settings['discounts'] as $key => $discount):?>
								<div>
									<input class="input-mini validate1" type="text" value="<?=$key?>" name="settings[discounts][sum][]" size="5">
									=>
									<input class="input-mini validate1" type="text" value="<?=$discount?>" name="settings[discounts][name][]" size="2">
									<a href="#" onclick="return deleteInputs(this)" class="delete">удалить</a>
									<br/>
								</div>
								<?endforeach;?>
							</div>

							<div><a href="#" onclick="return moreInputs('discount')" class="more">Добавить поле</a></div>
						</div>
					</fieldset>

				</div>
				<div class = "span6">
					<fieldset title = "Настройки редиректов">
						<legend>Настройки редиректов</legend>
						<div class = "control-group">
							<label>Адреса указывать от корня сайта. Если оставить пустыми, то покупатель будет просто попадать
								на
								главную страницу магазина.</label>
							<label>Страница успешного оформления заказа:</label>
							<input class = "span8" type = "text" name = "settings[returnUrl]" value = "<?= (isset($settings['returnUrl'])) ? $settings['returnUrl'] : '' ?>">
						</div>
						<div class = "control-group">
							<label>Страница успешной оплаты заказа:</label>
							<input class = "span8" type = "text" name = "settings[successUrl]" value = "<?= (isset($settings['successUrl'])) ? $settings['successUrl'] : '' ?>">
						</div>
						<div class = "control-group">
							<label>Страница ошибки при оплате заказа:</label>
							<input class = "span8" type = "text" name = "settings[failUrl]" value = "<?= (isset($settings['failUrl'])) ? $settings['failUrl'] : '' ?>">
						</div>
					</fieldset>

					<fieldset title = "Настройки админки">
						<legend>Настройки админки</legend>
						<div class = "control-group">
							<label>Логин админки:</label>
							<input class = "span6" type = "text" name = "settings[admin][login]" value = "<?= (isset($settings['admin']['login'])) ? $settings['admin']['login'] : '' ?>">
						</div>
						<div class = "control-group">
							<label>Пароль админки. Хранится в зашифрованом виде. Получить шифр пароля можно
								<a href = "http://pr-cy.ru/md5">здесь</a>:</label>
							<input class = "span7" type = "text" name = "settings[admin][password]" value = "<?= (isset($settings['admin']['password'])) ? $settings['admin']['password'] : '' ?>">
						</div>
						<div class = "control-group">
							<label>Количество заказов на странице в списке админки:</label>
							<input class = "input-mini validate1" type = "text" name = "settings[admin][ordersList]" value = "<?= (isset($settings['admin']['ordersList'])) ? $settings['admin']['ordersList'] : '' ?>">
						</div>
						<div class="control-group">
							<label>Количество продуктов на странице в списке админки:</label>
							<input class="input-mini validate1" type="text" name="settings[admin][productsList]" value="<?= (isset($settings['admin']['productsList'])) ? $settings['admin']['productsList'] : '' ?>">
						</div>
						<div class="control-group">
							<label>Количество клиентов на странице в списке админки:</label>
							<input class="input-mini validate1" type="text" name="settings[admin][usersList]" value="<?= (isset($settings['admin']['usersList'])) ? $settings['admin']['usersList'] : '' ?>">
						</div>
					</fieldset>

					<fieldset title="Настройки загрузки изображений товаров">
						<legend>Настройки загрузки изображений товаров</legend>
						<div class="control-group checkbox">
							<input type="checkbox" value="true" id="imagesEnabled" name="settings[images][enabled]" <?= (isset($settings['images']['enabled']) && $settings['images']['enabled']=='true') ? 'checked="checked"' : '' ?>>
							<label for="imagesEnabled">Загрузка изображений товаров включена?</label>
						</div>
						<div class="control-group">
							<label>Папка с изображениями товаров:</label><br>
							<input class="input-small" type="text" name="settings[images][dir]" value="<?= (isset($settings['images']['dir'])) ? $settings['images']['dir'] : '' ?>">
						</div>
						<div class="control-group checkbox">
							<input type="checkbox" value="true" id="resizeEnabled" name="settings[images][resize][enabled]" <?= (isset($settings['images']['resize']['enabled']) && $settings['images']['resize']['enabled']=='true') ? 'checked="checked"' : '' ?>>
							<label for="resizeEnabled">Изменение размеров изображений включено? (все изменённые файлы загружаются на сервер в формате jpg)</label>
						</div>
						<div class="control-group">
							<label>Максимальная ширина эскиза товара в пикселях:</label>
							<input class="input-mini validate" type="text" name="settings[images][thumb][width]" value="<?= (isset($settings['images']['thumb']['width'])) ? $settings['images']['thumb']['width'] : '' ?>">
						</div>
						<div class="control-group">
							<label>Максимальная высота изображения товара в пикселях:</label>
							<input class="input-mini validate" type="text" name="settings[images][height]" value="<?= (isset($settings['images']['height'])) ? $settings['images']['height'] : '' ?>">
						</div>
						<div class="control-group">
							<label>Максимальная ширина изображения товара в пикселях:</label>
							<input class="input-mini validate" type="text" name="settings[images][width]" value="<?= (isset($settings['images']['width'])) ? $settings['images']['width'] : '' ?>">
						</div>
						<div class="control-group checkbox">
							<input type="checkbox" value="true" id="fancyboxEnabled" name="settings[fancybox][enabled]" <?= (isset($settings['fancybox']['enabled']) && $settings['fancybox']['enabled']=='true') ? 'checked="checked"' : '' ?>>
							<label for="fancyboxEnabled">Увеличение изображений в модальном окне включено?</label>
						</div>
						<div class="control-group checkbox">
							<input type="checkbox" value="true" id="fancyboxGallery" name="settings[fancybox][gallery]" <?= (isset($settings['fancybox']['gallery']) && $settings['fancybox']['gallery']=='true') ? 'checked="checked"' : '' ?>>
							<label for="fancyboxGallery">Отображать увеличенные изображения в виде галлереи?</label>
							<div id="gallery_type">
								<label class="radio">
									<input type="radio" name="settings[fancybox][type]" id="gallery_type_product" value="product" <?= (isset($settings['fancybox']['type']) && $settings['fancybox']['type']=='product') ? 'checked' : '' ?>>
									В галерее отображать все изображения товара
								</label>
								<label class="radio">
									<input type="radio" name="settings[fancybox][type]" id="gallery_type_category" value="category" <?= (isset($settings['fancybox']['type']) && $settings['fancybox']['type']=='category') ? 'checked' : '' ?>>
									В галерее отображать все изображения категории
								</label>
								<label class="radio">
									<input type="radio" name="settings[fancybox][type]" id="gallery_type_all" value="all" <?= (isset($settings['fancybox']['type']) && $settings['fancybox']['type']=='all') ? 'checked' : '' ?>>
									В галерее отображать все изображения на странице
								</label>
							</div>
						</div>
						<div class="control-group checkbox">
							<input type="checkbox" value="true" id="fancyboxCyclic" name="settings[fancybox][cyclic]" <?= (isset($settings['fancybox']['cyclic']) && $settings['fancybox']['cyclic']=='true') ? 'checked="checked"' : '' ?>>
							<label for="fancyboxCyclic">Цикличность галлереи</label>
						</div>
						<div class="control-group checkbox">
							<input type="checkbox" value="true" id="images_cart_enabled" name="settings[images][cart][enabled]" <?= (isset($settings['images']['cart']['enabled']) && $settings['images']['cart']['enabled']=='true') ? 'checked="checked"' : '' ?>>
							<label for="images_cart_enabled">Изображение товара в корзине включено?</label>
						</div>
						<div class="control-group">
							<label>Максимальная ширина изображения товара в корзине в пикселях:</label>
							<input class="input-mini validate" type="text" name="settings[images][cart][width]" value="<?= (isset($settings['images']['cart']['width'])) ? $settings['images']['cart']['width'] : '' ?>">
						</div>

					</fieldset>

				</div>
			</div>
			<h2 class="ruler" title="Кликните, чтобы развернуть">Настройка полей формы заказа</h2>

			<div class="row-fluid">
				<div class="span6">

					<fieldset title = "Поле name">
						<legend>Поле name</legend>
						<div class = "control-group">
							<label>Название поля:</label>
							<input class = "span8" type = "text" name = "settings[form][order][name][label]" value = "<?= (isset($settings['form']['order']['name']['label'])) ? $settings['form']['order']['name']['label'] : '' ?>">
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_name_enabled" name = "settings[form][order][name][enabled]" <?= (isset($settings['form']['order']['name']['enabled']) && $settings['form']['order']['name']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_name_enabled">Используется?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_name_required" name = "settings[form][order][name][required]" <?= (isset($settings['form']['order']['name']['required']) && $settings['form']['order']['name']['required'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_name_required">Обязательное?</label>
						</div>
					</fieldset>

					<fieldset title = "Поле email">
						<legend>Поле email</legend>
						<div class = "control-group">
							<label>Название поля:</label>
							<input class = "span8" type = "text" name = "settings[form][order][email][label]" value = "<?= (isset($settings['form']['order']['email']['label'])) ? $settings['form']['order']['email']['label'] : '' ?>">
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_email_enabled" name = "settings[form][order][email][enabled]" <?= (isset($settings['form']['order']['email']['enabled']) && $settings['form']['order']['email']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_email_enabled">Используется?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_email_required" name = "settings[form][order][email][required]" <?= (isset($settings['form']['order']['email']['required']) && $settings['form']['order']['email']['required'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_email_required">Обязательное?</label>
						</div>
					</fieldset>

					<fieldset title = "Поле lastname">
						<legend>Поле lastname</legend>
						<div class = "control-group">
							<label>Название поля:</label>
							<input class = "span8" type = "text" name = "settings[form][order][lastname][label]" value = "<?= (isset($settings['form']['order']['lastname']['label'])) ? $settings['form']['order']['lastname']['label'] : '' ?>">
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_lastname_enabled" name = "settings[form][order][lastname][enabled]" <?= (isset($settings['form']['order']['lastname']['enabled']) && $settings['form']['order']['lastname']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_lastname_enabled">Используется?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_lastname_required" name = "settings[form][order][lastname][required]" <?= (isset($settings['form']['order']['name']['required']) && $settings['form']['order']['lastname']['required'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_lastname_required">Обязательное?</label>
						</div>
					</fieldset>

					<fieldset title = "Поле fathername">
						<legend>Поле fathername</legend>
						<div class = "control-group">
							<label>Название поля:</label>
							<input class = "span8" type = "text" name = "settings[form][order][fathername][label]" value = "<?= (isset($settings['form']['order']['fathername']['label'])) ? $settings['form']['order']['fathername']['label'] : '' ?>">
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_fathername_enabled" name = "settings[form][order][fathername][enabled]" <?= (isset($settings['form']['order']['fathername']['enabled']) && $settings['form']['order']['fathername']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_fathername_enabled">Используется?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_fathername_required" name = "settings[form][order][fathername][required]" <?= (isset($settings['form']['order']['fathername']['required']) && $settings['form']['order']['fathername']['required'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_fathername_required">Обязательное?</label>
						</div>
					</fieldset>

					<fieldset title = "Поле phone">
						<legend>Поле phone</legend>
						<div class = "control-group">
							<label>Название поля:</label>
							<input class = "span8" type = "text" name = "settings[form][order][phone][label]" value = "<?= (isset($settings['form']['order']['phone']['label'])) ? $settings['form']['order']['phone']['label'] : '' ?>">
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_phone_enabled" name = "settings[form][order][phone][enabled]" <?= (isset($settings['form']['order']['phone']['enabled']) && $settings['form']['order']['phone']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_phone_enabled">Используется?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_phone_required" name = "settings[form][order][phone][required]" <?= (isset($settings['form']['order']['phone']['required']) && $settings['form']['order']['phone']['required'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_phone_required">Обязательное?</label>
						</div>
					</fieldset>

				</div>
				<div class="span6">

					<fieldset title = "Поле zip">
						<legend>Поле zip</legend>
						<div class = "control-group">
							<label>Название поля:</label>
							<input class = "span8" type = "text" name = "settings[form][order][zip][label]" value = "<?= (isset($settings['form']['order']['zip']['label'])) ? $settings['form']['order']['zip']['label'] : '' ?>">
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_zip_enabled" name = "settings[form][order][zip][enabled]" <?= (isset($settings['form']['order']['zip']['enabled']) && $settings['form']['order']['zip']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_zip_enabled">Используется?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_zip_required" name = "settings[form][order][zip][required]" <?= (isset($settings['form']['order']['zip']['required']) && $settings['form']['order']['zip']['required'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_zip_required">Обязательное?</label>
						</div>
					</fieldset>

					<fieldset title = "Поле region">
						<legend>Поле region</legend>
						<div class = "control-group">
							<label>Название поля:</label>
							<input class = "span8" type = "text" name = "settings[form][order][region][label]" value = "<?= (isset($settings['form']['order']['region']['label'])) ? $settings['form']['order']['region']['label'] : '' ?>">
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_region_enabled" name = "settings[form][order][region][enabled]" <?= (isset($settings['form']['order']['region']['enabled']) && $settings['form']['order']['region']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_region_enabled">Используется?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_region_required" name = "settings[form][order][region][required]" <?= (isset($settings['form']['order']['region']['required']) && $settings['form']['order']['region']['required'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_region_required">Обязательное?</label>
						</div>
					</fieldset>

					<fieldset title = "Поле city">
						<legend>Поле city</legend>
						<div class = "control-group">
							<label>Название поля:</label>
							<input class = "span8" type = "text" name = "settings[form][order][city][label]" value = "<?= (isset($settings['form']['order']['city']['label'])) ? $settings['form']['order']['city']['label'] : '' ?>">
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_city_enabled" name = "settings[form][order][city][enabled]" <?= (isset($settings['form']['order']['city']['enabled']) && $settings['form']['order']['city']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_city_enabled">Используется?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_city_required" name = "settings[form][order][city][required]" <?= (isset($settings['form']['order']['city']['required']) && $settings['form']['order']['city']['required'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_city_required">Обязательное?</label>
						</div>
					</fieldset>

					<fieldset title = "Поле address">
						<legend>Поле address</legend>
						<div class = "control-group">
							<label>Название поля:</label>
							<input class = "span8" type = "text" name = "settings[form][order][address][label]" value = "<?= (isset($settings['form']['order']['address']['label'])) ? $settings['form']['order']['address']['label'] : '' ?>">
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_address_enabled" name = "settings[form][order][address][enabled]" <?= (isset($settings['form']['order']['address']['enabled']) && $settings['form']['order']['address']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_address_enabled">Используется?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_address_required" name = "settings[form][order][address][required]" <?= (isset($settings['form']['order']['address']['required']) && $settings['form']['order']['address']['required'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_address_required">Обязательное?</label>
						</div>
					</fieldset>

					<fieldset title = "Поле comment">
						<legend>Поле comment</legend>
						<div class = "control-group">
							<label>Название поля:</label>
							<input class = "span8" type = "text" name = "settings[form][order][comment][label]" value = "<?= (isset($settings['form']['order']['comment']['label'])) ? $settings['form']['order']['comment']['label'] : '' ?>">
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_comment_enabled" name = "settings[form][order][comment][enabled]" <?= (isset($settings['form']['order']['comment']['enabled']) && $settings['form']['order']['comment']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_comment_enabled">Используется?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_order_comment_required" name = "settings[form][order][comment][required]" <?= (isset($settings['form']['order']['comment']['required']) && $settings['form']['order']['comment']['required'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_order_comment_required">Обязательное?</label>
						</div>
					</fieldset>
				</div>
			</div>

			<h2 class="ruler" title="Кликните, чтобы развернуть">Настройка полей формы сообщения</h2>

			<div class="row-fluid">
				<div class="span6">

					<fieldset title = "Поле name">
						<legend>Поле name</legend>
						<div class = "control-group">
							<label>Название поля:</label>
							<input class = "span8" type = "text" name = "settings[form][feedback][name][label]" value = "<?= (isset($settings['form']['feedback']['name']['label'])) ? $settings['form']['feedback']['name']['label'] : '' ?>">
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_feedback_name_enabled" name = "settings[form][feedback][name][enabled]" <?= (isset($settings['form']['feedback']['name']['enabled']) && $settings['form']['feedback']['name']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_feedback_name_enabled">Используется?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_feedback_name_required" name = "settings[form][feedback][name][required]" <?= (isset($settings['form']['feedback']['name']['required']) && $settings['form']['feedback']['name']['required'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_feedback_name_required">Обязательное?</label>
						</div>
					</fieldset>

					<fieldset title = "Поле email">
						<legend>Поле email</legend>
						<div class = "control-group">
							<label>Название поля:</label>
							<input class = "span8" type = "text" name = "settings[form][feedback][email][label]" value = "<?= (isset($settings['form']['feedback']['email']['label'])) ? $settings['form']['feedback']['email']['label'] : '' ?>">
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_feedback_email_enabled" name = "settings[form][feedback][email][enabled]" <?= (isset($settings['form']['feedback']['email']['enabled']) && $settings['form']['feedback']['email']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_feedback_email_enabled">Используется?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_feedback_email_required" name = "settings[form][feedback][email][required]" <?= (isset($settings['form']['feedback']['email']['required']) && $settings['form']['feedback']['email']['required'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_feedback_email_required">Обязательное?</label>
						</div>
					</fieldset>

					<fieldset title = "Поле phone">
						<legend>Поле phone</legend>
						<div class = "control-group">
							<label>Название поля:</label>
							<input class = "span8" type = "text" name = "settings[form][feedback][phone][label]" value = "<?= (isset($settings['form']['feedback']['phone']['label'])) ? $settings['form']['feedback']['phone']['label'] : '' ?>">
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_feedback_phone_enabled" name = "settings[form][feedback][phone][enabled]" <?= (isset($settings['form']['feedback']['phone']['enabled']) && $settings['form']['feedback']['phone']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_feedback_phone_enabled">Используется?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_feedback_phone_required" name = "settings[form][feedback][phone][required]" <?= (isset($settings['form']['feedback']['phone']['required']) && $settings['form']['feedback']['phone']['required'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_feedback_phone_required">Обязательное?</label>
						</div>
					</fieldset>
				</div>

				<div class="span6">

					<fieldset title = "Поле subject">
						<legend>Поле subject</legend>
						<div class = "control-group">
							<label>Название поля:</label>
							<input class = "span8" type = "text" name = "settings[form][feedback][subject][label]" value = "<?= (isset($settings['form']['feedback']['subject']['label'])) ? $settings['form']['feedback']['subject']['label'] : '' ?>">
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_feedback_subject_enabled" name = "settings[form][feedback][subject][enabled]" <?= (isset($settings['form']['feedback']['subject']['enabled']) && $settings['form']['feedback']['subject']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_feedback_subject_enabled">Используется?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_feedback_subject_required" name = "settings[form][feedback][subject][required]" <?= (isset($settings['form']['feedback']['subject']['required']) && $settings['form']['feedback']['subject']['required'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_feedback_subject_required">Обязательное?</label>
						</div>
					</fieldset>

					<fieldset title = "Поле comment">
						<legend>Поле comment</legend>
						<div class = "control-group">
							<label>Название поля:</label>
							<input class = "span8" type = "text" name = "settings[form][feedback][comment][label]" value = "<?= (isset($settings['form']['feedback']['comment']['label'])) ? $settings['form']['feedback']['comment']['label'] : '' ?>">
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_feedback_comment_enabled" name = "settings[form][feedback][comment][enabled]" <?= (isset($settings['form']['feedback']['comment']['enabled']) && $settings['form']['feedback']['comment']['enabled'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_feedback_comment_enabled">Используется?</label>
						</div>
						<div class = "control-group checkbox">
							<input type = "checkbox" value = "true" id = "form_feedback_comment_required" name = "settings[form][feedback][comment][required]" <?= (isset($settings['form']['feedback']['comment']['required']) && $settings['form']['feedback']['comment']['required'] == 'true') ? 'checked="checked"' : '' ?>>
							<label for = "form_feedback_comment_required">Обязательное?</label>
						</div>
					</fieldset>
				</div>
			</div>

			<br/><br/><br/>
			<? if (isset($c_file)): ?>
			<div class = "row-fluid">
				<div class = "control-group">
					<label class = "control-label">Содержание файла config.php:</label>

					<div class = "controls">
						<textarea class = "span12" rows = "10"><?= (isset($c_file)) ? $c_file : ''; ?></textarea>
					</div>
				</div>
			</div>
				<? endif ?>
			<? if (isset($c_inc_file)): ?>
			<div class = "row-fluid">
				<div class = "control-group">
					<label class = "control-label">Содержание файла config.ini.php:</label>

					<div class = "controls">
						<textarea class = "span12" rows = "10"><?= (isset($c_inc_file)) ? $c_inc_file : ''; ?></textarea>
					</div>
				</div>
			</div>
				<? endif ?>
		</div>

		<div class = "form-actions type2">
			<input type = "submit" class = "btn btn-success" name = "save" value = "Сохранить" title = "Сохранить настройки">
			<button type = "submit" value = "Востановить" name = "backup" class = "btn btn-primary" title = "Востановить предыдущие настройки">Востановить</button>
			<a href="#top" class="btn-up pull-right"><button type = "button" class = "btn" title = "Вверх &uarr;">Вверх &uarr;</button></a>
			<a href="#bottom" class="btn-down pull-right"><button type = "button" class = "btn" title = "Вниз &darr;">Вниз &darr;</button></a>
			<button type = "button" class = "btn supreme pull-right" id="show" title = "Свернуть/Развернуть">Развернуть</button>
		</div>

		<div id="bottom"></div>
	</form>
</div>
<script type="text/javascript">
	$(".supreme").show();
	$(".ruler").next().addClass('collapse');
	$(".ruler").next().height(0);
	$(".ruler").on('click', function() {
		if($(this).next().height() == 0)
			$(this).next().collapse('show');
		else
			$(this).next().collapse('hide');
		return false;
	});
	$(".supreme").on('click', function() {
		if ($(this).attr('id') == 'show')
		{
			$(".ruler").next().collapse('show');
			$(this).attr('id', 'hide');
			$(this).text('Свернуть');
		}
		else{
			$(".ruler").next().collapse('hide');
			$(this).attr('id', 'show');
			$(this).text('Развернуть');
		}
		return false;
	});
	$(".btn-up").click(function(e){
		$("html,body").animate({scrollTop:0},400);
		e.preventDefault();
	})
	$(".btn-down").click(function(e){
		$("html,body").animate({scrollTop:$(document).height()},400);
		e.preventDefault();
	})

	function moreInputs(param) {
		var div = document.createElement("div");
		div.innerHTML = '<input  class="input-mini validate1" name="settings[discounts][sum][]" type="text" value="" size="5"> => <input  class="input-mini validate1" name="settings[discounts][name][]" type="text" value="" size="2"> <a href="#" onclick="return deleteInputs(this)" class="delete">удалить</a>';
		document.getElementById(param).appendChild(div);
		return false;
	}

	function deleteInputs(a) {
		$(a).parent().remove();
		return false;
	}

	$('input[type="submit"]').click(function() {
		var form = $(this).parent().parent();
		var validate = form.find('.validate');
		var validate1 = form.find('.validate1');
		for($i=0;$i<validate.size();$i++)
		{
			if (/\s+/.exec($(validate[$i]).val())!=null || /^$/.exec($(validate[$i]).val())!=null || /[^0-9]+/.exec($(validate[$i]).val())!=null)
			{
				$(".collapse").collapse('show');
				var ahtung = ($(validate[$i])).prev('label').text().slice(0, -1) + ' должна быть цифровым значением!';
				alert(ahtung);
				$(validate[$i]).focus();
				return false;
			}
		}
		for($i=0;$i<validate1.size();$i++)
		{
			if (/\s+/.exec($(validate1[$i]).val())!=null || /^$/.exec($(validate1[$i]).val())!=null)
			{
				$(".collapse").collapse('show');
				var ahtung = 'Поле не должно быть пустым!';
				alert(ahtung);
				$(validate1[$i]).focus();
				return false;
			}
		}
	})

	if($('#fancyboxGallery').attr('checked') == 'checked')
	{
		$('#gallery_type').show();
	}
	else
	{
		$('#gallery_type').hide();
	}

	$('#fancyboxGallery').click(function(){
		if($('#gallery_type').css('display') == 'none')
		{
			$('#gallery_type').show();
		}
		else
		{
			$('#gallery_type').hide();
		}
	});
</script>
</body>
</html>