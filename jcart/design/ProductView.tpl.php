<?/* Шаблон вывода товара. */?>

<? $key = 0; ?>
<? if (!empty($product_codes[$key])): ?>
	<?if(isset($_GET['product']) && isset($entry) && $entry != $product_cats[$key] && $jcart->config['products']['sub_cat'] != 'products') : ?>
	<a href="<?= 'http://'. $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] . '?sub_cat=' . $product_cats[$key] ?>" class="clearfix" title="Назад"><h4>Назад</h4></a>
	<?elseif(isset($_GET['product'])):?>
	<a href="<?= 'http://'. $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] ?>" class="clearfix" title="Назад"><h4>Назад</h4></a>
	<?endif;?>

<form method="post" action="" class="jcart">
	<input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken'];?>" />
	<input type="hidden" name="my_item_id" value="<?= $product_codes[$key] ?>" />
	<input type="hidden" name="my_item_name" value="<?= $product_titles[$key] ?>" />
	<input type="hidden" name="my_item_price" value="<?= $product_prices[$key] ?>" />
	<input type="hidden" name="my_item_unit" value="<?= $product_units[$key] ?>" />
	<input type="hidden" name="my_item_cat" value="<?= (isset($product_cats[$key]))? $product_cats[$key] : ''?>" />
	<input type="hidden" name="my_item_discount" value="<?= (isset($product_discounts[$key]))? $product_discounts[$key] : '' ?>" />

	<input type="hidden" name="my_item_color" value="<?= (isset($product_colors[$key]))? $product_colors[$key] : ''?>" />
	<input type="hidden" name="my_item_size" value="<?= (isset($product_sizes[$key]))? $product_sizes[$key] : ''?>" />
	<input type="hidden" name="my_item_param" value="<?= (isset($product_params[$key]))? $product_params[$key] : ''?>" />
	<input type="hidden" name="my_item_url" value="http://<?= $_SERVER['SERVER_NAME']  ?><?= $_SERVER['REQUEST_URI'] ?>" />
	
	<?if(true):?><!--fancybox-->
		<? if ($jcart->config['images']['enabled'] == true): ?>
		
			<? if ($jcart->config['images']['resize']['enabled'] == true): ?>
			
				<? if ($jcart->config['fancybox']['enabled'] == true): ?>
					<?if ($prod_pics[$key] != false && isset($product_cats[$key])): ?>
					<? foreach ($prod_pics[$key] as $i => $pic):?>
						<a href="<?= $jcart->config['sitelink'] . $jcart->config['jcartPath'] . $jcart->config['images']['dir'] . '/' . $product_cats[$key] . '/' . $pic?>" id="<?= $product_codes[$key] ?>" title="<?= $product_titles[$key] ?>" class="fancybox nolink" <? if ($jcart->config['fancybox']['gallery'] == true): ?>rel="<?= $product_codes[$key] ?>"<? endif ?>>
							<?if($i==0):?>
								<img src="<?= $jcart->config['sitelink'] . $jcart->config['jcartPath'] . $jcart->config['images']['dir'] . '/' . $product_cats[$key] . '/thumb/'. $product_codes[$key] ?>_0.jpg" alt="<?= $product_titles[$key] ?>" class="f-left-img">
							<?endif?>
						</a>
					<?endforeach?>
					<? endif ?>
				<? else: ?>
					<? if (is_file(dirname(__FILE__) . '/../' . $jcart->config['images']['dir'] . '/' . $product_cats[$key] . '/thumb/' . $product_codes[$key] . '_0.jpg')): ?>
					<img src="<?= $jcart->config['sitelink'] . $jcart->config['jcartPath'] . $jcart->config['images']['dir'] . '/' . $product_cats[$key] . '/thumb/'. $product_codes[$key] ?>_0.jpg" alt="<?= $product_titles[$key] ?>" class="f-left-img">
					<br>
					<? endif ?>
				<? endif ?>

			<? else: ?>
				<? if (is_file(dirname(__FILE__) . '/../' . $jcart->config['images']['dir'] . '/' . $product_cats[$key] . '/' . $product_codes[$key] . '_0.jpg')): ?>
				<img src="<?= $jcart->config['sitelink'] . $jcart->config['jcartPath'] . $jcart->config['images']['dir'] . '/' . $product_cats[$key] . '/'. $product_codes[$key] ?>_0.jpg" alt="<?= $product_titles[$key] ?>">
				<br>
				<? endif ?>
			<? endif ?>

		<? endif ?>
	<?endif?><!--fancybox end-->
		
	<!--<div class="<?=($jcart->config['images']['resize']['enabled'] == true)?'description':''?> clearfix">-->
	<? if ($product_discounts[$key] != 0): ?>
		<span class="price">Цена: <span class="striked"><?= $product_prices[$key] ?></span> <span class="attention"><?= $product_prices[$key] * (1 - $product_discounts[$key]/100) ?> <?= $currencySymbol ?></span></span>
		<br>
		<div id="countdown-<?=$product_codes[$key]?>" class="attention small"></div>
		
		<script type="text/javascript">
			$(function() {
				$("#countdown-<?=$product_codes[$key]?>").countdown(new Date(<?=$product_discount_dates['Y'][$key]?>, <?=$product_discount_dates['m'][$key] - 1?>, <?=$product_discount_dates['d'][$key]?>, <?=$product_discount_dates['H'][$key]?>, <?=$product_discount_dates['i'][$key]?>, <?=$product_discount_dates['s'][$key]?>), {prefix:'Скидка: ', finish: 'Акция закончилась!',reload: true});
			});
		</script>
		<? else: ?>
		<span class="price">Цена: <?= $product_prices[$key] ?> <?= $currencySymbol ?></span><br>
	<? endif; ?>

	<label><input type="text" name="my_item_qty" value="1" size="2" /> <?= $product_units[$key] ?></label>
	<input type="submit" name="my_add_button" value="В корзину" class="button" />
	
	<? if ($jcart->config['download']['enabled'] == true ): ?>
	<br>
	<a href="<?= $jcart->config['sitelink'] . $jcart->config['jcartPath'] ?>update/<?= $product_codes[$key] ?>/" title="Обновить <?= $product_titles[$key] ?>">Обновление</a>
	<? endif ?>
	<!--</div>-->
	<div class="clear"></div>
	
	<? if ($jcart->config['images']['enabled'] == true): ?>
		<? if ($jcart->config['images']['resize']['enabled'] == true): ?>
			<? if ($jcart->config['fancybox']['enabled'] == true): ?>
				<?if (isset($prod_pics[$key]) && count($prod_pics[$key])>1): ?>
				<div class="gallery">
				<? foreach ($prod_pics[$key] as $i => $pic): if($i!=0):?>
					<a href="<?= $jcart->config['sitelink'] . $jcart->config['jcartPath'] . $jcart->config['images']['dir'] . '/' . $product_cats[$key] . '/' . $pic?>" id="<?= $product_codes[$key] ?>" title="<?= $product_titles[$key] ?>" class="fancybox nolink" <? if ($jcart->config['fancybox']['gallery'] == true): ?>rel="<?= $product_codes[$key] ?>"<? endif ?>>
							<img src="<?= $jcart->config['sitelink'] . $jcart->config['jcartPath'] . $jcart->config['images']['dir'] . '/' .$product_cats[$key] . '/thumb/'. $pic ?>" alt="<?= $product_titles[$key] ?>">
						<?endif?>
					</a>
				<?endforeach?>
				</div>
				<? endif ?>
			<? else: ?>
				<?if (isset($prod_pics[$key]) && count($prod_pics[$key])>1): ?>
				<div class="gallery">
				<? foreach ($prod_pics[$key] as $i => $pic): if($i!=0):?>
				<img src="<?= $jcart->config['sitelink'] . $jcart->config['jcartPath'] . $jcart->config['images']['dir'] . '/' . $product_cats[$key] . '/thumb/'. $pic ?>" alt="<?= $product_titles[$key] ?>">
				
				<?endif?>
				<?endforeach?>
				</div>
				<? endif ?>
			<? endif ?>

		<? else: ?>
			<?if (isset($prod_pics[$key]) && count($prod_pics[$key])>1): ?>
				<div class="gallery">
				<? foreach ($prod_pics[$key] as $i => $pic): if($i!=0):?>
				<img src="<?= $jcart->config['sitelink'] . $jcart->config['jcartPath'] . $jcart->config['images']['dir'] . '/' . $product_cats[$key] . '/'. $pic ?>" alt="<?= $product_titles[$key] ?>">
				<br>
				<?endif?>
				<?endforeach?>
				</div>
			<? endif ?>
		<? endif ?>

	<? endif ?>
	
	<div class="clear"></div>

	<h4>Описание товара</h4>
	<div class="about clearfix">
		<?= $product_descriptions[$key] ?>
	</div>

</form>
<? else: ?>
<p>В этом разделе товаров пока нет.</p>
<? endif ?>