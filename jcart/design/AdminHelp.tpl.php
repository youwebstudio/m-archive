<?
include_once dirname(__FILE__) . '/../design/AdminHeader.tpl.php';
?>

<div class="container-fluid">

<ul class="breadcrumb">
	<li>
		<a title="Вернуться к началу" href="index.php"><i class="icon-home"></i>&nbsp;Админ.интерфейс</a>
		<span class="divider">/</span>
	</li>
	<li>
		<a href="help.php">Справка</a>
		<span class="divider">/</span>
	</li>
</ul>

<h3>Установка кода на сайт</h3>
			<h4>1. Установка версий Mini и Base. Простейшая</h4>
			<h5>Шаг 1. В самом начале файла подключаем скрипт (до любых тегов). Вставляем строки:</h5>
			<pre class="code">
				<code class="php boc-html-script">
&lt;?php

// http://conceptlogic.com/jcart/
// http://jcart.info

// Обязательно вставьте include_once jcart.inc.php перед вызовом session_start()
include_once \'jcart/jcart.inc.php\';

?>
				</code>
			</pre>
			<p>Очень важно подключить jcart.php до открытия сессии или вывода данных.</p>
			<h5>Шаг 2. Вставляем корзину в нужное место страницы:</h5>
			<pre class="code">
				<code class="php boc-html-script">
&lt;div id="jcart">&lt;?php $jcart->display_cart(); ?>&lt;/div>
				</code>
			</pre>
			<p>На странице оформления заказа (например, <strong>checkout.php</strong>) с помощью этого же кода будет выводиться полная корзина с формой отправки заказа.</p>
			<p><strong>Внимание!</strong> Код должен встречаться на странице только один раз.</p>
			<h5>Шаг 3. В секцию &lt;head> страницы подключаем CSS стили и JavaScript скрипты:</h5>
			<pre class="code">
				<code class="php boc-html-script">
&lt;link type="text/css" href="jcart/css/jcart.css" rel="stylesheet" media="screen, projection" />
&lt;script type="text/javascript" src="jcart/js/jquery-1.4.4.min.js">&lt;/script>
&lt;script type="text/javascript" src="jcart/js/jcart.min.js">&lt;/script>
				</code>
			</pre>
			<h5>Шаг 4. Оформляем товар в виде формы. Например, так:</h5>
			<pre class="code">
				<code class="php html boc-html-script">
&lt;form method="post" action="" class="jcart">
&lt;!-- Обязательные данные -->
&lt;input type="hidden" name="jcartToken" value="&lt;?php echo $_SESSION[\'jcartToken\']; ?>" />
&lt;input type="hidden" name="my_item_id" value="mini" />
&lt;input type="hidden" name="my_item_name" value="jCart. Версия mini" />
&lt;input type="hidden" name="my_item_price" value="24.00" />

&lt;!-- Дополнительные данные. Частично можно выводить в виде &lt;input type="text"> или &lt;select>, чтобы пользователь мог вводить данные сам. -->
&lt;input type="hidden" name="my_item_url" value="" />
&lt;input type="hidden" name="my_item_color" value="" />
&lt;input type="hidden" name="my_item_size" value="" />
&lt;input type="hidden" name="my_item_param" value="" />

&lt;h4>jCart. Версия mini&lt;/h4>

&lt;img src="&lt?= $jcart->config[\'sitelink\'] . $jcart->config[\'jcartPath\'] ?>images/mini.jpg" alt="jCart. Версия mini">
&lt;br>
&lt;span class="price">Цена: 24.00 $&lt;/span>
&lt;br>
&lt;label>&lt;input type="text" name="my_item_qty" value="1" size="2" /> шт.&lt;/label>
&lt;input type="submit" name="my_add_button" value="В корзину" class="button" />
&lt;/form>
				</code>
			</pre>
			<p>Проще всего посмотреть в примере, который поставляется в составе дистрибутива, и сделать также.</p>
			<p>Имена полей должны быть точно такие же, как в настройках корзины (файл <strong>config.php</strong>).</p>
			<h5>Шаг 5. Для детальной настройки правим файлы config.php и config.inc.php</h5>
			<h5>Шаг 6. Для изменения оформления правим файл css/jcart.css</h5>
			<h5>Шаг 7. По надобности правим шаблоны в папке <strong>design</strong>. Описание всех шаблонов можно посмотреть здесь <a href="http://jcart.info/templates.html">здесь</a>.</h5>
			<h5>Шаг 8. Дополнительные установки</h5>
			<p>Для максимально корректной работы в файле <strong>jcart/js/jcart.min.js</strong> найдите <strong>var f=\'jcart\'</strong> и подставьте полный путь до файла. Например, <strong>var f=\'http://mysite.ru/jcart\'</strong>.</p>
			<p>Для версии Shop следует изменить также в файле <strong>.htaccess</strong>, который лежит в корне сайта, строку <strong>RewriteBase /test/</strong> на путь до сайта. Если он лежит в корне, то <strong>RewriteBase /</strong>. Если в папке <strong>test</strong>, то <strong>RewriteBase /test/</strong>.</p>
			<h5>Шаг 9. Заходим в админку</h5>
			<h4>2. Установка версий Pro и Vip. Продвинутая</h4>
			<h5>Шаг 1. В самом начале файла подключаем скрипт (до любых тегов). Вставляем строки:</h5>
			<pre class="code">
				<code class="php boc-html-script">
&lt;?php

// http://conceptlogic.com/jcart/
// http://jcart.info

// Обязательно вставьте include_once jcart.inc.php перед вызовом session_start()
include_once \'jcart/jcart.inc.php\';

// Чтение данных из БД
include_once \'jcart/get-products.inc.php\';

?>
				</code>
			</pre>
			<p>Очень важно подключить jcart.php до открытия сессии или вывода данных.</p>
			<h5>Шаги 2-3. Идентичны простейшей установке</h5>
			<h5>Шаг 4. На место вывода товаров вставляем код:</h5>
			<pre class="code">
				<code class="php boc-html-script">
&lt;?php
// Указываем идентификатор товара
$code_product = \'test-prod1\';

// Вставляем обработчик, который ищет товар и выводит шаблон
include \'jcart/view-products.inc.php\';
?>
				</code>
			</pre>
			<p>Тоже самое можно записать кратко в одну строку:</p>
			<pre class="code">
				<code class="php boc-html-script">
&lt;?php $code_product = \'test-prod1\'; include \'jcart/view-products.inc.php\'; ?>
				</code>
			</pre>
			<p>Это самый простой вариант использования продвинутой корзины. В этом случае все товары магазина подгружаются в переменную и выводятся в нужном месте по указанному коду товара.</p>
			
			<h4>Варианты вывода списка товаров</h4>

			<h5>Вывод списка заданных товаров</h5>
			
			<p>Можно также коды товаров указать списком. Тогда будет выводиться другой шаблон.</p>
			
			<pre class="code">
				<code class="php boc-html-script">
&lt;?php

// http://conceptlogic.com/jcart/
// http://jcart.info

// Обязательно вставьте include_once jcart.inc.php перед вызовом session_start()
include_once \'jcart/jcart.inc.php\';

// Список кодов товаров
$code_products[] = \'test-prod1\';
$code_products[] = \'test-prod2\';
$code_products[] = \'test-prod3\';

// Чтение данных из БД
include_once \'jcart/get-products.inc.php\';
?>
				</code>
			</pre>
			
			<p>а в нужном месте вставляете обработчик:</p>

			<pre class="code">
				<code class="php boc-html-script">
&lt;? include_once \'jcart/view-products.inc.php\'; ?>
				</code>
			</pre>
			
			<h5>Вывод списка товаров в заданной категории</h5>

			<p>Вместо кодов товаров следует задать код категории. Например, так:</p>
			
			<pre class="code">
				<code class="php boc-html-script">
&lt;?php

// http://conceptlogic.com/jcart/
// http://jcart.info

// Обязательно вставьте include_once jcart.inc.php перед вызовом session_start()
include_once \'jcart/jcart.inc.php\';

// Код категории товаров
$code_category = \'test-cat1\';

// Чтение данных из БД
include_once \'jcart/get-products.inc.php\';
?>
				</code>
			</pre>
			
			<h5>Вывод списка товаров или отдельного товара</h5>
			
			<p>Можно также сделать вывод товара на странице отдельно или  списком в зависимости от GET параметра.</p>
			
			<pre class="code">
				<code class="php boc-html-script">
&lt;?php

// http://conceptlogic.com/jcart/
// http://jcart.info

// Обязательно вставьте include_once jcart.inc.php перед вызовом session_start()
include_once \'jcart/jcart.inc.php\';

// Если указан параметр, то выводим одиночный шаблон.
if ($_GET[\'product\'])
{
	$code_products[] = $_GET[\'product\'];
}
// Иначе шаблон для списка
else
{
	// Список кодов товаров
	$code_products[] = \'test-prod1\';
	$code_products[] = \'test-prod2\';
	$code_products[] = \'test-prod3\';

	// Или код категории товаров
	// $code_category = \'test-cat1\';
}

// Чтение данных из БД
include_once \'jcart/get-products.inc.php\';
?>
				</code>
			</pre>
			<p>Варианты вывода товаров можно посмотреть в примерах, которые поставляются в дистрибутиве.</p>
			<h5>Шаги 5-9. Идентичны простейшей установке</h5>
</div>
</body>
</html>
