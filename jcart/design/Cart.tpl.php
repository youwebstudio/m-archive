<?/*
Шаблон корзины
==============
*/?>
<?$user_sum = (isset($_COOKIE['user-sum'])) ? $_COOKIE['user-sum'] : 0 ?>
<?= $errorMessage ?>
<div id="cart_display" class="<?=($this->itemCount > 0 || $isCheckout == true)?'cart_show':'cart_hide'?>"></div>
<form method="post" action="<?= $checkout ?>" >

	<input type="hidden" name="jcartToken" value="<?= $_SESSION['jcartToken'] ?>">
	<table>
		<thead>
			<tr>
				<? if ($config['auth']['enabled'] == true): ?>
					<? if (isset($_SESSION['id_user'])): ?>
					<a href="<?= $config['sitelink'] . $config['auth']['cabinetURL'] ?>">Ваш кабинет</a> / <a href="<?= $config['sitelink'] . $config['auth']['authURL'] ?>?logout">Выйти</a><br><br>
					<? else: ?>
					<a href="<?= $config['sitelink'] . $config['auth']['authURL'] ?>?login">Войти</a> / <a href="<?= $config['sitelink'] . $config['auth']['authURL'] ?>?remind">Напомнить пароль</a>
					<? if (isset($config['discounts'])): ?><br><em>войдите для расчёта скидки</em><? endif ?><br><br>
					<? endif ?>
				<? endif ?>

				<? if (isset($config['discounts'])): ?>
				<th colspan="2">
					<? if (isset($_COOKIE['name'])): ?>Здравствуйте, <?= $_COOKIE['name'] ?>!<br><? endif; ?>
					<? if ($user['next_discount'] != 0 && $user['next_sum'] != 0): ?>
					Следующая скидка: <?= $user['next_discount'] ?>%<br>
					До скидки: <?= $user['next_sum'] - $user_sum - $this->subtotal ?> <?= $currencySymbol ?>
					<? else: ?>
					У вас максимальная скидка
					<? endif ?>
				</th>
				<? else: ?>
				<th colspan="3">
					<? if (isset($_COOKIE['name'])): ?>Здравствуйте, <?= $_COOKIE['name'] ?>!<br><? endif; ?>
					Ваша корзина
				</th>
				<? endif ?>
			</tr>
		</thead>
		<tbody>
		<? if ($this->itemCount > 0): ?>
			<?foreach ($this->get_contents($user) as $item): ?>
			<tr>
				<td class="jcart-item-name">
					<? if ($config['images']['cart']['enabled'] == true && isset($item['cat']) && is_file(dirname(__FILE__) . '/../images/' . $item['cat'] . '/' . $item['id'] . '_0.jpg')): ?>
					<img width="<?=$config['images']['cart']['width']?>" src="<?= $config['sitelink'] . $config['jcartPath'] . 'images/' . $item['cat'] . '/' . $item['id'] ?>_0.jpg" alt="<?= $item['name'] ?>" style="float: right;">
					<? endif; ?>
					<? if (isset($item['url']) && !empty($item['url'])): ?>
					<a href="<?= $item['url'] ?>"><?= $item['name'] ?></a>
					<? else: ?>
					<?= $item['name'] ?>
					<? endif; ?>
					<input name="jcartItemName[]" type="hidden" value="<?= $item['name'] ?>" /><br/>
					<? if (isset($item['color']) && !empty($item['color'])): ?>Параметр1: <?= $item['color'] ?><br><? endif; ?>
					<? if (isset($item['size']) && !empty($item['size'])): ?>Параметр2: <?= $item['size'] ?><br><? endif; ?>
					<? if (isset($item['param']) && !empty($item['param'])): ?>Параметр3: <?= $item['param'] ?><? endif; ?>
				</td>
				<td class="jcart-item-price">
					<input name="jcartItemId[]" type="hidden" value="<?= $item['id'] ?>" />
					<input id="jcartItemQty-<?= $item['id'] ?>" name="jcartItemQty[]" size="3" type="text" value="<?= $item['qty']?>" />
					<span><?= $item['unit'] ?></span>
					<div><!--price-->
					<? if ($item['subtotal'] != $item['price'] * $item['qty']): ?>
					<span class="striked"><?= $item['price'] * $item['qty'] ?></span> (-<?=max($item['discount'],$this->discount)?>%) <span class="attention">
					<? else: ?>
					<div><span class="attention">
					<? endif; ?>
					<?= number_format($item['subtotal'], $priceFormat['decimals'], $priceFormat['dec_point'], $priceFormat['thousands_sep']) ?> <?= $currencySymbol ?></span></div><!--/price-->
					</div>
					<input name="jcartItemPrice[]" type="hidden" value="<?= $item['price'] ?>" />
					<a class="jcart-remove" href="/<?=$config['checkoutPath']?>?jcartRemove=<?= $item['id'] ?>"><?= $config['text']['removeLink'] ?></a>
				</td>
			</tr>
			<? endforeach; ?>
		<? else: ?>
			<tr>
				<td id="jcart-empty" colspan="2"><?= $config['text']['emptyMessage'] ?></td>
			</tr>
		<? endif; ?>
		</tbody>
		<tfoot>
			<tr>
				<th colspan='2'>
					<? if ($isCheckout !== true): ?>
					<input type="<?= $inputTypeCheckout ?>" <?= (isset($srcCheckout)) ? $srcCheckout : '' ?> id="jcart-checkout" name="jcartCheckout" class="jcart-button" value="<?= $config['text']['checkout'] ?>" />
					<? endif; ?>
					<!--subtotal-->
					<span id="jcart-subtotal"><?= $config['text']['subtotal'] ?>: <strong><?= number_format($this->subtotal, $priceFormat['decimals'], $priceFormat['dec_point'], $priceFormat['thousands_sep']) ?> <?= $currencySymbol ?></strong></span>
					<script type="text/javascript">
						var cart_subtotal = <?= $this->subtotal ?>;
					</script>
					<!--subtotal end-->
				</th>
			</tr>
		</tfoot>
	</table>
	<div id="jcart-buttons">
		<input type="<?= $inputTypeUpdate ?>" <?= (isset($srcUpdate)) ? $srcUpdate : '' ?> name="jcartUpdateCart" value="<?= $config['text']['update'] ?>" class="jcart-button" />
		<input type="<?= $inputTypeEmpty ?>" <?= (isset($srcEmpty)) ? $srcEmpty : '' ?> name="jcartEmpty" value="<?= $config['text']['emptyButton'] ?>" class="jcart-button" />
	</div>
</form>

<?=(isset($modal_cart))?$modal_cart:''?>

<div id="jcart-tooltip"></div>