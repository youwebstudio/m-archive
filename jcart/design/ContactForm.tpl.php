<h3 id = "connect">Отправить сообщение</h3>
<script type = "text/javascript">
	function form_validator(form) {
		<?if (isset($config['form']['feedback']['name']['required']) && $config['form']['feedback']['name']['required'] == true):?>
		if (form.name.value == '') {
			alert('Пожалуйста, укажите Ваше имя.');
			form.name.focus();
			return false;
		}
		<?endif;?>
		<?if (isset($config['form']['feedback']['email']['required']) && $config['form']['feedback']['email']['required'] == true):?>
		if (form.email.value == '') {
			alert('Пожалуйста, укажите Ваш email адрес.');
			form.email.focus();
			return false;
		}
		<? endif ?>
		<?if (isset($config['form']['feedback']['phone']['required']) && $config['form']['feedback']['phone']['required'] == true):?>
		if (form.phone.value == '') {
			alert('Пожалуйста, укажите Ваш email адрес.');
			form.phone.focus();
			return false;
		}
		<? endif ?>
		<? if (isset($config['form']['feedback']['subject']['required']) && $config['form']['feedback']['subject']['required'] == true): ?>
		if (form.subject.value == '') {
			alert('Пожалуйста, укажите тему сообщения.');
			form.subject.focus();
			return false;
		}
		<? endif ?>
		<?if (isset($config['form']['feedback']['comment']['required']) && $config['form']['feedback']['comment']['required'] == true):?>
		if (form.comment.value == '') {
			alert('Вы не заполнили текст сообщения.');
			form.text.focus();
			return false;
		}
		<? endif ?>
		if (confirm('Проверьте ваши данные перед отправкой, чтобы мы могли с вами связаться. Всё верно?'))
			form.nospam.value = '<?= (isset($antispam)) ? $antispam : '' ?>';
		else
			return false;

		return true;
	}
</script>
<noscript><p class = "error">JavaScript в браузере отключён. Отправка не сработает.</p></noscript>
<? if (isset($form['message'])): ?>
<p class = "error"><?= $form['message'] ?></p>
<? endif ?>
<form action = "#connect" method = "post" class = "form contact" onsubmit = "return form_validator(this);">
	<?if(isset($config['form']['feedback']['email']['enabled']) && $config['form']['feedback']['email']['enabled'] == true):?>
	<p>
		<label><?=$config['form']['feedback']['name']['label']?> <?=(isset($config['form']['feedback']['name']['required']) && $config['form']['feedback']['name']['required'] == true)? '<span class="attention" title="Поле, обязательное к заполнению">*</span>':''?></label><br>
		<input type="text" name="name" value="<?= (isset($form['name'])) ? $form['name'] : '' ?>" size = "30">
	</p>
	<?endif;?>

	<?if(isset($config['form']['feedback']['email']['enabled']) && $config['form']['feedback']['email']['enabled'] == true):?>
	<p>
		<label><?=$config['form']['feedback']['email']['label']?> <?=(isset($config['form']['feedback']['email']['required']) && $config['form']['feedback']['email']['required'] == true)? '<span class="attention" title="Поле, обязательное к заполнению">*</span>':''?></label><br>
		<input type="text" name="email" value="<?= (isset($form['email'])) ? $form['email'] : '' ?>" size = "30">
	</p>
	<?endif;?>

	<?if(isset($config['form']['feedback']['phone']['enabled']) && $config['form']['feedback']['phone']['enabled'] == true):?>
	<p>
		<label><?=$config['form']['feedback']['phone']['label']?> <?=(isset($config['form']['feedback']['phone']['required']) && $config['form']['feedback']['phone']['required'] == true)? '<span class="attention" title="Поле, обязательное к заполнению">*</span>':''?></label><br>
		<input type="text" name="phone" value="<?= (isset($form['phone'])) ? $form['phone'] : '' ?>" size = "30">
	</p>
	<?endif;?>

	<?if(isset($config['form']['feedback']['subject']['enabled']) && $config['form']['feedback']['subject']['enabled'] == true):?>
	<p>
		<label><?=$config['form']['feedback']['subject']['label']?> <?=(isset($config['form']['feedback']['subject']['required']) && $config['form']['feedback']['subject']['required'] == true)? '<span class="attention" title="Поле, обязательное к заполнению">*</span>':''?></label><br>
		<input type="text" name="subject" value="<?= (isset($form['subject'])) ? $form['subject'] : '' ?>" size = "30">
	</p>
	<?endif;?>

	<?if(isset($config['form']['feedback']['comment']['enabled']) && $config['form']['feedback']['comment']['enabled'] == true):?>
	<p>
		<label><?=$config['form']['feedback']['comment']['label']?> <?=(isset($config['form']['feedback']['comment']['required']) && $config['form']['feedback']['comment']['required'] == true)? '<span class="attention" title="Поле, обязательное к заполнению">*</span>':''?></label><br>
		<textarea name = "comment" cols = "75" rows = "10"><?= (isset($form['comment'])) ? $form['comment'] : '' ?></textarea>
	</p>
	<?endif;?>

	<p>
		<input type = "hidden" name = "nospam" value = "">
		<input type = "submit" name = "send" class = "submit" value = "Отправить" title = "Отправить сообщение">
	</p>
</form>