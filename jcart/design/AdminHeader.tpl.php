<?/*
Шаблон шапки админки
===============================
*/
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Управление заказами - <?=$config['sitename']?></title>
	<!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
    <link rel="stylesheet" href="<?= $config['sitelink'] . $config['jcartPath'] ?>bootstrap/css/bootstrap.css" type="text/css" media="screen, projection" />
	<style type="text/css">
		/*bootstrap navbar-fixed-top fix*/
		body { padding-top: 60px; padding-bottom: 70px; }
		@media screen and (max-width: 768px) {
		body { padding-top: 0; padding-bottom: 70px; }
		}

		.type2 {
			margin: 0 -20px !important;
			padding-left: 20px !important;
			width: 100%;
			position: fixed;
			bottom: 0px;
		}

		.type2 div {
			padding: 0;
			margin: 0;
			height: 30px;
		}

		@media screen and (max-width: 540px) {
			.btn-up, .btn-down {
				display: none;
			}
			.supreme{
				margin-right: 40px;
			}
		}

		.ruler {
			cursor: pointer;
			background-color: #FBFBFB;
			background-image: -o-linear-gradient(top, #FFFFFF, #F5F5F5);
			background-repeat: repeat-x;
			border: 1px solid #DDDDDD;
			border-radius: 3px;
			box-shadow: inset 0px 1px 0px #FFFFFF;
			margin: 0px 0px 18px;
			padding: 7px 14px;
		}

		.supreme{
			width: 100px;
			display: none;
		}

		.btn-up {
			margin-right: 40px;
		}

		.well-inverse {
			background-color: transparent;
		}

		.message-block {
			padding-bottom: 0;
		}

		.breadcrumb {
		background-color: #FBFBFB;
		background-image: -o-linear-gradient(top, #FFFFFF, #F5F5F5);
		background-repeat: repeat-x;
		border: 1px solid #DDDDDD;
		border-radius: 3px;
		box-shadow: inset 0px 1px 0px #FFFFFF;
		list-style: none outside none;
		margin: 0px 0px 18px;
		padding: 7px 14px;
		}

		@media (max-width: 979px) and (min-width: 767px) {
		.form-horizontal .control-label {
		float: none;
		width: auto;
		padding-top: 0;
		text-align: left;
		}
		.form-horizontal .controls {
		margin-left: 0;
		}
		.form-horizontal .control-list {
		padding-top: 0;
		}
		}

		.fix1 {
			height:60px;
		}

		table.no-deform {
			min-width:800px;
		}

		.check_all {
			cursor: pointer;
		}

		.alert {
			border: 1px solid #ededed;
			border-radius: 3px;
			box-shadow: inset 0px 0px 3px #fafafa;
		}
	</style>
    <link rel="stylesheet" href="<?= $config['sitelink'] . $config['jcartPath'] ?>bootstrap/css/bootstrap-responsive.css" type="text/css" media="screen, projection" />
	<script type="text/javascript" src="<?=$config['sitelink']?>jcart/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="<?= $config['sitelink'] . $config['jcartPath'] ?>bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(window).resize(function (){
				if($('body').width() > 980)
				{
					if($('body').width() < $('a.brand').width() + $('#nav1').width() + $('#nav2').width() + 70)
					{
						$('body').css('padding-top','100px');
						$('body').css('padding-bottom','70px');
						if($('body').width() < $('#nav1').width() + $('#nav2').width() + 50)
							$('body').css('padding-top','140px');
					}
					else
					{
						$('body').css('padding-top','60px');
						$('body').css('padding-bottom','70px');
					}
				}
				else
					$('body').css('padding-top','0');
			})
			if($('body').width() > 980)
			{
				if($('body').width() < $('a.brand').width() + $('#nav1').width() + $('#nav2').width() + 70)
				{
					$('body').css('padding-top','100px');
					$('body').css('padding-bottom','70px');
					if($('body').width() < $('#nav1').width() + $('#nav2').width() + 50)
						$('body').css('padding-top','140px');
				}
			}
			else
				$('body').css('padding-top','0');
		});
	</script>
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<header class="navbar navbar-fixed-top">
	<nav class="navbar-inner">
		<div class="container-fluid">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="<?=$config['sitelink']?>" title="<?=$config['sitename']?>"><span class="text-info"><?=$config['sitename']?></span></a>

			<div class="nav-collapse collapse">
				<ul class="nav" id="nav1">
					<?= GetMenuItems($file, $menu_items);?>
				</ul>

				<ul class="nav pull-right" id="nav2">
					<li>
						<a href="logout.php" onclick="return confirm('Вы действительно хотите выйти? Заново войти вы сможете только после перезагрузки браузера')"><i class='icon-off'></i>  Выйти</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>