<?/*
Шаблон формы заказа
===================
*/?>
<? if ($isCheckout != true || $isCheckout === true && count($this->get_contents()) > 0): ?>
<script type="text/javascript">
	function form_validator(id)
	{		
		form = document.getElementById(id);

		<? if (isset($config['form']['order']['lastname']['required']) && $config['form']['order']['lastname']['required'] == true): ?>
        if (form.order_lastname.value == '')
        {
            alert('Пожалуйста, укажите Вашу фамилию.');
            form.order_lastname.focus();
            return false;
        }
		<? endif ?>

		<? if (isset($config['form']['order']['name']['required']) && $config['form']['order']['name']['required'] == true): ?>
		if (form.order_name.value == '')
		{
			alert('Пожалуйста, укажите Ваше имя.');
			form.order_name.focus();
			return false;
		}
		<? endif ?>

		<? if (isset($config['form']['order']['fathername']['required']) && $config['form']['order']['fathername']['required'] == true): ?>
        if (form.order_fathername.value == '')
        {
            alert('Пожалуйста, укажите Ваше отчество.');
            form.order_fathername.focus();
            return false;
        }
		<? endif ?>
		
		<? if (isset($config['form']['order']['email']['required']) && $config['form']['order']['email']['required'] == true): ?>
		if (form.order_email.value == '')
		{
			alert('Пожалуйста, укажите Ваш email адрес.');
			form.order_email.focus();
			return false;
		}
		<? endif ?>

		<? if (isset($config['form']['order']['phone']['required']) && $config['form']['order']['phone']['required'] == true): ?>
		if (form.order_phone.value == '')
		{
			alert('Пожалуйста, укажите Ваш контактный телефон.');
			form.order_phone.focus();
			return false;
		}
		<? endif ?>

		<? if (isset($config['form']['order']['zip']['required']) && $config['form']['order']['zip']['required'] == true): ?>
		if (form.order_zip.value == '')
		{
			alert('Пожалуйста, укажите Ваш почтовый индекс.');
			form.order_zip.focus();
			return false;
		}
		<? endif ?>

		<? if (isset($config['form']['order']['region']['required']) && $config['form']['order']['region']['required'] == true): ?>
		if (form.order_region.value == '')
		{
			alert('Пожалуйста, укажите Ваш регион.');
			form.order_region.focus();
			return false;
		}
		<? endif ?>

		<? if (isset($config['form']['order']['city']['required']) && $config['form']['order']['city']['required'] == true): ?>
		if (form.order_city.value == '')
		{
			alert('Пожалуйста, укажите Ваш город.');
			form.order_city.focus();
			return false;
		}
		<? endif ?>

		<? if (isset($config['form']['order']['address']['required']) && $config['form']['order']['address']['required'] == true): ?>
		if (form.order_address.value == '')
		{
			alert('Пожалуйста, укажите Ваш адрес.');
			form.order_address.focus();
			return false;
		}
		<? endif ?>

		<? if (isset($config['form']['order']['comment']['required']) && $config['form']['order']['comment']['required'] == true): ?>
		if (form.order_comment.value == '')
		{
			alert('Вы не заполнили текст сообщения.');
			form.order_comment.focus();
			return false;
		}
		<? endif ?>

		if ($('#jcart input[name="order_payment"]:checked').val() == 'invoice')
		{
            if ($('#jcart input[name="invoice_type"]:checked').val() == 'juridical')
            {
                if (form.juridical_inn.value == '')
                {
                    alert('Пожалуйста, укажите ИНН Вашей фирмы.');
                    form.juridical_inn.focus();
                    return false;
                }

                if (form.juridical_kpp.value == '')
                {
                    alert('Пожалуйста, укажите КПП Вашей фирмы.');
                    form.juridical_kpp.focus();
                    return false;
                }

                if (form.juridical_firm.value == '')
                {
                    alert('Пожалуйста, укажите название Вашей фирмы.');
                    form.juridical_firm.focus();
                    return false;
                }
            }
            else if ($('#jcart input[name="invoice_type"]:checked').val() == 'physical')
            {
                // Ничего :)
            }
            else
            {
                alert('Пожалуйста, укажите тип счёта.');
                $('#jcart input[name="invoice_type"]:checked').focus();
                return false;
            }
		}

		if (confirm('Проверьте ваши данные перед отправкой, чтобы мы могли с вами связаться. Всё верно?'))
			form.order_nospam.value = '<?= (isset($antispam)) ? $antispam : '' ?>';
		else
			return false;

		return true;
	}

    function show_invoice()
    {
        if ($('#jcart input[name="order_payment"]:checked').val() == 'invoice')
            $('#invoice_form').css('display', 'block');
        else
            $('#invoice_form').css('display', 'none');
    }

    function show_payment_info()
    {
        <?if($config['encoding'] == 'windows-1251')
		{
			foreach($config['payments'] as $key => $tmp_payment)
			{
				array_walk_recursive($tmp_payment, create_function('&$val, $key', '$val = iconv("windows-1251", "utf-8", $val);'));
				$output1[$key] = $tmp_payment;
			}
		}
		else
			$output1 = $config['payments'];
		?>
		var payments = eval(<?= json_encode($output1) ?>);
        var orderPayment = $('#jcart input[name="order_payment"]:checked').val();

        for (var key in payments)
        {
            $('#jcart #payment_info #' + key).css('display', 'none');

            if (key == orderPayment)
                $('#jcart #payment_info #' + key).css('display', 'block');
        }
    }

	function show_delivery_info()
	{
		<?if($config['encoding'] == 'windows-1251')
		{
			foreach($config['deliveries'] as $key => $tmp_delivery)
			{
				array_walk_recursive($tmp_delivery, create_function('&$val, $key', '$val = iconv("windows-1251", "utf-8", $val);'));
				$output2[$key] = $tmp_delivery;
			}
		}
		else
			$output2 = $config['deliveries'];
		?>
		var deliveries = eval(<?= json_encode($output2) ?>);
		var orderDelivery = $('#jcart input[name="order_delivery"]:checked').val();
		var full_sum = cart_subtotal;

		for (var key in deliveries)
		{
			$('#jcart #delivery_info #' + key).css('display', 'none');

			if (key == orderDelivery)
			{
				$('#jcart #delivery_info #' + key).css('display', 'block');
				if(parseInt(deliveries[key]['cost']))
					full_sum += parseInt(deliveries[key]['cost']);
				$('#full_sum').html(full_sum + " <?= $currencySymbol ?>");
			}
		}
	}

	function show_invoice_form()
	{
		if ($('#jcart input[name="invoice_type"]:checked').val() == 'juridical')
		{
			$('#juridical-invoice').css('display', 'block');
		}
		else
		{
			$('#juridical-invoice').css('display', 'none');
		}
	}

</script>
<div id="order">
    <form method="post" action="<?= $checkout ?>" class="order_form" id="order_form">
		<? if (isset($_SESSION['order_tmp']['message'])): ?>
			<p style="color: red;"><?= $_SESSION['order_tmp']['message'] ?></p>
			<?unset($_SESSION['order_tmp']['message']) ?>
		<? endif; ?>
		<div class="deliveryMethod">
		<p>
			<label class="block labTitle">Выберите способ доставки:</label>
			<? $k = 1; foreach ($config['deliveries'] as $type => $delivery): ?>
				<? if ($delivery['enabled'] == true): ?>
					<label style="float:left;"><input type="radio" name="order_delivery" value="<?= $type ?>" <? if ((isset($_SESSION['order_tmp']['delivery']) && $type == $_SESSION['order_tmp']['delivery']) || (!isset($_SESSION['order_tmp']['delivery']) && $k == 1)): ?> checked="checked" <? endif; ?> onchange="show_delivery_info();"><span style="margin-right:30px; margin-left:5px; font-size:15px; line-height:40px;"><?= $delivery['title'] ?></span></label>
				<? $k++; ?>
				<? endif; ?>			
			<? endforeach; ?>		
			<div class="clear"></div>

			<div id="delivery_info">
				<? foreach ($config['deliveries'] as $type => $delivery): ?>
				<? if ($delivery['enabled'] == true): ?>
					<p id="<?= $type ?>"><?= $delivery['info'] ?></p>
					<? endif; ?>
				<? endforeach; ?>
			</div>
		</p>
		</div>
		<div class="payMethod">
		<p>
			<label class="block labTitle">Выберите метод оплаты:</label>
			<? $k = 1; foreach ($config['payments'] as $type => $payment): ?>
				<? if ($payment['enabled'] == true): ?>
					<label style="float:left;"><input type="radio" name="order_payment" value="<?= $type ?>" <? if ((isset($_SESSION['order_tmp']['payment']) && $type == $_SESSION['order_tmp']['payment']) || (!isset($_SESSION['order_tmp']['payment']) && $k == 1)): ?> checked="checked" <? endif; ?> onchange="show_invoice(); show_payment_info();"><span style="margin-right:30px; margin-left:5px; font-size:15px; line-height:40px;"><?= $payment['title'] ?></span></label>
				<? $k++; ?>
				<? endif; ?>			
			<? endforeach; ?>
		</p>
		<div class="clear"></div>
		
		<div id="payment_info">
			<? foreach ($config['payments'] as $type => $payment): ?>
			<? if ($payment['enabled'] == true): ?>
				<p id="<?= $type ?>"><?= $payment['info'] ?></p>
				<? endif; ?>
			<? endforeach; ?>
		</div>

		<div id="invoice_form"<? if (!isset($order_payment) || $order_payment != 'invoice'): ?> style="display: none;"<? endif; ?>>
			<p>
				<label style="display:block;">Тип счёта:</label>
				<label style="float:left;"><input type="radio" name="invoice_type" value="physical" <? if (empty($juridical)): ?>checked="checked"<? endif; ?> onchange="show_invoice_form();"><span style="margin-right:30px; margin-left:5px; font-size:15px; line-height:40px;">Физическое лицо</span></label>
				<label><input type="radio" name="invoice_type" value="juridical" <? if (!empty($juridical)): ?>checked="checked"<? endif; ?> onchange="show_invoice_form();"><span style="margin-right:30px; margin-left:5px; font-size:15px; line-height:40px;">Юридическое лицо</span></label>
			</p>
			<div id="juridical-invoice" <? if (empty($juridical)): ?>style="display: none;"<? endif; ?>>
				<p>
					<label>И.Н.Н.:</label>
					<input type="text" name="juridical_inn" value="<?= (isset($juridical_inn)) ? $juridical_inn : '' ?>" class="input">
				</p>
				<p>
					<label>К.П.П.:</label>
					<input type="text" name="juridical_kpp" value="<?= (isset($juridical_kpp)) ? $juridical_kpp : '' ?>" class="input">
				</p>
				<p>
					<label>Название фирмы:</label>
					<input type="text" name="juridical_firm" value='<?= (isset($juridical_firm)) ? $juridical_firm : '' ?>' class="input">
				</p>
			</div>
		</div>
		</p>
		</div>
		<div class="lastPrice">
		<p>
			<h4>
				Общая стоимость заказа с учётом доставки: <span id="full_sum">Для отображения суммы включите JavaScript в браузере</span>
			</h4>
		</p>
		</div>
		<h3 class="formH3">Введите свои данные:</h3>
		<br>


		<p class="clearfix">
			<?if(isset($config['form']['order']['lastname']['enabled']) && $config['form']['order']['lastname']['enabled'] == true):?>
			<span class="f-left" style="display:block; width: 32%;">
				<label class="marg"><?=$config['form']['order']['lastname']['label']?> <?=(isset($config['form']['order']['lastname']['required']) && $config['form']['order']['lastname']['required'] == true)? '<span class="attention" title="Поле, обязательное к заполнению">*</span>':''?></label>
				<input type="text" name="order_lastname" value="<?= (isset($_SESSION['order_tmp']['lastname'])) ? $_SESSION['order_tmp']['lastname'] : ((isset($lastname)) ? $lastname : '') ?>" class="float-input">
			</span>
			<?endif;?>

			<?if(isset($config['form']['order']['name']['enabled']) && $config['form']['order']['name']['enabled'] == true):?>
			<span class="f-left" style="display:block; width: 32%;">
				<label><?=$config['form']['order']['name']['label']?> <?=(isset($config['form']['order']['name']['required']) && $config['form']['order']['name']['required'] == true)? '<span class="attention" title="Поле, обязательное к заполнению">*</span>':''?></label>
				<input type="text" name="order_name" value="<?= (isset($_SESSION['order_tmp']['name'])) ? $_SESSION['order_tmp']['name'] : ((isset($name)) ? $name : '') ?>" class="float-input">
			</span>
			<?endif;?>

			<?if(isset($config['form']['order']['fathername']['enabled']) && $config['form']['order']['fathername']['enabled'] == true):?>
			<span class="f-left" style="display:block; width: 32%;">
				<label><?=$config['form']['order']['fathername']['label']?> <?=(isset($config['form']['order']['fathername']['required']) && $config['form']['order']['fathername']['required'] == true)? '<span class="attention" title="Поле, обязательное к заполнению">*</span>':''?></label>
				<input type="text" name="order_fathername" value="<?= (isset($_SESSION['order_tmp']['fathername'])) ? $_SESSION['order_tmp']['fathername'] : ((isset($fathername)) ? $fathername : '') ?>" class="float-input">
			</span>
			<?endif;?>
		</p>
		
		<p class="clearfix">
		<?if(isset($config['form']['order']['email']['enabled']) && $config['form']['order']['email']['enabled'] == true):?>
			<span class="f-left" style="display:block; width: 50%;">
			<label  class="marg"><?=$config['form']['order']['email']['label']?> <?=(isset($config['form']['order']['email']['required']) && $config['form']['order']['email']['required'] == true)? '<span class="attention" title="Поле, обязательное к заполнению">*</span>':''?></label>
			<input type="text" name="order_email" value="<?= (isset($_SESSION['order_tmp']['email'])) ? $_SESSION['order_tmp']['email'] : ((isset($email)) ? $email : '') ?>" class="input">
			</span>
		<?endif;?>
		</p>
		
		<p class="clearfix">
		<?if(isset($config['form']['order']['phone']['enabled']) && $config['form']['order']['phone']['enabled'] == true):?>
			<label class="marg"><?=$config['form']['order']['phone']['label']?> <?=(isset($config['form']['order']['phone']['required']) && $config['form']['order']['phone']['required'] == true)? '<span class="attention" title="Поле, обязательное к заполнению">*</span>':''?></label>
			<input type="text" name="order_phone" value="<?= (isset($_SESSION['order_tmp']['phone'])) ? $_SESSION['order_tmp']['phone'] : ((isset($phone)) ? $phone : '') ?>" class="input">
		<?endif;?>
		</p>
		
		<p class="clearfix">
			<?if(isset($config['form']['order']['zip']['enabled']) && $config['form']['order']['zip']['enabled'] == true):?>
			<span class="f-left" style="display: block; width: 32%;">
				<label  class="marg"><?=$config['form']['order']['zip']['label']?> <?=(isset($config['form']['order']['zip']['required']) && $config['form']['order']['zip']['required'] == true)? '<span class="attention" title="Поле, обязательное к заполнению">*</span>':''?></label>
				<input type="text" name="order_zip" value="<?= (isset($_SESSION['order_tmp']['zip'])) ? $_SESSION['order_tmp']['zip'] : ((isset($zip)) ? $zip : '') ?>" class="float-input">
			</span>
			<?endif;?>

			<?if(isset($config['form']['order']['region']['enabled']) && $config['form']['order']['region']['enabled'] == true):?>
			<span class="f-left" style="display: block; width: 32%;">
				<label><?=$config['form']['order']['region']['label']?> <?=(isset($config['form']['order']['region']['required']) && $config['form']['order']['region']['required'] == true)? '<span class="attention" title="Поле, обязательное к заполнению">*</span>':''?></label>
				<input type="text" name="order_region" value="<?= (isset($_SESSION['order_tmp']['region'])) ? $_SESSION['order_tmp']['region'] : ((isset($region)) ? $region : '') ?>" class="float-input">
			</span>
			<?endif;?>

			<?if(isset($config['form']['order']['city']['enabled']) && $config['form']['order']['city']['enabled'] == true):?>
			<span class="f-left" style="display: block; width: 32%;">
				<label><?=$config['form']['order']['city']['label']?> <?=(isset($config['form']['order']['city']['required']) && $config['form']['order']['city']['required'] == true)? '<span class="attention" title="Поле, обязательное к заполнению">*</span>':''?></label>
				<input type="text" name="order_city" value="<?= (isset($_SESSION['order_tmp']['city'])) ? $_SESSION['order_tmp']['city'] : ((isset($city)) ? $city : '') ?>" class="float-input">
			</span>
			<?endif;?>
		</p>

		<?if(isset($config['form']['order']['address']['enabled']) && $config['form']['order']['address']['enabled'] == true):?>
		<p>
			<label  class="marg"><?=$config['form']['order']['address']['label']?> <?=(isset($config['form']['order']['address']['required']) && $config['form']['order']['address']['required'] == true)? '<span class="attention" title="Поле, обязательное к заполнению">*</span>':''?></label>
			<textarea name="order_address" rows='2' cols="50"><?= (isset($_SESSION['order_tmp']['address'])) ? $_SESSION['order_tmp']['address'] : ((isset($address)) ? $address : '') ?></textarea>
		</p>
		<?endif;?>

		<?if(isset($config['form']['order']['comment']['enabled']) && $config['form']['order']['comment']['enabled'] == true):?>
		<p>
			<label><?=$config['form']['order']['comment']['label']?> <?=(isset($config['form']['order']['comment']['required']) && $config['form']['order']['comment']['required'] == true)? '<span class="attention" title="Поле, обязательное к заполнению">*</span>':''?></label>
			<textarea name="order_comment" rows="5" cols="50"><?= (isset($_SESSION['order_tmp']['comment'])) ? $_SESSION['order_tmp']['comment'] : ((isset($comment)) ? $comment : '') ?></textarea>
		</p>
		<?endif;?>

		<p>
			<input type="hidden" name="order_nospam" value="">
			<input type="hidden" id="jcart-is-checkout" name="jcartIsCheckout" value="true" />
			<input type="<?= $inputTypeCheckoutPaypal ?>" <?= (isset($srcCheckoutPaypal)) ? $srcCheckoutPaypal : '' ?> id="jcart-paypal-checkout" name="jcartPaypalCheckout" value="<?= $config['text']['checkoutPaypal'] ?>" <?= (isset($disablePaypalCheckout)) ? $disablePaypalCheckout : '' ?>  onclick="return form_validator('order_form')"/>
		</p>
    </form>
</div>

<script type="text/javascript">
    show_payment_info();
    show_delivery_info();
</script>
<? else: ?>
	<br>
	<p>В вашей корзине пусто. Добавьте, пожалуйста, товары для оформления заказа.</p>
<? endif; ?>