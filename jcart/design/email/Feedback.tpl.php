<?if($config['form']['feedback']['name']['enabled'] == true && !empty($fromName)):?>
<p><b><?=$config['form']['feedback']['name']['label']?></b> <?= $fromName ?></p>
<?endif;?>
<?if($config['form']['feedback']['email']['enabled'] == true && !empty($fromEmail)):?>
<p><b><?=$config['form']['feedback']['email']['label']?></b> <?= $fromEmail ?></p>
<?endif;?>
<?if($config['form']['feedback']['phone']['enabled'] == true && !empty($fromPhone)):?>
<p><b><?=$config['form']['feedback']['phone']['label']?></b> <?= $fromPhone ?></p>
<?endif;?>
<?if($config['form']['feedback']['subject']['enabled'] == true && !empty($subject)):?>
<p><b><?=$config['form']['feedback']['subject']['label']?></b><br><?= $subject ?></p>
<?endif;?>
<?if($config['form']['feedback']['comment']['enabled'] == true && !empty($content)):?>
<p><b><?=$config['form']['feedback']['comment']['label']?></b><br><?= $content ?></p>
<?endif;?>
<p><b>Данные отправки:</b><br>
<?= date("d.m.Y") ?><br>
<?= $_SERVER['REMOTE_ADDR'] ?></p>