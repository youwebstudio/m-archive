<p>Здравствуйте, Вы зарегистрировались на сайте <a href="<?= $sitelink ?>"><?= $sitename ?></a></p>

<p><strong>Ваши данные доступа:</strong></p>
<p>E-mail: <?= $email ?><br>
Пароль: <?= $password ?></p>

<p>Для входа можете использовать эту ссылку: <a href="<?= $sitelink . $authURL ?>?login&email=<?= $email ?>&pass=<?= $password ?>">Войти</a></p>

<p>Ждём вас в любое время!</p>