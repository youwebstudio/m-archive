<? if (!$admin): ?>
<?= $config['email']['answerMessage'] ?>
<? endif; ?>
<p><strong>Заказ №:</strong> <?= $id_order ?></p>
<h3>Данные заказчика:</h3>
<? if(!empty($lastName) && !empty($name) && !empty($fatherName)): ?>
	<p><strong>ФИО:</strong> <?= $lastName ?> <?= $name ?> <?= $fatherName ?></p>
<? else: ?>
	<?if($config['form']['order']['lastname']['enabled'] == true && !empty($lastName)):?>
	<p><strong><?=$config['form']['order']['lastname']['label']?></strong> <?= $lastName ?></p>
	<? endif; ?>
	<?if($config['form']['order']['name']['enabled'] == true && !empty($name)):?>
	<p><strong><?=$config['form']['order']['name']['label']?></strong> <?= $name ?></p>
	<? endif; ?>
	<?if($config['form']['order']['fathername']['enabled'] == true && !empty($fatherName)):?>
	<p><strong><?=$config['form']['order']['fathername']['label']?></strong> <?= $fatherName ?></p>
	<? endif; ?>
<? endif; ?>
	<?if($config['form']['order']['email']['enabled'] == true && !empty($email)):?>
	<p><strong><?=$config['form']['order']['email']['label']?></strong> <?= $email ?></p>
	<? endif; ?>
	<?if($config['form']['order']['phone']['enabled'] == true && !empty($phone)):?>
	<p><strong><?=$config['form']['order']['phone']['label']?></strong> <?= $phone ?></p>
	<? endif; ?>
<?if(!empty($zip) || !empty($region) || !empty($city) || !empty($address)):?>
<p><strong>Адрес:</strong><br>
<? if ($zip): ?><?= $zip ?><?=($region || $city || $address)?', ':''?><? endif; ?>
<? if ($region): ?><?= $region ?><?=($city || $address)?', ':''?><? endif; ?>
<? if ($city): ?><?= $city ?><?=($address)?', ':''?><? endif; ?>
<?= $address ?></p>
<?endif;?>
<?if(!empty($juridical)):?>
<p><strong>И.Н.Н.:</strong> <?= $juridical[0] ?></p>
<p><strong>К.П.П.:</strong> <?= $juridical[1] ?></p>
<p><strong>Название фирмы:</strong> <?= $juridical[2] ?></p>
<?endif;?>
<?if($config['form']['order']['comment']['enabled'] == true && !empty($comment)):?>
<p><strong><?=$config['form']['order']['comment']['label']?></strong><br><?= $comment ?></p>
<? endif; ?>
<h3>Данные заказа:</h3>
<? foreach ($order_items as $item): ?>
    <p>Наименование:
    <? if (isset($item['url']) && $item['url']): ?>
        <a href="<?= $item['url'] ?>"><?= $item['product'] ?></a>
    <? else: ?>
        <?= $item['product'] ?>
    <? endif; ?>
	<?=((isset($item['size']) && $item['size']) || (isset($item['color']) && $item['color']) || (isset($item['param']) && $item['param']))? '(' : ''?>
	<? if (isset($item['color']) && $item['color']): ?><?=$item['color'] ?><?=((isset($item['size']) && $item['size']) || (isset($item['param']) && $item['param']))?' / ': ' )'?><? endif; ?>
	<? if (isset($item['size']) && $item['size']): ?><?=$item['size'] ?><?=((isset($item['param']) && $item['param']))? ' / ' : ' )'?><? endif; ?>
	<?if((isset($item['param']) && $item['param'])):?><?=$item['param'] . ' )'?><? endif; ?>
	<? if (isset($item['link']) && $item['link']): ?><br><br>Ссылка для скачивания: <a href="<?= $item['link'] ?>">скачать</a><? endif; ?>
    <br>
    Код товара: <?= $item['id'] ?><br>
    Цена: <?= $item['price'] ?> <?= $currencySymbol ?>
	<? if ($item['discount']): ?> - <?= $item['discount'] ?>% скидки = <?= $item['real_price'] ?> <?= $currencySymbol ?><? endif; ?><br>
    Количество: <?= $item['quantity'] ?><br>
    Всего: <?= $item['subtotal'] ?> <?= $currencySymbol ?></p>
<? endforeach; ?>
<p><strong>Сумма заказа:</strong> <?= number_format($order_sum - $delivery['cost'], 2, '.', '') ?> <?= $currencySymbol ?></p>
<p><strong>Форма оплаты:</strong> <?= $payment['title'] ?> – <?= $payment['info'] ?></p>
<? if (isset($payment['link'])): ?>
<p><strong>Оплатить заказ:</strong> <a href="<?= $payment['link'] ?>" title="С помощью RoboKassa">к оплате</a></p>
<? endif; ?>
<? if (isset($payment['form'])): ?>
<p><strong>Оплатить заказ:</strong> <?= $payment['form'] ?></p>
<? endif; ?>
<? if (!empty($payment['details'])): ?>
<p><strong>Реквизиты для оплаты:</strong> <?= $payment['details'] ?></p>
<? endif; ?>
<? if (isset($delivery)): ?>
<p><strong>Cпособ доставки:</strong> <?= $delivery['title'] ?> – <?= $delivery['info'] ?></p>
	<? if (isset($delivery['cost']) && $delivery['cost'] > 0): ?>
	<p><strong>Стоимость доставки:</strong> <?= $delivery['cost'] ?> <?= $currencySymbol ?></p>
	<p><strong>Итого:</strong> <?= $order_sum ?> <?= $currencySymbol ?></p>
	<? endif; ?>
<? endif; ?>
<p><strong>Данные отправки:</strong><br>
<?= date("d.m.Y") ?><br>
<?= $_SERVER['REMOTE_ADDR'] ?></p>
<? if (!$admin): ?>
<?= $config['email']['signature'] ?>
<? else: ?>
<hr><p><a href="<?= $config['sitelink'] . $config['jcartPath'] ?>admin/orders.php?order=<?= $id_order ?>">Редактировать заказ</a></p>
<? endif; ?>