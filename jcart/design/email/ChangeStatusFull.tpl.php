<p><b>Статус заказа #<?= $order['id_custom'] ?> изменён. Новый статус: <?= $status ?></b></p>
<h3>Данные заказчика:</h3>
<? if(!empty($order['lastname']) && !empty($order['name']) && !empty($order['fathername'])): ?>
<p><strong>ФИО:</strong> <?= $order['lastname'] ?> <?= $order['name'] ?> <?= $order['fathername'] ?></p>
<? else: ?>
<?if(!empty($order['lastname'])):?>
	<p><strong><?=$config['form']['order']['lastname']['label']?></strong> <?= $order['lastname'] ?></p>
	<? endif; ?>
<?if(!empty($order['name'])):?>
	<p><strong><?=$config['form']['order']['name']['label']?></strong> <?= $order['name'] ?></p>
	<? endif; ?>
<?if(!empty($order['fathername'])):?>
	<p><strong><?=$config['form']['order']['fathername']['label']?></strong> <?= $order['fathername'] ?></p>
	<? endif; ?>
<?if(!empty($order['email'])):?>
	<p><strong><?=$config['form']['order']['email']['label']?></strong> <?= $order['email'] ?></p>
	<? endif; ?>
<?if(!empty($order['phone'])):?>
	<p><strong><?=$config['form']['order']['phone']['label']?></strong> <?= $order['phone'] ?></p>
	<? endif; ?>
<? endif; ?>
<?if(!empty($order['zip']) || !empty($order['region']) || !empty($order['city']) || !empty($order['address'])):?>
<p><strong>Адрес:</strong><br>
	<? if ($order['zip']): ?><?= $order['zip'] ?><?=($order['region'] || $order['city'] || $order['address'])?', ':''?><? endif; ?>
	<? if ($order['region']): ?><?= $order['region'] ?><?=($order['city'] || $order['address'])?', ':''?><? endif; ?>
	<? if ($order['city']): ?><?= $order['city'] ?><?=($order['address'])?', ':''?><? endif; ?>
	<?= $order['address'] ?></p>
<?endif;?>
<?if($config['form']['order']['comment']['enabled'] == true && !empty($order['comment'])):?>
<p><strong><?=$config['form']['order']['comment']['label']?></strong><br><?= $order['comment'] ?></p>
<? endif; ?>
<h3>Данные заказа:</h3><? $sum = 0; ?>
<? foreach ($order_items as $item): ?>
<p>Наименование: <?= $item['product'] ?>
	<?=((isset($item['size']) && $item['size']) || (isset($item['color']) && $item['color']) || (isset($item['param']) && $item['param']))? '(' : ''?>
	<? if (isset($item['color']) && $item['color']): ?><?=$item['color'] ?><?=((isset($item['size']) && $item['size']) || (isset($item['param']) && $item['param']))?' / ': ' )'?><? endif; ?>
	<? if (isset($item['size']) && $item['size']): ?><?=$item['size'] ?><?=((isset($item['param']) && $item['param']))? ' / ' : ' )'?><? endif; ?>
	<?if((isset($item['param']) && $item['param'])):?><?=$item['param'] . ' )'?><? endif; ?>
	<? if (isset($item['link']) && $item['link']): ?><br><br>Ссылка для скачивания: <a href="<?= $item['link'] ?>">скачать</a><? endif; ?>

	<? if (isset($item['download_link'])): ?>Ссылка на скачивание: <a href="<?= $item['download_link'] ?>">скачать</a> (Этот товар вы всегда сможете обновить <a href="<?= $item['update_link'] ?>">здесь</a>)<br><? endif; ?>
	Код товара: <?= $item['id_product'] ?><br>
	Цена: <?= $item['price'] ?> <?= $currencySymbol ?>
	<? if ($item['discount']): ?> - <?= $item['discount'] ?>% скидки = <?= number_format($item['price'] * (1 - $item['discount'] / 100), 2, '.', '') ?> <?= $currencySymbol ?><? endif; ?><br>
	Количество: <?= $item['quantity'] ?><br>
	Всего: <?= $subtotal = number_format(($item['price'] * $item['quantity'] * (1 - $item['discount'] / 100)), $priceFormat['decimals'], $priceFormat['dec_point'], $priceFormat['thousands_sep']) ?> <?= $currencySymbol ?></p>
<? $sum += $subtotal; ?>
<? endforeach; ?>
<p><strong>Сумма заказа:</strong> <?= number_format($order['sum'] - $order['delivery_cost'], 2, '.', '') ?> <?= $currencySymbol ?></p>
<p><strong>Форма оплаты:</strong> <?= $payment['title'] ?> – <?= $payment['info'] ?></p>
<? if (isset($payment['link']) && $order['status'] != 1): ?>
<p><strong>Оплатить заказ:</strong> <a href="<?= $payment['link'] ?>" title="С помощью RoboKassa">к оплате</a></p>
<? endif; ?>
<? if (isset($payment['form']) && $order['status'] != 1): ?>
<p><strong>Оплатить заказ:</strong> <?= $payment['form'] ?></p>
<? endif; ?>
<? if (!empty($payment['details']) && $order['status'] != 1): ?>
<p><strong>Реквизиты для оплаты:</strong> <?= $payment['details'] ?></p>
<? endif; ?>
<? if (isset($delivery)): ?>
<p><strong>Cпособ доставки:</strong> <?= $delivery['title'] ?> – <?= $delivery['info'] ?></p>
<? if (isset($delivery['cost']) && $delivery['cost'] > 0): ?>
	<p><strong>Стоимость доставки:</strong> <?= $delivery['cost'] ?> <?= $currencySymbol ?></p>
	<p><strong>Итого:</strong> <?= $order['sum'] ?> <?= $currencySymbol ?></p>
	<? endif; ?>
<? endif; ?>
<p><strong>Данные отправки:</strong><br>
	<?= date("d.m.Y") ?></p>