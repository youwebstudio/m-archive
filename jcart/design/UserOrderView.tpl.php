<?/*
Шаблон формы редактирования заказа в админке
============================================
*/?>
<h2>Ваш заказ #<?= $order['id_custom'] ?></h2>

<? if (isset($message)): ?>
<p color="color: red;"><?= $message ?></p>
<? endif; ?>
<h3>Данные покупателя</h3>

<p>
	<strong>E-mail:</strong> <?= (isset($order['email'])) ? $order['email'] : '' ?>
</p>
<p>
	<strong>Имя:</strong> <?= (isset($order['lastname'])) ? $order['lastname'] : '' ?> <?= (isset($order['name'])) ? $order['name'] : '' ?> <?= (isset($order['fathername'])) ? $order['fathername'] : '' ?>
</p>
<p>
	<strong>Телефон:</strong> <?= (isset($order['phone'])) ? $order['phone'] : '' ?>
</p>
<p>
	<strong>Адрес:</strong> <?= (isset($order['zip'])) ? $order['zip'] : '' ?> <?= (isset($order['region'])) ? $order['region'] : '' ?> <?= (isset($order['city'])) ? $order['city'] : '' ?> <?= (isset($order['address'])) ? $order['address'] : '' ?>
</p>
<p>
	<strong>Комментарий:</strong> <?= (isset($order['comment'])) ? $order['comment'] : '' ?>
</p>
<h3>Данные заказа</h3>
<p>
	<strong>Форма оплаты:</strong> <?= $config['payments'][$order['payment']]['title'] ?>

	<? if ($order['payment'] == 'invoice'): ?>
		<? if (is_file(dirname(__FILE__) . '/../invoices/invoice_' . $order['id_custom'] . '.html')): ?>
			<a href="<?= $config['sitelink'] . $config['jcartPath'] ?>invoices/invoice_<?= $order['id_custom'] ?>.html" target="_blank">распечатать счёт</a>
		<? endif; ?>

		<? if (is_file(dirname(__FILE__) . '/../invoices/invoice_fact_' . $order['id_custom'] . '.html')): ?>
			<a href="<?= $config['sitelink'] . $config['jcartPath'] ?>invoices/invoice_fact_<?= $order['id_custom'] ?>.html" target="_blank">распечатать счёт-фактуру</a>
		<? endif; ?>
	<? endif; ?>
</p>
<p>
	<strong>Способ доставки:</strong> <?= $config['deliveries'][$order['delivery']]['title'] ?>
</p>
<? if (isset($order['delivery_cost'])): ?>
	<p>
		<strong>Стоимость доставки:</strong> <?= $order['delivery_cost'] ?> <?= $currencySymbol ?>
	</p>
<? endif; ?>
<p>
	<strong>Статус заказа:</strong> <?= $statuses[$order['status']] ?>
</p>
<p>
	<strong>Дата:</strong> <?= $order['date'] ?>
</p>
<? if (isset($order['delivery_cost'])): ?>
<p>
	<strong>Товаров на сумму:</strong> <?= number_format($order['sum']-$order['delivery_cost'], 2, '.', '') ?> <?= $currencySymbol ?>
</p>
<? endif; ?>
<p>
	<strong>Итого к оплате:</strong> <?= $order['sum'] ?> <?= $currencySymbol ?>
</p>

<h3>Товары</h3>
<p>
	<table class="table">
		<tr>
			<th>Код</th>
			<th>Товар</th>
			<th>Количество</th>
			<th>Цена</th>
			<th>Размер</th>
			<th>Цвет</th>
			<th>Доп. параметр</th>
			<? if ($config['download']['enabled'] && $order['status'] == 1): ?><th>&nbsp;</th><? endif ?>
		</tr>
		<? foreach ($order_items as $id_item => $order_item): ?>
		<tr>
			<td><?= $order_item['id_product'] ?></td>
			<td><?= $order_item['product'] ?></td>
			<td><?= $order_item['quantity'] ?></td>
			<td><?= number_format($order_item['price'] * (1 - $order_item['discount']/100), 2, '.', '') ?></td>
			<td><?= $order_item['size'] ?></td>
			<td><?= $order_item['color'] ?></td>
			<td><?= $order_item['param'] ?></td>
			<? if ($config['download']['enabled'] && $order['status'] == 1): ?><th><a href="<?= $config['sitelink'] . $config['jcartPath'] ?>update/<?= $order_item['id_product'] ?>/?email=<?= $order['email'] ?>">обновить</a></th><? endif ?>
		</tr>
		<? endforeach ?>
	</table>
</p>
<p>
	<a href="<?= $config['sitelink'] . $config['auth']['cabinetURL'] ?>">Перейти к списку заказов</a>
</p>
<p>
	<a href="<?= $config['sitelink'] ?>" title="Перейти на главную">Вернуться на главную страницу магазина</a>
</p>