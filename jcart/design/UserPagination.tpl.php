<?/*
Шаблон пагинации
================
$navi - массив данных
	page - текущая страница
	total - общее количество страниц
$file - файл, к которому подключается пагинация
$config - массив со всеми настройками
*/?>
<? if ($navi['total'] > 1): ?>
<div class="pagination">
	<? if ($navi['page'] != 1): ?>
	<a href="<?= $config['sitelink'] . $config['auth']['cabinetURL'] ?>" title="Перейти к первой странице"><</a>
	<? endif ?>

	<? if ($navi['page'] - 3 > 0): ?>
	<a href="<?= $config['sitelink']. $config['auth']['cabinetURL'] ?>?page=<?= $navi['page'] - 3 ?>" title="Перейти к <?= $page - 3 ?> странице"><?= $navi['page'] - 3 ?></a>
	<? endif ?>

	<? if ($navi['page'] - 2 > 0): ?>
	<a href="<?= $config['sitelink'] . $config['auth']['cabinetURL'] ?>?page=<?= $navi['page'] - 2 ?>" title="Перейти к <?= $page - 2 ?> странице"><?= $navi['page'] - 2 ?></a>
	<? endif ?>

	<? if ($navi['page'] - 1 > 0): ?>
	<a href="<?= $config['sitelink'] . $config['auth']['cabinetURL'] ?>?page=<?= $navi['page'] - 1 ?>" title="Перейти к <?= $navi['page'] - 1 ?> странице"><?= $navi['page'] - 1 ?></a>
	<? endif ?>

	<strong title="Вы здесь"><?= $navi['page'] ?></strong>

	<? if ($navi['page'] + 1 <= $navi['total']): ?>
	<a href="<?= $config['sitelink'] . $config['auth']['cabinetURL'] ?>?page=<?= $navi['page'] + 1 ?>" title="Перейти к <?= $page + 1 ?> странице"><?= $navi['page'] + 1 ?></a>
	<? endif ?>

	<? if ($navi['page'] + 2 <= $navi['total']): ?>
	<a href="<?= $config['sitelink'] . $config['auth']['cabinetURL'] ?>?page=<?= $navi['page'] + 2 ?>" title="Перейти к <?= $page + 2 ?> странице"><?= $navi['page'] + 2 ?></a>
	<? endif ?>	

	<? if ($navi['page'] + 3 <= $navi['total']): ?>
	<a href="<?= $config['sitelink'] . $config['auth']['cabinetURL'] ?>?page=<?= $navi['page'] + 3 ?>" title="Перейти к <?= $page + 3 ?> странице"><?= $navi['page'] + 3 ?></a>
	<? endif ?>

	<? if ($navi['page'] != $navi['total']): ?>
	<a href="<?= $config['sitelink'] . $config['auth']['cabinetURL'] ?>?page=<?= $navi['total'] ?>" title="Перейти к последней странице">></a>
	<? endif ?>
</div>
<? endif ?>