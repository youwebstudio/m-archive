<script type="text/javascript">
function form_validator(form)
{
	if (form.user_email.value == '')
	{
		alert('Пожалуйста, укажите Ваш email адрес.');
		form.user_email.focus();
		return false;
	}
	if (confirm('Всё верно?'))
		form.user_nospam.value='<?= (isset($antispam)) ? $antispam : '' ?>';
	else
		return false;

	return true;
}
</script>

<? if (isset($form['message'])): ?>
<p style="color:red;"><?= $form['message'] ?></p>
<? endif ?>

<form action="" method="post" onsubmit="return form_validator(this)";>
	<p>
		Здесь вы можете в любой момент восстановить ваш пароль. Введите адрес, на который была оформлена покупка, и в течении нескольких минут на него будет отправлено письмо с данными для входа.
	</p>
	<p>
		<label>Введите ваш email:</label>
		<input type="text" name="user_email" size="30" value=""> 
		<input type="hidden" name="user_nospam" value="">
		<input type="submit" name="user_remind" class="submit" value="Напомнить" title="Напомнить">
	</p>
</form>

<p>
	<a href="<?= $config['sitelink'] ?>" title="Перейти на главную">Вернуться на главную страницу магазина</a>
</p>