<?/*
Шаблон списка в админке
=======================
*/
include_once dirname(__FILE__) . '/../design/AdminHeader.tpl.php';
?>
<div class="container-fluid">

	<ul class="breadcrumb">
		<li>
			<a title="Вернуться к началу" href="index.php"><i class="icon-home"></i>&nbsp;Админ.интерфейс</a>
			<span class="divider">/</span>	</li>
		<li class="active">
			<a href="users.php">Клиенты</a>
		</li>
	</ul>

	<table class = "table table-hover table-striped table-bordered data-table no-deform">
		<tr>
			<th>#</th>
			<th>E-mail</th>
			<th>Дата регистрации</th>
			<th>Статус</th>
			<th>Действия</th>
		</tr>
		<? foreach ($users as $user): ?>
		<tr>
			<td><?= $user['id_user'] ?></td>
			<td><?= $user['email'] ?></td>
			<td><?= $user['date'] ?></td>
			<td><?= $user['status'] ?></td>
			<td>
				<a href = "<?= $config['sitelink'] . $config['jcartPath'] ?>admin/users.php?user=<?= $user['id_user'] ?>">Редактировать пользователя→</a><br>
				<a href = "<?= $config['sitelink'] . $config['jcartPath'] ?>admin/orders.php?user=<?= $user['id_user'] ?>">Перейти к заказам пользователя→</a>
			</td>
		</tr>
		<? endforeach ?>
	</table>

	<?= $pagination ?>
</div>
</body>
</html>