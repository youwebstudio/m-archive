<?/*
Шаблон формы редактирования пользователя в админке
============================================
*/
include_once dirname(__FILE__) . '/../design/AdminHeader.tpl.php';
?>
<div class="container-fluid">

	<ul class="breadcrumb">
		<li>
			<a title="Вернуться к началу" href="index.php"><i class="icon-home"></i>&nbsp;Админ.интерфейс</a>
			<span class="divider">/</span>	
		</li>
		
		<li>
			<a href="orders.php">Клиенты</a>
			<span class="divider">/</span>
		</li>
		
		<li class="active">
			<a href="orders.php?order=<?= $user['id_user'] ?>">Клиент #<?= $user['id_user'] ?></a>
		</li>
	</ul>
	<form action="" class="form-horizontal" method="post">
		<? if (isset($message)): ?>
		<p style="color: red;"><?= $message ?></p>
		<? endif; ?>
		<div class="row-fluid well well-inverse">
			<div class="span12">
				<h3>Информация о заказчике</h3><br />
				<div class="control-group">
					<label class="control-label">E-mail:</label>
					<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><a href="mailto:<?= $order['email'] ?>"<i class="icon-envelope"></i></a></span>
							<input type="text" name="user_email" value="<?= $user['email'] ?>" size="60">
						</div>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="page-parent">Статус:</label>
					<div class="controls">
						<div class="input-prepend">
						<span class="add-on"><i class="icon-check"></i></span>
							<select name="user_status">
								<? foreach ($statuses as $id => $status): ?>
								<? if ($id == $user['status']): ?>
								<option value="<?= $id ?>" selected="selected"><?= $status ?></option>
								<? else: ?>
								<option value="<?= $id ?>"><?= $status ?></option>
								<? endif ?>
								<? endforeach ?>
							</select>
						</div>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="page-parent">Дата регистрации:</label>
					<div class="controls">
					<div class="input-prepend">
						<span class="add-on"><i class="icon-calendar"></i></span><input type="text"  name="user_date" value="<?= $user['date'] ?>" size="16"><br>
					</div>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="page-parent">Сумма оплаченных заказов:</label>
					<div class="controls">
						<div class="input-append">
							<!--<span class="add-on"><i class="icon-star"></i></span>--><input type="text" name="order_sum" class="input-mini disabled" value="<?= $user['sum'] ?>" disabled><span class="add-on"><?= $currencySymbol ?></span>
						</div>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="page-parent">Сумма всех заказов:</label>
					<div class="controls">
						<div class="input-append">
							<!--<span class="add-on"><i class="icon-star"></i></span>--><input type="text" name="order_sum" class="input-mini disabled" value="<?= $user['whole_sum'] ?>" disabled><span class="add-on"><?= $currencySymbol ?></span>
						</div>
					</div>
				</div>

				<p>
					<a href="<?= $config['sitelink'] . $config['jcartPath'] ?>admin/orders.php?user=<?= $user['id_user'] ?>">Перейти к заказам пользователя→</a>
				</p>

			</div>

			<div class="form-actions type2">
				<input type="hidden" name="user_id" value="<?= $user['id_user'] ?>">
				<input type="submit" name="user_edit" class="btn btn-success" value="Сохранить">
				<input type="submit" name="user_delete" class="btn btn-danger" value="Удалить" class="delete" onclick="return confirm('Вы действительно хотите удалить клиента?')">
				<input type="submit" name="user_cancel" class="btn" value="Отмена">
			</div>
		</div>
	</form>
</div>
</body>
</html>