<?/*
Шаблон экспорта заказов в 1C
============================
*/?>
Заказ	<?= $order['id_custom'] . "\n" ?>
ФИО	<?= $order['lastname'] ?> <?= $order['name'] ?> <?= $order['fathername'] . "\n" ?>
Организация	<?= $order['firm'] . "\n" ?>
Телефон	<?= $order['phone'] . "\n" ?>
E-mail	<?= $order['email'] . "\n" ?>
Почтовый адрес	<? if ($order['zip']): ?><?= $order['zip'] ?><? endif; ?><? if ($order['region']): ?>, <?= $order['region'] ?><? endif; ?><? if ($order['city']): ?>, <?= $order['city'] ?><? endif; ?>, <?= $order['address'] . "\n" ?>
Дата	Д<?= $order['date'][8] . $order['date'][9] ?>.<?= $order['date'][5]. $order['date'][6] ?>.<?= $order['date'][0] . $order['date'][1] . $order['date'][2] . $order['date'][3] . "\n" ?>
Время	В<?= $order['date'][11] . $order['date'][12] ?>:<?= $order['date'][14] . $order['date'][15] ?>:<?= $order['date'][17] . $order['date'][18] . "\n" ?>
Комментарий<?= "\n" ?>
<?= $order['comment'] ?>
<?= "\n\n" ?>
Код	Кол-во	Цена<?= "\n" ?>
<? foreach ($order_items as $item): ?>
<?= $item['id_product'] ?>	<?= $item['quantity'] ?>	<?= $item['price'] . "\n" ?>
<? endforeach; ?>
<?= "\n\n" ?>