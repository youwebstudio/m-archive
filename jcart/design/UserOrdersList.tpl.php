<?/*
Шаблон списка заказов в кабинете пользователя
=============================================
*/?>
<h3>Список ваших заказов</h3>

<br><br>

<table class="table">
	<tr>
		<th>#</th>
		<!--<th>Имя</th>
		<th>Телефон</th>
		<th>Адрес</th>
		<th>Комментарий</th>-->
		<th>Сумма</th>
		<th>Дата</th>
		<th>Статус</th>
		<th>&nbsp;</th>
	</tr>
	<? foreach ($orders as $order): ?>
	<tr>
		<td><?= $order['id_custom'] ?></td>
		<!--<td><?= $order['name'] ?></td>
		<td><?= $order['phone'] ?></td>
		<td><?= $order['address'] ?></td>
		<td><?= $order['comment'] ?></td>-->
		<td><?= $order['sum'] ?></td>
		<td><?= $order['date'] ?></td>
		<td><?= $order['status'] ?></td>
		<td><a href="<?= $config['sitelink'] . $config['auth']['cabinetURL'] ?>?order=<?= $order['id_custom'] ?>">Перейти к заказу</a></td>
	</tr>
	<? endforeach ?>
</table>

<p>
	<label>Сумма оплаченных заказов:</label>
	<?= $user['sum'] ?> <?= $currencySymbol ?>
</p>
<p>
	<label>Сумма всех заказов:</label>
	<?= $user['whole_sum'] ?> <?= $currencySymbol ?>
</p>

<?= $pagination ?>

<p>
	<a href="<?= $config['sitelink'] ?>" title="Перейти на главную">Вернуться на главную страницу магазина</a>
</p>