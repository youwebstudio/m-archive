<?php
# Чтение товаров из БД

# Подключение модуля работы с базой данных
include_once dirname(__FILE__) . '/modules/M_DB.inc.php';
$mDB = M_DB::Instance();

$jtemp['url'] = strtok('http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'], '?');

if(isset($_GET['product']) && $jcart->config['products']['title_link'] === true)
{
	$code_products[0] = $_GET['product'];

	if(isset($code_category))
	{
		$entry = $code_category;
		unset($code_category);
	}
}
if(isset($_GET['sub_cat']) && $jcart->config['products']['sub_cat_link'] === true)
{
	$entry = $code_category;
	$code_category = $_GET['sub_cat'];
	if(isset($code_products))
		unset($code_products);
}

# Чтение данных из БД в зависимости от заданных параметров
if (isset($code_category) && !isset($code_products))
{	
	# Чтение всех категорий из базы в переменную $jcart_categories
	$jcart_categories = $mDB->GetAllItems('category');

	# Определение кодов и заголовков подкатегорий
	foreach ($jcart_categories as $category)
	{
		if ($category['parent'] == $code_category)
		{
			$code_subcategories[] = $category['code'];
			$title_subcategories[] = $category['title'];
        }

		# Определение заголовка и родителя категории
		if ($category['code'] == $code_category)
		{
			$category_title = $category['title'];
			$category_parent = $category['parent'];
		}
	}

	# Вывод ошибки, если такой категории не существует
	if (empty($category_title))
		error404(true, $jcart->config['encoding']);
	
	# Разбивка на страницы и сортировка
	if ($jcart->config['products']['pagination_enabled'] === true || $jcart->config['products']['sort_enabled'] === true)
	{
		$jtemp['cats'] = (isset($code_subcategories) && $jcart->config['products']['sub_cat'] == 'products') ? $code_subcategories : array();
		$jtemp['cats'][] = $code_category;

		
		//$jtemp['url'] = $mDB->GetPreviewFromText('?', 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
		
		if($jcart->config['products']['pagination_enabled'] === true)
		{
			if (isset($_GET['items']))
			{
				$_SESSION['jcart_pagination'] = $_GET['items'];
				//header('Location: ' . $jtemp['url']);
			}

			$jtemp['pagination'] = (isset($_SESSION['jcart_pagination'])) ? $_SESSION['jcart_pagination'] : $jcart->config['products']['pagination'];

			# Определение значений для пагинации
			//if (isset($_GET['page']) && $_GET['page'] == 1)
			//if (isset($_GET['page']))
				//header('Location: ' . $jtemp['url']);
			
			$jtemp['page'] = (isset($_GET['page'])) ? $_GET['page'] : 1;
			$navi = $mDB->PaginateWithArrayParam('product', $jtemp['page'], $jtemp['pagination'], 'category', $jtemp['cats']);

			# Заглушка для отображения списка подкатегорий
			if(!$navi && $jcart->config['products']['sub_cat'] == 'list')
			{
				$navi['total'] = 1;
				$navi['start'] = 1;
			}


			# Вывод ошибки при неверном значении $page
			if ($jtemp['page'] > $navi['total'])
				error404(true, $jcart->config['encoding']);
		}
		else
		{
			$navi['start'] = $jtemp['pagination'] = false;
		}

		#Save sort & sort_type to session
		if($jcart->config['products']['sort_enabled'] === true)
		{
			if (isset($_GET['sort']))
			{
				$_SESSION['sort'] = $_GET['sort'];
				//header('Location: ' . $jtemp['url']);
			}
			if (isset($_GET['sort_type']))
			{
				$_SESSION['sort_type'] = $_GET['sort_type'];
				//header('Location: ' . $jtemp['url']);
			}
			if (!isset($_SESSION['sort']))
			{
				$_SESSION['sort'] = $jcart->config['products']['sort'];
			}
			if (!isset($_SESSION['sort_type']))
				$_SESSION['sort_type'] = $jcart->config['products']['sort_type'];
		}
		else
		{
			$_SESSION['sort'] = $_SESSION['sort_type'] = false;
		}

		# Чтение товаров из базы в переменную $jcart_products
		$jcart_products = $mDB->GetPaginatedListWithArrayParam('product', $navi['start'], $jtemp['pagination'], 'category', $jtemp['cats'], $_SESSION['sort'], $_SESSION['sort_type']);
	}
	else
	{
		# Чтение всех товаров из базы в переменную $jcart_products
		$jcart_products = $mDB->GetAllItems('product');
	}
}
elseif (isset($code_products) && count($code_products) == 1 && !empty($code_products[0]))
{
    # Чтение товара из базы в переменную $jcart_products
    $jcart_products = $mDB->GetItemsByParam('product', 'code', $code_products[0]);
}
else
{
    # Чтение всех товаров из базы в переменную $jcart_products
    $jcart_products = $mDB->GetAllItems('product');
	$multi_check = true;
}

# Adding parameters to url(v_1.1)
if(!isset($_SESSION['check_GET']) && isset($code_category) && !isset($code_products))
{
	$url_param = array();
	//$jtemp['url'] = strtok('http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'], '?');
	if(isset($_SESSION['jcart_pagination']) && $_SESSION['jcart_pagination'] != $jcart->config['products']['pagination'])
		$url_param[] = 'items=' . $_SESSION['jcart_pagination'];
	if(isset($_SESSION['sort'])&& $_SESSION['sort']!=$jcart->config['products']['sort'])
		$url_param[] = 'sort=' . $_SESSION['sort'];
	if(isset($_SESSION['sort_type']) && $_SESSION['sort_type'] != $jcart->config['products']['sort_type'])
		$url_param[] = 'sort_type=' . $_SESSION['sort_type'];
	if(isset($_GET['page']) && $_GET['page']!= 1)
		$url_param[] = 'page=' .$_GET['page'];
	if(isset($_GET))
	{
		foreach($_GET as $get_param => $get_value)
		{
			if($get_param != 'items' && $get_param != 'sort' && $get_param != 'sort_type' && $get_param != 'page'
		)
				$url_param[] = $get_param .'='.$get_value;
		}
	}
	$add_url = '';
	if (isset($code_category))
	{
		if(count($url_param)>0)
		{
			foreach($url_param as $i=>$param)
			{
				if($i==0)
					$add_url .= '?' . $param;
				else
					$add_url .= '&' . strtok($param, '?');
			}
		}
	}

	unset($url_param);
	#Set check var. Needed for preventing endless loop.
	$_SESSION['check_GET'] = (isset($_POST)) ? $_POST : false;
	
	header('Location: ' . $jtemp['url'] . $add_url);
	exit;
}

#Clear check var
if(isset($_SESSION['check_GET']))
{
	if($_SESSION['check_GET'])
		$_POST = $_SESSION['check_GET'];
	unset($_SESSION['check_GET']);
}

# Заглушка для отображения списка подкатегорий
if (empty($jcart_products) && !empty($code_subcategories) && $jcart->config['products']['sub_cat'] == 'list')
	echo '';
elseif (empty($jcart_products))
    error404(true, $jcart->config['encoding']);
