<?php

# Значения по умолчанию (если товар не найден)
$product_titles = $product_codes = $product_prices = $product_discounts = $product_discount_dates = $product_descriptions = $product_quantities = $product_units = $product_colors = $product_sizes = $product_params = $product_cats = $prod_pics = array();

# Подключение модуля работы с товарами
include_once dirname(__FILE__) . '/modules/M_Products.inc.php';
$mProducts = M_Products::Instance();
include_once dirname(__FILE__) . '/modules/M_Pictures.inc.php';
$mPictures = M_Pictures::Instance();

# Перебираем все элементы массива товаров
foreach ($jcart_products as $product)
{
	if (isset($code_category) && empty($code_products) || isset($code_subcategories) && !isset($code_products))
	{
		$code_subcategories = (isset($code_subcategories)) ? $code_subcategories : array();

		# Выбираем товары нужной категории или подкатегорий
		#if ($product['category'] == $code_category || in_array($product['category'], $code_subcategories))
		if ($product['category'] == $code_category || (in_array($product['category'], $code_subcategories) && $jcart->config['products']['sub_cat'] == 'products'))
		{
			$time = strtotime($product['discount_date']);
			$product_discount_dates['Y'][] = date('Y', $time);
			$product_discount_dates['m'][] = date('m', $time);
			$product_discount_dates['d'][] = date('d', $time);
			$product_discount_dates['H'][] = date('H', $time);
			$product_discount_dates['i'][] = date('i', $time);
			$product_discount_dates['s'][] = date('s', $time);

			$product_titles[] = $product['title'];
			$product_codes[] = $product['code'];
			$product_prices[] = $product['price'];
			$product_discounts[] = ($product['discount_date'] > date('Y-m-d H:i:s')) ? $product['discount'] : '';
			$product_descriptions[] = $mDB->GetPreviewFromText('<!--more-->', $product['description']);
			$product_quantities[] = $product['quantity'];
			$product_units[] = $product['unit'];
			$product_colors[] = $product['color'];
			$product_sizes[] = $product['size'];
			$product_params[] = $product['param'];
			$product_cats[] = $product['category'];

			if ($jcart->config['images']['enabled'] === true)
				$prod_pics[] = $mPictures->GetProdPics($product['code'], $product['category']);
		}
	} elseif (isset($code_products) && !empty($code_products[0]))
	{
		# Выбираем товар по коду
		if (in_array($product['code'], $code_products))
		{
			$time = strtotime($product['discount_date']);
			$product_discount_dates['Y'][] = date('Y', $time);
			$product_discount_dates['m'][] = date('m', $time);
			$product_discount_dates['d'][] = date('d', $time);
			$product_discount_dates['H'][] = date('H', $time);
			$product_discount_dates['i'][] = date('i', $time);
			$product_discount_dates['s'][] = date('s', $time);

			$product_titles[] = $product['title'];
			$product_codes[] = $product['code'];
			$product_prices[] = $product['price'];
			$product_discounts[] = ($product['discount_date'] > date('Y-m-d H:i:s')) ? $product['discount'] : '';
			$product_descriptions[] = $mDB->GetContentFromText('<!--more-->', $product['description']);
			$product_quantities[] = $product['quantity'];
			$product_units[] = $product['unit'];
			$product_colors[] = $product['color'];
			$product_sizes[] = $product['size'];
			$product_params[] = $product['param'];
			$product_cats[] = $product['category'];

			if ($jcart->config['images']['enabled'] === true)
				$prod_pics[] = $mPictures->GetProdPics($product['code'], $product['category']);
		}
	} else
		# Выбираем любые товары
	{
		$time = strtotime($product['discount_date']);
		$product_discount_dates['Y'][] = date('Y', $time);
		$product_discount_dates['m'][] = date('m', $time);
		$product_discount_dates['d'][] = date('d', $time);
		$product_discount_dates['H'][] = date('H', $time);
		$product_discount_dates['i'][] = date('i', $time);
		$product_discount_dates['s'][] = date('s', $time);

		$product_titles[] = $product['title'];
		$product_codes[] = $product['code'];
		$product_prices[] = $product['price'];
		$product_discounts[] = ($product['discount_date'] > date('Y-m-d H:i:s')) ? $product['discount'] : '';
		$product_descriptions[] = $mDB->GetPreviewFromText('<!--more-->', $product['description']);
		$product_quantities[] = $product['quantity'];
		$product_units[] = $product['unit'];
		$product_colors[] = $product['color'];
		$product_sizes[] = $product['size'];
		$product_params[] = $product['param'];
		$product_cats[] = $product['category'];

		if ($jcart->config['images']['enabled'] == true)
			$prod_pics[] = $mPictures->GetProdPics($product['code'], $product['category']);
	}
}

# Валюта
$currencySymbol = $jcart->currencySymbol($jcart->config['currencyCode']);

# Подключение шаблона вывода товаров
if (count($product_codes) == 1 && !isset($code_category))
    include dirname(__FILE__) . '/design/ProductView.tpl.php';
else
	include dirname(__FILE__) . '/design/ProductsView.tpl.php';
if(count($product_codes) < 2 && !isset($_GET['page']))
	$multi_check = true;

$cat_add = ($jcart->config['products']['sub_cat_link'] === true && isset($_GET['sub_cat'])?'&sub_cat='. $_GET['sub_cat']:'');

$the_page = strtok('http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'], '?');

# Подключение шаблона пагинации
if (isset($navi['total']) && $jcart->config['products']['pagination_enabled'] === true && isset($code_category) && $navi['total'] > 1)
	include dirname(__FILE__) . '/design/Pagination.tpl.php';

# Подключение шаблона выбора количества товаров на странице
if ($jcart->config['products']['pagination_items_enabled'] === true && !empty($code_category) && !isset($multi_check))
	include dirname(__FILE__) . '/design/PaginationItems.tpl.php';

# Подключение шаблона сортировки
if($jcart->config['products']['sort_items_enabled'] === true && !empty($code_category) && !isset($multi_check))
	include dirname(__FILE__) . '/design/SortItems.tpl.php';

$code_products = array();