<?php

# jCart v1.3.5
# http://conceptlogic.com/jcart/
# http://jcart.info

# This file takes input from Ajax requests and passes data to jcart.php
# Returns updated cart HTML back to submitting page

# Include jcart before session start

include_once dirname(__FILE__) . '/jcart.inc.php';
// Set flag that this is a parent file.
define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);

if (file_exists(dirname(__FILE__) . '/../defines.php')) {
	include_once dirname(__FILE__) . '/../defines.php';
}

if (!defined('_JDEFINES')) {
	define('JPATH_BASE', dirname(__FILE__) .'/../');
	require_once JPATH_BASE.'/includes/defines.php';
}

require_once JPATH_BASE.'/includes/framework.php';

// Mark afterLoad in the profiler.
JDEBUG ? $_PROFILER->mark('afterLoad') : null;

// Instantiate the application.
$app = JFactory::getApplication('site');

// Initialise the application.
$app->initialise();

include_once (dirname(__FILE__) . '/jcart_init.inc.php');

header('Content-type: text/html; charset=' . $jcart->config['encoding']);

# Process input and return updated cart HTML
$jcart->display_cart();

?>