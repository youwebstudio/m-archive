���������� �� ������������� ������� ������� jCart 1.3

����������� �������: http://conceptlogic.com/jcart/
����� ����������� � ����������: http://neverlex.com
���� �������: http://jcart.info

1. ���������.

1.0. �������������� ���������.

	���������� ����� � ��������� ����� �� ������.
	���������� ����� �� ������ ����� jcart/. ��� ����� ������� ����� ������������.
	���������� � ����� jcart/install/.
	���������� ������ ������ ��������.
	���������� ��� �� ����.
	����� ��������� ������� ����� install/ c �������.

	����� ������� ��������� ����.

1.1. ��������� ������ Mini � Base. ����������

��� 1. � ����� ������ ����� ���������� ������ (�� ����� �����). ��������� ������:

	<?php

    # http://conceptlogic.com/jcart/
    # http://jcart.info

	# ����������� �������� include_once jcart.inc.php ����� ������� session_start()
	include_once 'jcart/jcart.inc.php';

	?>

	����� ����� ���������� jcart.inc.php �� �������� ������ ��� ������ ������.

��� 2. ��������� ������� � ������ ����� ��������:

	<div id="jcart"><?php $jcart->display_cart();?></div>
	
	�� �������� ���������� ������ (��������, checkout.php) � ������� ����� �� ���� ����� ���������� ������ ������� � ������ �������� ������.
	
	��������! ��� ������ ����������� �� �������� ������ ���� ���.
	
��� 3. � ������ <head> �������� ���������� CSS ����� � JavaScript �������:

	<link type="text/css" href="jcart/css/jcart.css" rel="stylesheet" media="screen, projection" />
	<script type="text/javascript" src="jcart/js/jquery-1.4.4.min.js">
	<script type="text/javascript" src="jcart/js/jcart.min.js">
	
��� 4. ��������� ����� � ���� �����. ��������, ���:

	<form method="post" action="" class="jcart">
		<!-- ������������ ������ -->
		<input type="hidden" name="jcartToken" value="<?php echo $_SESSION['jcartToken']; ?>" />
		<input type="hidden" name="my_item_id" value="mini" />
		<input type="hidden" name="my_item_name" value="jCart. ������ mini" />
		<input type="hidden" name="my_item_price" value="24.00" />

		<!-- �������������� ������. �������� ����� �������� � ���� <input type="text"> ��� <select>, ����� ������������ ��� ������� ������ ���. -->
		<input type="hidden" name="my_item_url" value="" />
		<input type="hidden" name="my_item_color" value="" />
		<input type="hidden" name="my_item_size" value="" />
		<input type="hidden" name="my_item_param" value="" />

		<h4>jCart. ������ mini</h4>
		
		<!-- ������ �������� �����������, ��������� �� ���� ������. -->
		<img src="config['sitelink'] . $jcart->config['jcartPath'] ?>images/mini.jpg" alt="jCart. ������ mini">
		<br>
		<span class="price">����: 24.00 $</span>
		<br>
		<label><input type="text" name="my_item_qty" value="1" size="2" /> ��.</label>
		<input type="submit" name="my_add_button" value="� �������" class="button" />
	</form>

	
	����� ����� ������ ���� ����� ����� ��, ��� � ���������� ������� (���� jcart/config.php).
	
��� 5. ��� ��������� ��������� ������ ����� jcart/config.php � jcart/config.inc.php. ��� ��� ��������� ��� ��������� � ����������� �������. �� �������� ����������������.
	
��� 6. ��� ��������� ���������� ������ ���� css/jcart.css
	
��� 7. �� ���������� ������ ������� � ����� design/. �������� ���� �������� ����� ���������� ����� �����: http://jcart.info/templates.html

	�������� �������:
	
	������� �������:
	----------------
	������ ������� - design/Cart.tpl.php
	������ ������� ������ ������� - design/CartSmall.tpl.php
	������ ����� ������ - design/CartOrderForm.tpl.php
	������ ����������� ����� ������ (��� �������� ������������ ������) - design/EmailForm.tpl.php
	
	������� �����:
	--------------
	������ ����� ��� ������������ ������� - design/invoices/Invoice.tpl.php
	������ ����� ��� ������������ ������� - design/invoices/Invoice.tpl.php
	
	������� ������ �������:
	-----------------------
	������ ������ ������ - design/ProductView.tpl.php
	������ ������ ������ ������� - design/ProductsView.tpl.php
	
	������� �����:
	--------------
	������ ������ ������ - design/Order.tpl.php
	������ ������� ������ �� ��������� ������� ������ - design/ChangeStatusFull.tpl.php
	������ �������� ������ �� ��������� ������� ������ - design/ChangeStatusSmall.tpl.php	
	
��� 8. �������������� ���������
	
	��� ����������� ���������� ������ � ����� jcart/js/jcart.min.js ������� var f='jcart' � ���������� ������ ���� �� �����. ��������, var f='http://mysite.ru/jcart'.
	
��� 9. ������� � ������� (jcart/admin) � �������� � ������� ������.
	
1.2. ��������� ������ Pro � Vip. �����������

1.2.1. ����� ����������� ������ � ������ ����� ��������

��� 1. � ����� ������ ����� ���������� ������ (�� ����� �����). ��������� ������:

	<?php

	# http://conceptlogic.com/jcart/
	# http://jcart.info

	# ����������� �������� include_once jcart.inc.php ����� ������� session_start()
	include_once 'jcart/jcart.inc.php';

	# ������ ������ �� ��
	include_once 'jcart/get-products.inc.php';

	?>
	
	����� ����� ���������� jcart.php �� �������� ������ ��� ������ ������.
	
	
���� 2-3. ��������� ���������� ���������

��� 4. �� ����� ������ ������� ��������� ���:

	<?php
	# ��������� ������������� ������
	$code_product = 'test-prod1';

	# ��������� ����������, ������� ���� ����� � ������� ������
	include 'jcart/view-products.inc.php';
	?>
	
	���� ����� ����� �������� ������ � ���� ������:
	
	<?php $code_product = 'test-prod1'; include 'jcart/view-products.inc.php'; ?>
	
	��� ����� ������� ������� ������������� ����������� �������. � ���� ������ ��� ������ �������� ������������ � ���������� � ��������� � ������ ����� �� ���������� ���� ������.
	
1.2.2 �������� ������ ������ �������

1.2.2.1. ����� ������ �������� �������

	����� ����� ���� ������� ������� �������. ����� ����� ���������� ������ ������.	

	<?php

	// http://conceptlogic.com/jcart/
	// http://jcart.info

	// ����������� �������� include_once jcart.inc.php ����� ������� session_start()
	include_once 'jcart/jcart.inc.php';

	// ������ ����� �������
	$code_products[] = 'test-prod1';
	$code_products[] = 'test-prod2';
	$code_products[] = 'test-prod3';

	// ������ ������ �� ��
	include_once 'jcart/get-products.inc.php';
	?>
	
	� � ������ ����� ���������� ����������:
	
	<? include_once 'jcart/view-products.inc.php'; ?>

1.2.2.2. ����� ������ ������� � �������� ���������

	������ ����� ������� ������� ������ ��� ���������. ��������, ���:	
	
	<?php

	// http://conceptlogic.com/jcart/
	// http://jcart.info

	// ����������� �������� include_once jcart.inc.php ����� ������� session_start()
	include_once 'jcart/jcart.inc.php';

	// ��� ��������� �������
	$code_category = 'test-cat1';

	// ������ ������ �� ��
	include_once 'jcart/get-products.inc.php';
	?>

1.2.2.3. ����� ������ ������� ��� ���������� ������

	����� ����� ������� ����� ������ �� �������� �������� ��� ������� � ����������� �� GET ���������.

	<?php

	// http://conceptlogic.com/jcart/
	// http://jcart.info

	// ����������� �������� include_once jcart.inc.php ����� ������� session_start()
	include_once 'jcart/jcart.inc.php';

	// ���� ������ ��������, �� ������� ��������� ������.
	if ($_GET['product'])
	{
		$code_products[] = $_GET['product'];
	}
	// ����� ������ ��� ������
	else
	{
		// ������ ����� �������
		$code_products[] = 'test-prod1';
		$code_products[] = 'test-prod2';
		$code_products[] = 'test-prod3';

		// ��� ��� ��������� �������
		// $code_category = 'test-cat1';
	}

	// ������ ������ �� ��
	include_once 'jcart/get-products.inc.php';
	?>
	
	�������� ������ ������� ����� ���������� � ��������, ������� ������������ � ������������.
	
���� 5-9. ��������� ���������� ���������

	���� �� ����������� ����� ������� �� ��, �� ��� ��������� ��������� ������� ����� ������� ������� jcart/design/ProductView.tpl.php � jcart/design/ProductsView.tpl.php.
	
	��� ������ Shop ������� �������� ����� � ����� .htaccess, ������� ����� � ����� �����, ������ RewriteBase /test/ �� ���� �� �����. ���� �� ����� � �����, �� RewriteBase /. ���� � ����� test, �� RewriteBase /test/.

2. �������� ���������� ������

2.1. ��� �������� ����������� ����� ������. � �����, ��� ������ ���� ������� ������� � ������ ���������� ������ ��������� ������� ��������� ��������:

	<div id="jcart"><?php $jcart->display_cart();?></div>

	� ������ ����� ������� �� ���� �������� ���������� �� ������.

3. �������.

3.1. �������� ������ ������� ����������� �����: http://jcart.info/faq.html

3.2. ������� ����� ���������� ����� � ��������� ����� ���������� �� info@jcart.info

������� �� ������������ ������� ������� ������� jCart!