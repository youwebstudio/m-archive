<?php

# jCart v1.3.5
# http://conceptlogic.com/jcart/
# http://jcart.info

# Этот файл возвращает массив настроек $config для использования в PHP скриптах
# Если файл вызван с помощью Ajax, то массив кодируется как JSON и выводится в браузер

# Не меняйте здесь, меняйте в config.php
include_once dirname(__FILE__) . '/config.php';

# Значения по умолчанию любых настроек, которые могут остаться пустыми
if (empty($config['currencyCode'])) $config['currencyCode']                       = 'USD';
if (empty($config['text']['cartTitle'])) $config['text']['cartTitle']             = 'Корзина';
if (empty($config['text']['singleItem'])) $config['text']['singleItem']           = 'товар';
if (empty($config['text']['multipleItems1'])) $config['text']['multipleItems1']   = 'товара';
if (empty($config['text']['multipleItems2'])) $config['text']['multipleItems2']   = 'товаров';
if (empty($config['text']['subtotal'])) $config['text']['subtotal']               = 'Всего';
if (empty($config['text']['update'])) $config['text']['update']                   = 'Обновить';
if (empty($config['text']['checkout'])) $config['text']['checkout']               = 'Оформить заказ';
if (empty($config['text']['checkoutPaypal'])) $config['text']['checkoutPaypal']   = 'Отправить заказ';
if (empty($config['text']['removeLink'])) $config['text']['removeLink']           = 'Удалить';
if (empty($config['text']['emptyButton'])) $config['text']['emptyButton']         = 'Очистить';
if (empty($config['text']['emptyMessage'])) $config['text']['emptyMessage']       = 'Ваша корзина пуста!';
if (empty($config['text']['itemAdded'])) $config['text']['itemAdded']             = 'Товар добавлен!';
if (empty($config['text']['priceError'])) $config['text']['priceError']           = 'Неверный формат цены!';
if (empty($config['text']['quantityError'])) $config['text']['quantityError']     = 'Количество товаров должно быть целым числом!';
if (empty($config['text']['checkoutError'])) $config['text']['checkoutError']     = 'Ваш заказ не может быть обработан!';
if (empty($config['text']['itemNotAdded'])) $config['text']['itemNotAdded']       = 'Количество этого товара должно быть целым числом!';

$i = 0;
foreach ($config['item'] as $key => $value)
{
	if ($i>1)
	{
		$j = $i-2;
		$config['param'][$j] = $key;
	}
	$i++;
}

# Вывод данных в сессию
if (isset($_GET['ajax']) && $_GET['ajax'] == 'true') {
	if($config['encoding'] == 'windows-1251')
	{
		array_walk_recursive($config['text'], create_function('&$val, $key', '$val = iconv("windows-1251", "utf-8", $val);'));
	}
	header('Content-type: application/json; charset=utf-8');
	echo json_encode($config);
}
# Или подключение настроек
else
	include_once dirname(__FILE__) . '/config.inc.php';