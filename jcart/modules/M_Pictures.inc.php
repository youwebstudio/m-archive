<?php
# Модель работы с файлами
include_once dirname(__FILE__) . '/M_Admin.inc.php';
include_once dirname(__FILE__) . '/MSQL.inc.php';

class M_Pictures
{
	private static $instance;	# ссылка на экземпляр класса
	private $admin;				# признак админа

	# Получение единственного экземпляра класса
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_Pictures();

		return self::$instance;
	}

	# Конструктор
	function __construct()
	{
		$this->admin = M_Admin::Instance();
	}

	# Определяем тип файла
	public function GetFileType($file)
	{
		if ($file['type'] == 'image/gif')
			$picType = 'gif';
		elseif ($file['type'] == 'image/png')
			$picType = 'png';
		elseif ($file['type'] == 'image/jpeg')
			$picType = 'jpg';
		else
			return false;
		
		return $picType;
	}

	# Загрузка изображения на сервер
	public function UploadPicture($file, $path, $dir)
	{
		# Проверка наличия прав
		if (!$this->admin->CheckLogin())
			return false;

		# Проверка на существование папки загрузки
		if (!is_dir($path))
			return false;

		$path .= '/' . $dir;
		if (!is_dir($path))
		mkdir($path, 0755);
		if (!is_dir($path.'/thumb/'))
		mkdir($path.'/thumb/', 0755);

		# Создание нового имени файла
		$filename = explode('.', $file['name']);
		$filename = str_replace('.' . $filename[1], '', $file['name']);
		$filename = $filename . '.jpg';

		# Составляем полный путь до файла
		$fullpath_new = $path . '/'. $filename;
		$fullpath_thumb = $path . '/thumb/' . $filename;

		# Ресайз изображений
		$file_new = $this->ResizePicture($file);
		$file_thumb = $this->ResizePicture($file, 2);
				
		# Загрузка файла
		if	(!@copy($file_new, $fullpath_new))
		{
			return false;
		}
		else
		{
			copy($file_thumb, $fullpath_thumb);
			chmod($fullpath_new, 0644);
			chmod($fullpath_thumb, 0644);
			return true;
		}
	}

	# Ресайз изображения
	//	filetype - тип файла (jpg, gif или png)
	//	rotate - поворот на количество градусов
	//	quality - качество изображения, по умолчанию 75%
	public function ResizePicture($image, $type = null, $rotate = null, $quality = null)
	{
		ini_set("gd.jpeg_ignore_warning", 1);

		# Определение типа файла
		$filetype = $this->GetFileType($image);

		# Качество изображения
		if ($quality == null)
			$quality = 75;

		# Cоздаём исходное изображение на основе исходного файла
		if ($filetype == 'jpg' || $filetype == 'jpeg' || $filetype == 'JPG' || $filetype == 'JPEG')
			$source = @imagecreatefromjpeg($image['tmp_name']);
		elseif ($filetype == 'png')
			$source = @imagecreatefrompng($image['tmp_name']);
		elseif ($filetype == 'gif')
			$source = @imagecreatefromgif($image['tmp_name']);
		else
			return false;

		if(!$source)
			return false;

		# Поворачиваем изображение
		if ($rotate == 270)
			$src = imagerotate($source, 270, 0); #поворот на 270 градусов
		elseif ($rotate == 90)
			$src = imagerotate($source, 90, 0); #поворот на 90 градусов
		elseif ($rotate == 180)
			$src = imagerotate($source, 180, 0); #поворот на 180 градусов
		else
			$src = $source;

		# Определяем ширину и высоту изображения
		$w_src = imagesx($src); 
		$h_src = imagesy($src);
		
		# Тип преобразования
		global $config;

		if ($type == null)
		{
			$type = 3;
		}

		if ($type == 1) # banner
			$w = TYPE_1_W;
		elseif ($type == 2) # thumb
			$w = $config['images']['thumb']['width'];
		elseif ($type == 3) # img
		{
			$h = $config['images']['height'];
			$w = $config['images']['width'];
		}
		elseif ($type == 4) # big
		{
			$h = TYPE_4_H;
			$w = TYPE_4_W;
		}
#		header("Content-type: image/jpeg");
		if ($type < 3 )
		{
			if ($w_src > $w)
			{
				
				# Вычисление пропорций
				$ratio = $w_src/$w;
				$w_dest = round($w_src/$ratio);
				$h_dest = round($h_src/$ratio);

				# Создаём пустую картинку
				# Важно именно truecolor!, иначе будем иметь 8-битный результат
				$dest = imagecreatetruecolor($w_dest, $h_dest);
				imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);

				# Вывод картинки и очистка памяти
				$name = dirname(__FILE__) . '/../tmp/' . mktime() . rand(0, 10000) . '.jpg';
				
				imagejpeg($dest, $name, $quality);
				imagedestroy($dest);
				imagedestroy($src);

				return $name;
			}
			else
			{
				$name = dirname(__FILE__) . '/../tmp/' . mktime() . rand(0, 10000) . '.jpg';
			
				# Вывод картинки и очистка памяти
				imagejpeg($src, $name, $quality);
				//imagedestroy($dest);
				imagedestroy($src);

				return $name;
			}
		}	
		elseif ( $w_src > $w || $h_src > $h)
		{
			$k1=$w/$w_src;
			$k2=$h/$h_src;
			$k=$k1>$k2?$k2:$k1;

			$w_dest=intval($w_src*$k);
			$h_dest=intval($h_src*$k);
			
			# Создаём пустую картинку
			# Важно именно truecolor!, иначе будем иметь 8-битный результат
			$dest = imagecreatetruecolor($w_dest, $h_dest);
			imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);

			# Вывод картинки и очистка памяти
			$name = dirname(__FILE__) . '/../tmp/' . mktime() . rand(0, 10000) . '.jpg';
			
			imagejpeg($dest, $name, $quality);
			imagedestroy($dest);
			imagedestroy($src);

			return $name;
		}
		else
		{
			$name = dirname(__FILE__) . '/../tmp/' . mktime() . rand(0, 10000) . '.jpg';
		
			# Вывод картинки и очистка памяти
			imagejpeg($src, $name, $quality);
			//imagedestroy($dest);
			imagedestroy($src);

			return $name;
		}
	}
	public function GetProdPics($product, $cat)
	{
		if (!is_dir(dirname(__FILE__) . '/../images/' . $cat .'/'))
			return false;
		# Парсим каталог
		$handle = opendir(dirname(__FILE__) . '/../images/' . $cat .'/');

		# Выбираем массив картинок
		if ($handle != false)
		{
			while (($file = readdir($handle)) !== false)
			{
				if ($file != '..' && $file != '.' && $file !='thumb')
				{					
					$i = strtok($file, '.');
					$patt = '|(.*)_(.*)|';
					preg_match($patt, $i, $j);
					if($product)
					{
						if($product == $j[1])
							$pictures[$j[2]] = $file;
					}
					else
					{
						$pictures[$i] = $file;
					}
				}
			}
			closedir($handle);
		}
		
		# Сортируем массив
		if (isset($pictures))
			natsort($pictures);
		else
			return false;
		return $pictures;
	}
	# Переименование изображения и его эскиза
	public function RenamePicture($product, $new_pic_id, $old_pic_id, $category)
	{
		# Генерируем пути
		$path = dirname(__FILE__) . '/../images/'. $category . '/' . $product . '_' . $old_pic_id . '.jpg';
		$newPath = dirname(__FILE__) . '/../images/'. $category . '/' . $product . '_' . $new_pic_id . '.jpg';
		
		if (is_file($newPath))
			return false;
		
		$thumbPath = dirname(__FILE__) . '/../images/'. $category . '/thumb/' . $product . '_' . $old_pic_id . '.jpg';
		$newThumbPath = dirname(__FILE__) . '/../images/'. $category . '/thumb/' . $product . '_' . $new_pic_id . '.jpg';		
		
		# Переименование
		rename($path, $newPath);
		rename($thumbPath, $newThumbPath);

		return true;
	}
		# Переименование изображения и его эскиза
	public function RenamePictures($old_prod_code, $new_prod_code, $category)
	{
		$pics = $this->GetProdPics($old_prod_code, $category);
		if(!$pics)
			return false;
		foreach($pics as $i=>$old_name)
		{
			$tmp = explode('_', $old_name);
			$new_name = $new_prod_code . '_' . $tmp[1];
			
			# Генерируем пути
			$path = dirname(__FILE__) . '/../images/'. $category . '/' . $old_name;
			$newPath = dirname(__FILE__) . '/../images/'. $category . '/' . $new_name;
			
			if (is_file($newPath))
				return false;
			
			$thumbPath = dirname(__FILE__) . '/../images/'. $category . '/thumb/' . $old_name;
			$newThumbPath = dirname(__FILE__) . '/../images/'. $category . '/thumb/' . $new_name;		
			
			# Переименование
			rename($path, $newPath);
			rename($thumbPath, $newThumbPath);			
		}
		return true;
	}
	public function ChengeCategory($old_cat, $new_cat, $product=false)
	{
		
		$path = dirname(__FILE__) . '/../images/'. $old_cat;		
		$newPath = dirname(__FILE__) . '/../images/'. $new_cat;
		$cart_pics = $this->GetProdPics(false, $old_cat);
		$prod_pics = false;	

		if($product)	
			$prod_pics = $this->GetProdPics($product, $old_cat);
		else
		{	
			if(is_dir($path))
				rename($path, $newPath);
		}	
		if($prod_pics)
		{	
			if (!is_dir($newPath) && $cart_pics)
				mkdir($newPath, 0755);
			if (!is_dir($newPath.'/thumb/') && $cart_pics)
				mkdir($newPath.'/thumb/', 0755);
			$prod_num = count($prod_pics);
			foreach($prod_pics as $i=>$pic)
			{
				$pic_path = $path .'/' . $pic;
				$pic_new_path = $newPath .'/' . $pic;
				$pic_thumb_path = $path .'/thumb/' . $pic;
				$new_pic_thumb_path = $newPath .'/thumb/' . $pic;
				chmod($pic_path, 0755);
				chmod($pic_thumb_path, 0755);
				if(copy($pic_path, $pic_new_path))
					unlink($pic_path);						
				if(copy($pic_thumb_path, $new_pic_thumb_path))
					unlink($pic_thumb_path);
				chmod($pic_new_path, 0644);
				chmod($new_pic_thumb_path, 0644);
				unset($prod_pics[$i]);
			}
			if (count($prod_pics)==0 && $prod_num == count($cart_pics))
			{
				rmdir($path .'/thumb/');
				rmdir($path);
			}
		}
	}
	public function DelProdPics($product, $cat)
	{
		$prod_pics = $this->GetProdPics($product, $cat);
		$cart_pics = $this->GetProdPics(false, $cat);
		global $config;
		if (count($prod_pics) == count($cart_pics))
			$del_fold = dirname(__FILE__) . '/../' . $config['images']['dir'] . '/' . $cat;
		if($prod_pics)
		{			
			foreach ($prod_pics as $i=>$pic)
			{
				$file = dirname(__FILE__) . '/../' . $config['images']['dir'] . '/' . $cat . '/' . $pic;
				$thumb = dirname(__FILE__) . '/../' . $config['images']['dir'] .'/' . $cat . '/thumb/' . $pic;
				if (is_file($file))
					unlink($file);
				if (is_file($thumb))
					unlink($thumb);
				if(!is_file($file))
					unset($prod_pics[$i]);
			}
		}
		if($prod_pics!= false && count($prod_pics)>0)
			return false;
		else
			if(isset($del_fold))
			{
				rmdir($del_fold .'/thumb/');
				rmdir($del_fold);
			}
			return true;
	}
}