<?php
# Контроллер вывода контактной формы.

# Подключаем конфиги на всякий случай.
include_once dirname(__FILE__) . '/../jcart.inc.php';
$config = $jcart->config;

# Подключение модуля обработки форм.
include_once dirname(__FILE__) . '/M_Email.inc.php';
$mEmail = M_Email::Instance();

# Обработка отправки формы
if ($_SERVER['REQUEST_METHOD'] == 'POST' && !isset($form['sent']))
{
	# Проверка на спам
	$spam = $mEmail->CheckSpam($_POST['nospam'], $config['secretWord']);

	# Обработка и сохранение данных в переменные
	$form['email'] = (isset($_POST['email'])) ? $mEmail->ProcessText($_POST['email']) : '';
	$form['name'] = (isset($_POST['name'])) ? $mEmail->ProcessText($_POST['name']) : '';
	$form['phone'] = (isset($_POST['phone'])) ? $mEmail->ProcessText($_POST['phone']) : '';
	$form['subject'] = (isset($_POST['subject'])) ? $mEmail->ProcessText($_POST['subject']) : '';
	$form['comment'] = (isset($_POST['comment'])) ? $mEmail->ProcessText($_POST['comment']) : '';

	# Валидация данных
	$validate = $mEmail->ValidateFeedbackForm($form['email'], $form['name'], $form['phone'], $form['subject'], $form['comment']);

	# Подготовка сообщения к отправке
	$content = $mEmail->PrepareFeedbackEmail($form['email'], $form['comment'], $form['subject'], $form['name'], $form['phone']);

	if (!$spam)
	{
		$form['message'] = $config['form']['isSpam'];
	}
	elseif ($validate)
	{
		$form['message'] = $validate;
	}
	# Отправка письма
	elseif (!$mEmail->SendEmail($config['email']['receiver'], $form['email'], $config['email']['subjectFeedback'], $content, $form['name']))
	{
		$form['message'] = $config['form']['notSent'];
	}
	# Подтверждение отправки
	else
	{
		$form = array();
		$form['sent'] = 1;
	}
}

# Если письмо отправлено, выводим сообщение об этом.
if (isset($form['sent']))
{
	echo $config['form']['feedbackSent'] . '<meta http-equiv="refresh" content="3; url=' . $config['sitelink'] . '">';
}
# Иначе выводим форму.
else
{
	# Генерируем антиспам.
	$antispam = $mEmail->GenerateAntispam($config['secretWord']);

	# Шаблон вывода формы.
	include_once dirname(__FILE__) . '/../design/ContactForm.tpl.php';
}