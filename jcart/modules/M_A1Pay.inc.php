<?php
# Модуль оплаты заказа с помощью сервиса A1Pay

include_once dirname(__FILE__) . '/M_DB.inc.php';
$mDB = M_DB::Instance();

class M_A1Pay
{
	private static $instance; 	# Ссылка на экземпляр класса

	# Получение единственного экземпляра класса
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_A1Pay();

		return self::$instance;
	}

	# Функция инициализирует оплату
	public function InitiatePayment($key, $sum, $title, $email, $order_id = 0)
	{
		# Формирование формы
		$payment_form = '
		<form method="POST" class="application" accept-charset="UTF-8" action="https://partner.a1pay.ru/a1lite/input">
			<input type="hidden" name="key" value="' . $key . '" />
			<input type="hidden" name="cost" value="' . $sum . '" />
			<input type="hidden" name="name" value="' . $title . '" />
			<input type="hidden" name="default_email" value="' . $email . '" />
			<input type="hidden" name="order_id" value="' . $order_id . '" />
			<input type="submit" value="Оплатить" />
		</form>';

		return $payment_form;
	}

	# Проверка результата оплаты
	public function CheckResult($post, $secret)
	{
		$params = array(
			'tid' => $post['tid'],
			'name' => urldecode($post['name']),
			'comment' => urldecode($post['comment']),
			'partner_id' => $post['partner_id'],
			'service_id' => $post['service_id'],
			'order_id' => $post['order_id'],
			'type' => $post['type'],
			'partner_income' => $post['partner_income'],
			'system_income' => $post['system_income'],
			'test' => 1
		);
		$params['check'] = md5(join('', array_values($params)) . $secret);

		if ($params['check'] === $post['check'])
			return true;
		else
			return false;
	}

	public function SuccessPayment()
	{

	}

	public function FailurePayment()
	{

	}
}