<?php
# Контроллер обработки формы заказа и отправки заказа на почту

# Подключение модуля отправки почты
include_once dirname(__FILE__) . '/M_Email.inc.php';
$mEmail = M_Email::Instance(); 

# Обработка POST запроса
if ($_SERVER['REQUEST_METHOD'] == 'POST' && !isset($sent))
{
	if (!empty($_POST['order_sum'])) $order_sum = $_POST['order_sum'];
	if (!empty($_POST['order_items'])) $order_items = unserialize(stripslashes($_POST['order_items']));

	# Проверка на спам
	$spam = $mEmail->CheckSpam($_POST['order_nospam'], $config['secretWord']);
	
	# Запись данных в сессию
	$_SESSION['order_tmp']['email'] = (isset($_POST['order_email'])) ? $_POST['order_email'] : '';
	$_SESSION['order_tmp']['name'] = (isset($_POST['order_name'])) ? $_POST['order_name'] : '';
	$_SESSION['order_tmp']['lastname'] = (isset($_POST['order_lastname'])) ? $_POST['order_lastname'] : '';
	$_SESSION['order_tmp']['fathername'] = (isset($_POST['order_fathername'])) ? $_POST['order_fathername'] : '';
	$_SESSION['order_tmp']['phone'] = (isset($_POST['order_phone'])) ? $_POST['order_phone'] : '';
	$_SESSION['order_tmp']['region'] = (isset($_POST['order_region'])) ? $_POST['order_region'] : '';
	$_SESSION['order_tmp']['zip'] = (isset($_POST['order_zip'])) ? $_POST['order_zip'] : '';
	$_SESSION['order_tmp']['city'] = (isset($_POST['order_city'])) ? $_POST['order_city'] : '';
	$_SESSION['order_tmp']['address'] = (isset($_POST['order_address'])) ? $_POST['order_address'] : '';
	$_SESSION['order_tmp']['comment'] = (isset($_POST['order_comment'])) ? $_POST['order_comment'] : '';
	$_SESSION['order_tmp']['payment'] = (isset($_POST['order_payment'])) ? $_POST['order_payment'] : '';
	$_SESSION['order_tmp']['delivery'] = (isset($_POST['order_delivery'])) ? $_POST['order_delivery'] : '';

	# Обработка и сохранение данных в переменные
	$email = (isset($_POST['order_email'])) ? $mEmail->ProcessText($_POST['order_email']) : '';
	$name = (isset($_POST['order_name'])) ? $mEmail->ProcessText($_POST['order_name']) : '';
	$lastname = (isset($_POST['order_lastname'])) ? $mEmail->ProcessText($_POST['order_lastname']) : '';
	$fathername = (isset($_POST['order_fathername'])) ? $mEmail->ProcessText($_POST['order_fathername']) : '';
	$phone = (isset($_POST['order_phone'])) ? $mEmail->ProcessText($_POST['order_phone']) : '';
	$region = (isset($_POST['order_region'])) ? $mEmail->ProcessText($_POST['order_region']) : '';
	$zip = (isset($_POST['order_zip'])) ? $mEmail->ProcessText($_POST['order_zip']) : '';
	$city = (isset($_POST['order_city'])) ? $mEmail->ProcessText($_POST['order_city']) : '';
	$address = (isset($_POST['order_address'])) ? $mEmail->ProcessText($_POST['order_address']) : '';
	$comment = (isset($_POST['order_comment'])) ? $mEmail->ProcessText($_POST['order_comment']) : '';
	$payment = (isset($_POST['order_payment'])) ? $mEmail->ProcessText($_POST['order_payment']) : '';
	$delivery = (isset($_POST['order_delivery'])) ? $mEmail->ProcessText($_POST['order_delivery']) : '';

	if ($payment == 'invoice')
	{
		$invoice_type = (isset($_POST['invoice_type'])) ? $mEmail->ProcessText($_POST['invoice_type']) : '';
		$juridical[0] = (isset($_POST['juridical_inn'])) ? $mEmail->ProcessText($_POST['juridical_inn']) : '';
		$juridical[1] = (isset($_POST['juridical_kpp'])) ? $mEmail->ProcessText($_POST['juridical_kpp']) : '';
		$juridical[2] = (isset($_POST['juridical_firm'])) ? $mEmail->ProcessText($_POST['juridical_firm']) : '';
		$juridical = ($juridical[0] != '' && $juridical[1] != '' && $juridical[2] != '') ? $juridical : '';
	}

	# Определение названия формы оплаты
	$payment_type = $payment;
	$payment = $config['payments'][$payment_type];
	$payment['type'] = $payment_type;

	# Определение названия способа доставки
	$delivery_type = $delivery;
	$delivery = $config['deliveries'][$delivery_type];
	$delivery['type'] = $delivery_type;

	# Добавочная стоимость доставки
	if ($delivery['cost'] > 0)
		$order_sum += $delivery['cost'];

	# Валидация данных
	$validate = $mEmail->ValidateForm($email, $name, $lastname, $fathername, $phone, $zip, $region, $city, $address, $comment);

	if (!$spam)
	{
		$message = $config['form']['isSpam'];
		$_SESSION['order_tmp']['message'] = $config['form']['isSpam'];
	}
	elseif (empty($order_items))
	{
		$_SESSION['order_tmp']['message'] = 'Заказ не может быть пустым';
		header('Location: ' . $config['sitelink'] . $config['checkoutPath']);
		exit;
	}
	elseif ($validate)
	{
		$message = $validate;
		$_SESSION['order_tmp']['message'] = $validate;
	}
	# Обработка успешного заказа
	else
	{
		# Маркер, сигнализирующий создание нового заказа
		$new_order = 1;

		# Генерация идентификатора заказа, если его ещё не существует.
		if (empty($id_order))
			$id_order = mktime();

		# Подключение модуля пользователей (быстрая регистрация).
		if ($config['auth']['enabled'] === true)
			include_once dirname(__FILE__) . '/C_Users.inc.php';

		# Подключение модуля работы с заказами (сохранение заказа в БД).
		if ($config['database']['enabled'] === true)
			include_once dirname(__FILE__) . '/C_Orders.inc.php';
echo 1;
		# Подключение модуля работы с товарами (изменение количества товара).
		if ($config['database']['quantityEnabled'] == true && $config['database']['enabled'] == true)
			include_once dirname(__FILE__) . '/C_Products.inc.php';
echo 1;
		# Подключение модуля оплаты (генерация формы или счёта для оплаты)
		if (is_file(dirname(__FILE__) . '/C_Payment.php'))
			include_once dirname(__FILE__) . '/C_Payment.php';

		# Подготовка сообщения к отправке.
		$adminContent = $mEmail->PrepareOrder($id_order, $email, $lastname, $name, $fathername, $phone, $zip, $region, $city, $address, $comment, $order_items, $order_sum, $payment, $delivery, $currencySymbol, $config, $juridical, 'true');
		$customerContent = $mEmail->PrepareOrder($id_order, $email, $lastname, $name, $fathername, $phone, $zip, $region, $city, $address, $comment, $order_items, $order_sum, $payment, $delivery, $currencySymbol, $config, $juridical);

		$invoices = (isset($invoices)) ? $invoices : '';
		
//		file_put_contents('test.txt', $invoices . '123'); die;

		# Отправка письма.
		if (!$mEmail->SendEmail($config['email']['receiver'], $email, $config['email']['subjectOrder'], $adminContent, $name, $invoices, $config['encoding']))
		{
			$_SESSION['order_tmp']['message'] = $jcart->config['form']['notSent'];
		}
		# Подтверждение отправки.
		else
		{
			if (!isset($delayed_order))
				$mEmail->SendEmail($email, $config['email']['answer'], $config['email']['subjectOrder'], $customerContent, $config['email']['answerName'], /*$invoices*/ null, $config['encoding']);
			$sent = 1;
		}
	}
}

# Если письмо отправлено, выводим сообщение об этом.
if (isset($sent))
{
	if (isset($delayed_order))
		$mess = $config['form']['delayed'];
	else
		$mess = $config['form']['sent'];

	echo '
	<!DOCTYPE HTML PUBLIC  "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
	<html>
		<head>
			<title>Отправка письма</title>
			<meta http-equiv="Content-Type" content="text/html; charset=' . $config['encoding'] . '">
		</head>
		<body>
		' . $mess;
}
# Иначе выводим форму.
else
{
	# Генерируем антиспам.
	$antispam = $mEmail->GenerateAntispam($config['secretWord']);

	header('Location: ' . $config['sitelink'] . $config['checkoutPath']);
	exit;
}