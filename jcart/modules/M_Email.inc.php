<?php
# Модуль обработки заказа и отправки заказа на почту

include_once dirname(__FILE__) . '/M_DB.inc.php';
$mDB = M_DB::Instance();

class M_Email
{
	private static $instance; 	# Ссылка на экземпляр класса
	private $msql; 				# драйвер БД
	private $db; 				# основная модель работы с БД

	# Получение единственного экземпляра класса
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_Email();

		return self::$instance;
	}

	# Конструктор
	public function __construct()
	{
		# Подключение драйвера работы с БД
		$this->msql = MSQL::Instance();
        $this->db = M_DB::Instance();
	}

	# Генерация антиспама
	public function GenerateAntispam($secret)
	{
		return md5('s' . date("Y-m-d") . 'p' . date("d-m-Y") . 'a' . $secret . 'm');
	}

	# Проверка антиспама
	public function CheckSpam($antispam, $secret)
	{
		return (md5('s' . date("Y-m-d") . 'p' . date("d-m-Y") . 'a' . $secret . 'm') == $antispam);
	}

	# Валидация формы (обязательные поля)
	public function ValidateForm($email, $name, $lastName, $fatherName, $phone, $zip, $region, $city, $address, $comment)
	{
		global $config;

		$email = filter_var($email, FILTER_VALIDATE_EMAIL);
		$name = filter_var($name, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$lastName = filter_var($lastName, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$fatherName = filter_var($fatherName, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$phone = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
		$zip = filter_var($zip, FILTER_SANITIZE_NUMBER_INT);
		$region = filter_var($region, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$city = filter_var($city, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$address = filter_var($address, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$comment = filter_var($comment, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);

		if ((empty($name) && $config['form']['order']['name']['enabled'] == true && $config['form']['order']['name']['required'] == true) || (empty($lastName) && $config['form']['order']['lastname']['enabled'] == true && $config['form']['order']['lastname']['required'] == true) || (empty($fatherName) && $config['form']['order']['fathername']['enabled'] == true && $config['form']['order']['fathername']['required'] == true))
			return $config['form']['emptyName'];
		elseif (empty($email) && $config['form']['order']['email']['required'] == true && $config['form']['order']['email']['enabled'] == true)
			return $config['form']['emptyEmail'];
		elseif (empty($phone) && $config['form']['order']['phone']['required'] == true && $config['form']['order']['phone']['enabled'] == true)
			return $config['form']['emptyPhone'];
		elseif ((empty($zip) && $config['form']['order']['zip']['required'] == true  && $config['form']['order']['zip']['enabled'] == true) || (empty($region) && $config['form']['order']['region']['required'] == true  && $config['form']['order']['region']['enabled'] == true) || (empty($city) && $config['form']['order']['city']['required'] == true  && $config['form']['order']['city']['enabled'] == true) || (empty($address) && $config['form']['order']['address']['required'] == true && $config['form']['order']['address']['enabled'] == true))
			return $config['form']['emptyAddress'];
		elseif (empty($comment) && $config['form']['order']['comment']['required']==true && $config['form']['order']['comment']['enabled']==true)
			return $config['form']['emptyMessage'];
		else
			return false;
	}

	# Валидация email адреса
	#upd not needed?
    public function ValidateEmail($email)
	{
		global $config;
	
		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false)
			return $config['form']['emptyEmail'];
		else
			return false;
	}

	# Валидация формы обратной связи (обязательные поля)
	public function ValidateFeedbackForm($email, $name, $phone, $subject, $comment)
	{
		global $config;

		$name = filter_var($name, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$email = filter_var($email, FILTER_VALIDATE_EMAIL);
		$phone = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
		$subject = filter_var($subject, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
		$comment = filter_var($comment, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);

		if (empty($name) && $config['form']['feedback']['name']['required']==true && $config['form']['feedback']['name']['enabled']==true)
			return $config['form']['emptyName'];
		elseif (empty($email) && $config['form']['feedback']['email']['required']==true && $config['form']['feedback']['email']['enabled']==true)
			return $config['form']['emptyEmail'];
		elseif (empty($phone) && $config['form']['feedback']['phone']['required']==true && $config['form']['feedback']['phone']['enabled']==true)
			return $config['form']['emptyPhone'];
		elseif (empty($subject) && $config['form']['feedback']['subject']['required']==true && $config['form']['feedback']['subject']['enabled']==true)
			return $config['form']['emptyTopic'];
		elseif (empty($comment) && $config['form']['feedback']['comment']['required']==true && $config['form']['feedback']['comment']['enabled']==true)
			return $config['form']['emptyMessage'];
		else
			return false;
	}

	# Подготовка письма о составлении заказа
	public function PrepareOrder($id_order, $email, $lastName, $name, $fatherName, $phone = null, $zip, $region, $city, $address, $comment, $order_items, $order_sum, $payment, $delivery, $currencySymbol, $config, $juridical, $admin = null)
	{
		global $config;
		ob_start();
		include dirname(__FILE__) . '/../design/email/Order.tpl.php';
		return ob_get_clean();
	}

	# Подготовка письма обратной связи
	public function PrepareFeedbackEmail($fromEmail, $content, $subject, $fromName, $fromPhone)
	{
		global $config;
		ob_start();
		include_once dirname(__FILE__) . '/../design/email/Feedback.tpl.php';
		return ob_get_clean();
	}

	# Подготовка письма о вопросе
	public function PrepareQuestionEmail($fromEmail, $content, $subject, $fromName, $id_question, $config)
	{
		ob_start();
		include_once dirname(__FILE__) . '/../design/email/Question.tpl.php';
		return ob_get_clean();
	}

	# Подготовка письма об ответе
	public function PrepareAnswerEmail($fromName, $title, $answer, $config)
	{
		ob_start();
		include_once dirname(__FILE__) . '/../design/email/Answer.tpl.php';
		return ob_get_clean();
	}

	# Подготовка письма со ссылкой на скачивание
	public function PrepareDownloadLink($email, $product, $link, $uses, $hours)
	{
		ob_start();
		include_once dirname(__FILE__) . '/../design/email/DownloadLink.tpl.php';
		return ob_get_clean();
	}

	# Подготовка отправки пароля
	public function PreparePassword($email, $password, $sitelink, $sitename, $authURL)
	{
		ob_start();
		include_once dirname(__FILE__) . '/../design/email/Password.tpl.php';
		return ob_get_clean();
	}

	# Подготовка письма об изменении статуса
	public function PrepareChangeStatus($order, $payment, $delivery = null, $status, $order_items = null, $currencySymbol, $priceFormat)
	{
		global $config;
		# Если данные заказа указаны отсылаем полное письмо с данными заказа, иначе отсылаем краткое уведомление
		if (isset($order['email']) && isset($order['sum']) && $order_items != null)
		{
			ob_start();
			include_once dirname(__FILE__) . '/../design/email/ChangeStatusFull.tpl.php';
			return ob_get_clean();
		}
		else
		{
			ob_start();
			include_once dirname(__FILE__) . '/../design/email/ChangeStatusSmall.tpl.php';
			return ob_get_clean();
		}
	}

	# Отправка письма (без аттача)
	/*public function SendEmail($toEmail, $fromEmail, $subject, $content, $from = null, $encoding = 'utf-8')
	{
		# Если от кого не указано, подставляем робота :)
		if ($from == null)
			$from = 'Robo';

		# Обработка темы.
		$subject = "=?$encoding?b?" . base64_encode($subject) . "?=";
		# Формирование заголовков.
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=$encoding\r\n";
		$headers .= "From: =?$encoding?b?" . base64_encode($from) . "?= <" . $fromEmail . ">";

		return (mail($toEmail, $subject, $content, $headers));
	}*/

	# Отправка письма
	public function SendEmail($toEmail, $fromEmail, $subject, $content, $fromName = null, $invoices = null, $encoding = 'utf-8')
    {
		if ($fromName == null)
			$fromName = 'Robo';

		# Обработка темы
		$subject = "=?$encoding?b?" . base64_encode($subject) . "?=";
		# Генерируем разделитель
		$boundary = "--" . md5(uniqid(time()));
		# Формирование заголовков
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "From: =?$encoding?b?" . base64_encode($fromName) . "?= <" . $fromEmail . ">\r\n";
		$headers .= "Content-Type: multipart/mixed; boundary=\"" . $boundary . "\"\r\n";

		# Открываем тело письма. Вначале текстовая часть
		$multipart = "--" . $boundary . "\r\n";
		$multipart .= "Content-Type: text/html; charset=$encoding\r\n";
		$multipart .= "Content-Transfer-Encoding: quoted-printable\r\n\r\n";
		# Обрабатываем текст письма
		$multipart .= $this->QuotedPrintableEncode($content) . "\r\n\r\n";

		# Обработка вложений
		$file = '';
		$count = (is_array($invoices)) ? count($invoices) : '';

		if ($count > 0)
		{
			for ($i = 0; $i < $count; $i++)
			{
				$attach = file_get_contents($invoices[$i]['filepath']);
				$file .= "--" . $boundary . "\r\n";
				$file .= "Content-Type: text/html; charset=$encoding\r\n";
				$file .= "Content-Transfer-Encoding: quoted-printable\r\n";
				$file .= "Content-Disposition: attachment; filename=\"" . $invoices[$i]['filename'] . "\"\r\n\r\n";
				$file .= $this->QuotedPrintableEncode($attach) . "\r\n";
			}
		}
		$multipart .= $file . "--" . $boundary . "--\r\n";

		return (mail($toEmail, $subject, $multipart, $headers));
    }

	# Кодирование строки методом printable_encode
	public function QuotedPrintableEncode($string)
	{
		# rule #2, #3 (leaves space and tab characters in tact)
		$string = preg_replace_callback(
			'/[^\x21-\x3C\x3E-\x7E\x09\x20]/',
			@array($this, QuotedPrintableEncodeCharacter),
			$string
		);
		$newline = "=\r\n"; // '=' + CRLF (rule #4)
		# make sure the splitting of lines does not interfere with escaped characters
		# (chunk_split fails here)
		$string = preg_replace('/(.{73}[^=]{0,3})/', '$1' . $newline, $string);
		return $string;
	}

	# Вспомогательная функция для кодирования
	public function QuotedPrintableEncodeCharacter($matches)
	{
		$character = $matches[0];
		return sprintf('=%02x', ord($character));
	}

	# Обработка текста для сохранения в базу данных или отправки по почте.
	public function ProcessText($text)
	{
		$text = trim($text); # Удаляем пробелы по бокам.
		$text = stripslashes($text); # Удаляем слэши, лишние пробелы и переводим html символы.
		$text = htmlspecialchars($text); # Переводим HTML в текст.
		$text = preg_replace("/ +/"," ", $text); # Множественные пробелы заменяем на одинарные.
		$text = preg_replace("/(\r\n){3,}/","\r\n\r\n", $text); # Убираем лишние пробелы (больше 1 строки).
		$text = str_replace("\r\n","<br>", $text); # Заменяем переводы строк на тег.

		return $text; # Возвращаем переменную.
	}

	# Рассылка
	public function SetMailDelivery($emails)
	{
		$mail = $this->db->GetAllItems('mail');

		$id_mail = $mail[0]['id_mail'];
		foreach($emails as $email)
		{
			$par = array (	'email' => $email,
							'id_sendmail' => $id_mail
						);
			$this->db->CreateItemById('sendmail', $par, true);
		}
		return true;
	}
	public function GetMailDelivery($sendmail = null)
	{
		if ($sendmail == null)
		{
			$result = $this->db->GetAllItems('sendmail');
		}
		else
		{
			$result = $this->db->GetItemsByParam('sendmail', 'id_sendmail', $sendmail);
		}
		return $result;
	}

	public function MailDelivery($sendmail = null)
	{
		global $config;
		if($sendmail == null)
		{
			$tmp1 = array_reverse($this->db->GetItemsByParam('mail', 'done', 0));
			$check = true;
			if(!empty($tmp1))
				$sendmail = $tmp1[0]['id_mail'];
			else
				return 0;
		}
		else
			$check = false;
		
		$tmp_email = $this->GetMailDelivery($sendmail);
		if(count($tmp_email)>0)
		{
			$tmp_mail = $this->db->GetItemById('mail', $tmp_email[0]['id_sendmail']);
			$res = $this->SendEmail($tmp_email[0]['email'], $config['email']['answer'], $tmp_mail['title'], $tmp_mail['content']);
			if($res)
				sleep(1);
			$this->db->DeleteItemBy2Params('sendmail', 'email', 'id_sendmail', $tmp_email[0]['email'], $tmp_email[0]['id_sendmail']);
		}
		else
		{
			$params['done'] = 1;
			$this->db->EditItemById('mail', $params, $sendmail);
			if($check)
				$this->MailDelivery();
		}
		If($check)
			return count($this->db->GetAllItems('sendmail')) + 1;
		else
			return count($tmp_email);
	}
}