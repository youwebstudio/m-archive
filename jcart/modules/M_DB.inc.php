<?php
# Модуль основных запросов к БД

include_once dirname(__FILE__) . '/MSQL.inc.php';
include_once dirname(__FILE__) . '/M_Admin.inc.php';

class M_DB
{
	private static $instance; 	# Ссылка на экземпляр класса
	private $msql; 				# Драйвер БД
	private $admin; 			# Признак админа

	# Получение единственного экземпляра класса
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_DB();

		return self::$instance;
	}

	# Конструктор
	public function __construct()
	{
		# Подключение драйвера работы с БД и модели Администратора
		$this->msql = MSQL::Instance();
		$this->admin = M_Admin::Instance();
	}

	# Чтение всех элементов из базы
	public function GetAllItems($table)
	{
		$query = "SELECT * FROM $table";

		return $this->msql->Select($query);
	}

	# Генерация пагинации
	public function Paginate($table, $page, $num)
	{
		# Находим общее количество заказов
		$query = "SELECT COUNT(*) as count FROM $table";
		$result = $this->msql->Select($query);
		$items = $result[0]['count'];

		# Если записей нет, то отдаём false
		if (empty($items))
			return false;

		# Находим общее количество страниц
		$total = (($items - 1) / $num) + 1;
		$navi['total'] =  intval($total);

		# Находим начальную статью
		# Если значение текущей страницы больше максимального или меньше нуля, то отдаём ошибку 404
		$page = intval($page);
		if (empty($page) or $page < 0)
			return false;
		if ($page > $total)
			return false;
		$navi['start'] = $page * $num - $num;

		# Сохраняем также в массив текущую страницу
		$navi['page'] = $page;

		return $navi;
	}

	# Генерация пагинации с параметром
	public function PaginateWithParam($table, $page, $num, $param, $value)
	{
		# Находим общее количество заказов
		$t = "SELECT COUNT(*) as count FROM $table WHERE $param = '%s'";
		$query = sprintf($t, mysql_real_escape_string($value));

		$result = $this->msql->Select($query);
		$items = $result[0]['count'];

		# Если записей нет, то отдаём false
		if (empty($items))
			return false;

		# Находим общее количество страниц
		$total = (($items - 1) / $num) + 1;
		$navi['total'] =  intval($total);

		# Находим начальную статью
		# Если значение текущей страницы больше максимального или меньше нуля, то отдаём ошибку 404
		$page = intval($page);
		if (empty($page) or $page < 0)
			return false;
		if ($page > $total)
			return false;
		$navi['start'] = $page * $num - $num;

		# Сохраняем также в массив текущую страницу
		$navi['page'] = $page;

		return $navi;
	}

	# Генерация пагинации с параметром
	public function PaginateWithArrayParam($table, $page, $num, $param, $values)
	{
		# Находим общее количество заказов
		$query = "SELECT COUNT(*) as count FROM $table WHERE ";
		foreach ($values as $key => $value)
		{
			$query .= "$param = '$value'";
			if (isset($values[$key + 1]))
				$query .= " OR ";
		}

		$result = $this->msql->Select($query);
		$items = $result[0]['count'];

		# Если записей нет, то отдаём false
		if (empty($items))
			return false;

		# Находим общее количество страниц
		$total = (($items - 1) / $num) + 1;
		$navi['total'] =  intval($total);

		# Находим начальную статью
		# Если значение текущей страницы больше максимального или меньше нуля, то отдаём ошибку 404
		$page = intval($page);
		if (empty($page) or $page < 0)
			return false;
		if ($page > $total)
			return false;
		$navi['start'] = $page * $num - $num;

		# Сохраняем также в массив текущую страницу
		$navi['page'] = $page;

		return $navi;
	}

	# Выбор списка с пагинацией
	public function GetPaginatedList($table, $start, $num)
	{
		$t = "SELECT * FROM $table ORDER BY id_$table DESC LIMIT %d, %d";
		$query = sprintf($t, $start, $num);

		return $this->msql->Select($query);
	}

	# Выбор списка с пагинацией. С параметром
	public function GetPaginatedListWithParam($table, $start, $num, $param, $value)
	{
		$t = "SELECT * FROM $table WHERE $param = '%s' ORDER BY id_$table DESC LIMIT %d, %d";
		$query = sprintf($t, mysql_real_escape_string($value), $start, $num);

		return $this->msql->Select($query);
	}

	# Выбор списка с пагинацией. С параметром в виде массива
	/*public function GetPaginatedListWithArrayParam($table, $start, $num, $param, $values, $sort, $sort_type)
	{
		$t = "SELECT * FROM $table WHERE ";
		foreach ($values as $key => $value)
		{
			$t .= "$param = '$value'";
			if (isset($values[$key + 1]))
				$t .= " OR ";
		}
		if($sort == 'up')
			$t .= " ORDER BY id_$table ASC LIMIT %d, %d";
		else 
			$t .= " ORDER BY id_$table DESC LIMIT %d, %d";
		$query = sprintf($t, $start, $num);

		return $this->msql->Select($query);
	}*/
	# Выбор списка с пагинацией. С параметром в виде массива + сортировка
	public function GetPaginatedListWithArrayParam($table, $start, $num, $param, $values, $sort, $sort_type='up')
	{
		$t = "SELECT * FROM $table WHERE ";
		foreach ($values as $key => $value)
		{
			$t .= "$param = '$value'";
			if (isset($values[$key + 1]))
				$t .= " OR ";
		}
		if(!$start && !$num)
			$limit = '';
		else
			$limit = "LIMIT %d, %d";

		if(!$sort)
			$sort = 'id_' . $table;
		if($sort_type == 'down')
			$type = 'DESC';
		else
			$type = 'ASC';

		$t .= " ORDER BY $sort $type $limit";
		$query = sprintf($t, $start, $num);

		return $this->msql->Select($query);
	}

	//Get items by param array and group(mailing special)
	public function GetItemsByParamArray($table, $param, $values, $group)
	{
		$t = "SELECT $group FROM $table WHERE ";
		foreach ($values as $key => $value)
		{
			$t .= "$param = '$value'";
			if (isset($values[$key + 1]))
				$t .= " OR ";
		}
		$t .= " group by $group";
		$query = sprintf($t);

		return $this->msql->Select($query);
	}

	public function SearchItemsByParamArray($table, $params, $values, $search_type)
	{
		$t = "SELECT * FROM $table WHERE ";
		foreach ($values as $key => $value)
		{
			$t .= "$params[$key] $value";
			if (isset($values[$key + 1]))
				$t .= " $search_type[$key] ";
		}
		$query = sprintf($t);

		return $this->msql->Select($query);
	}

	# Выбор элемента по идентификатору
	public function GetItemById($table, $id_item)
	{
		$t = "SELECT * FROM $table WHERE id_$table = '%d'";
		$query = sprintf($t, $id_item);
		$result = $this->msql->Select($query);
		return $result[0];
	}

	# Выбор элементов по параметку
	public function GetItemsByParam($table, $param, $value)
	{
		$t = "SELECT * FROM $table WHERE $param = '%s' ORDER BY id_$table DESC";
		$query = sprintf($t, mysql_real_escape_string($value));
		$result = $this->msql->Select($query);
		return $result;
	}

	# Выбор элемента по имени
	public function GetItemByCode($table, $code_item)
	{
		$t = "SELECT * FROM $table WHERE code = '%s'";
		$query = sprintf($t, mysql_real_escape_string($code_item));
		$result = $this->msql->Select($query);

		return (isset($result[0])) ? $result[0] : false;
	}

	# Добавление элемента по идентификатору
	public function CreateItemById($table, $params, $admin = false)
	{
		# Проверка наличия прав
		if ($admin == false)
			if (!$this->admin->CheckLogin())
				return false;

		# Проверка данных.
		if (!$table || !$params)
			return false;

		return $this->msql->Insert($table, $params);
	}

	# Редактирование элемента по идентификатору
	public function EditItemById($table, $params, $id_item, $admin = false)
	{
		# Проверка наличия прав
		if ($admin == false)
			if (!$this->admin->CheckLogin())
				return false;

		# Проверка данных.
		if (!$table || !$params || !$id_item)
			return false;

		$t = "id_$table = '%d'";
		$where = sprintf($t, $id_item);
		$this->msql->Update($table, $params, $where);
	}

	# Удаление элемента по идентификатору
	public function DeleteItemById($table, $id_item)
	{
		# Проверка наличия прав
		if (!$this->admin->CheckLogin())
			return false;

		$t = "id_$table = '%s'";
		$where = sprintf($t, mysql_real_escape_string($id_item));
		$this->msql->Delete($table, $where);
		return true;
	}

	# Удаление элементов по параметку
	public function DeleteItemsByParam($table, $param, $value)
	{
		# Проверка наличия прав
		if (!$this->admin->CheckLogin())
			return false;

		$t = "$param = '%s'";
		$where = sprintf($t, mysql_real_escape_string($value));
		$this->msql->Delete($table, $where);
		return true;
	}

	public function DeleteItemBy2Params($table, $param_1, $param_2, $value_1, $value_2)
	{
		$t = "$param_1 = '$value_1' AND $param_2 = '$value_2'";
		$where = sprintf($t, mysql_real_escape_string($value_1), mysql_real_escape_string($value_2));
		$this->msql->Delete($table, $where);
		return true;
	}

	# Изменение статуса элемента по идентификатору
	public function ChangeStatusById($table, $id_item, $status)
	{
		# Проверка данных.
		if ($table == '' || $id_item == '' || $status == '')
			return false;

		# Запрос.
		$obj = array();
		$obj['status'] = $status;

		$t = "id_$table = '%d'";
		$where = sprintf($t, $id_item);
		$this->msql->Update($table, $obj, $where);
		return true;
	}

	# Проверка наличия категории с указанным кодом (если указан идентификатор записи в БД, то она не учитывается).
	public function IssetItem($table, $code, $id_item = null)
	{
		if ($id_item == null)
		{
			$t = "SELECT COUNT(*) as count FROM $table WHERE code = '%s'";
			$query = sprintf($t, mysql_real_escape_string($code));
		}
		else
		{
			$t = "SELECT COUNT(*) as count FROM $table WHERE code = '%s' AND id_$table != '%d'";
			$query = sprintf($t, mysql_real_escape_string($code), $id_item);
		}

		$result = $this->msql->Select($query);

		return ($result[0]['count'] > 0);
	}

	# Генерация случайной строки
	public function GenerateCode($length = 10, $all_chars = true)
	{
		if ($all_chars == true)
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
		else
			$chars = "ABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
		$code = '';
		$clen = strlen($chars) - 1;

		while (strlen($code) < $length)
			$code .= $chars[mt_rand(0, $clen)];

		return $code;
	}

	# Выбор содержимого дива из текста
	public function GetDivFromContent($text, $div)
	{
		preg_match('|<div class="' . $div . '">(.*)</div>|Uis', $text, $matches);

		return (isset($matches[1])) ? $matches[1] : '';
	}

	# Выбор содержимого тега из текста
	public function GetTagFromContent($text, $tag)
	{
		preg_match('|<' . $tag . '">(.*)</' . $tag . '>|Uis', $text, $matches);

		return $matches[1];
	}

	# Выбор краткого описания статьи
	public function GetPreviewFromText($separator, $text)
	{
		$text_after_more = strstr($text, $separator); # находим текст после тега more
		$preview = str_replace($text_after_more, '', $text); # удаляем его

		return $preview;
	}

	# Выбор полного текста статьи
	public function GetContentFromText($separator, $text)
	{
		$content = strstr($text, $separator); # находим текст после тега more
		$content = str_replace($separator, '', $content);

		return $content;
	}

	# Склонение существительных с числительными (http://mcaizer.habrahabr.ru/blog/11555/)
	public function Plural($n, $form1, $form2, $form5)
	{
		$n = abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20) return $form5;
		else if ($n1 > 1 && $n1 < 5) return $form2;
		else if ($n1 == 1) return $form1;

		return $form5;
	} # echo $n." ".plural($n, "письмо", "письма", "писем")." у Вас в ящике";
}