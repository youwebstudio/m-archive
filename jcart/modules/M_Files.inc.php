<?php
# Модель работы с файлами

include_once dirname(__FILE__) . '/M_Admin.inc.php';
include_once dirname(__FILE__) . '/MSQL.inc.php';

class M_Files
{
	private static $instance; 	# ссылка на экземпляр класса
	private $admin;				# признак админа
	private $msql; 				# драйвер БД

	# Получение единственного экземпляра класса
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_Files();

		return self::$instance;
	}

	# Конструктор
	function __construct()
	{
		# Подключение модели администратора
		$this->admin = M_Admin::Instance();
		$this->msql = MSQL::Instance();
	}

	# Извлечение контента файла
	public function Get($file)
	{
		return file_get_contents($file);
	}

	# Сохранение контента в файл
	public function Save($file, $content)
	{
		return (file_put_contents($file, stripslashes($content)));
	}

	# Загрузка файла на сервер
	public function UploadFile($file, $path, $sub_dir='')
	{
		# Проверка наличия прав
		//if (!$this->admin->CheckLogin())
		//    return false;

		# Проверка на существование папки загрузки
		if (!is_dir($path))
			return false;
		
		if (!empty($sub_dir))
		{
			$path .= '/' . $sub_dir . '/';
			if (!is_dir($path))
			{
				mkdir($path, 0755);
			}
		}

		# Составляем полный путь до файла
		$fullpath = $path . $file['name'];

		# Загрузка файла
		if	(!@copy($file['tmp_name'], $fullpath))
		{
			return false;
		}
		else
		{
			chmod($fullpath, 0644);
			return $fullpath;
		}
	}

	# Очистка устаревших ссылок [доп. модуль Download Products]
	public function ClearLinks()
	{
		$date = date('Y-m-d H:i:s', (time() - 60 * 60 * 24));

		$where = "date < '$date' OR uses = '0'";
		$this->msql->Delete('download', $where);

		return true;
	}

	# Скачивание файла [доп. модуль Download Products]
	public function DownloadFile($file, $path)
	{
		# Проверка на существование папки загрузки
		if (!is_dir($path))
			return false;
		
		# Проверка на существование файла
		if (!is_file($path . $file))
			return false;
		
		# Расширение файла
		$app = explode('.', $file);

		# Составляем полный путь до файла
		$fullpath = $path . $file;

		# Скачивание файла
		header ("Content-Type: application/$app");
		header ("Accept-Ranges: bytes");
		header ("Content-Length: ".filesize($path . $file)); 
		header ("Content-Disposition: attachment; filename=$file");  
		readfile($path . $file);
		exit;
		
		return true;
	}

	# Проверка наличия валидного ключа. [доп. модуль Download Products]
	public function CheckCode($code)
	{
		$t = "SELECT * FROM download WHERE code = '%s' AND uses > 0";
		$query = sprintf($t, $code);
		$result = $this->msql->Select($query);
	
		return (isset($result[0])) ? $result[0] : false;
	}

	# Проверка оплаченного товара клиентов. [доп. модуль Download Products]
	public function CheckPaidProduct($email, $product)
	{
		# Выборка всех заказов.
		$t = "SELECT id_custom FROM custom WHERE email = '%s' AND status = 1";
		$query = sprintf($t, $email);
		$id_customs1 = $this->msql->Select($query);

		# Выборка всех заказанных товаров.
		$t = "SELECT id_custom FROM custom_item WHERE id_product = '%s'";
		$query = sprintf($t, $product);
		$id_customs2 = $this->msql->Select($query);
		
		if (empty($id_customs1) || empty($id_customs2))
			return false;
		
		# Сравниваем массивы.
		foreach ($id_customs2 as $id)
			$ids[] = $id['id_custom'];
		
		foreach ($id_customs1 as $id)
			if (in_array($id['id_custom'], $ids))
				return true;
	
		return false;
	}

	# Использование ключа. [доп. модуль Download Products]
	public function UseCode($code, $uses)
	{
		$obj = array();
		$obj['uses'] = $uses - 1;

		$t = "code = '%s'";
		$where = sprintf($t, mysql_real_escape_string($code));
		$this->msql->Update('download', $obj, $where);
		
		return true;
	}

	# Генерация ключа. [доп. модуль Download Products]
	public function CreateLink($code_product, $uses)
	{
		include_once dirname(__FILE__) . '/M_DB.inc.php';
		$mDB = M_DB::Instance();

		global $config;
		# Выбор товара
		//$product = $mDB->GetItemByCode('product', $code_product);

		//if ($product['type'] != '')
			//$file = $code_product . '.' . $product['type'];

		if (isset($config['download']['type']))
			$file = $code_product . '.' . $config['download']['type'];
	
		# Генерация ключа
		$code = $mDB->GenerateCode(6);
		
		# Сохранение в БД
		$obj = array();
		$obj['code'] = $code;
		$obj['file'] = $file;
		$obj['uses'] = $uses;
		$obj['date'] = date('Y-m-d H:i:s');

		$this->msql->Insert('download', $obj);

		return $code;
	}

	# Перекодирование из Windows-1251 в UTF-8
	public function Win2UTF($file)
	{
		$content = file_get_contents($file);
		$content = iconv("cp1251", "utf-8", $content);
	
		return (file_put_contents($file, stripslashes($content)));
	}

	# Загрузка данных из CSV в MySQL
	public function CSV2MySQL($file, $table)
	{	
		# Проверка наличия прав
	    if (!$this->admin->CheckLogin())
		    return false;

		$lines = file($file);
		//$lines[2] = iconv( "utf-8","cp1251", $lines[2]);
		if(!$this->is_utf8($lines[2]))
		{
			# Перекодирование CSV файла в UTF-8
			$this->Win2UTF($file);
		}

		# Установка локали в UTF-8
		if (!setlocale(LC_ALL, 'ru_RU.utf8'));

		# Подключение модулей
		if($table == 'product')
		{
			include_once dirname(__FILE__) . '/M_Products.inc.php';
			$mProducts = M_Products::Instance();
		}
		elseif($table == 'category')
		{
			include_once dirname(__FILE__) . '/M_Categories.inc.php';
			$mCategories = M_Categories::Instance();
		}
        include_once dirname(__FILE__) . '/M_DB.inc.php';
        $mDB = M_DB::Instance();
		
        $handle = fopen($file, "r");
        $row = 0;
        while (($string = fgetcsv($handle, 0, ';')) !== false)
        {
            if ($row > 0)
            {
				if($table == 'product')
				{
					$code = $string[0];
					$category = $string[1];
					$title = $string[2];
					$quantity = $string[3];
					$price = $string[4];
					$description = $string[5];
					/*$unit = $string[6];
					$color = $string[7];
					$size = $string[8];
					$param = $string[9];*/

					# Поиск в базе идентификатора
					if ($id_product = $mProducts->GetIdByCodeAndCategory($code, $category))
					{
						$mProducts->EditProduct($id_product, $code, $category, $title, $quantity, $price, $discount='', $discount_date='', $description, $unit='', $color='', $size='', $param='');
					}
					else
					{
						$mProducts->CreateProduct($code, $category, $title, $quantity, $price, $description, $unit='', $color='', $size='', $param='');
					}
				}
				elseif($table == 'category')
				{
					$code = $string[0];
					$parent = $string[1];
					$title = $string[2];

					if ($id_category = $mDB->GetItemsByParam('category', 'code', $code))
					{
						$mCategories->EditCategory($id_category[0]['id_category'], $code, $parent, $title);
					}
					else
					{
						$mCategories->CreateCategory($code, $parent, $title);
					}
				}
			}
			# Считаем количество обновлённых строк
			$row++;
		}
		fclose($handle);

		return $row - 1;
	}

	# Загрузка данных из CSV в MySQL
	public function CSV2MySQLv2($file, $table)
	{
		# Проверка наличия прав
		if (!$this->admin->CheckLogin())
			return false;

		$lines = file($file);
		//$lines[2] = iconv( "utf-8","cp1251", $lines[2]);
		if(!$this->is_utf8($lines[2]))
		{
			# Перекодирование CSV файла в UTF-8
			$this->Win2UTF($file);
		}

		# Установка локали в UTF-8
		if (!setlocale(LC_ALL, 'ru_RU.utf8'));

		# Подключение модулей
		if($table == 'product')
		{
			include_once dirname(__FILE__) . '/M_Products.inc.php';
			$mProducts = M_Products::Instance();
		}
		elseif($table == 'category')
		{
			include_once dirname(__FILE__) . '/M_Categories.inc.php';
			$mCategories = M_Categories::Instance();
		}
		include_once dirname(__FILE__) . '/M_DB.inc.php';
		$mDB = M_DB::Instance();

		$handle = fopen($file, "r");
		$row = 0;
		$fields = fgetcsv($handle, 0, ';');
		$fields = array_flip($fields);

		while (($string = fgetcsv($handle, 0, ';')) !== false)
		{
			if ($row >= 0)
			{
				if($table == 'product')
				{
					$code = (isset($string[$fields['code']]))? $string[$fields['code']] : '';
					$category = (isset($string[$fields['category']]))? $string[$fields['category']] : '';
					$title = (isset($string[$fields['title']]))? $string[$fields['title']] : '';
					$quantity = (isset($string[$fields['quantity']]))? $string[$fields['quantity']] : '';
					$price = (isset($string[$fields['price']]))? $string[$fields['price']] : '';
					$discount = (isset($string[$fields['discount']]))? $string[$fields['discount']] : '';
					$tmp_date = (isset($string[$fields['discount_date']]) && strtotime($string[$fields['discount_date']]))?date("Y-m-d H:i:s", strtotime($string[$fields['discount_date']])): false ;
					$discount_date = (isset($string[$fields['discount_date']]) && $tmp_date)? $tmp_date : '';
					$description = (isset($string[$fields['description']]))? $string[$fields['description']] : '';
					$unit = (isset($fields['unit']) && isset($string[$fields['unit']]))? $string[$fields['unit']] : '';
					$color = (isset($fields['color']) && isset($string[$fields['color']]))? $string[$fields['color']] : '';
					$size = (isset($fields['size']) && isset($string[$fields['size']]))? $string[$fields['size']] : '';
					$param = (isset($fields['param']) && isset($string[$fields['param']]))? $string[$fields['param']] : '';

					# Поиск в базе идентификатора
					if ($id_product = $mProducts->GetIdByCodeAndCategory($code, $category))
					{
						$mProducts->EditProduct($id_product, $code, $category, $title, $quantity, $price, $discount, $discount_date, $description, $unit, $color, $size, $param);
					}
					else
					{
						$mProducts->CreateProduct($code, $category, $title, $quantity, $price, $description, $unit, $color, $size, $param);
					}
				}
				elseif($table == 'category')
				{
					$code = $string[$fields['code']];
					$parent = $string[$fields['parent']];
					$title = $string[$fields['title']];

					if ($id_category = $mDB->GetItemsByParam('category', 'code', $code))
					{
						$mCategories->EditCategory($id_category[0]['id_category'], $code, $parent, $title);
					}
					else
					{
						$mCategories->CreateCategory($code, $parent, $title);
					}
				}
			}

			# Считаем количество обновлённых строк
			$row++;
		}
		fclose($handle);

		return $row;
	}

	# Returns true if $string is valid UTF-8 and false otherwise.
	public function is_utf8($string) {
		# From http://w3.org/International/questions/qa-forms-utf-8.html
		return preg_match('%^(?:
           [\x09\x0A\x0D\x20-\x7E]            # ASCII
         | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
         |  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
         | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
         |  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
         |  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
         | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
         |  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
     )*$%xs', $string);

	} # function is_utf8
	# Выгрузка таблици в файл csv
	public function MySQL2CSV($table, $first_line)
	{
		# Проверка наличия прав
	    if (!$this->admin->CheckLogin())
		    return false;
			
		# Подключение модулей
		include_once dirname(__FILE__) . '/M_DB.inc.php';
		$mDB = M_DB::Instance();

		$exp = $mDB->GetAllItems($table);

		if(!empty($exp))
		{
			$row = $first_row = $check =  '';
			$f = 1;
			foreach($exp as $i=>$val)
			{				
				$c = 1;
				foreach ($val as $j=>$field)
				{
					if ($c<count($val))
						$sep1 = ';';
					elseif ($c == count($val))
						$sep1 = "\n";
					if ($field != '')
						$sep2 = '"';
					else 
						$sep2 = '';
					$row .= $sep2 . $field . $sep2 . $sep1;
					if($first_line && $f == 1)
					{
						$first_row .= '"' . $j . '"' . $sep1;
					}
					if($f == 2)
						$check .= $field;
					$c++;
				}
				$f++;
			}
			$row = $first_row . $row;

			if($this->is_utf8($check))
				$row = iconv("utf-8", "cp1251", $row);

			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Length: " . strlen($row));
			# Output to browser with appropriate mime type, you choose
			//header('Content-Type: text/xml, charset=UTF-8; encoding=UTF-8');
			//header('Content-Type: text/csv, charset=UTF-8');
			//header("Content-type: text/x-csv charset=cp1251");
			header("Content-type: text/csv charset=cp1251");
			//header("Content-type: application/csv charset=cp1251");
			$filename = $table."_".date("Y-m-d_H-i",time());
			//header("Content-disposition: csv" . date("Y-m-d") . ".csv");
			header( "Content-disposition: filename=$filename.csv");
			//header("Content-Disposition: attachment; filename=$filename");
			echo $row;
			exit;
		}
		else
		return false;
	}

	# Функция очищает заданную директорию
	public function CleanDir($folder)
	{
		$handle = opendir($folder);
		# Удаляем все файлы
		if ($handle != false)
		{
			while (($file = readdir($handle)) !== false)
			{
				if ($file != '..' && $file != '.')
				{
					unlink($folder . '/' . $file);
				}
			}

			closedir($handle);
		}
		return true;
	}

	# Функция очищает временную директорию
	public function CleanTemp()
	{
		$folder = dirname(__FILE__) . '/../tmp' ;
		$this->CleanDir($folder);
		return true;
	}
}