<?php
// Модуль оплаты заказа с помощью сервиса LiqPay
include_once dirname(__FILE__) . '/M_DB.inc.php';
$mDB = M_DB::Instance();

class M_PayPal
{
	private static $instance; 	// Ссылка на экземпляр класса.

	// Получение единственного экземпляра класса.
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_PayPal();

		return self::$instance;
	}

	/**
	* Функция инициализации оплаты
	*
	* @return string $payment_form
	*/
	public function InitiatePayment($returnURL, $resultURL, $liqpay_ID, $order_id, $price, $currency, $productDesc, $liqpay_sign)
	{
		// Формирование формы.
		
		if ($test == true)
			$form_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
		else
			$form_url = 'https://www.paypal.com/cgi-bin/webscr';

        $payment_form = '
		<form action="' . $form_url . '" method="post">

		<!-- the cmd parameter is set to _xclick for a Buy Now button -->
		<input type="hidden" name="cmd" value="_xclick">

		<input type="hidden" name="business" value="' . $email . '">
		<input type="hidden" name="item_name" value="' . $description . '">
		<input type="hidden" name="item_number" value="' . $id_order . '">
		<input type="hidden" name="amount" value="' . $price . '">
		<input type="hidden" name="no_shipping" value="1">
		<input type="hidden" name="no_note" value="1">
		<input type="hidden" name="currency_code" value="' . $currency . '">
		<input type="hidden" name="lc" value="GB">
		<input type="hidden" name="bn" value="PP-BuyNowBF">
		<input type="submit" name="submit" alt="Оплатить заказ" value="Оплата">

		<!-- If you have turned on auto return on your paypal profile the next line shows where they will go after they have paid by paypal and they click return to site -->
		<input type="hidden" name="return" value="' . $successURL . '"> 

		 <!-- and this is where they will go if they click cancel -->
		<input type="hidden" name="cancel_return" value="' . $cancelURL . '">

		<!-- notes about the following parameter rm
		If your return_url page is a static web page and if Instant Payment Notification is on within the profile, you must to set rm = 1 in the button code so that the return_url page can be called through a GET. If you do not set it, your users will encounter HTTP 405 errors when they try to go to the return page because a static web page can\'t accept a POST.
		So make your return_url a .php file if you have rm=2-->
		<input type="hidden" name="rm" value="2">          <!--Auto return must be off if rm=2     -->
		<!--Next line is where paypal should send the ipn to. we will have set a global value in our paypal account but we can re specify it here  -->
		<input type="hidden" name="notify_url" value="' . $config['sitelink'] . $config['jcartPath'] . 'modules/C_Payment.php" /> 
		<input type="hidden" name="custom" value="any other custom field you want to pass">     

		</form>';

		return $payment_form;
	}

	/**
	* Функция проверки результата оплаты
	*
	* @return boolean
	*/
	public function CheckResult($operation_xml, $signature, $liqpay_sign)
	{
		$req = 'cmd=_notify-validate';
		
		foreach ($_POST as $key => $value)
		{
			$value = urlencode(stripslashes($value));
			$req .= "&$key=$value";
		}
		
		$header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
		$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
		$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";

		if ($test == true)
			$fp = fsockopen ('www.sandbox.paypal.com', 80, $errno, $errstr, 30);
		else
			$fp = fsockopen ('www.paypal.com', 80, $errno, $errstr, 30);
			
		if (!$fp)
		{
			$mail_From = 'From: IPN@tester.com';
			$mail_To = $email;
			$mail_Subject = 'HTTP ERROR';
			$mail_Body = $errstr;//error string from fsockopen
			
			mail($mail_To, $mail_Subject, $mail_Body, $mail_From); 
		}
		else
		{
			fputs ($fp, $header . $req);
			while (!feof($fp))
			{
				$res = fgets ($fp, 1024);
				if (strcmp ($res, "VERIFIED") == 0)
				{
					$item_name = $_POST['item_name'];
					$item_number = $_POST['item_number'];
					$item_colour = $_POST['custom'];  
					$payment_status = $_POST['payment_status'];
					$payment_amount = $_POST['mc_gross'];         //full amount of payment. payment_gross in US
					$payment_currency = $_POST['mc_currency'];
					$txn_id = $_POST['txn_id'];                   //unique transaction id
					$receiver_email = $_POST['receiver_email'];
					$payer_email = $_POST['payer_email'];
					
					$amount_they_should_have_paid = lookup_price($item_name); // you need to create this code to find out what the price for the item they bought really is so that you can check it against what they have paid. This is an anti hacker check.
					
					if (($payment_status == 'Completed') &&   //payment_status = Completed
						($receiver_email == "<insert your business account email>") &&   // receiver_email is same as your account email
						($payment_amount == $amount_they_should_have_paid ) &&  //check they payed what they should have
						($payment_currency == "GBP") &&  // and its the correct currency 
						(!txn_id_used_before($txn_id)))
					{  //txn_id isn't same as previous to stop duplicate payments. You will need to write a function to do this check.
						
						if ($test)
						{
							$mail_To = "payit@designertuts.com";
							$mail_Subject = "completed status received from paypal";
							$mail_Body = "completed: $item_number  $txn_id";
							mail($mail_To, $mail_Subject, $mail_Body);
						}
					}
					else
					{
					  $mail_To = "payit@designertuts.com";
					  $mail_Subject = "PayPal IPN status not completed or security check fail";
					
					  $mail_Body = "Something wrong. \n\nThe transaction ID number is: $txn_id \n\n Payment status = $payment_status \n\n Payment amount = $payment_amount";
					  mail($mail_To, $mail_Subject, $mail_Body);
					}
				}
				else if (strcmp ($res, "INVALID") == 0)
				{
					$mail_To = "payit@designertuts.com";
					$mail_Subject = "PayPal - Invalid IPN ";
					$mail_Body = "We have had an INVALID response. \n\nThe transaction ID number is: $txn_id \n\n username = $username";
					mail($mail_To, $mail_Subject, $mail_Body);
				}
			} //end of while
		fclose ($fp);
		}
        
		if ($signature == $signature_new && $payment_data['status'] == 'success')
			return $payment_data;
		else
			return false;
	}

    private function parseTag($rs, $tag)
    {
        $rs = str_replace("\n", "", str_replace("\r", "", $rs));
        $tags = '<'.$tag.'>';
        $tage = '</'.$tag;
        $start = strpos($rs, $tags)+strlen($tags);
        $end = strpos($rs, $tage);
        return substr($rs, $start, ($end-$start));
    }

	public function SuccessPayment()
	{
		
	}

	public function FailurePayment()
	{
		
	}
}