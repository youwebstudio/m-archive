<?php
# Контроллер обработки оплаты заказа

# Подключение настроек
include_once dirname(__FILE__) . '/../jcart.inc.php';
$config = $jcart->config;

# Подключение модулей оплаты
if (is_file(dirname(__FILE__) . '/M_Robokassa.inc.php'))
{
	include_once dirname(__FILE__) . '/M_Robokassa.inc.php';
	$mRobokassa = M_Robokassa::Instance();
}
if (is_file(dirname(__FILE__) . '/M_Interkassa.inc.php'))
{
	include_once dirname(__FILE__) . '/M_Interkassa.inc.php';
	$mInterkassa = M_Interkassa::Instance();
}
if (is_file(dirname(__FILE__) . '/M_A1Pay.inc.php'))
{
	include_once dirname(__FILE__) . '/M_A1Pay.inc.php';
	$mA1Pay = M_A1Pay::Instance();
}
if (is_file(dirname(__FILE__) . '/M_Liqpay.inc.php'))
{
	include_once dirname(__FILE__) . '/M_Liqpay.inc.php';
	$mLiqpay = M_Liqpay::Instance();
}
if (is_file(dirname(__FILE__) . '/M_Invoices.inc.php'))
{
	include_once dirname(__FILE__) . '/M_Invoices.inc.php';
	$mInvoices = M_Invoices::Instance();
}

# Если заказ отправлен, то составление счёта на оплату
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if (isset($new_order))
	{
		if ($payment['type'] == 'robokassa' && $config['payments']['robokassa']['enabled'] === true)
		{
			$payment['link'] = $mRobokassa->InitiatePayment($config['payments']['robokassa']['login'], $config['payments']['robokassa']['pass1'], $id_order, $config['payments']['robokassa']['description'], $order_sum, $config['payments']['robokassa']['test']);
		}
		elseif ($payment['type'] == 'interkassa' && $config['payments']['interkassa']['enabled'] === true)
		{
			$payment['form'] = $mInterkassa->InitiatePayment($config['payments']['interkassa']['shop_id'], $order_sum, $id_order, $config['payments']['interkassa']['description']);
		}
		elseif ($payment['type'] == 'a1pay' && $config['payments']['a1pay']['enabled'] === true)
		{
			$payment['form'] = $mA1Pay->InitiatePayment($config['payments']['a1pay']['secret_key'], $order_sum, $config['payments']['a1pay']['description'], $config['email']['receiver'], $id_order);
		}
		elseif ($payment['type'] == 'invoice' && $config['payments']['invoice']['enabled'] === true)
		{
			$fio['name'] = $name;
			$fio['lastname'] = $lastname;
			$fio['fathername']	= $fathername;

			$invoices[0]['filename'] = $mInvoices->CreateInvoice($id_order, $order_items, $currencySymbol, $fio, $juridical);
			$invoices[0]['filepath'] = $config['sitelink'] . $config['jcartPath'] . 'invoices/' . $invoices[0]['filename'];

			if ($juridical != '')
			{
				$invoices[1]['filename'] = $mInvoices->CreateInvoiceFact($id_order, $order_items, $currencySymbol, $address, $juridical);
				$invoices[1]['filepath'] = $config['sitelink'] . $config['jcartPath'] . 'invoices/' . $invoices[1]['filename'];
			}
		}
		elseif ($payment['type'] == 'liqpay' && $config['payments']['liqpay']['enabled'] === true)
		{
			$resultURL = $returnURL = $config['sitelink'] . $config['jcartPath'] . 'modules/C_Payment.php';
			$payment['form'] = $mLiqpay->InitiatePayment($returnURL, $resultURL, $config['payments']['liqpay']['id'], $id_order, $order_sum, $config['currencyCode'], $config['payments']['liqpay']['description'], $config['payments']['liqpay']['sign']);
		}
	}
	else
	{
		if (isset($_POST['SignatureValue']))
		{
			if ($mRobokassa->CheckResult($config['payments']['robokassa']['pass2'], $_POST) == true)
			{
				$paid_order_id = $_POST['InvId'];
				$paid_sum = $_POST['OutSum'];
				$payment_type = 'robokassa';
			}
		}
		elseif (isset($_POST['ik_sign_hash']))
		{
			if ($mInterkassa->CheckResult($config['payments']['interkassa']['shop_id'], $config['payments']['interkassa']['secret_key'], $_POST) == true)
			{
				$paid_order_id = $_POST['ik_payment_id'];
				$paid_sum = $_POST['ik_payment_amount'];
				$payment_type = 'interkassa';
			}
		}
		elseif (isset($_POST['check']))
		{
			if ($mA1Pay->CheckResult($_POST, $config['payments']['a1pay']['secret_word']) === true)
			{
				$paid_order_id = $_POST['order_id'];
				$paid_sum = $_POST['system_income'];
				$payment_type = 'a1pay';
			}
		}
		elseif (isset($_POST['signature']))
		{
			if ($payment_data = $mLiqpay->CheckResult($_POST['operation_xml'], $_POST['signature'], $config['payments']['liqpay']['sign']))
			{
				$paid_order_id = $payment_data['order_id'];
				$paid_sum = $payment_data['sum'];
				$payment_type = 'liqpay';
			}
		}
		else
			die;

		# Если оплата прошла, изменение статуса заказа и отправка уведомления на почту
		if (isset($payment_type) && isset($paid_sum) && isset($paid_order_id))
		{
			if ($config['database']['enabled'] == true)
			{
				# Выбор заказа и его элементов
				$order = $mDB->GetItemById('custom', $paid_order_id);
				$order_items = $mDB->GetItemsByParam('custom_item', 'id_custom', $paid_order_id);

				# Проверка суммы
				if ($paid_sum != $order['sum'])
					die;

				# Проверка статуса
				if ($order['status'] == 1)
					die;

				# Изменение статуса заказа
				$mDB->ChangeStatusById('custom', $paid_order_id, 1);
			}

			# Подключение модуля работы с формами
			include_once dirname(__FILE__) . '/M_Email.inc.php';
			$mEmail = M_Email::Instance();

			# Подстановка значений при работе без БД
			if (empty($order))
				$order = array ('id_custom' => $paid_order_id, 'payment' => $payment_type, 'delivery' => 1);

			# Определение данных формы оплаты
			$payment['title'] = $config['payments'][$order['payment']]['title'];
			$payment['info'] = $config['payments'][$order['payment']]['info'];

			# Определение данных способа доставки
			$delivery['title'] = $config['deliveries'][$order['delivery']]['title'];
			$delivery['info'] = $config['deliveries'][$order['delivery']]['info'];

			# Определение статуса заказа
			$status = $config['statuses'][1];

			# Определение валюты заказа
			$currencySymbol = $jcart->currencySymbol($config['currencyCode']);

			# Подключение модуля работы с файлами (создание ссылки на скачивание)
			if ($config['download']['enabled'] == true && is_file(dirname(__FILE__) . '/C_Files.inc.php'))
				include_once dirname(__FILE__) . '/C_Files.inc.php';

			$order_items = (isset($order_items)) ? $order_items : '';

			# Подготовка текста письма
			$content = $mEmail->PrepareChangeStatus($order, $payment, $delivery, $status, $order_items, $currencySymbol, $config['priceFormat']);

			# Отправка письма владельцу и покупателю
			$mEmail->SendEmail($config['email']['receiver'], $config['email']['answer'], $config['email']['subjectStatus'], $content, $config['email']['answerName'], null, $config['encoding']);
			$mEmail->SendEmail($order['email'], $config['email']['answer'], $config['email']['subjectStatus'], $content, $config['email']['answerName'], null, $config['encoding']);

			# Вывод результата на экран
			//echo 'OK';

			# Перенаправление на страницу успешной покупки
			header('Location: ' . $config['sitelink'] . $config['successUrl']);
			die;
		}
		else
			die;
	}
}