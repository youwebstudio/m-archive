<?php
# Модуль оплаты заказа с помощью сервиса InterKassa

include_once dirname(__FILE__) . '/M_DB.inc.php';
$mDB = M_DB::Instance();

class M_Interkassa
{
	private static $instance; 	// Ссылка на экземпляр класса

	# Получение единственного экземпляра класса
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_Interkassa();

		return self::$instance;
	}

	/**
	* Функция инициализирует оплату
	*
	* @return string $payment_form
	*/
	public function InitiatePayment($shop_id, $sum, $payment_id, $payment_desc)
	{
		# Формирование формы.
        $payment_form = '
        <form name="payment" action="https://interkassa.com/lib/payment.php" method="post" enctype="application/x-www-form-urlencoded" accept-charset="utf-8">
            <input type="hidden" name="ik_shop_id" value="' . $shop_id . '">
            <input type="hidden" name="ik_payment_amount" value="' . $sum . ' ">
            <input type="hidden" name="ik_payment_id" value="' . $payment_id . '">
            <input type="hidden" name="ik_payment_desc" value="' . $payment_desc . '">
            <input type="submit" name="process" value="Оплатить">
        </form>';

		return $payment_form;
	}

	/**
	* Функция проверяем результат оплаты
	*
	* @return boolean
	*/
	public function CheckResult($shop_id, $secret_key, $status_data)
	{
        if ($status_data['ik_shop_id'] != $shop_id)
            return false;

        $sing_hash_str = $status_data['ik_shop_id'].':'.
                        $status_data['ik_payment_amount'].':'.
                        $status_data['ik_payment_id'].':'.
                        $status_data['ik_paysystem_alias'].':'.
                        $status_data['ik_baggage_fields'].':'.
                        $status_data['ik_payment_state'].':'.
                        $status_data['ik_trans_id'].':'.
                        $status_data['ik_currency_exch'].':'.
                        $status_data['ik_fees_payer'].':'.
                        $secret_key;

        if ($status_data['ik_sign_hash'] === strtoupper(md5($sing_hash_str)) && $status_data['ik_payment_state'] == 'success')
			return true;
		else
			return false;
	}

	public function SuccessPayment()
	{
		
	}

	public function FailurePayment()
	{
		
	}
}