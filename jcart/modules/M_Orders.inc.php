<?php
# Модуль работы с заказами

include_once dirname(__FILE__) . '/M_DB.inc.php';
$mDB = M_DB::Instance();

class M_Orders
{
	private static $instance; 	# ссылка на экземпляр класса
	private $msql; 				# драйвер БД
	private $admin; 			# признак админа

	# Получение единственного экземпляра класса
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_Orders();

		return self::$instance;
	}

	# Конструктор
	public function __construct()
	{
		# Подключение драйвера работы с БД и модели администратора
		$this->msql = MSQL::Instance();
		$this->admin = M_Admin::Instance();
	}

	# Создание заказа
	public function CreateOrder($lastName, $name, $fatherName, $email, $phone, $zip, $region, $city, $address, $comment, $payment, $juridical = null, $delivery = null, $delivery_cost = 0, $date, $order_sum, $status, $id_user = 0)
	{
		# Проверка данных.
		if (!isset($name) || !isset($email) || !isset($phone) || !isset($address) || !isset($payment) || !isset($date) || !isset($order_sum))
			return false;

		if (isset($juridical) && is_array($juridical))
			$juridical = implode('|', $juridical);

		# Запрос.
		$obj = array();
		$obj['name'] = $name;
		$obj['lastname'] = $lastName;
		$obj['fathername'] = $fatherName;
		$obj['email'] = $email;
		$obj['phone'] = $phone;
		$obj['zip'] = $zip;
		$obj['region'] = $region;
		$obj['city'] = $city;
		$obj['address'] = $address;
		$obj['comment'] = $comment;
		$obj['payment'] = $payment;
		$obj['juridical'] = $juridical;
		$obj['delivery'] = $delivery;
		$obj['delivery_cost'] = $delivery_cost;
		$obj['date'] = $date;
		$obj['sum'] = $order_sum;
		$obj['status'] = $status;
		$obj['id_user'] = $id_user;

		return $this->msql->Insert('custom', $obj);
	}

	# Создание элементов заказа
	public function CreateOrderItem($id_custom, $id_product, $product, $quantity, $price, $discount, $unit, $color, $size, $param)
	{
		# Проверка данных
		if ($id_custom == '' || $id_product == '' || $product == '' || $quantity == '' || $price == '')
			return false;

		# Запрос
		$obj = array();
		$obj['id_custom'] = $id_custom;
		$obj['id_product'] = $id_product;
		$obj['product'] = $product;
		$obj['quantity'] = $quantity;
		$obj['price'] = $price;
		$obj['discount'] = $discount;
		$obj['unit'] = $unit;
		$obj['color'] = $color;
		$obj['size'] = $size;
		$obj['param'] = $param;

		return $this->msql->Insert('custom_item', $obj);
	}

	# Редактирование элементов заказа
	public function EditOrderItem($id_custom_item, $id_custom, $id_product, $product, $quantity, $price, $discount, $unit, $color, $size, $param)
	{
		# Проверка данных.
		if ($id_custom == '' || $id_product == '' || $product == '' || $quantity == '' || $price == '')
			return false;

		# Запрос.
		$obj = array();
		$obj['id_custom'] = $id_custom;
		$obj['id_product'] = $id_product;
		$obj['product'] = $product;
		$obj['quantity'] = $quantity;
		$obj['price'] = $price;
		$obj['discount'] = $discount;
		$obj['unit'] = $unit;
		$obj['color'] = $color;
		$obj['size'] = $size;
		$obj['param'] = $param;

		$t = "id_custom_item = '%d'";
		$where = sprintf($t, $id_custom_item);
		$this->msql->Update('custom_item', $obj, $where);
		return true;
	}

	# Редактирование заказа
	public function EditOrder($id_custom, $lastName = null, $name, $fatherName = null, $email, $phone = null, $zip = null, $region = null, $city = null, $address = null, $comment = null, $payment, $juridical = null, $delivery = null, $delivery_cost = null, $date = null, $order_sum, $status)
	{
		# Проверка наличия прав
		if (!$this->admin->CheckLogin())
			return false;

		# Проверка данных.
		if ($id_custom == '' || $email == '' || $payment == '' || $date == '' || $order_sum == '' || $status == '')
			return false;

        if ($juridical)
            $juridical = implode('|', $juridical);

		# Запрос.
		$obj = array();
		$obj['name'] = $name;
		$obj['lastname'] = $lastName;
		$obj['fathername'] = $fatherName;
		$obj['email'] = $email;
		$obj['phone'] = $phone;
		$obj['zip'] = $zip;
		$obj['region'] = $region;
		$obj['city'] = $city;
		$obj['address'] = $address;
		$obj['comment'] = $comment;
		$obj['payment'] = $payment;
		$obj['juridical'] = $juridical;
		$obj['delivery'] = $delivery;
		$obj['delivery_cost'] = $delivery_cost;
		$obj['date'] = $date;
		$obj['sum'] = $order_sum;
		$obj['status'] = $status;

		$t = "id_custom = '%d'";
		$where = sprintf($t, $id_custom);
		$this->msql->Update('custom', $obj, $where);
		return true;
	}

	# Выбор заказов за посление N дней
	public function GetLastOrders($days)
	{
		$date = date("Y-m-d H:i:s", mktime() - $days * 24 * 60 * 60);

		$t = "SELECT * FROM custom WHERE date > '%s'";
		$query = sprintf($t, $date);
		$result = $this->msql->Select($query);

		return $result;
	}
}