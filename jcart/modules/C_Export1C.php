<?php
# Контроллер для экспорта заказов в 1С

# Подключение конфигов
include_once dirname(__FILE__) . '/../jcart.inc.php';

# Подключение модуля работы с базой данных
include_once dirname(__FILE__) . '/M_Orders.inc.php';
$mOrders = M_Orders::Instance();

# Выбор последних 3 заказов
$orders = $mOrders->GetLastOrders(3);

# Вывод заказов
foreach ($orders as $order)
{
	$order_items = $mDB->GetItemsByParam('custom_item', 'id_custom', $order['id_custom']);
	$juridical = explode ('|', $order['juridical']);
	if (!empty($juridical))
		$order['firm'] = str_replace('&quot;', '"', $juridical[2]);
	
	include dirname(__FILE__) . '/../design/Export1C.tpl.php';
	
	$order_items = $juridical = '';
}

$orders = '';