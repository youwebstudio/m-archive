<?php
# Модель работы с товарами

include_once dirname(__FILE__) . '/M_DB.inc.php';
$mDB = M_DB::Instance();

class M_Products
{
	private static $instance; 	# ссылка на экземпляр класса
	private $msql; 				# драйвер БД
	private $admin; 			# признак админа
	private $db; 				# основная модель работы с БД

	# Получение единственного экземпляра класса
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_Products();

		return self::$instance;
	}

	# Конструктор
	public function __construct()
	{
		# Подключение драйвера работы с БД и модели Администратора
		$this->msql = MSQL::Instance();
		$this->admin = M_Admin::Instance();
		$this->db = M_DB::Instance();
	}

	# Создание товара
	public function CreateProduct($code, $category, $title, $quantity, $price, $description, $unit, $color, $size, $param)
	{
		# Проверка наличия прав
		if (!$this->admin->CheckLogin())
			return false;

		# Проверка данных.
		if ($code == '')
			return false;

		# Проверка на уникальность идентификатора. Если такой уже есть в базе, то генерируется новый
		$id = $code;
		$i = 1;
		while ($this->db->IssetItem('product', $code))
		{
			$code = $id . '-' . $i;
			$i++;
		}

		# Запрос.
		$obj = array();
		$obj['code'] = $code;
		$obj['category'] = $category;
		$obj['title'] = $title;
		$obj['quantity'] = $quantity;
		$obj['price'] = $price;
		$obj['description'] = $description;
		$obj['unit'] = $unit;
		$obj['color'] = $color;
		$obj['size'] = $size;
		$obj['param'] = $param;

		return $this->msql->Insert('product', $obj);
	}

	# Редактирование товара
	public function EditProduct($id_product, $code, $category, $title, $quantity, $price, $discount, $discount_date, $description, $unit, $color, $size, $param)
	{
		# Проверка наличия прв
		if (!$this->admin->CheckLogin())
			return false;

		# Проверка данных.
		if ($id_product == '' || $code == '')
			return false;

		# Проверка на уникальность идентификатора. Если такой уже есть в базе, то генерируется новый
		$id = $code;
		$i = 1;
		while ($this->db->IssetItem('product', $code, $id_product))
		{
			$code = $id . '-' . $i;
			$i++;
		}

		# Запрос.
		$obj = array();
		$obj['code'] = $code;
		$obj['category'] = $category;
		$obj['title'] = $title;
		$obj['quantity'] = $quantity;
		$obj['price'] = $price;
		$obj['discount'] = $discount;
		$obj['discount_date'] = $discount_date;
		$obj['description'] = $description;
		$obj['unit'] = $unit;
		$obj['color'] = $color;
		$obj['size'] = $size;
		$obj['param'] = $param;	

		$t = "id_product = '%d'";
		$where = sprintf($t, $id_product);
		$this->msql->Update('product', $obj, $where);
		return true;
	}

	# Генерация пагинации при работе с категориями.
	public function Paginate($category, $page, $num)
	{
		# Находим общее количество заказов
		$t = "SELECT COUNT(*) as count FROM product WHERE category = '%s'";
		$query = sprintf($t, mysql_real_escape_string($category));
		$result = $this->msql->Select($query);
		$items = $result[0]['count'];

		# Если записей нет, то отдаём false
		if (empty($items))
			return false;

		# Находим общее количество страниц
		$total = (($items - 1) / $num) + 1;
		$navi['total'] =  intval($total);

		# Находим начальную статью
		# Если значение текущей страницы больше максимального или меньше нуля, то отдаём ошибку 404
		$page = intval($page);
		if (empty($page) or $page < 0)
			return false;
		if ($page > $total)
			return false;
		$navi['start'] = $page * $num - $num;

		# Сохраняем также в массив текущую страницу
		$navi['page'] = $page;

		return $navi;
	}

	# Выбор списка с пагинацией при работе с категориями.
	public function GetPaginatedList($category, $start, $num)
	{
		$t = "SELECT * FROM product WHERE category = '%s' ORDER BY id_product DESC LIMIT %d, %d";
		$query = sprintf($t, mysql_real_escape_string($category), $start, $num);

		return $this->msql->Select($query);
	}

	# Выбор идентификатора товара по коду и категории
	public function GetIdByCodeAndCategory($code, $category)
	{
		# Проверка данных.
		if ($code == '' || $category == '')
			return false;

		# Выбор товара.
		$t = "SELECT * FROM product WHERE code = '%s' AND category='%s'";
		$query = sprintf($t, mysql_real_escape_string($code), mysql_real_escape_string($category));
		$result = $this->msql->Select($query);

		if (!empty($result[0]['id_product']))
			return $result[0]['id_product'];
		else
			return false;
	}

	# Изменение статуса элемента по идентификатору
	public function ChangeQuantityByCode($code, $quantity)
	{
		# Проверка данных.
		if ($code == '' || $quantity == '')
			return false;

		# Выбор товара по идентификатору.
		$t = "SELECT * FROM product WHERE code = '%s'";
		$query = sprintf($t, mysql_real_escape_string($code));
		$result = $this->msql->Select($query);
		if (!$result)
			return false;

		# Обновление количества товара.
		$obj = array();
		$obj['quantity'] = $result[0]['quantity'] - $quantity;

		$t = "id_product = '%d'";
		$where = sprintf($t, $result[0]['id_product']);
		$this->msql->Update('product', $obj, $where);
		return true;
	}

    # Сохранение в сессию данных о просмотренном товаре [доп. модуль Seen Products]
	public function SetSeenProduct($url, $title, $img, $max_seen_products)
	{
		$first_key = '';

		if (!empty($_SESSION['seen']))
		{
			foreach ($_SESSION['seen'] as $key => $product)
			{
				if ($product['url'] == $url)
					unset($_SESSION['seen'][$key]);

				if (!$first_key)
					$first_key = $key;
			}
		}

		$_SESSION['seen'][] = array('url' => $url, 'title' => $title, 'img' => $img);

		if (count($_SESSION['seen']) > $max_seen_products + 1)
			unset($_SESSION['seen'][$first_key]);

		return true;
	}

	# Выбор из сессии данных о просмотренных ранее товарах [доп. модуль Seen Products]
	public function GetSeenProducts()
	{
		$seen_products = (isset($_SESSION['seen'])) ? $_SESSION['seen'] : array();
		krsort($seen_products);

		foreach ($seen_products as $key => $seen_product)
		{
			unset($seen_products[$key]);
			break;
		}

		return $seen_products;
	}
}