<?php
# Модуль оплаты заказа с помощью сервиса LiqPay

include_once dirname(__FILE__) . '/M_DB.inc.php';
$mDB = M_DB::Instance();

class M_Liqpay
{
	private static $instance; 	// ссылка на экземпляр класса

	// Получение единственного экземпляра класса
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_Liqpay();

		return self::$instance;
	}

	/**
	* Функция инициализирует оплату
	*
	* @return string $payment_form
	*/
	public function InitiatePayment($returnURL, $resultURL, $liqpay_ID, $order_id, $price, $currency, $productDesc, $liqpay_sign)
	{
        $xml = "<request>
            <version>1.2</version>
            <result_url>$returnURL</result_url>
            <server_url>$resultURL</server_url>
            <merchant_id>$liqpay_ID</merchant_id>
            <order_id>$order_id</order_id>
            <amount>$price</amount>
            <currency>$currency</currency>
            <description>$productDesc</description>
            <default_phone></default_phone>
            <pay_way></pay_way>
            </request>";

        $xml_encoded = base64_encode($xml);
        $signature = base64_encode(sha1($liqpay_sign . $xml . $liqpay_sign, 1));

        $payment_form = "
		<form action='https://liqpay.com/?do=clickNbuy' method='post'>
            <input type='hidden' name='operation_xml' value='$xml_encoded'>
            <input type='hidden' name='signature' value='$signature'>
            <input type='submit' title='С помощью LiqPay' value='к оплате'>
		</form>";

		return $payment_form;
	}

	/**
	* Функция проверки результата оплаты
	*
	* @return boolean
	*/
	public function CheckResult($operation_xml, $signature, $liqpay_sign)
	{
        // Распаковка данных
        $xml = base64_decode($operation_xml);

        // Парсинг данных
        $payment_data['order_id'] = $this->parseTag($xml, 'order_id');
        $payment_data['status'] = $this->parseTag($xml, 'status');
        $payment_data['sum'] = $this->parseTag($xml, 'amount');

        // Проверка подлинности
        $signature_new = base64_encode(sha1($liqpay_sign . $xml . $liqpay_sign, 1));

		if ($signature == $signature_new && $payment_data['status'] == 'success')
			return $payment_data;
		else
			return false;
	}

    private function parseTag($rs, $tag)
    {
        $rs = str_replace("\n", "", str_replace("\r", "", $rs));
        $tags = '<'.$tag.'>';
        $tage = '</'.$tag;
        $start = strpos($rs, $tags)+strlen($tags);
        $end = strpos($rs, $tage);
        return substr($rs, $start, ($end-$start));
    }

	public function SuccessPayment()
	{
		
	}

	public function FailurePayment()
	{
		
	}
}