<?php
# Модуль работы с пользователями

# Подключение настроек, если не подключены
if (empty($config))
{
    include_once dirname(__FILE__) . '/../jcart.inc.php';
	$config = $jcart->config;
}

# Подключение модуля работы с БД
include_once dirname(__FILE__) . '/M_DB.inc.php';
$mDB = M_DB::Instance();

class M_Users
{
	private static $instance; 	# ссылка на экземпляр класса
	private $msql; 				# драйвер БД
	private $admin; 			# признак админа

	# Получение единственного экземпляра класса
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_Users();

		return self::$instance;
	}

	# Конструктор
	public function __construct()
	{
		# Подключение драйвера работы с БД и модели администратора
		$this->msql = MSQL::Instance();
		$this->admin = M_Admin::Instance();
	}
	
	# Регистрация (создание пользователя)
	public function CreateUser($email, $date)
	{
		# Проверка данных.
		if ($email == '' || $date == '')
			return false;

		# Генерация пароля.
		$password = $this->GenerateCode(6);

		# Запрос.
		$obj = array();
		$obj['email'] = $email;
		$obj['password'] = $password;
		$obj['date'] = $date;

		$id_user = $this->msql->Insert('user', $obj);

		$user['id_user'] = $id_user;
		$user['password'] = $password;

		return $user;
	}

	# Отправка пароля на почту
	public function SendPassword($email, $password, $config)
	{
		include_once dirname(__FILE__) . '/M_Email.inc.php';
		$mEmail = M_Email::Instance();

		$content = $mEmail->PreparePassword($email, $password, $config['sitelink'], $config['sitename'], $config['auth']['authURL']);

		return ($mEmail->SendEmail($email, $config['email']['answer'], $config['email']['subjectPassword'], $content, $config['email']['answerName'], null, $config['encoding']));
	}

	# Генерация кода
	# $length 		- длина
	# результат	- случайная строка
	private function GenerateCode($length = 10, $all_chars = true)
	{
		if ($all_chars == true)
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
		else
			$chars = "ABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
		$code = '';
		$clen = strlen($chars) - 1;

		while (strlen($code) < $length)
			$code .= $chars[mt_rand(0, $clen)];

		return $code;
	}

	# Редактирование пользователя
	public function EditUser($id_user, $email, $date, $status)
	{
		# Проверка данных.
		if ($id_user == '' || $email == '' || $date == '' || $status == '')
			return false;

		# Запрос.
		$obj = array();
		$obj['email'] = $email;
		$obj['date'] = $date;
		$obj['status'] = $status;

		$t = "id_user = '%d'";
		$where = sprintf($t, $id_user);
		$this->msql->Update('user', $obj, $where);
		return true;
	}
	
	# Получение пользователя по идентификатору
	public function GetById($id_user)
	{
		$t = "SELECT * FROM user WHERE id_user = '%d'";
		$query = sprintf($t, $id_user);
		$result = $this->msql->Select($query);
		return $result[0];
	}

	# Получение пользователя по логину
	public function GetByLogin($login)
	{
		$t = "SELECT * FROM user WHERE email = '%s'";
		$query = sprintf($t, mysql_real_escape_string($login));
		$result = $this->msql->Select($query);
		return (!empty($result[0])) ? $result[0] : false;
	}

	# Выбор последнего заказа пользователя
	public function GetLastOrder($id_user)
	{
		$t = "SELECT * FROM custom WHERE id_user = '%d' AND status = 1 ORDER BY id_custom DESC";
		$query = sprintf($t, $id_user);
		$result = $this->msql->Select($query);

		$sum = 0;
		foreach ($result as $res)
		{
			$sum += $res['sum'];
		}
		$result[0]['sum'] = $sum;

		return $result[0];
	}

	# Авторизация
	# $login 		- логин
	# $password 	- пароль
	# $remember 	- нужно ли запомнить в куках
	# результат	- true или false
	public function Login($login, $password, $remember = true)
	{
		$login = trim($login);
		$password = trim($password);
		# выбор пользователя из БД
		$user = $this->GetByLogin($login);

		if ($user == null)
			return false;

		$id_user = $user['id_user'];

		# проверка пароля
		if ($user['password'] != $password)
			return false;

		# запоминаник имени и пароля
		if ($remember)
		{
			$expire = time() + 3600 * 24 * 100;
			setcookie('email', $login, $expire, '/', $_SERVER['SERVER_NAME']);
			setcookie('password', $password, $expire, '/', $_SERVER['SERVER_NAME']);
		}

		# открытие сессии
		$_SESSION['id_user'] = $id_user;

		return true;
	}

	# Выход
	public function Logout()
	{
		# Удаление кук и сессии
		setcookie('email', '', time() - 1, '/', $_SERVER['SERVER_NAME']);
		setcookie('password', '', time() - 1, '/', $_SERVER['SERVER_NAME']);
		unset($_COOKIE['email']);
		unset($_COOKIE['password']);
		unset($_COOKIE['user-sum']);
		unset($_SESSION['id_user']);

		return true;
	}

	# Подсчёт скидки
	public function CountDiscount($sum, $discounts)
	{
        $mark = 0;
		
		foreach ($discounts as $key => $discount)
		{
			if ($sum >= $key)
				$user['discount'] = $discount;

			if ($mark != 1)
			{
				if ($sum < $key)
				{
					$user['next_discount'] = $discount;
					$user['next_sum'] = $key;
					$mark = 1;
				}
			}
		}
		
		$user['discount'] = (isset($user['discount'])) ? $user['discount'] : 0;
		$user['next_sum'] = (isset($user['next_sum'])) ? $user['next_sum'] : 0;
		$user['next_discount'] = (isset($user['next_discount'])) ? $user['next_discount'] : 0;

		return $user;
	}
}