<?php

# Контроллер работы с пользователями
include_once dirname(__FILE__) . '/../jcart.inc.php';
$config = $jcart->config;

# Подключение модуля работы с базой данных
include_once dirname(__FILE__) . '/M_Users.inc.php';
include_once dirname(__FILE__) . '/M_Email.inc.php';
$mUsers = M_Users::Instance();
$mEmail = M_Email::Instance();

# Если заказ отправлен, то добавление клиента в БД
if (isset($new_order))
{
	$date = date("Y-m-d H:i:s");

	# Выбор пользователя из БД
	$user = $mUsers->GetByLogin($email);

	# Если клиента нет в БД
	if (empty($user))
	{
		# добавление пользователя в БД
		$user = $mUsers->CreateUser($email, $date);

		# Отправка уведомления о регистрации
		$result = $mUsers->SendPassword($email, $user['password'], $config);
	}
}

if (isset($_POST['user_enter']) || isset($_POST['user_remind']))
{
	# Проверка на спам
	$spam = $mEmail->CheckSpam($_POST['user_nospam'], $config['secretWord']);

	if (!$spam)
		$form['message'] = $config['form']['isSpam'];
	else
	{
		if (isset($_POST['user_enter']))
		{
			$user_remember = (isset($_POST['user_remember'])) ? true : false;
			if ($mUsers->Login($_POST['user_email'], $_POST['user_password'], $user_remember))
			{
				header ('Location: ' . $config['sitelink'] . $config['auth']['cabinetURL']);
				die;
			}
			else
				$form['message'] = $config['form']['notLogined'];
		}

		if (isset($_POST['user_remind']))
		{
			if ($user = $mUsers->GetByLogin($_POST['user_email']))
			{
				$mUsers->SendPassword($user['email'], $user['password'], $config);
				header ('Location: ' . $config['sitelink'] . $config['auth']['authURL'] . '?login&email=' . $_POST['user_email']);
				die;
			}
			else
				$form['message'] = $config['form']['emptyEmail'];
		}
	}
}

if (isset($_GET['login']))
{
	if (isset($_GET['email']))
		$email = $_GET['email'];

	if (isset($_GET['pass']))
		$pass = $_GET['pass'];

	# Генерирация антиспама
	$antispam = $mEmail->GenerateAntispam($config['secretWord']);

	# Заголовок
	$title = 'Вход в кабинет покупателя';
	
	# Вывод шаблона
	ob_start();
	include_once dirname(__FILE__) . '/../design/UserEnter.tpl.php';
	$content = ob_get_clean();
}

if (isset($_GET['remind']))
{
	# Генерирация антиспама
	$antispam = $mEmail->GenerateAntispam($config['secretWord']);

	# Заголовок
	$title = 'Напоминание пароля';

	# Вывод шаблона
	ob_start();
	include_once dirname(__FILE__) . '/../design/UserRemindPassword.tpl.php';
	$content = ob_get_clean();
}


if (isset($_GET['logout']))
{
	if ($mUsers->Logout())
	{
		header ('Location: ' . $config['sitelink']); // $_SERVER['HTTP_REFERER']
		die;
	}
}