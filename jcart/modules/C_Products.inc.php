<?php
# Контроллер работы с товарами.

# Подключение модуля работы с товарами.
include_once dirname(__FILE__) . '/M_Products.inc.php';
$mProducts = M_Products::Instance();

# Если заказ отправлен, то изменяем его количество.
if (isset($new_order))
{
	foreach ($orderItems as $orderItem)
	{
		$mProducts->ChangeQuantityByCode($orderItem['id'], $orderItem['quantity']);
	}
}