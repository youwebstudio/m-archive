<?php
# Модуль работы со счетами

class M_Invoices {

	private static $instance; 	# ссылка на экземпляр класса

	# Получение единственного экземпляра класса
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_Invoices();

		return self::$instance;
	}

	public function CreateInvoice($id_order, $order_items, $currency, $name = null, $juridical = null)
	{
		# Запись данных в переменную
		ob_start();
		include_once dirname(__FILE__) . '/../design/invoices/Invoice.tpl.php';
		$content = ob_get_contents();
		ob_end_clean();

		# Определение названия файла
		$filename = 'invoice_' . $id_order . '.html';
		$filepath = dirname(__FILE__) . '/../invoices/' . $filename;

		# Запись файла для скачивания
		file_put_contents($filepath, $content);

		return $filename;
	}

	public function CreateInvoiceFact($id_order, $order_items, $currency, $address, $juridical = null)
	{
		# Запись данных в переменную
		ob_start();
		include_once dirname(__FILE__) . '/../design/invoices/InvoiceFact.tpl.php';
		$content = ob_get_contents();
		ob_end_clean();

		# Определение названия файла
		$filename = 'invoice_fact_' . $id_order . '.html';
		$filepath = dirname(__FILE__) . '/../invoices/' . $filename;

		# Запись файла для скачивания
		file_put_contents($filepath, $content);

		return $filename;
	}
}