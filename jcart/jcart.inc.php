<?php

# jCart v1.3.5
# http://conceptlogic.com/jcart/
# http://jcart.info

class Jcart
{
    public $config = array();
    private $items = array();
    private $params = array();
    private $subtotal = 0;
    private $discount = 0;
    private $itemCount = 0;



    function __construct()
    {

        # Get $config array
        if (is_file(dirname(__FILE__) . '/config.php'))
            include_once dirname(__FILE__) . '/config-loader.php';
        else
        {
            preg_match('/(.*)jcart/', 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'], $uri);
            header('Location: ' . $uri[1] . 'jcart/install/');
        }
        $this->config = $config;
        foreach ($config['param'] as $param_name)
        {
            $this->params[$param_name] = array();
        }

        # Добавляем в корзину товар по умолчанию
        if (isset($config['default_product']))
            $this->add_item($config['default_product']['id'], $config['default_product']['name'], $config['default_product']['price'], $config['default_product']['discount'], $config['default_product']['qty'], $config['default_product']['url'], $config['default_product']['size'], $config['default_product']['color'], $config['default_product']['param']);
    }

    public function get_subtotal()
    {
        return $this->subtotal;
    }

    /**
     * Get cart contents
     *
     * @return array
     */
    public function get_contents()
    {
        $items = array();
        $params = array();
        foreach ($this->items as $tmpItem)
        {
            $item = null;
            $item['id'] = $tmpItem;

            foreach ($this->config['param'] as $param_name)
            {
                $item[$param_name] = $this->params[$param_name][$tmpItem];
            }

            $item['subtotal'] = $item['price'] * $item['qty'] * (1 - max($this->discount, $item['discount']) / 100);
            $items[] = $item;
        }
        return $items;
    }

    /**
     * Add an item to the cart
     *
     * @param string $id
     * @param string $name
     * @param float $price
     * @param mixed $qty
     * @param string $url
     *
     * @return mixed
     */
    private function add_item($cart_data)
    {
        $id = $cart_data['id'];
        foreach ($this->config['param'] as $param_name)
        {
            if ($this->config['encoding'] == 'windows-1251' && is_string($cart_data[$param_name]))
            {
                $$param_name = iconv('utf-8', 'windows-1251', $cart_data[$param_name]);
            }
            else
                $$param_name = $cart_data[$param_name];
        }

        $validPrice = false;
        $validQty = false;


        # Verify the price is numeric

        if (is_numeric($price))
        {
            $validPrice = true;
        }

        # If decimal quantities are enabled, verify the quantity is a positive float
        if ($this->config['decimalQtys'] === true && filter_var($qty, FILTER_VALIDATE_FLOAT) && $qty > 0)
        {
            $validQty = true;
        } # By default, verify the quantity is a positive integer
        elseif (filter_var($qty, FILTER_VALIDATE_INT) && $qty > 0)
        {
            $validQty = true;
        }

        # Add the item
        if ($validPrice !== false && $validQty !== false)
        {


            # If the item is already in the cart , increase its quantity

            if (isset($this->params['qty'][$id]) && $this->params['qty'][$id] > 0)
            {
                $this->params['qty'][$id] += $qty;
                $this->update_subtotal();
            } # This is a new item
            else
            {
                $this->items[] = $id;
                foreach ($this->config['param'] as $param_name)
                {
                    $this->params[$param_name][$id] = $$param_name;
                }
            }
            $this->update_subtotal();
            return true;
        } elseif ($validPrice !== true)
        {
            $errorType = 'price';
            return $errorType;
        } elseif ($validQty !== true)
        {
            $errorType = 'qty';
            return $errorType;
        }
    }

    # Склонение существительных с числительными (http://mcaizer.habrahabr.ru/blog/11555/)
    public function plural($n, $form1, $form2, $form5)
    {
        $n = abs($n) % 100;
        $n1 = $n % 10;
        if ($n > 10 && $n < 20) return $form5;
        else if ($n1 > 1 && $n1 < 5) return $form2;
        else if ($n1 == 1) return $form1;

        return $form5;
    } # echo $n." ".plural($n, "письмо", "письма", "писем")." у Вас в ящике";

    /**
     * Update an item in the cart
     *
     * @param string $id
     * @param mixed $qty
     *
     * @return boolean
     */
    private function update_item($id, $qty)
    {
        $validQty = false;

        # If the quantity is zero, no futher validation is required
        if ((int)$qty === 0)
        {
            $validQty = true;
        } # If decimal quantities are enabled, verify it's a float
        elseif ($this->config['decimalQtys'] === true && filter_var($qty, FILTER_VALIDATE_FLOAT))
        {
            $validQty = true;
        } # By default, verify the quantity is an integer
        elseif (filter_var($qty, FILTER_VALIDATE_INT))
        {
            $validQty = true;
        }

        # If it's a valid quantity, remove or update as necessary
        if ($validQty === true)
        {
            if ($qty < 1)
            {
                $this->remove_item($id);
            }
            else
            {
                $this->params['qty'][$id] = $qty;
            }
            $this->update_subtotal();
            return true;
        }
    }


    /* Using post vars to remove items doesn't work because we have to pass the
    id of the item to be removed as the value of the button. If using an input
    with type submit, all browsers display the item id, instead of allowing for
    user-friendly text. If using an input with type image, IE does not submit
    the    value, only x and y coordinates where button was clicked. Can't use a
    hidden input either since the cart form has to encompass all items to
    recalculate    subtotal when a quantity is changed, which means there are
    multiple remove    buttons and no way to associate them with the correct
    hidden input. */

    /**
     * Reamove an item from the cart
     *
     * @param string $id   *
     */
    private function remove_item($id)
    {
        $tmpItems = array();
        foreach ($this->config['param'] as $param_name)
        {
            unset($this->params[$param_name][$id]);
        }
        # Rebuild the items array, excluding the id we just removed
        foreach ($this->items as $item)
        {
            if ($item != $id)
            {
                $tmpItems[] = $item;
            }
        }
        $this->items = $tmpItems;
        $this->update_subtotal();
    }

    /**
     * Empty the cart
     */
    public function empty_cart()
    {

        foreach ($this->config['param'] as $param_name)
        {
            $this->params[$param_name] = array();
        }
        $this->items = array();
        $this->subtotal = 0;
        $this->discount = 0;
        $this->itemCount = 0;
    }

    /**
     * Update the entire cart
     */
    public function update_cart()
    {

        # Post value is an array of all item quantities in the cart
        # Treat array as a string for validation
        if (isset($_POST['jcartItemQty']) && is_array($_POST['jcartItemQty']))
        {
            $qtys = implode($_POST['jcartItemQty']);
        }

        # If no item ids, the cart is empty
        if (isset($_POST['jcartItemId']))
        {

            $validQtys = false;

            # If decimal quantities are enabled, verify the combined string only contain digits and decimal points
            if ($this->config['decimalQtys'] === true && preg_match("/^[0-9.]+$/i", $qtys))
            {
                $validQtys = true;
            } # By default, verify the string only contains integers
            elseif (filter_var($qtys, FILTER_VALIDATE_INT) || $qtys == '')
            {
                $validQtys = true;
            }

            if ($validQtys === true)
            {

                # The item index
                $count = 0;

                # For each item in the cart, remove or update as necessary
                foreach ($_POST['jcartItemId'] as $id)
                {

                    $qty = $_POST['jcartItemQty'][$count];

                    if ($qty < 1)
                    {
                        $this->remove_item($id);
                    } else
                    {
                        $this->update_item($id, $qty);
                    }

                    # Increment index for the next item
                    $count++;
                }
                return true;
            }
        } # If no items in the cart, return true to prevent unnecssary error message
        elseif (!isset($_POST['jcartItemId']))
        {
            return true;
        }
    }

    /**
     * Recalculate subtotal
     */
    private function update_subtotal()
    {
        $this->itemCount = 0;
        $this->subtotal = 0;

        if (sizeof($this->items > 0))
        {
            foreach ($this->items as $item)
            {
                $this->subtotal += ($this->params['qty'][$item] * $this->params['price'][$item] * (1 - max($this->params['discount'][$item], $this->discount) / 100));

                # Total number of items
                $this->itemCount += $this->params['qty'][$item];
            }
        }
    }

    /**
     * Get currency symbol by code
     *
     * @param  string $currencyCode
     * @return void
     */

    function currencySymbol($currencyCode)
    {
        switch ($currencyCode)
        {
            case 'EUR':
                return $currencySymbol = '&#128;';
            case 'GBP':
                return $currencySymbol = '&#163;';
            case 'JPY':
                return $currencySymbol = '&#165;';
            case 'CHF':
                return $currencySymbol = 'CHF&nbsp;';
            case 'NOK':
                return $currencySymbol = 'Kr&nbsp;';
            case 'PLN':
                return $currencySymbol = 'z&#322;&nbsp;';
            case 'HUF':
                return $currencySymbol = 'Ft&nbsp;';
            case 'CZK':
                return $currencySymbol = 'K&#269;&nbsp;';
            case 'ILS':
                return $currencySymbol = '&#8362;&nbsp;';
            case 'TWD':
                return $currencySymbol = 'NT$';
            case 'THB':
                return $currencySymbol = '&#3647;';
            case 'MYR':
                return $currencySymbol = 'RM';
            case 'PHP':
                return $currencySymbol = 'Php';
            case 'BRL':
                return $currencySymbol = 'R$';
            case 'RUR':
                return $currencySymbol = 'руб.';
            case 'UAH':
                return $currencySymbol = 'грн.';
            case 'USD':
            default:
                return $currencySymbol = '$';
        }
    }

    /**
     * Process and display cart
     */
    public function display_cart()
    {
        $config = $this->config;
        $errorMessage = null;

        # Simplify some config variables
        $checkout = $config['checkoutPath'];
        $priceFormat = $config['priceFormat'];

        foreach ($this->config['item'] as $param_name => $atr_name)
        {
            ${$param_name} = $atr_name;
        }

        # Use config values as literal indices for incoming POST values
        # Values are the HTML name attributes set in config.json
        $id = (isset($_POST[$id])) ? $_POST[$id] : '';

        foreach ($this->config['param'] as $param_name)
        {
            $$param_name = (isset($_POST[$$param_name])) ? $_POST[$$param_name] : '';
        }

        # Optional CSRF protection, see: http://conceptlogic.com/jcart/security.php
        $jcartToken = (isset($_POST['jcartToken'])) ? $_POST['jcartToken'] : '';
        
        global $jsession;
        $jtoken = $jsession->get('jcartToken');

        # Only generate unique token once per session
        if (!isset($jtoken) && isset($_SERVER['HTTP_USER_AGENT']))
        {
            $jtoken = md5(session_id() . time() . $_SERVER['HTTP_USER_AGENT']);
            $jsession->set('jcartToken', $jtoken);
        }

        
        /*
        # Only generate unique token once per session
        if (!isset($_SESSION['jcartToken']))
        {
            $_SESSION['jcartToken'] = md5(session_id() . time() . $_SERVER['HTTP_USER_AGENT']);
        }
        */

        # If enabled, check submitted token against session token for POST requests
        if ($config['csrfToken'] == true && !empty($_POST) && $jcartToken != $jtoken && isset($_POST[$add]))
        {
            $errorMessage = 'Invalid token!' . $jcartToken . ' / ' . $jtoken;
        }

        # Sanitize values for output in the browser
        $id = filter_var($id, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
        $name = filter_var($name, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW);
        if (isset($url))
            $url = filter_var($url, FILTER_SANITIZE_URL);

        # Round the quantity if necessary
        if ($config['decimalQtys'] === true)
        {
            $qty = round($qty, $config['decimalPlaces']);
        }

        # Add an item
        if (isset($config['unique']))
        {
            foreach ($config['unique'] as $unic)
            {
                $id .= '_' . $$unic;
            }
        }
        $cart_data = array();
        $cart_data['id'] = $id;
        foreach ($this->config['param'] as $param_name)
        {
            $cart_data[$param_name] = $$param_name;
        }

        if (isset($_POST[$add]) || isset($_POST[$add . '_x']))
        {
            $itemAdded = $this->add_item($cart_data);
            # If not true the add item function returns the error type

            if ($itemAdded !== true)
            {
                $errorType = $itemAdded;
                switch ($errorType)
                {
                    case 'qty':
                        $errorMessage = $config['text']['quantityError'];
                        break;
                    case 'price':
                        $errorMessage = $config['text']['priceError'];
                        break;
                }
            }
        }

        # Update a single item
        if (isset($_POST['jcartUpdate']))
        {
            $itemUpdated = $this->update_item($_POST['itemId'], $_POST['itemQty']);
            if ($itemUpdated !== true)
            {
                $errorMessage = $config['text']['quantityError'];
            }
        }

        # Update all items in the cart
        if (isset($_POST['jcartUpdateCart']) || isset($_POST['jcartCheckout']))
        {
            $cartUpdated = $this->update_cart();
            if ($cartUpdated !== true)
            {
                $errorMessage = $config['text']['quantityError'];
            }
        }

        # Remove an item
        /* After an item is removed, its id stays set in the query string,
        preventing the same item from being added back to the cart in
        subsequent POST requests.  As result, it's not enough to check for
        GET before deleting the item, must also check that this isn't a POST
        request. */
        if (isset($_GET['jcartRemove']) && empty($_POST))
        {
            $this->remove_item($_GET['jcartRemove']);
        }

        # Empty the cart
        if (isset($_POST['jcartEmpty']))
        {
            $this->empty_cart();
        }

        # Determine which text to use for the number of items in the cart
        $itemsText = $this->plural($this->itemCount, $config['text']['singleItem'], $config['text']['multipleItems1'], $config['text']['multipleItems2']);

        # Determine if this is the checkout page
        /* First we check the request uri against the config checkout (set when
        the visitor first clicks checkout), then check for the hidden input
        sent with Ajax request (set when visitor has javascript enabled and
        updates an item quantity). */
        #$isCheckout = strpos(request_uri(), $checkout);
        $isCheckout = strpos('http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'], $checkout);
        if ($isCheckout !== false || isset($_REQUEST['jcartIsCheckout']) && $_REQUEST['jcartIsCheckout'] == 'true')
        {
            $isCheckout = true;
        } else
        {
            $isCheckout = false;
        }

        # Overwrite the form action to post to gateway.php instead of posting back to checkout page
        if ($isCheckout === true)
        {

            # Sanititze config path
            $path = filter_var($config['jcartPath'], FILTER_SANITIZE_URL);

            # Trim trailing slash if necessary
            $path = rtrim($path, '/');

            $checkout = $config['sitelink'] . $path . '/gateway.php';
        }
		else
			$checkout = $config['sitelink'] . $config['checkoutPath'];

        # Default input types
        # Overridden if using button images in config.php
        $inputTypeCheckout = $inputTypeUpdate = $inputTypeEmpty = $inputTypeCheckoutPaypal = 'submit';

        # If this error is true the visitor updated the cart from the checkout page using an invalid price format
        # Passed as a session var since the checkout page uses a header redirect
        # If passed via GET the query string stays set even after subsequent POST requests
        if (isset($_SESSION['quantityError']) && $_SESSION['quantityError'] === true)
        {
            $errorMessage = $config['text']['quantityError'];
            unset($_SESSION['quantityError']);
        }

        # Set currency symbol based on config currency code
        $currencyCode = trim(strtoupper($config['currencyCode']));
        $currencySymbol = $this->currencySymbol($currencyCode);

        # Модуль работы с пользователями
        if (($config['auth']['enabled'] == true || isset($config['discounts'])) && is_file(dirname(__FILE__) . '/modules/M_Users.inc.php'))
        {
            include_once dirname(__FILE__) . '/modules/M_Users.inc.php';
            $mUsers = M_Users::Instance();

            # Авторизация пользователя
            if (!isset($_SESSION['id_user']) && isset($_COOKIE['email']) && isset($_COOKIE['password']))
            {
                $mUsers->Login($_COOKIE['email'], $_COOKIE['password']);
            }

            if (isset($_SESSION['id_user']))
            {
                $user = $mUsers->GetLastOrder($_SESSION['id_user']);

                $email = (isset($user['email'])) ? $user['email'] : '';
                $name = (isset($user['name'])) ? $user['name'] : '';
                $lastname = (isset($user['lastname'])) ? $user['lastname'] : '';
                $fathername = (isset($user['fathername'])) ? $user['fathername'] : '';
                $zip = (isset($user['zip'])) ? $user['zip'] : '';
                $region = (isset($user['region'])) ? $user['region'] : '';
                $city = (isset($user['city'])) ? $user['city'] : '';
                $address = (isset($user['address'])) ? $user['address'] : '';
                $phone = (isset($user['phone'])) ? $user['phone'] : '';
                $order_delivery = (isset($user['delivery'])) ? $user['delivery'] : '';
                $order_payment = (isset($user['payment'])) ? $user['payment'] : '';
                $juridical = (!empty($user['juridical'])) ? explode('|', $user['juridical']) : '';

                if (!empty($juridical))
                {
                    $juridical_inn = (isset($juridical[0])) ? $juridical[0] : '';
                    $juridical_kpp = (isset($juridical[1])) ? $juridical[1] : '';
                    $juridical_firm = (isset($juridical[2])) ? $juridical[2] : '';
                }

                if (isset($config['discounts']))
                    $user += $mUsers->CountDiscount($this->subtotal + $user['sum'], $config['discounts']);

                $_COOKIE['user-sum'] = (isset($user['sum'])) ? $user['sum'] : '';
            }
        }

        if (isset($config['discounts']) && !isset($user['discount']))
            $user = $mUsers->CountDiscount($this->subtotal, $config['discounts']);

        if (!isset($user['discount']))
            $user['discount'] = 0;
        else
            $this->discount = $user['discount'];

        if (isset($config['discounts']))
            $this->update_subtotal();

        # If there's an error message wrap it in some HTML
        if (isset($errorMessage))
        {
            $errorMessage = '<p id="jcart-error">' . $errorMessage . '</p>';
        }

        # If this is the checkout hide the cart checkout button
        if ($isCheckout !== true)
        {
            if ($config['button']['checkout'])
            {
                $inputTypeCheckout = 'image';
                $srcCheckout = ' src="' . $config['button']['checkout'] . '" alt="' . $config['text']['checkout'] . '" title="" ';
            }
        }

        if ($config['button']['update'])
        {
            $inputTypeUpdate = 'image';
            $srcUpdate = ' src="' . $config['button']['update'] . '" alt="' . $config['text']['update'] . '" title="" ';
        }

        if ($config['button']['empty'])
        {
            $inputTypeEmpty = 'image';
            $srcEmpty = ' src="' . $config['button']['empty'] . '" alt="' . $config['text']['emptyButton'] . '" title="" ';
        }

        # If this is the checkout display the PayPal checkout button
        if ($isCheckout === true)
        {
            # Hidden input allows us to determine if we're on the checkout page
            # We normally check against request uri but ajax update sets value to relay.php

            # PayPal checkout button
            if ($config['button']['paypal'])
            {
                $inputTypeCheckoutPaypal = 'image';
                $srcCheckoutPaypal = ' src="' . $config['button']['paypal'] . '" alt="' . $config['text']['checkoutPaypal'] . '" title="" ';
            }

            if ($this->itemCount <= 0)
            {
                $disablePaypalCheckout = ' disabled="disabled"';
            }

            if (is_file(dirname(__FILE__) . '/modules/M_Email.inc.php'))
            {
                # Генерация антиспама
                include_once dirname(__FILE__) . '/modules/M_Email.inc.php';
                $mEmail = M_Email::Instance();
                $antispam = $mEmail->GenerateAntispam($config['secretWord']);
            }
        }
        if ($isCheckout != true && $config['modal'] == true)
        {
            ob_start();
            include_once dirname(__FILE__) . '/design/CartModal.tpl.php';
            $modal_cart = ob_get_clean();
        }
        include dirname(__FILE__) . '/design/CartSmall.tpl.php';
//        if ($isCheckout != true && $config['smallCart'] == true)
//            include_once dirname(__FILE__) . '/design/CartSmall.tpl.php';
//        else
        if($isCheckout)
            include_once dirname(__FILE__) . '/design/Cart.tpl.php';

        if ($isCheckout === true) {
            include_once dirname(__FILE__) . '/design/CartOrderForm.tpl.php';
        }
    }
    public function session_update()
    {
        global $jsession;
        $jsession->set('jcart', $this);
    }
}