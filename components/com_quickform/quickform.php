<?php
/**
* @Copyright ((c) bigemot.ru
* @ http://bigemot.ru/
* @license    GNU/GPL
*/

defined('_JEXEC') or die;
require_once JPATH_ADMINISTRATOR.'/components/com_quickform/helpers/quickform.php';

$reqForm=JRequest::getInt('formreq', NULL);
if($reqForm){
	header ("Content-type: text/html; charset=utf-8");
	require_once(JPATH_ADMINISTRATOR."/components/com_quickform/helpers/form.php");
	
	$contents = new QuickForm((int)$reqForm);
	echo $contents->ajaxHTML();
	exit;
}
	
JRequest::checkToken() or jexit( 'Invalid Token' );
require_once JPATH_COMPONENT.'/helpers/class.php';

if(JRequest::getCmd( 'task')!='form')return;
if(!JRequest::getInt('id', NULL))return;

$post = JRequest::get('post');

$db		= JFactory::getDBO();
$db->setQuery('SELECT * FROM #__quickform WHERE published=1 AND id = '.(int)$post['id']);
$row = $db->loadObject();

if(!$row->settlement)return;

$user = JFactory::getUser();
$groups	= $user->getAuthorisedViewLevels();
if(!in_array($row->access, $groups))return;

$start=(float)str_replace(',','.',$row->price);
$sum=$GLOBALS['qfSum']=0;
$qfCheck = new qfCheck;
$c=$row->calc?true:false;
$params=json_decode($row->params, TRUE);

$html =$qfCheck->getFilds(QuickFormHelper::coder($row->settlement,'d'),$c, $post['id']) ;

if($c){
	$arr=explode(';',$GLOBALS['qfSum']);
	
	if(!$params['formul']){
		$sum=$start;
		foreach($arr as $ar){
			if($ar{0}=='*')$sum*=substr($ar, 1);
			elseif($ar{0}=='=')$sum=substr($ar, 1);
			elseif($ar{0}=='-')$sum-=substr($ar, 1);
			elseif($ar{0}=='+')$sum+=substr($ar, 1);
		}
		$res=$sum;
	}
	elseif($params['formul']==1){
		foreach($arr as $ar){
			if($ar{0}=='*')$start*=substr($ar, 1);
			elseif($ar{0}=='=')$start=substr($ar, 1);
			elseif($ar{0}=='-')$sum-=substr($ar, 1);
			elseif($ar{0}=='+')$sum+=substr($ar, 1);
		}
		$res=$start+$sum;
	}
	elseif($params['formul']==2){
		$sum=$start;
		$mul=1;
		foreach($arr as $ar){
			if($ar{0}=='*')$mul*=substr($ar, 1);
			elseif($ar{0}=='=')$sum=substr($ar, 1);
			elseif($ar{0}=='-')$sum-=substr($ar, 1);
			elseif($ar{0}=='+')$sum+=substr($ar, 1);
		}
		$res=$sum*$mul;
	}
	
	$html.='<tr height="60"><td colspan="2" align="right" style="padding: 0 40px;"></td><td>'.($res).' '.$row->cur.'</td></tr>';
}
$html2 ='<br />'.$row->title.'<br /><br />'.'<table width="600" cellspacing="1" border="1">'.$html.'</table>';

$mailer = JFactory::getMailer();
$jAp = JFactory::getApplication();

$lsFromEmail = $jAp->getCfg('mailfrom');
$lsFromName  = $jAp->getCfg('fromname');
$lsFrom 	 = array($lsFromEmail, $lsFromName);

if($row->toemail){
	$arr=explode(',',$row->toemail);
	foreach($arr as $ar){
		$mailer->addRecipient(trim($ar));
	}
}
else $mailer->addRecipient($lsFromEmail);

if($post['back'])$mailer->addRecipient($post['email']);

$mailer->setSender($lsFrom);
$mailer->addReplyTo($lsFrom);
$mailer->setSubject(JText::_('MESSAGE').' '.$_SERVER['HTTP_HOST']);
$mailer->setBody($html2);
$mailer->isHTML(true);

$files = $jAp->input->files->get( 'qffile', array(), 'array' );
foreach ( $files as $file ) {
	$mailer->addAttachment( $file['tmp_name'], $file['name'] ); 
}


if ($mailer->Send() !== true)$msg=JText::_('JERROR_LAYOUT_ERROR_HAS_OCCURRED_WHILE_PROCESSING_YOUR_REQUEST');
else {
	$msg=JText::_('COM_MAILTO_EMAIL_SENT');
	
	$fields = array(
		'st_formid' => (int)$row->id, 
		'st_date' => gmdate('Y-m-d H:i:s'),
		'st_form' => $html, 
		'st_title' => $row->title, 
		'st_cur' => $row->cur, 
		'st_price' => $start+$sum, 
		'st_ip' => $qfCheck->getip(), 
		'params' => '', 
		'st_user' => $user->get('id'), 
		'st_status' => 0
	);
	foreach($fields as $key=>$value){
		$v_key.=",$key";
		$v_value.=",'$value'";
	}
	$v_key=substr($v_key, 1);
	$v_value=substr($v_value, 1);

	$db->setQuery("INSERT INTO `#__quickform_ps` ($v_key) VALUES ($v_value)");
	$db->query();
	
	$db->setQuery("UPDATE `#__quickform` SET hits = ( hits + 1 ) WHERE id = ".(int)$row->id);
	$db->query();
}

if($_SERVER['HTTP_REFERER'])$link=$_SERVER['HTTP_REFERER'];
else $link='/';

$jAp->redirect($link,$msg);

