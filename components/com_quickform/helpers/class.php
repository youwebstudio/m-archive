<?php
/**
* @Copyright ((c) bigemot.ru
* @ http://bigemot.ru/
* @license    GNU/GPL
*/

defined( '_JEXEC' ) or die( 'Restricted access' );

class qfCheck
{
	
	function getSelect($pat, $post, $c) 
	{
		static $i=0;$add='';$k=0;$value='';$svz=0;
		
		preg_match('/([^"]+)(?=" class="inp_sel")/', $pat, $m);
		$title=$m[0];
		
		$opts=explode('</div><div>',$pat);
		
		foreach($opts as $opt) {
			$calc='';
			$vals=explode('input',$opt);
			
			if($c){
				foreach($vals as $val){
					if(strpos($val,'opt_modifer'))$calc.=$val{strpos($val,'value="')+7};
				}
				
				preg_match('/([^"]+)(?=" class="opt_price)/', $opt, $o);
				$calc.=trim($o[0]);
			}
		
			if($k.'_'.$calc==$post['sel'][$i]){
				preg_match('/([^"]+)(?=" class="inp_opt")/', $opt, $m);
				$value=$m[0];
				if($c) {
					$add=$calc;
					$GLOBALS['qfSum'].=';'.$calc;
				}
				preg_match('/([^"]+)(?=" class="inp_svz")/', $opt, $m);
				if($m)$svz=(int)$m[0];
			}
			
			$k++;
		}
		
		$i++;
		
		$html = $this->getTr($title,$value,$add);
		if($svz)$html.=$this->getChildForm($svz);
		
		return $html;
	}
	
	function getChildForm($id) 
	{
		$db		=& JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__quickform WHERE published=1 AND id = '.(int)$id);
		$row = $db->loadObject();
		
		if(!$row->settlement) return '';
		$c=$row->calc?true:false;
		return $this->getFilds(QuickFormHelper::coder($row->settlement,'d'),$c, $id);
	}
	
	function getFilds($settlement,$c, $reqid) 
	{
		$html = '';$arr=explode('</tr><tr>',$settlement);$post = JRequest::get('post');
		foreach($arr as $ar) {
			if(strpos($ar,'select')!== false)$html.=$this->getSelect($ar,$post,$c);
			elseif(strpos($ar,'checkbox')!== false)$html.=$this->getCheckbox($ar,$post,$c);
			elseif(strpos($ar,'email')!== false)$html.=$this->getEmail($ar,$post);
			elseif(strpos($ar,'radio')!== false)$html.=$this->getRadio($ar,$post,$c,(int)$reqid);
			elseif(strpos($ar,'<td class="r_td">text</td>')!== false)$html.=$this->getText($ar,$post);
			elseif(strpos($ar,'textarea</td>')!== false)$html.=$this->getTextarea($ar,$post);
			elseif(strpos($ar,'<td class="r_td">calctext</td>')!== false)$html.=$this->getCalctext($ar,$post,$c);
			elseif(strpos($ar,'<td class="r_td">file</td>')!== false)$html.=$this->getFile($ar,$post);
		}
		
		return $html;
	}
	
	
	function getCheckbox($pat, $post, $c) 
	{
		$add='';static $i=0;
		
		preg_match('/([^"]+)(?=" class="inp_ch")/', $pat, $m);
		
		$calc='';
		$vals=explode('input',$pat);
		
		if($c){
			foreach($vals as $val){
				if(strpos($val,'opt_modifer'))$calc.=$val{strpos($val,'value="')+7};
			}
			
			preg_match('/([^"]+)(?=" class="opt_price)/', $pat, $o);
			$calc.=trim($o[0]);
		}
		
		if($i.'_'.$calc==$post['chbx'][$i]){
			$value=JText::_('JYES');
			if($c) {
				$add=$calc;
				$GLOBALS['qfSum'].=';'.$calc;
			}
		}
		else $value=JText::_('JNO');
		
		$i++;
		return $this->getTr($m[0],$value,$add);
	}
	
	function getEmail($pat,$post) 
	{
		static $i=0;
		
		preg_match('/([^"]+)(?=" class="inp_sel")/', $pat, $m);
		$html= $this->getTr($m[0],$post['email'][$i],'');
		$i++;
		return $html;
	}
	
	function getRadio($pat, $post, $c, $reqid) 
	{
		static $a=array();$add='';$k=0;$value='';
		if(!isset($a[$reqid]))$a[$reqid]=1;
		
		preg_match('/([^"]+)(?=" class="inp_sel")/', $pat, $m);
		$title=$m[0];
		
		$opts=explode('</div><div>',$pat);
		$name='r'.$reqid.'_'.$a[$reqid]; $a[$reqid]++;
		
		foreach($opts as $opt) {
			$calc='';
			$vals=explode('input',$opt);
			
			if($c){
				foreach($vals as $val){
					if(strpos($val,'opt_modifer'))$calc.=$val{strpos($val,'value="')+7};
				}
				
				preg_match('/([^"]+)(?=" class="opt_price)/', $opt, $o);
				$calc.=trim($o[0]);
			}
		
			if($k.'_'.$calc==$post[$name]){
				preg_match('/([^"]+)(?=" class="inp_opt")/', $opt, $m);
				$value=$m[0];
				if($c) {
					$add=$calc;
					$GLOBALS['qfSum'].=';'.$calc;
				}
				preg_match('/([^"]+)(?=" class="inp_svz")/', $opt, $m);
				if($m)$svz=(int)$m[0];
			}
			$k++;
		}
		$html = $this->getTr($title,$value,$add);
		if($svz)$html.=$this->getChildForm($svz);
		
		return $html;
	}
	
	function getText($pat,$post) 
	{
		static $i=0;
		
		preg_match('/([^"]+)(?=" class="inp_sel")/', $pat, $m);
		$html= $this->getTr($m[0],$post['qftext'][$i],'');
		$i++;
		return $html;
	}
	
	function getCalctext($pat,$post,$c) 
	{
		static $i=0;$add='';
		
		preg_match('/([^"]+)(?=" class="inp_ch")/', $pat, $m);
		
		$calc='';
		$vals=explode('input',$pat);
		
		if($c){
			foreach($vals as $val){
				if(strpos($val,'opt_modifer'))$calc.=$val{strpos($val,'value="')+7};
			}
			
			preg_match('/([^"]+)(?=" class="opt_price2)/', $pat, $o);
			$calc.=trim($o[0]);
		}
		
		if($calc==$post['qfchtext'][$i]){
			if($c) {
				$add=$calc{0}.'('.trim($o[0]).'*'.$post['qfctext'][$i].')';
				$GLOBALS['qfSum'].=';'.$calc{0}.trim($o[0])*$post['qfctext'][$i];
			}
		}

		$html= $this->getTr($m[0],$post['qfctext'][$i],$add);
		$i++;
		return $html;
	}
	
	function getTextarea($pat,$post) 
	{
		static $i=0;
		
		preg_match('/([^"]+)(?=" class="inp_sel")/', $pat, $m);
		$html= $this->getTr($m[0],$post['qftextarea'][$i],'');
		$i++;
		return $html;
	}
	
	function getFile($pat,$post) 
	{
		static $i=0;
		
		preg_match('/([^"]+)(?=" class="inp_sel")/', $pat, $m);
		$html= $this->getTr($m[0],$_FILES['qffile']['name'][$i],'');
		$i++;
		return $html;
	}
	
	function getip()
	{
		if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"),"unknown"))
			$ip = getenv("HTTP_CLIENT_IP");
	
		elseif (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
			$ip = getenv("HTTP_X_FORWARDED_FOR");
	
		elseif (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
			$ip = getenv("REMOTE_ADDR");
	
		elseif (!empty($_SERVER['REMOTE_ADDR']) && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
			$ip = $_SERVER['REMOTE_ADDR'];
		
		else
			$ip = "unknown";
		
		return($ip);
	}
	
	function getTr($title,$value,$add) 
	{
		$add=strlen($add)>1?$add:'';
		return '<tr><td style="padding: 0 10px;">'.$title.'</td><td style="padding: 0 10px;">'.$value.'</td><td style="padding: 0 10px;">'.$add.'</td></tr>';
	}

}

