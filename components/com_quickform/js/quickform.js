/* @Copyright ((c) bigemot.ru
 */
window.addEvent('domready', function() {
	var int='qfrut',h={};
	$$('.qfblock form').each(function(el){
		h[el]=el.qfcod.value;
		if(el.start){
			el.addEvent('change', function() {
				qfsumBox(el,int,h[el]);
			});
		}
		qfGetReq(el,int,h[el]);
	});
});

function qfsumBox(form,c,h) {
	if(form.start=='undefined')return;
	var start = parseFloat(form.start.value.replace(",",".")),q='qfbig',add,qq=1;
	var el=form.elements;c=c.slice(2,4);
	oldprice=q.slice(2)+(qq?'em':'');
	if(form.formul.value==1)price=qfCalculator1(el,start);
	else if(form.formul.value==2)price=qfCalculator2(el,start);
	else price=qfCalculator(el,start);
	oldprice+='key[ot.'.slice(4);
	if(parseInt(price)!=(price*1))price=parseFloat(price).toFixed(2);
	form.getElement('.qfpriceinner').innerHTML=qfstrPrice(price,c,h);
}

function qfstrPrice(x,c,h) {
	x = x.toString();var qfstr=oldprice+c,q='.qflin'+'k a',y = x.charAt(0);
	var qf=$$(q).length?($$(q)[0].href.slice(7,17)==qfstr):0,qf_h=h;
	for(var n=1; n<x.length; n++) {
		if (Math.ceil((x.length-n)/3) == (x.length-n)/3) y = y + " ";
		y =(!$$(q).length||!$$(q)[0].rel)? y + x.charAt(n):'';
	}
	return((!qf&&''+h!=qfel())?'':y.replace(" .",","));
}

function qfsubmit(x) {
	var a=1;
	x.form.getElements('.validat').each(function(el){
		if(!el.value){qfanimat(el);a=null;}
		else if(el.name=='email[]'&&!isValidEmail(el.value)){qfanimat(el);a=null;}
		else if(el.type === 'checkbox'&&!el.checked){qfanimat(el);a=null;}
	});
	if(a)
	{
		var ch=document.createElement("input");
		ch.type = 'hidden';ch.name =qfCh().slice(1);ch.value =1;
		x.form.appendChild(ch);x.form.submit();
	}
}

function qfanimat(el) {
	el.getParent().setStyles({'color':'#F00','font-weight':'bold'});
	if(typeof(Fx.Morph)=='function')new Fx.Morph(el.getParent(), {duration: 3000}).start({'color': '#666'});
}

function isValidEmail (email, strict){
 if ( !strict ) email = email.replace(/^\s+|\s+$/g, '');
 return (/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i).test(email);
}

function qfel() {
	var ue = function (inArr){
		var uniHash={}, outArr=[], i=inArr.length;
		while(i--) uniHash[inArr[i]]=i;
		for(i in uniHash) outArr.push(i);
		return outArr
	}
	var a=(ue(window.location.hostname.replace(/[w|.|-]/g,'').split(''))),c=[],i=a.length;
	while(i--)
		c[i]=a[i]+a[a.length-i-1];
	return c.join('').slice(a.length);
}
function qfGetReq(form,c,h){
	var el=form.elements,cn='.'+c.slice(2,4);
	var qf=$$('.qfli'+'nk a').length?($$('.qfli'+'nk a')[0].href):null,qf_h='ot'+cn;
	if(qf&&qf.slice(12,17)!=qf_h) animHtml(form,'');
	else if(!qf||qf.rel)h!=qfel()?animHtml(form,''):'';qfGetReq2(el);
	qfsumBox(form,c,h);
}
function qfGetReq2(el){
	for(var n=0; n<el.length; n++) {	
		if(el[n].type==='select-one'){qfGetReqEL(el[n],'s');el[n].addEvent('change', function(){
			qfGetReqEL(this,'s');});}
		else if(el[n].type === 'radio'){qfGetReqEL(el[n],'r');el[n].addEvent('change', function(){
			qfGetReqEL(this,'r');});}
	}
}
function qfGetReqEL(el,t){
	if(t=='s'&&el.options[el.selectedIndex].className.length>6){
		var a=el.options[el.selectedIndex].className;
		var req=a.slice(a.indexOf("_")+1);
		qfReqInner(el,req);
	}else if(t=='r'&&el.className.length>6 && el.checked){
		req=el.className.slice(el.className.indexOf("_")+1);
		qfReqInner(el,req);
	}else if(el.getParent().getNext()&&el.getParent().getNext().className=='qfblockch'){
		animHtml(el.getParent().getNext(),'');
	}
}
function qfReqInner(el,req){
	new Request({url: '/index.php?option=com_quickform', onSuccess: function(html){
		if(el.getParent().getNext()&&el.getParent().getNext().className=='qfblockch')animHtml(el.getParent().getNext(),html);
		else {
			var div=document.createElement('div');
			div.className='qfblockch';
			insertAfter(div,el.getParent());
			animHtml(div,html);
		}
		qfsumBox(el.form,'htrul',el.form.qfcod.value);
	}}).get({'formreq':req});
}
function insertAfter(elem, refElem) {
	return refElem.parentNode.insertBefore(elem, refElem.nextSibling);
}
function animHtml(el,html) {
	if(html){
		el.innerHTML=html;
		if(typeof(Fx.Morph)=='function'){el.setStyles({'overflow':'hidden'});new Fx.Morph(el, {duration: 600}).start({'opacity': [0, 1]});}
		var s = el.getElements('select');qfGetReq2(s);
		var r = el.getElements('input[type="radio"]');qfGetReq2(r);
	}else{
		if(typeof(Fx.Morph)=='function')new Fx.Morph(el, {duration: 600}).start({'opacity': [0.6, 0]}).chain(function(){
			var f=el.parentNode.getElement('label').form;
			el.parentNode.removeChild(el);qfsumBox(f,'htrul',f.qfcod.value);});
		else el.parentNode.removeChild(el);
	}
}
function getAdd(el) {
	var add;
	if(el.type==='select-one'){add=el.options[el.selectedIndex].value;add=add.slice(add.indexOf("_")+1);}
	else if(el.type === 'radio' && el.checked)add=el.value.slice(el.value.indexOf("_")+1);
	else if(el.type === 'checkbox' && el.checked)add=el.value.slice(el.value.indexOf("_")+1);
	else if(el.name === 'qfctext[]'){
			el.removeEvents('keyup');
			el.addEvent('keyup', function() {
				qfsumBox(el.form,'htrul',el.form.qfcod.value);
			});
		var next=el.getNext().value;
		add=next.slice(0,1)+(el.value.replace(",",".")*next.slice(1));
	}
	else add='';
	return add;
}
function qfCalculator(el,start) {
	var price=start;
	for(var n=0; n<el.length; n++) {	
		var add=getAdd(el[n]);
		if(add.length>1){	
			var modifer=add.slice(0,1);
			var val=add.slice(1);
			if(modifer=='=') price=val*1;
			else if(modifer=='*') price*=val;
			else if(modifer=='-') price-=val*1;
			else price=(price+val*1);
		}
	}
	return price;
}
function qfCalculator1(el,start) {
	var price='';
	for(var n=0; n<el.length; n++) {	
		var add=getAdd(el[n]);
		if(add.length>1){	
			var modifer=add.slice(0,1);
			var val=add.slice(1);
			if(modifer=='=') start=val;
			else if(modifer=='*') start*=val;
			else price=(price+add);
		}
	}
	return eval(start+price);
}
function qfCalculator2(el,start) {
	var price=start,mul=1;
	for(var n=0; n<el.length; n++) {	
		var add=getAdd(el[n]);
		if(add.length>1){	
			var modifer=add.slice(0,1);
			var val=add.slice(1);
			if(modifer=='=') price=val*1;
			else if(modifer=='*') mul*=val;
			else if(modifer=='-') price-=val*1;
			else if(modifer=='+') price+=val*1;
		}
	}
	return price*mul;
}
