<?php
	
	// настройки
	define("SEO_ADD_LastModified", false); // добавление заголовка Last-Modified
	
	define("SEO_META", false); // использовать модуль метатегов
	
	define("SEO_LINK", false); // использовать модуль обработки ссылок
	define("SEO_LINK_ABS", false); // абсолютные ссылки
	define("SEO_LINK_ADD_WWW", false); // добавление www
	define("SEO_LINK_REPLACE", false); // подмена ссылок
	define("SEO_LINK_ADD_SLASH", false); // добавление к ссылкам /
	define("SEO_LINK_OUT", false); // внешние ссылки !!! корректно работают только при включеных абсолютных ссылках !!!
	
	define("SEO_REDIR", true); // использовать модуль редиректов
	define("SEO_REDIR_ADD_WWW", false); // редирект на WWW
	define("SEO_REDIR_DEL_WWW", false); // редирект на без-WWW
	define("SEO_REDIR_ADD_SLASH", false); // редирект на /
	define("SEO_REDIR_NOT_SLASH", true); // редирект на без-/
	define("SEO_REDIR_REDIR", false); // редиректы
	define("SEO_REDIR_REPLACE", false); // перенаправления

	function seoEditPage($content) {
        //$content = gzdecode($content);
		if(SEO_META && file_exists($_SERVER['DOCUMENT_ROOT'].'/seo/data.php')) {
			$promo = array(
				'title' => '',
				'keywords' => '',
				'description' => '',
				'h1' => '',
				'text_1' => '',
				'text_2' => '',
				);
			include($_SERVER['DOCUMENT_ROOT'].'/seo/data.php');
			if($promo['title']) $content = ereg_replace("<title>([^<>]*)</title>","<title>".$promo['title']."</title>", $content);
			if($promo['keywords']) {
				if(!strpos($content,'meta name="keywords"')) $content = str_replace('</title>','</title>
				<meta name="keywords" content=" " />', $content);
				$content = ereg_replace('<meta name="keywords" content="([^<>]*)"[^<>]*>','<meta name="keywords" content="'.$promo['keywords'].'" />', $content);
			}
			if($promo['description']) {
				if(!strpos($content,'meta name="description"')) $content = str_replace('</title>','</title>
				<meta name="description" content=" " />', $content);
				$content = ereg_replace('<meta name="description" content="([^<>]*)"[^<>]*>','<meta name="description" content="'.$promo['description'].'" />', $content);
			}
		}
		if(SEO_LINK && file_exists($_SERVER['DOCUMENT_ROOT'].'/seo/links.php')) include($_SERVER['DOCUMENT_ROOT'].'/seo/links.php');
        //$content = gzencode($content);
		return $content;
	}

	if(SEO_ADD_LastModified) header("Last-Modified: ".gmdate("D, d M Y H:i:s",time()-3600)." GMT");
	if(SEO_REDIR && !$_POST && file_exists($_SERVER['DOCUMENT_ROOT'].'/seo/redirects.php')) include($_SERVER['DOCUMENT_ROOT'].'/seo/redirects.php');
	ob_start('seoEditPage');
	
?>