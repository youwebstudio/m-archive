<?
/* ���������� ������ + �������� ������� + ������� ���������� */

	$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/';

	// ������� ������ ��� �������
	$link_in = array();
	$link_out = array();

	// �������� ������ �� url.php
	$url_in = '';
	if(file_exists($_SERVER['DOCUMENT_ROOT'].'/seo/url.php')) include($_SERVER['DOCUMENT_ROOT'].'/seo/url.php');
	if($url_in){
		foreach($url_in as $key=>$value){
			if (!preg_match('~^(http|#|javascript|mailto|\?)~', $key)) $key = $base_url . ($key == '/' ? '' : ltrim($key,'/'));
			if (!preg_match('~^(http|#|javascript|mailto)~', $value)) $value = $base_url . ($value == '/' ? '' : ltrim($value,'/'));
			$link_in[] = $key;
			$link_out[] = $value;
		}
	}

	// ��������� ������
	preg_match_all("~<[a|A][^<>]*href=[\"|\']([^\"|^\']+)([\"|\'])[^<>]*>~", $content, $matches);
	foreach ($matches[1] as $key => $url) {
		$quote = $matches[2][$key];
		$link = $matches[0][$key];
		
		// ���������� ������
		if (SEO_LINK_ABS && !preg_match('~^(http|#|javascript|mailto|\?)~', $url)) {
			$content = str_replace ("href=".$quote.$url.$quote, "href=".$quote.$base_url.($url == '/' ? '' : ltrim($url,'/')).$quote, $content);
			$url = $base_url.ltrim($url,'/');
		}
		
		// ���������� www
		if (SEO_LINK_ADD_WWW && strpos($url, str_replace('www.', '', $_SERVER['HTTP_HOST'])) && !strpos($url, $_SERVER['HTTP_HOST'])){
			$url_new = str_replace(str_replace('www.', '', $_SERVER['HTTP_HOST']), $_SERVER['HTTP_HOST'], $url);
			$content = str_replace($url, $url_new, $content);
			$url = $url_new;
		}
		
		// ������� ������
		if (SEO_LINK_REPLACE && strpos($url, $_SERVER['HTTP_HOST']) && !preg_match('~^(#|javascript|mailto|\?)~', $url)) {
			//$content = str_replace($url,str_replace($link_in,$link_out,$url),$content);
			if($url_in){
				if(isset($url_in[str_replace($base_url, '/', $url)]))
					$content = str_replace($url.$quote,$url_in[str_replace($base_url, '/', $url)].$quote,$content);
			}
		}
		
		// ������� ������
		if (SEO_LINK_OUT && !strpos($url, $_SERVER['HTTP_HOST']) && !strpos($link, 'nofollow') && !preg_match('~^(#|javascript|mailto)~', $url))
			$content = str_replace($link, str_replace('href=','rel='.$quote.'nofollow'.$quote.' href=', $link), $content);
			
		// ���������� / � �������
		if (SEO_LINK_ADD_SLASH && strpos($url, $_SERVER['HTTP_HOST']) && !preg_match('~^(#|javascript|mailto)~', $url) && !preg_match('~.*(\.)[a-z0-9]{2,4}$~', $url))
			$content = str_replace($url.$quote, rtrim($url, '/').'/'.$quote, $content);
	}
    
    $content = str_replace('?limitstart=0','', $content);
?>