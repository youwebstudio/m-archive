<?php
/**
 *  @package     AkeebaStrapper
 *  @copyright   Copyright (c)2010-2014 Nicholas K. Dionysopoulos
 *  @license     GNU General Public License version 2, or later
 */

// Protect from unauthorized access
defined('_JEXEC') or die;

define('AKEEBASTRAPPER_VERSION', 'rev44D406D-1406643790');
define('AKEEBASTRAPPER_DATE', '2014-07-29 16:23:10');
define('AKEEBASTRAPPER_MEDIATAG', md5(AKEEBASTRAPPER_VERSION . AKEEBASTRAPPER_DATE));